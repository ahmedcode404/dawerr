//datepicker
$(function () {
    $('.datepick').datepicker({})
});

$(document).ready(function(){

    var customersDataString = sessionStorage.getItem('price');

    $('.price_result').val(JSON.parse(customersDataString));

}); // append price to input

//upload file single
$(function () {
    $(".single-file input").change(function () {
        var self = $(this).parent().find(".img-preview");
        self.html('');
        self.append("<img src=''/>")
        self.find("img").fadeIn().css("display", "block");

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    self.find("img").attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        readURL(this)
    })
});


//payment form validation 
var $form = $(".payment-form");
$.validator.addMethod("letters", (function (e, i) {
return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form.validate({
    errorPlacement: function (e, i) {
        i.before(e)
    },
    rules: {
        transfer_name : {
            minlength: 2
        },

    },
submitHandler: function () {
    $form[0].submit()
}
});

