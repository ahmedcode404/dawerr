//profile-img
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function () {
    readURL(this);
});

//enable edit form

$(".open-edit-form").click(function(){
    $(this).addClass("active");
    $(".profile-form .form-control").attr("disabled",false).parent().addClass("active");
$(".hide-element").fadeIn()
})

//slide profile
$(document).on("click", ".arrow-profile-text", (function () {
    $(this).toggleClass("active");
    $(this).next(".slide-profile").slideToggle();
}));
$(document).on("click", ".badges", (function () {
    $(this).next(".product-change-status").fadeToggle();
}));


//profile form validation 
var $form = $(".profile-form");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form.validate({
    errorPlacement: function (e, i) {
        i.after(e)
    },
    rules: {
        password_confirm : {
            equalTo : "#password"
        },

    },
    submitHandler: function () {


        $form[0].submit()


    }
});

$("#imageUpload").change(function () {
    readURL(this);

    var form_data = new FormData();                  
    var file_data = $(this)[0].files;

    form_data.append('image', file_data[0]);  
    var url = $(this).data('url');

    $.ajax({
        url: url,
        type: "POST",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},        
        data: form_data,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data){

            var db = firebase.firestore();

            // Add a new document in collection "users"
            db.collection("users").doc('userId_' + data.user_id).update({
                'photoUrl': data.image,
            })
            .then(() => {
        
                console.log("Document successfully written!");
        
            })
            .catch((error) => {
        
                console.error("Error writing document: ", error);
                
            });                       
        }
    });

});

$(function(){

    if($('#name-route').val())
    {

        alert()

    } 
       
})