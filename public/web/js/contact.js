//contact form validation 
var $form = $(".contact-form");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form.validate({
    errorPlacement: function (e, i) {
        i.after(e)
    },
    rules: {
       
    },
    submitHandler: function () {
        $form[0].submit()
    }
});
