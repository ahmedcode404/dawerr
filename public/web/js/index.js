
//owl-carousel
$("#main-owl").owlCarousel({
  loop: true,
  items: 1,
  nav: false,
  dots: true,
  autoplay: true,
  mouseDrag: true,
  touchDrag: true,
  rtl: true,
  autoplayTimeout: 8000
});



//full-search-form  validation 
var $form_full = $(".full-search-form");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form_full.validate({
    errorPlacement: function (e, i) {
        i.after(e)
    },
    rules: {},
    submitHandler: function () { 
        $form_full[0].submit()
    }
});