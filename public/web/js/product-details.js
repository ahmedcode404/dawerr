//jssor slider
jssor_1_slider_init();

$(window).on("load", function () {
  var textWrappers = document.querySelector('.product-note div');
  textWrappers.innerHTML = textWrappers.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
  anime.timeline({
      loop: true
    })
    .add({
      targets: '.product-note  .letter',
      opacity: [0, 1],
      easing: "easeInOutQuad",
      duration: 2000,
      delay: (el, i) => 20 * (i + 1)
    }).add({
      targets: '.product-note div',
      opacity: 0,
      duration: 1000,
      easing: "easeOutExpo",
      delay: 1000
    });


});



//flag form validation 
var $form = $(".flag-form");
$form.validate({
    errorPlacement: function (e, i) {
        i.after(e)
    },
    rules: {
    },
    submitHandler: function () {
      
      $form[0].submit();

    }
});