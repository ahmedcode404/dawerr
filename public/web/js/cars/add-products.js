
//upload file single
$(function () {
    $(".single-file input").change(function () {
        var self = $(this).parent().find("img");
        self.fadeIn().css("display", "block");

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    self.attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        readURL(this)
    })
});

//upload file multiple
$(function () {
    // Multiple images preview in browser
    var imagesPreview = function (input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function (event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('.multi-file input').on('change', function () {
        $(this).parent().find("label > img").remove();
        imagesPreview(this, '.images-multi');
        $(this).parent().append("<span class='remove-file auto-icon'><i class='fa fa-times'></span>");
    });

    $(document).on('click', '.remove-file', function () {
        $(this).parents(".multi-file").find("input").val('');
        $(this).parents(".multi-file").find(".images-multi img").remove();
        $(this).parents(".multi-file").find("label").append('<img src="../images/products/add.png" alt="إصافة صورة">');
        $(".remove-file").remove();
    });
});

//advanced-search-form  validation 
var $form_advanced = $(".add-product-form");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), 
$form_advanced.validate({
    errorPlacement: function (e, i) {
        i.before(e)
    },
    rules: {

        status: {
            required: true
        },
        address: {
            required: true
        },
        option_2: {
            required: true
        },
        option_3: {
            required: true
        },
        option_4: {
            required: true
        },
        option_5: {
            required: true
        },
        option_909: {
            required: true
        },
        option_910: {
            required: true
        },
        option_911: {
            required: true
        },
        option_912: {
            required: true
        },
        option_913: {
            required: true
        },
        option_914: {
            required: true
        },
        option_915: {
            required: true
        },
        option_916: {
            required: true
        },
        option_917: {
            required: true
        },
        option_918: {
            required: true
        },
        option_919: {
            required: true
        },
        option_920: {
            required: true
        },
        option_921: {
            required: true
        },
        option_948: {
            required: true
        },
        option_949: {
            required: true
        },
        option_950: {
            required: true
        },
        option_951: {
            required: true
        },

    },
    submitHandler: function () {
        $form_advanced[0].submit()
    }
});





//map
$("#map-text").on("click", () => {
    $("#map2").show();
});

function initMap() {
    var lat = 24.774265,
    lng = 46.738586;
    const map = new google.maps.Map(document.getElementById("map2"), {
        center: {
            lat: lat,
            lng: lng
        },
        zoom: 13,
        mapTypeId: "roadmap",
    });
    // Create the search box and link it to the UI element.
    const input = document.getElementById("pac-input");
    const searchBox = new google.maps.places.SearchBox(input);
    // Bias the SearchBox results towards current map's viewport.
    map.addListener("bounds_changed", () => {
        searchBox.setBounds(map.getBounds());
    });
    let markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener("places_changed", () => {
        const places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }
        // Clear out the old markers.
        markers.forEach((marker) => {
            marker.setMap(null);
        });
        markers = [];
        // For each place, get the icon, name and location.
        const bounds = new google.maps.LatLngBounds();
        places.forEach((place) => {
            if (!place.geometry || !place.geometry.location) {
                console.log("Returned place contains no geometry");
                return;
            }
            const icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25),
            };
            // Create a marker for each place.
            markers.push(
                new google.maps.Marker({
                    map,
                    icon,
                    title: place.name,
                    position: place.geometry.location,
                })
            );

            $("#lat").val(place.geometry.location.lat());
            $("#lng").val(place.geometry.location.lng());            

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
  // Add address marker on map
  var address_marker = new google.maps.Marker({
    position: {
        lat: parseFloat(lat),
        lng: parseFloat(lng)
    },
    draggable: true,
});

// $("#lat").val(position.lat);
// $("#lng").val(position.lng); 

    infoWindow = new google.maps.InfoWindow();
    const locationButton = document.getElementById("myloc");
    function geocodePosition(pos) {
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({
            latLng: pos
        }, function (responses) {
            if (responses && responses.length > 0) {
                updateMarkerAddress(responses[0].formatted_address);
                infoWindow.setContent(responses[0].formatted_address);
            } else {
                updateMarkerAddress('لم نتمكن من تحديد موقعك');
            }
        });
    }
    // locationButton.addEventListener("click", () => {
    // });
           // Try HTML5 geolocation.
           if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    infoWindow.setPosition(pos);
                    geocodePosition(pos)
                    infoWindow.open(map);
                    map.setCenter(pos);
                    placeMarker(pos);
                    $("#lat").val(pos.lat);
                    $("#lng").val(pos.lng);    
                },
                () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

 // To add the marker to the map, call setMap();
 address_marker.setMap(map);

 // Add event listner to address_marker
 google.maps.event.addListener(address_marker, 'dragend', function (event) {
     placeMarker(event.latLng);

     $("#lat").val(event.latLng.lat());
     $("#lng").val(event.latLng.lng()); 

 });

 google.maps.event.addListener(map, 'click', function (event) {
     placeMarker(event.latLng);
 });

 function placeMarker(location) {
     address_marker.setPosition(location);
     map.setCenter(location);
     geocodePosition(address_marker.getPosition());
     infoWindow.setPosition(location);

 }
}

function updateMarkerAddress(address) {
    $('#pac-input').val(address);
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(
        browserHasGeolocation ?
        "لم نتمكن من تحديد موقعك'" :
        "متصفحك لا يدعم تحديد الموقع"
    );
    infoWindow.open(map);
}
