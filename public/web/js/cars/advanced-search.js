    //data appened due to status
    $('.directions').on('select2:select', function () {
        var select_val = $(this).val();

        // شمال 
        if (select_val == "north") {
            var append_div = `<div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="radio" name="check_1" class="hidden-input"
                                id="checks_1">
                            <label for="checks_1" class="second_color"> يوجد</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="radio" name="check_1" class="hidden-input"
                            id="checks_2">
                        <label for="checks_2" class="second_color"> لا يوجد</label>
                    </div>
                </div>
            </div>
        </div>
        </div>`

            // شرق 
        } else if (select_val == "east") {
            var append_div = `<div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="radio" name="check_2" class="hidden-input"
                                id="checks_21">
                            <label for="checks_21" class="second_color"> يوجد</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="radio" name="check_2" class="hidden-input"
                            id="checks_22">
                        <label for="checks_22" class="second_color"> لا يوجد</label>
                    </div>
                </div>
            </div>
        </div>
        </div>`

            // غرب 
        } else if (select_val == "west") {
            var append_div = `<div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="radio" name="check_3" class="hidden-input"
                                id="checks_31">
                            <label for="checks_31" class="second_color"> لا</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="radio" name="check_3" class="hidden-input"
                            id="checks_32">
                        <label for="checks_32" class="second_color"> خلفي</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
        <div class="form-group checkboxes-inline-title">
            <div class="inline-custom-checkboxes">
                <div class="custom-checkbox">
                    <input type="radio" name="check_3" class="hidden-input"
                        id="checks_33">
                    <label for="checks_33" class="second_color"> أمامي</label>
                </div>
            </div>
        </div>
    </div>
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="radio" name="check_3" class="hidden-input"
                            id="checks_34">
                        <label for="checks_34" class="second_color"> أمامي و خلفي</label>
                    </div>
                </div>
            </div>
        </div>
        </div>`
            // جنوب 
        } else if (select_val == "south") {
            var append_div = `<div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="radio" name="check_4" class="hidden-input"
                                id="checks_41">
                            <label for="checks_41" class="second_color"> لا</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="radio" name="check_4" class="hidden-input"
                            id="checks_42">
                        <label for="checks_42" class="second_color"> خفيف</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
        <div class="form-group checkboxes-inline-title">
            <div class="inline-custom-checkboxes">
                <div class="custom-checkbox">
                    <input type="radio" name="check_4" class="hidden-input"
                        id="checks_43">
                    <label for="checks_43" class="second_color"> ثقيل</label>
                </div>
            </div>
        </div>
    </div>
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="radio" name="check_4" class="hidden-input"
                            id="checks_44">
                        <label for="checks_44" class="second_color"> خفيف و ثقيل</label>
                    </div>
                </div>
            </div>
        </div>
        </div>`
            // شمال  شرق
        } else {}

        $(".directions-append").html('');
        $(".directions-append").html(append_div);
        $('.select-input').select2({
            theme: 'bootstrap4',
            language: "ar",
            width: '100%'
        });

    })

    //datepicker
    $(function () {
        $('.datepick').datepicker({
            dateFormat: 'yy'
        })
    });

    //advanced-search-form  validation 
    var $form_advanced = $(".advanced-search-form");
    $.validator.addMethod("letters", (function (e, i) {
        return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
    })), $form_advanced.validate({
        errorPlacement: function (e, i) {
            i.after(e)
        },
        rules: {},
        submitHandler: function () {
            $form_advanced[0].submit()
        }
    });