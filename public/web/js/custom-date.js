//datepicker
$(function () {
    $('.datepick').datepicker({
        dateFormat:"DD, d MM, yy",
        setDate: new Date(),
        beforeShowDay: noMondays
    });
    function noMondays(date){
          if (date.getDay() === 5 || date.getDay() === 6) 
                return [ false, "closed", "Closed on friday and saturday" ]
          else
          return [ true, "", "" ]
        }
});