//data appened due to status
$('#state-select').on('select2:select', function () {
    var select_val = $(this).val();
    // $('.modal-select select').val("dis").trigger("change");

   //main select
    //أراضي
    if (select_val == "land") {
        var append_div = ` <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="الحي">
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>الحالة</option>
                <option value="1">سكنية</option>
                <option value="2">تجارية</option>
                <option value="3">تجارية سكنية</option>
                <option value="4">زراعية</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="المساحة">
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="hodod" min="1"
                placeholder="الحدود  ( تبدأ من ثلاثة حدود )">
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="عرض شارع الواجهة">
        </div>
    </div>
    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat">
                <option disabled selected>عدد الشوارع</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <div class="row">
                <div class="col-12">
                    <h3 class="arrow-title second_color">
                        خدمات البنية التحتية المتوفرة في منطقة العقار :
                    </h3>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> كهرباء :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_1" class="hidden-input" id="check_1">
                                <label for="check_1">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_1" class="hidden-input" id="check_2">
                                <label for="check_2">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> مياه :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_2" class="hidden-input" id="check_3">
                                <label for="check_3">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_2" class="hidden-input" id="check_4">
                                <label for="check_4">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> هاتف :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_3" class="hidden-input" id="check_5">
                                <label for="check_5">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_3" class="hidden-input" id="check_6">
                                <label for="check_6">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> إنارة :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_4" class="hidden-input" id="check_7">
                                <label for="check_7">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_4" class="hidden-input" id="check_8">
                                <label for="check_8">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> شوارع مسفلتة :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_5" class="hidden-input" id="check_9">
                                <label for="check_9">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_5" class="hidden-input" id="check_10">
                                <label for="check_10">لا</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <div class="row">
                <div class="col-12">
                    <h3 class="arrow-title second_color">
                        المرافق و الخدمات المحيطة :
                    </h3>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> تموينات :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_6" class="hidden-input" id="check2_1">
                                <label for="check2_1">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_6" class="hidden-input" id="check2_2">
                                <label for="check2_2">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> مدارس :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_7" class="hidden-input" id="check2_3">
                                <label for="check2_3">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_7" class="hidden-input" id="check2_4">
                                <label for="check2_4">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> مطاعم :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_8" class="hidden-input" id="check2_5">
                                <label for="check2_5">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_8" class="hidden-input" id="check2_6">
                                <label for="check2_6">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> أسواق :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_9" class="hidden-input" id="check2_7">
                                <label for="check2_7">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_9" class="hidden-input" id="check2_8">
                                <label for="check2_8">لا</label>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>`
    }
    //محل\صالة
    else if (select_val == "market") {
        var append_div = `<div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="الحي">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="المساحة">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="hodod" min="1"
                placeholder="الحدود  ( تبدأ من ثلاثة حدود )">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="عرض شارع أو الممر">
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat">
                <option disabled selected>عدد الشوارع</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="dor" min="1" placeholder="عدد الأدوار">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="height" min="1" placeholder="الارتفاع من الداخل">
        </div>
    </div>`
    }
    //مستودع
    else if (select_val == "warehouse") {
        var append_div = `<div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="الحي">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="المساحة">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="hodod" min="1"
                placeholder="الحدود  ( تبدأ من ثلاثة حدود )">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="عرض شارع أو الممر">
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat">
                <option disabled selected>عدد الشوارع</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>


    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="height" min="1" placeholder="الارتفاع من الداخل">
        </div>
    </div>`

    }
    //شقة
    else if (select_val == "department") {
        var append_div = `<div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="الحي">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="المساحة">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="hodod" min="1" placeholder="الدور">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="عدد غرف النوم">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="woman" min="1"
            placeholder="مجلس نساء">
    </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="men" min="1"
        placeholder="مجلس رجال">
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sala" min="1"
        placeholder="عدد الصالات">
</div>
</div>
    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="kitchen" min="1"
            placeholder="عدد المطابخ">
    </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="bathroom" min="1"
        placeholder="عدد دورات المياه">
</div>
</div>

<div class="col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="street" min="1"
        placeholder="مستودع">
</div>
</div>
    <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat">
                <option disabled selected>المدخل</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>
`
    }

    //منتجع / استراحة
    else if (select_val == "rest") {
        var append_div = `<div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="الحي">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="المساحة">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="hodod" min="1"
            placeholder="الحدود  ( تبدأ من ثلاثة حدود )">
    </div>
</div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="عرض شارع الواجهة">
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <select class="form-control select-input" name="cat">
            <option disabled selected>عدد الشوارع</option>
            <option value="1">1</option>
            <option value="2">2</option>
        </select>
    </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="cats" min="1"
                placeholder="عدد الأقسام">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="men" min="1"
        placeholder="عدد مجالس الرجال">
</div>
</div>


    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="woman" min="1"
            placeholder="عدد مجالس النساء">
    </div>
</div>


<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="pool" min="1"
        placeholder="عدد المسابح">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="kitchen" min="1"
        placeholder="عدد المطابخ">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="bedroom" min="1"
        placeholder="عدد غرف النوم">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sala" min="1"
        placeholder="محلات تجارية">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_1">
                <option disabled selected>مسطحات خضراء  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_2">
                <option disabled selected>جلسات خارجية  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_3">
                <option disabled selected>ألعاب  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>
`
    }
    //عمارة
    else if (select_val == "building") {
        var append_div = `  <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="الحي">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="المساحة">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="hodod" min="1" placeholder="الحدود">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="عرض شارع الواجهة">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat">
                <option disabled selected>عدد الشوارع</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="woman" min="1"
                placeholder="عدد الأدوار">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_2">
                <option disabled selected>عدد الشقق</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="men" min="1"
                placeholder="عدد المحلات">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_8">
                <option disabled selected>مصعد ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <div class="row">
                <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                    <h3 class="arrow-title second_color">
                        وصف الحدود :
                    </h3>
                </div>

                <div class="col-xl-10 col-lg-10 col-md-9 col-12">
                    <div class="form-group" data-aos="fade-in">
                        <select class="form-control select-input directions" name="cat_9">
                            <option disabled selected>الجهات الأصلية و الفرعية</option>
                            <option value="north">شمال</option>
                            <option value="east">شرق </option>
                            <option value="west">غرب</option>
                            <option value="south">جنوب </option>
                            <option value="north_east"> شمال شرق</option>
                            <option value="north_west">شمال غرب</option>
                            <option value="south_east"> جنوب شرق</option>
                            <option value="south_west">جنوب غرب </option>
                        </select>
                    </div>

                    <div class="directions-append"></div>

                </div>
            </div>


        </div>
    </div>
`
    }
    //مزرعة
    else if (select_val == "farm") {
        var append_div = `<div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>


    <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="المساحة">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="hodod" min="1"
                placeholder="الحدود  ( تبدأ من ثلاثة حدود )">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_1">
                <option disabled selected>سكن خاص  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_2">
                <option disabled selected>مجالس  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_3">
                <option disabled selected>سكن للعمال  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>
    
    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_4">
                <option disabled selected>بئر ماء  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_5">
                <option disabled selected>بيوت بلاستيكية  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat_6">
                <option disabled selected>مستودع  ( يوجد / لا يوجد )</option>
                <option value="1">يوجد</option>
                <option value="2">لا يوجد</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <select class="form-control select-input" name="cat_7">
            <option disabled selected>حظيرة للماشة  ( يوجد / لا يوجد )</option>
            <option value="1">يوجد</option>
            <option value="2">لا يوجد</option>
        </select>
    </div>
</div>
    `

    }
    //فيلا دور + شقة / فيلا دور + شقة و ملحق
    else if (select_val == "villa/department") {
        var append_div = `<div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="الحي">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="مساحة الأرض">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="hodod" min="1" placeholder="مساحة مسطح البناء">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="الحدود">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="street2" min="1"
            placeholder="عرض شارع الواجهة">
    </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat">
                <option disabled selected>عدد الشوارع</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="beds" min="1"
        placeholder="عدد غرف النوم الماستر">
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="bedroom" min="1"
        placeholder="عدد غرف النوم">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="bathroom" min="1"
        placeholder="عدد دورات المياه">
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sala" min="1"
        placeholder="عدد الصالات">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="men" min="1"
        placeholder="عدد مجالس الرجال">
</div>
</div>


    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="woman" min="1"
            placeholder="عدد مجالس النساء">
    </div>
</div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="kitchen" min="1"
            placeholder="عدد المطابخ">
    </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_1">
        <option disabled selected>غرف الخدم</option>
        <option value="1">1</option>
        <option value="2"> 2</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_2">
        <option disabled selected>صالة طعام</option>
        <option value="1">1</option>
        <option value="2"> 2</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_3">
        <option disabled selected>سكن للعمال</option>
        <option value="1">1</option>
        <option value="2">2</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_4">
        <option disabled selected>مشب خارجي  ( يوجد / لا يوجد )</option>
        <option value="1">يوجد</option>
        <option value="2">لا يوجد</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_5">
        <option disabled selected>حديقة  ( يوجد / لا يوجد )</option>
        <option value="1">يوجد</option>
        <option value="2">لا يوجد</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_6">
        <option disabled selected>مسبح  ( يوجد / لا يوجد )</option>
        <option value="1">يوجد</option>
        <option value="2">لا يوجد</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
<select class="form-control select-input" name="cat_7">
    <option disabled selected>غرفة لغسيل الملابس  ( يوجد / لا يوجد )</option>
    <option value="1">يوجد</option>
    <option value="2">لا يوجد</option>
</select>
</div>
</div>


<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
<select class="form-control select-input" name="cat_8">
    <option disabled selected>مصعد  ( يوجد / لا يوجد )</option>
    <option value="1">يوجد</option>
    <option value="2">لا يوجد</option>
</select>
</div>
</div>


<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
<select class="form-control select-input" name="cat_9">
    <option disabled selected>ملحق  ( يوجد / لا يوجد )</option>
    <option value="1">يوجد</option>
    <option value="2">لا يوجد</option>
</select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_1" min="1"
        placeholder="عدد غرف نوم الشقة 1">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_2" min="1"
        placeholder="عدد دورات مياه الشقة 1">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_3" min="1"
        placeholder="مجلس الرجال للشقة 1">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_4" min="1"
        placeholder="مجلس النساء الشقة 1">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_5" min="1"
        placeholder="عدد الصالات للشقة 1">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_6" min="1"
        placeholder="عدد المطابخ للشقة 1">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_7" min="1"
        placeholder="عدد غرف نوم الشقة 2 ">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_8" min="1"
        placeholder="مجلس الرجال للشقة 2">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_9" min="1"
        placeholder="مجلس نساء للشقة 2">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_10" min="1"
        placeholder="عدد الصالات للشقة 2 ">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_11" min="1"
        placeholder="عدد المطابخ للشقة 2">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sec_12" min="1"
        placeholder="محلات تجارية">
</div>
</div>
    
`

    }
    //بيت شعبي / فيلا
    else if (select_val == "villa/home") {
        var append_div = `<div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="الحي">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="مساحة الأرض">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="hodod" min="1" placeholder="مساحة مسطح البناء">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="الحدود">
        </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="street2" min="1"
            placeholder="عرض شارع الواجهة">
    </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat">
                <option disabled selected>عدد الشوارع</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="beds" min="1"
        placeholder="عدد غرف النوم الماستر">
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="bedroom" min="1"
        placeholder="عدد غرف النوم">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="bathroom" min="1"
        placeholder="عدد دورات المياه">
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="sala" min="1"
        placeholder="عدد الصالات">
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <input type="number" class="form-control" name="men" min="1"
        placeholder="عدد مجالس الرجال">
</div>
</div>


    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="woman" min="1"
            placeholder="عدد مجالس النساء">
    </div>
</div>

    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <input type="number" class="form-control" name="kitchen" min="1"
            placeholder="عدد المطابخ">
    </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_1">
        <option disabled selected>غرف الخدم</option>
        <option value="1">1</option>
        <option value="2"> 2</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_2">
        <option disabled selected>صالة طعام</option>
        <option value="1">1</option>
        <option value="2"> 2</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_3">
        <option disabled selected>سكن للعمال</option>
        <option value="1">1</option>
        <option value="2">2</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_4">
        <option disabled selected>مشب خارجي  ( يوجد / لا يوجد )</option>
        <option value="1">يوجد</option>
        <option value="2">لا يوجد</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_5">
        <option disabled selected>حديقة  ( يوجد / لا يوجد )</option>
        <option value="1">يوجد</option>
        <option value="2">لا يوجد</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
    <select class="form-control select-input" name="cat_6">
        <option disabled selected>مسبح  ( يوجد / لا يوجد )</option>
        <option value="1">يوجد</option>
        <option value="2">لا يوجد</option>
    </select>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
<select class="form-control select-input" name="cat_7">
    <option disabled selected>غرفة لغسيل الملابس  ( يوجد / لا يوجد )</option>
    <option value="1">يوجد</option>
    <option value="2">لا يوجد</option>
</select>
</div>
</div>


<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
<select class="form-control select-input" name="cat_8">
    <option disabled selected>مصعد  ( يوجد / لا يوجد )</option>
    <option value="1">يوجد</option>
    <option value="2">لا يوجد</option>
</select>
</div>
</div>


<div class="col-xl-4 col-lg-4 col-md-6 col-12">
<div class="form-group" data-aos="fade-in">
<select class="form-control select-input" name="cat_9">
    <option disabled selected>ملحق  ( يوجد / لا يوجد )</option>
    <option value="1">يوجد</option>
    <option value="2">لا يوجد</option>
</select>
</div>
</div>
    
`

    }
    //محطة بنزين
    else if (select_val == "gasstation") {
        var append_div = ` <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="city">
                <option disabled selected>المدينة</option>
                <option value="1">الرياض</option>
                <option value="2">الدمام</option>
                <option value="3">القريات</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="way" placeholder="على طريق سريع">
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="alhay" placeholder="داخل حي">
        </div>
    </div>


    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="status">
                <option disabled selected>السعر ( مثال : من 100 - 250 رس )</option>
                <option value="1">من 10.000 إلي 20.000</option>
                <option value="2">من 20.000 إلي 30.000</option>
                <option value="3">من 30.000 إلي 30.000</option>
            </select>
        </div>
    </div>

    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="area" min="1" placeholder="المساحة">
        </div>
    </div>

 
    <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="street" min="1"
                placeholder="عرض شارع الواجهة">
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="cat">
                <option disabled selected>عدد الشوارع</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>

    <div class="col-md-6 col-12">
    <div class="form-group" data-aos="fade-in">
        <select class="form-control select-input" name="cat_2">
            <option disabled selected>عناصر الوقود</option>
            <option value="1">بنزين1</option>
            <option value="2">بنزين2</option>
        </select>
    </div>
</div>

<div class="col-12">
<div class="form-group" data-aos="fade-in">
    <div class="row">
        <div class="col-12">
            <h3 class="arrow-title second_color">
             عدد المضخات :
            </h3>
        </div>
        <div class="col-xl-4 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="gas_1" min="1"
                    placeholder="رقم مضخات البنزين">
            </div>
        </div>

        <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="gas_2" min="1"
                placeholder="رقم مضخات الديزل">
        </div>
        </div>

        <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="gas_3" min="1"
                placeholder="رقم مضخات الكيروسين">
        </div>
        </div>
      </div>
     </div>  
   </div>
 </div>

 <div class="col-12">
<div class="form-group" data-aos="fade-in">
    <div class="row">
        <div class="col-12">
            <h3 class="arrow-title second_color">
             سعة خزانات الوقود :
            </h3>
        </div>
        <div class="col-xl-4 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="gas2_1" min="1"
                    placeholder="رقم لتر البنزين">
            </div>
        </div>

        <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="gas2_2" min="1"
                placeholder="رقم لتر الديزل">
        </div>
        </div>

        <div class="col-xl-4 col-md-6 col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="number" class="form-control" name="gas2_3" min="1"
                placeholder="رقم لتر الكيروسين">
        </div>
        </div>
      </div>
     </div>  
   </div>
 </div>

    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <div class="row">
                <div class="col-12">
                    <h3 class="arrow-title second_color">
                    إضافات أخرى:
                    </h3>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> تموينات :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_1" class="hidden-input" id="check_1">
                                <label for="check_1">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_1" class="hidden-input" id="check_2">
                                <label for="check_2">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> مسجد و دورات مياه :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_2" class="hidden-input" id="check_3">
                                <label for="check_3">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_2" class="hidden-input" id="check_4">
                                <label for="check_4">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color">  تغيير زيوت :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_3" class="hidden-input" id="check_5">
                                <label for="check_5">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_3" class="hidden-input" id="check_6">
                                <label for="check_6">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> غسيل سيارات :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_4" class="hidden-input" id="check_7">
                                <label for="check_7">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_4" class="hidden-input" id="check_8">
                                <label for="check_8">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> كهرباء سيارات :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_5" class="hidden-input" id="check_9">
                                <label for="check_9">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_5" class="hidden-input" id="check_10">
                                <label for="check_10">لا</label>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color">  محل قهوة :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_6" class="hidden-input" id="check2_1">
                                <label for="check2_1">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_6" class="hidden-input" id="check2_2">
                                <label for="check2_2">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color"> مطعم :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_7" class="hidden-input" id="check2_3">
                                <label for="check2_3">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_7" class="hidden-input" id="check2_4">
                                <label for="check2_4">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color">  صراف آلي :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_8" class="hidden-input" id="check2_5">
                                <label for="check2_5">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_8" class="hidden-input" id="check2_6">
                                <label for="check2_6">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="form-group checkboxes-inline-title">
                        <h4 class="second_color">  فندق للطرق :</h4>
                        <div class="inline-custom-checkboxes">
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_9" class="hidden-input" id="check2_7">
                                <label for="check2_7">يوجد</label>
                            </div>
                            <div class="custom-checkbox">
                                <input type="radio" name="radio_9" class="hidden-input" id="check2_8">
                                <label for="check2_8">لا</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                <div class="form-group checkboxes-inline-title">
                    <h4 class="second_color">   سكن للعمال :</h4>
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="radio" name="radio_9" class="hidden-input" id="check2_9">
                            <label for="check2_9">يوجد</label>
                        </div>
                        <div class="custom-checkbox">
                            <input type="radio" name="radio_9" class="hidden-input" id="check2_10">
                            <label for="check2_10">لا</label>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>`
    } else {}

    $(".appended-div").html('');
    $(".appended-div").html(append_div);
    $('.select-input').select2({
        theme: 'bootstrap4',
        language: "ar",
        width: '100%'
    });


    //if select building
    //data appened due to status
    $('.directions').on('select2:select', function () {
        var select_val = $(this).val();

        // شمال 
        if (select_val == "north") {
            var append_div = `<div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="checkbox" class="hidden-input"
                                id="check_1">
                            <label for="check_1" class="second_color"> جار :</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control" name="height_1"
                        min="1" placeholder="بطول">
                </div>
            </div>
        </div>

        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="checkbox" class="hidden-input"
                                id="check_2">
                            <label for="check_2" class="second_color"> فضاء
                                :</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control" name="height_2"
                        min="1" placeholder="بطول">
                </div>
            </div>
        </div>

        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="checkbox" class="hidden-input"
                                id="check_3">
                            <label for="check_3" class="second_color"> شارع بعرض
                                :</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control" name="height_3"
                        min="1" placeholder="بطول">
                </div>
            </div>
        </div>

        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="checkbox" class="hidden-input"
                                id="check_4">
                            <label for="check_4" class="second_color"> ممر مشاه
                                :</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control" name="height_4"
                        min="1" placeholder="بطول">
                </div>
            </div>
        </div>

        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="checkbox" class="hidden-input"
                                id="check_5">
                            <label for="check_5" class="second_color"> مواقف
                                :</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control" name="height_5"
                        min="1" placeholder="بطول">
                </div>
            </div>
        </div>

        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="checkbox" class="hidden-input"
                                id="check_6">
                            <label for="check_6" class="second_color"> حديقة
                                :</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control" name="height_6"
                        min="1" placeholder="بطول">
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="checkbox" class="hidden-input"
                                id="check_7">
                            <label for="check_7" class="second_color"> مسجد
                                :</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control" name="height_7"
                        min="1" placeholder="بطول">
                </div>
            </div>
        </div>

        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
                <div class="form-group checkboxes-inline-title">
                    <div class="inline-custom-checkboxes">
                        <div class="custom-checkbox">
                            <input type="checkbox" class="hidden-input"
                                id="check_8">
                            <label for="check_8" class="second_color"> مرافق عامة
                                :</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control" name="height_8"
                        min="1" placeholder="بطول">
                </div>
            </div>
        </div>
            `

            // شرق 
        } else if (select_val == "east") {
            var append_div = `
            <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check2_1">
                        <label for="check2_1" class="second_color"> جار :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height2_1"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check2_2">
                        <label for="check2_2" class="second_color"> فضاء
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height2_2"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check2_3">
                        <label for="check2_3" class="second_color"> شارع بعرض
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height2_3"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check2_4">
                        <label for="check2_4" class="second_color"> ممر مشاه
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height2_4"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check2_5">
                        <label for="check2_5" class="second_color"> مواقف
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height2_5"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check2_6">
                        <label for="check2_6" class="second_color"> حديقة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height2_6"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check2_7">
                        <label for="check2_7" class="second_color"> مسجد
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height2_7"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check2_8">
                        <label for="check2_8" class="second_color"> مرافق عامة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height2_8"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>`
            // غرب 
        } else if (select_val == "west") {
            var append_div = `
            <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check3_1">
                        <label for="check3_1" class="second_color"> جار :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height3_1"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check3_2">
                        <label for="check3_2" class="second_color"> فضاء
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height3_2"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check3_3">
                        <label for="check3_3" class="second_color"> شارع بعرض
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height3_3"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check3_4">
                        <label for="check3_4" class="second_color"> ممر مشاه
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height3_4"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check3_5">
                        <label for="check3_5" class="second_color"> مواقف
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height3_5"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check3_6">
                        <label for="check3_6" class="second_color"> حديقة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height3_6"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check3_7">
                        <label for="check3_7" class="second_color"> مسجد
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height3_7"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check3_8">
                        <label for="check3_8" class="second_color"> مرافق عامة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height3_8"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>`

            // جنوب 
        } else if (select_val == "south") {
            var append_div = `
            <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check4_1">
                        <label for="check4_1" class="second_color"> جار :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height4_1"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check4_2">
                        <label for="check4_2" class="second_color"> فضاء
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height4_2"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check4_3">
                        <label for="check4_3" class="second_color"> شارع بعرض
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height4_3"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check4_4">
                        <label for="check4_4" class="second_color"> ممر مشاه
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height4_4"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check4_5">
                        <label for="check4_5" class="second_color"> مواقف
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height4_5"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check4_6">
                        <label for="check4_6" class="second_color"> حديقة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height4_6"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check4_7">
                        <label for="check4_7" class="second_color"> مسجد
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height4_7"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check4_8">
                        <label for="check4_8" class="second_color"> مرافق عامة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height4_8"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>`

            // شمال  شرق
        } else if (select_val == "north_east") {
            var append_div = `
            <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check5_1">
                        <label for="check5_1" class="second_color"> جار :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height5_1"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check5_2">
                        <label for="check5_2" class="second_color"> فضاء
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height5_2"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check5_3">
                        <label for="check5_3" class="second_color"> شارع بعرض
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height5_3"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check5_4">
                        <label for="check5_4" class="second_color"> ممر مشاه
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height5_4"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check5_5">
                        <label for="check5_5" class="second_color"> مواقف
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height5_5"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check5_6">
                        <label for="check5_6" class="second_color"> حديقة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height5_6"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check5_7">
                        <label for="check5_7" class="second_color"> مسجد
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height5_7"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check5_8">
                        <label for="check5_8" class="second_color"> مرافق عامة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height5_8"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>`

            // شمال غرب
        } else if (select_val == "north_west") {
            var append_div = `
            <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check6_1">
                        <label for="check6_1" class="second_color"> جار :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height6_1"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check6_2">
                        <label for="check6_2" class="second_color"> فضاء
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height6_2"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check6_3">
                        <label for="check6_3" class="second_color"> شارع بعرض
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height6_3"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check6_4">
                        <label for="check6_4" class="second_color"> ممر مشاه
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height6_4"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check6_5">
                        <label for="check6_5" class="second_color"> مواقف
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height6_5"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check6_6">
                        <label for="check6_6" class="second_color"> حديقة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height6_6"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check6_7">
                        <label for="check6_7" class="second_color"> مسجد
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height6_7"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check6_8">
                        <label for="check6_8" class="second_color"> مرافق عامة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height6_8"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>`

            // جنوب  شرق
        } else if (select_val == "south_east") {
            var append_div = `
            <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check7_1">
                        <label for="check7_1" class="second_color"> جار :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height7_1"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check7_2">
                        <label for="check7_2" class="second_color"> فضاء
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height7_2"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check7_3">
                        <label for="check7_3" class="second_color"> شارع بعرض
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height7_3"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check7_4">
                        <label for="check7_4" class="second_color"> ممر مشاه
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height7_4"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check7_5">
                        <label for="check7_5" class="second_color"> مواقف
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height7_5"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check7_6">
                        <label for="check7_6" class="second_color"> حديقة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height7_6"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check7_7">
                        <label for="check7_7" class="second_color"> مسجد
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height7_7"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check7_8">
                        <label for="check7_8" class="second_color"> مرافق عامة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height7_8"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>`

            // جنوب غرب
        } else if (select_val == "south_west") {
            var append_div = `
            <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check8_1">
                        <label for="check8_1" class="second_color"> جار :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height8_1"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check8_2">
                        <label for="check8_2" class="second_color"> فضاء
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height8_2"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check8_3">
                        <label for="check8_3" class="second_color"> شارع بعرض
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height8_3"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check8_4">
                        <label for="check8_4" class="second_color"> ممر مشاه
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height8_4"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check8_5">
                        <label for="check8_5" class="second_color"> مواقف
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height8_5"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check8_6">
                        <label for="check8_6" class="second_color"> حديقة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height8_6"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check8_7">
                        <label for="check8_7" class="second_color"> مسجد
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height8_7"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>

    <div class="row align-items-center">
        <div class="col-xl-2 col-lg-3 col-sm-5 col-6">
            <div class="form-group checkboxes-inline-title">
                <div class="inline-custom-checkboxes">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input"
                            id="check8_8">
                        <label for="check8_8" class="second_color"> مرافق عامة
                            :</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-10 col-lg-9 col-sm-7 col-6">
            <div class="form-group" data-aos="fade-in">
                <input type="number" class="form-control" name="height8_8"
                    min="1" placeholder="بطول">
            </div>
        </div>
    </div>`

        } else {}

        $(".directions-append").html('');
        $(".directions-append").html(append_div);
        $('.select-input').select2({
            theme: 'bootstrap4',
            language: "ar",
            width: '100%'
        });

    })

})






//advanced-search-form  validation 
var $form_advanced = $(".advanced-search-form");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form_advanced.validate({
    errorPlacement: function (e, i) {
        i.after(e)
    },
    rules: {},
    submitHandler: function () {
        $form_advanced[0].submit()
    }
});