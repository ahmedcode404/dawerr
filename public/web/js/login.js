//login form validation 

$.validator.addMethod("emailtrue", function (value, element) {
    return this.optional(element) || value == value.match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);
});

var $form = $(".login-form");
$form.validate({
    errorPlacement: function errorPlacement(error, element) {
            element.before(error)
    },

    rules: {
        confirm_password : {
            equalTo : "#password"
        },

        agreemnt:{
            required:true
        },

        email:{
            required:true,
            email: true,
            emailtrue : true
        },

        first_name:{
            minlength : 2
        },
        second_name:{
            minlength : 2
        },
        last_name:{
            minlength : 2
        },
    },

    messages: {
        phone:{
            letters : 'يججب ان يكون رقم الجوال يبدأ برقم 5'
        },

        email:{
            emailtrue : 'يجب ان يحتوي البريد الالكتروني علي نطاقات مثال : com , .net , .org , .info. , الي اخره....'
        },
    },
    submitHandler: function () {

            $form[0].submit();

    }
});

