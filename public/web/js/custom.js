
//AOS
AOS.init();

//loading
$(window).on("load", function () {

   
    
    $("body").removeClass("no-trans");
    $(".loading").addClass("active");


    setTimeout(() => {
        $(".loading").fadeOut("slow").css("display", "none")
    }, 1100);
});


$(function(){
    var customersDataString = sessionStorage.getItem('price');

    if (customersDataString == 0) {
        
        $("body").addClass("dark_theme");
        $('.custom-colors').find("i").toggleClass("fa fa-moon fa fa-sun")

    } 
})
//header scroll
$(document).ready(function () {

  
    //progressbar
    var docHeight = $(document).height(),
        windowHeight = $(window).height(),
        scrollPercent;

    var updateProgressbar = function () {
        scrollPercent = $(window).scrollTop() / (docHeight - windowHeight) * 100;
        $('.scroll_progress div').width(scrollPercent + '%');
    }

    updateProgressbar();
    $(window).scroll(updateProgressbar);
    //circle progress
    var progressPath = document.querySelector('.progress-wrap path');
    var pathLength = progressPath.getTotalLength();
    progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
    progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
    progressPath.style.strokeDashoffset = pathLength;
    progressPath.getBoundingClientRect();
    progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
    var updateProgress = function () {
        var scroll = $(window).scrollTop();
        var height = $(document).height() - $(window).height();
        var progress = pathLength - (scroll * pathLength / height);
        progressPath.style.strokeDashoffset = progress;
    }
    updateProgress();
    $(window).scroll(updateProgress);


});

//theme colors
$(".custom-colors").click(function(){

    // sessionStorage.setItem('price' , 1);

    var customersDataString = sessionStorage.getItem('price');

    if (customersDataString == 1) {

        sessionStorage.setItem('price' , 0);
        $(this).find("i").toggleClass("fa fa-moon fa fa-sun")
        $("body").addClass("dark_theme");

    } else 
    {

        $(this).find("i").toggleClass("fa fa-sun fa fa-moon")
        $("body").removeClass("dark_theme");
        sessionStorage.setItem('price' , 1);

    }
    
  })

//slide menu
$(document).on("click", ".slide-link", (function () {
    $(this).toggleClass("active");
    $(this).next(".slide-menu").slideToggle();
    $(this).closest(".header-icon").siblings().find(".slide-link");
    $(this).closest(".header-icon").siblings().find(".slide-menu").slideUp("fast")
}));

$(function () {
    var $wins = $(window); // or $box parent container
    var $boxs = $(".header-icon");
    $wins.on("click.Bst", function (event) {
        if (
            $boxs.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxs.is(event.target) //checks if the $box itself was clicked
        ) {
            $(".slide-menu").slideUp("fast")

        }
    });
});

//search
$(document).on("click", ".responsive-search", (function () {
    $(".search-bg").addClass("active");
}));

$(document).on("click", ".close-search", (function () {
    $(".search-bg").removeClass("active");
}));

//images
$(".converted-img").each((function (e, i) {
    var s = $(i);
    s.parent(".full-width-img").css({
        backgroundImage: "url(" + s.attr("src") + ")"
    })
}));



//search form validation 
var $form = $("#searchform");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form.validate({
    errorPlacement: function (e, i) {
        i.before(e)
    },
    rules: {},
    submitHandler: function () {
        $form[0].submit()
    }
});

$(function(){

    var email   = $('#email-firebase').val();
    var name   = $('#name-firebase').val();
    var image   = $('#image-firebase').val();
    var slug   = $('#slug-firebase').val();
    var user_id = $('#id-firebase').val();

    // console.log(email);
    // console.log(name);
    // console.log(image);
    // console.log(slug);
    // console.log(user_id);

    var db = firebase.firestore();
    
    var docRef = db.collection("users").doc("userId_"+user_id);
    docRef.get().then(function(doc) {
        if (doc.exists) { 
    
    
         } else {
    
            firebase.auth().createUserWithEmailAndPassword(email, "daweer" + user_id).then((userCredential) => {
    
                    // Signed in
                    var user = userCredential.user;
    
                    // Add a new document in collection "users"
                    db.collection("users").doc('userId_' + user_id).set({
    
                            'name': name,
                            'email': email,
                            'photoUrl': image,
                            'id': "userId_" + user_id,
                            'createdAt': firebase.firestore.FieldValue.serverTimestamp(),
                            'chattingWith': null,
                            'typingTo': null,
                            'unReadingCount': 0,
                            'status': 'online',
                            'slug': slug,
                            'lastSeen': firebase.firestore.FieldValue.serverTimestamp(),
                        })
                        .then(() => {
                            
                            
    
                        })
                        .catch((error) => {
                            console.error("Error writing document: ", error);
                        });
    
                    })
                    .catch((error) => {
    
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // ..
                    });
    
    
         }  
    
    }).catch(function(error) {
        console.log("Error getting document:", error);
    }); 

});


$(function() {

    // search in firebase
    function searchPeerId(nameKey, myArray) {

        for (var i = 0; i < myArray.length; i++) {
            var str = myArray[i].userId;
            var reciver = str.split("userId_");

            if (reciver[1] === nameKey) {

                return myArray[i];

            } // end of if

        } // end of for

    } // end of function search in firebase


    // get all user with first messages
    var allChatUsersList1 = [];
    var allMessagesList = [];
    var allMessagesListReadCount = [];

    var sender_id = $('#sender-id').val();

    var db = firebase.firestore();

    db.collection("users").orderBy("createdAt", "desc")
        .onSnapshot(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {

                if (doc.data().id != "userId_" + sender_id) {

                    var newPhoto = doc.data().photoUrl;

                    allChatUsersList1.push({

                        'name': doc.data().name,
                        'photoUrl': newPhoto,
                        'userId': doc.data().id,
                        'status': doc.data().status,
                        'slug': doc.data().slug,
                        'lastSeen': doc.data().lastSeen,
                        'typingTo': doc.data().typingTo,

                    });

                }


            });


            var i = 0;

            db.collection("messages").orderBy("timestamp", "asc").limitToLast(3)
                .onSnapshot(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {

                        const myId = "userId_" + sender_id;

                        if (doc.data().idAll.includes(sender_id)) {

                            var peerId = doc.data().idAll.replace('-', '').replace(sender_id, '');

                            var peerData = searchPeerId(peerId, allChatUsersList1);
                            const userMesIndex = allMessagesList.findIndex(user => user.userId === peerId);

                            if (userMesIndex === -1) {

                                var d = new Date(doc.data().timestamp['seconds'] * 1000);

                                var message_time = d.toLocaleTimeString('ar-EG', { hour: 'numeric', minute: 'numeric' });
                                var message_date = d.toLocaleDateString('ar-EG', { year: 'numeric', month: 'long', day: 'numeric' });


                                var image = 'صوره';

                                if (doc.data().type == 2) {

                                    var message = '<p>' + image + ' </p>';

                                } else {

                                    var message = '<p>' + doc.data().content + '</p>'

                                }

                                // image = peerData.photoUrl;


                                allMessagesList.push({
                                    'userId': peerId,
                                    'name': peerData.name,
                                    'userPhoto': peerData.photoUrl,
                                    'slug': peerData.slug,
                                    'content': message, // getContentValue(doc.data().content, doc.data().type),
                                    'date': message_time, // getDateValue(doc.data().timestamp),
                                    'allData': message_date, // getDateAllValue(doc.data().timestamp)
                                });

                                allMessagesListReadCount.push({
                                    'userId': peerId,
                                    'messageId': doc.id,
                                    'status': doc.data().idTo == sender_id ? doc.data().read : true
                                });

                            } else {

                                const allMessagesListReadCountIndex = allMessagesListReadCount.findIndex(count => count.messageId === doc.id);
                                if (allMessagesListReadCountIndex === -1) {
                                    allMessagesListReadCount.push({
                                        'userId': peerId,
                                        'messageId': doc.id,
                                        'status': doc.data().idTo == sender_id ? doc.data().read : true
                                    });

                                } else {
                                    const olduserId = allMessagesListReadCount[allMessagesListReadCountIndex].userId;

                                    const olduserMessageId = allMessagesListReadCount[allMessagesListReadCountIndex].messageId;
                                    allMessagesListReadCount[allMessagesListReadCountIndex] = {
                                        'userId': olduserId,
                                        'messageId': olduserMessageId,
                                        'status': doc.data().idTo == sender_id ? doc.data().read : true
                                    }
                                }


                                var d = new Date(doc.data().timestamp * 1000);

                                var message_time = d.toLocaleTimeString('ar-EG', { hour: 'numeric', minute: 'numeric' });
                                var message_date = d.toLocaleDateString('ar-EG', { year: 'numeric', month: 'long', day: 'numeric' });


                                var image = 'صوره';

                                if (doc.data().type == 2) {

                                    var message = '<p>' + image + ' </p>';

                                } else {

                                    var message = '<p>' + doc.data().content + '</p>'

                                }
                                
                                image = peerData.photoUrl;


                                allMessagesList[userMesIndex] = {
                                    'userId': peerId,
                                    'name': peerData.name,
                                    'userPhoto': peerData.photoUrl,
                                    'slug': peerData.slug,
                                    'content': message, //getContentValue(doc.data().content, doc.data().type),
                                    'date': message_time, //getDateValue(doc.data().timestamp),
                                    'allData': message_date //getDateAllValue(doc.data().timestamp)
                                };
                                //}
                            }
                        }
                        allMessagesList.sort(function(a, b) {
                            return new Date(b.allData) - new Date(a.allData);
                        });
                    });

                    if (allMessagesList.length == 0) {
                        var str = '';
                        str += '<div class="enquiriesContainer" style="text-align: center;padding-top: 1.5rem; padding-bottom: 1.5rem">' + 'لا يوجد محادثات' +
                            '</div>';
                        $('#message-unread').html(str);
                    } else {

                        var str = '';
                        var countunread = 0;

                        //str+='<tr>';

                        allMessagesList.forEach(function(message) {

                            var count = allMessagesListReadCount.filter(mess => mess.userId === message.userId && mess.status === false);
                            var text1 = ' قام ' ;
                            var text2 = ' بإرسال رساله لك  ';


                            countunread += count.length
                            // if (count.length > 0) {


                            //     var sound = $('#sound').val();

                            //     var audio = new Audio(sound);

                            //     audio.play();

                            // }
                            
                            
                            // class = "unread"

                            img_user = message.userPhoto;

                            
                            
                            if (count.length > 0) {

                                str += '<li class="unread-message">';    
                                str += '<span class="count-unread">'+ count.length +'</span>';    
                                
                            } else {

                                str += '<li>';
                                
                            }                            
                            if (count.length > 0) {

                                str += '<a href="' +
                                window.location.origin + '/chat/' + message.slug +
                                '">'    

                            } else {

                                str += '<a class="unread" href="' +
                                window.location.origin + '/chat/' + message.slug +
                                '">'
                                
                            }

                            str += '<img src=' + img_user + ' alt="" width="70px" />' +
                                '<h4>' + text1 + message.name + text2 + '</h4>' +
                                '</a>' +
                                '</li>';

                        });

                        $('#count-message').html(countunread ? '<span class="num">'+ countunread +'</span>' : '');

                        $('#message-unread').html(str);

                        i++;
                    }
                });

        }); // end of get user



}) // end of funcrion

