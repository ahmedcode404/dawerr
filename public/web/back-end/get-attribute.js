
$('#subcategory-select').change(function(){

    var id = $(this).val();

    var action = $(this).data('action');

    $.ajax({
        type: 'GET',
        url: action,
        data: {
            'id' : id,
        },
        dataType: 'html',
        success: function(result) {

            $(".all-option").each(function() {
                $(this).rules('add', {
                    required: true
                });
            });            

            $('.appended-div').html(result);

        } // end of success

    }); // end of ajax 

});


$(document).on('keyup' , '.keypress-checkbox' , function(){

    var id = $(this).attr('id');
    var dir_id = $(this).data('id');

    if ($(this).val() != '') {

        $('.direction-'+id).prop('checked' , true);
        
    } else 
    {

        $('.direction-'+id).prop('checked' , false);

    }

})


$(document).on('change' , '#add-input-fuel' , function(){
    

    $('.append-waqod').html('');

    var append = `
    <div class="col-md-12 col-12">
        <div class="form-group" data-aos="fade-in">
            <input required type="number" class="form-control" name="number_pumps" placeholder="عدد مضخات الوقود">
        </div>
    </div>  

    <div class="col-md-12 col-12">
        <div class="form-group" data-aos="fade-in">
            <input required type="number" class="form-control" name="fuel_tank_capacity" placeholder="سعة خزانات الوقود">
        </div>
    </div>  
    `;

    $('.append-waqod').html(append);

});


$(document).on('change' , '.check-option' , function(){

    var id = $(this).data('id');
    
    $('.remove-'+id).not($(this)).prop("checked", false);

});


// $("#sayed").on("change",function(){
//     $(".dd").prop("required",true);
// })

$(document).on('change' , '.check-directione' , function(){

    var id = $(this).val();
    var dir_id = $(this).data('id');

    if (this.checked) 
    {

        // alert('checked');

    } else 
    {

        $('.get-value-'+id+'-'+dir_id).val('');

    }

});


$(document).on('change' , '.click-append-direction' , function(){

    var id = $(this).data('id');
    var action = $(this).data('action');
    if (this.checked)
    {

        $.ajax({
            type: 'GET',
            url: action,
            data: {
                'id' : id,
            },
            dataType: 'html',
            success: function(result) {


                $('#append-direction').prepend(result);

            } // end of success

        }); // end of ajax

    } else
    {
        $('.pro-append-'+id).remove();
    }


});