$('.add-to-favorite').click(function(){

    var id = $(this).data('id');
    var action = $(this).data('action');

    $.ajax({
        type: 'GET',
        url: action,
        data: {
            'id' : id,
        },
        dataType: 'json',
        success: function(result) {

            if(result.status == true)
            {

                swal.fire({
                    title: 'تم اضافه المنتج الي المفضلة',
                    // showConfirmButton: false,
                    type: 'success',
                    timer: 1600
                });

            } else 
            {

                swal.fire({
                    title: 'تم حذف المنتج من المفضلة',
                    // showConfirmButton: false,
                    type: 'success',
                    timer: 1600
                });

            }


        } // end of success

    }); // end of ajax 

});

$('.login-first').click(function(e) {
    e.preventDefault();
    var redirect_login = $(this).data('redirectlogin');

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
        title: "برجاء التسجيل اولا , هل تريد التسجيل الان ؟",
        type: 'question',
        showCancelButton: true,
        heightAuto:true,
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا',
    }).then((result) => {
        if (result.value) {

            window.location.href = redirect_login;

        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
        ) {
        
        }
    })


}); // end of login first

$('.category-diffrent').click(function(e) {

    e.preventDefault();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
        title: " يمكنك رؤية المنتجات فقط خارج النشاط الخاص بك",
        type: 'warning',
        showCancelButton: false,
        heightAuto:true,
        confirmButtonText: 'تم',
    });


}); // end of login first

//datepicker
$(function () {

        $(document).on('click', '.pagination a', function(event){

            event.preventDefault(); 

            var page = $(this).attr('href').split('page=')[1];

            fetch_data(page);

        });

        function fetch_data(page)
        {
            $.ajax({

                url: window.location.origin + "/pagination?page="+page,
                success:function(data)
                {
                    $('#append-data').html(data);
                }

            });
        }


    $('.datepick').datepicker({
        dateFormat: 'yy'
    })
});

