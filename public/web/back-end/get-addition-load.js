$(window).on('load' , function(){
    
    var id = $('#get-addition').val();

    if(id != null)
    {

        var action = $('#get-addition').data('action');
    
        $.ajax({
            type: 'GET',
            url: action,
            data: {
                'id' : id,
            },
            dataType: 'html',
            success: function(result) {
    
                $('#append-this-addition').html(result);
    
            } // end of success
    
        }); // end of ajax 

    }

})