
$(document).on('click', '.done-sale', function (e) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })
  
    swalWithBootstrapButtons.fire({
      title: 'هل تريد تغيير حالة الاعلان الي تم البيع ؟',
      type: 'question',
      showCancelButton: true,
      heightAuto:true,
      confirmButtonText: 'نعم',
      cancelButtonText: 'لا',
    }).then((result) => {
        if (result.value) {

            var id  = $(this).data('id');
            var url = $(this).data('action');
            var $this = $(this);
            $.ajax({
                type: 'GET',
                url: url,
                data: { 'id' : id },
                dataType: 'json',
                success: function(result) {

                    swalWithBootstrapButtons.fire({
                        type: 'success',
                        title: 'تم تغيير حالة الاعلان  بنجاح',
                        showConfirmButton: false,
                        timer: 1000
                    });

                    // $('.hide-this').hide();
                    $this.closest('.hide-this').remove();
                    $('.appent-done-sale-'+id).html('<span class="badges red-badge">تم البيع</span>');

                } // end of success

            }); // end of ajax 

        } else if (result.dismiss === Swal.DismissReason.cancel) 
        {
        
        }
    })
  
  });