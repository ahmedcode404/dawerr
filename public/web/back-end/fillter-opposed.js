$('#fillter-opposed').change(function(){

    var id = $(this).val();
    var action = $(this).data('action');

    $.ajax({
        type: 'GET',
        url: action,
        data: {
            'city_id' : id,
        },
        dataType: 'html',
        success: function(result) {

            $('#itemContainer').html(result);
            $('.paginate-center').remove();
        } // end of success

    }); // end of ajax 

});