$(document).on('click', '.delete-image', function (e) {

    e.preventDefault();

    var $this = $(this);

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })
  
    swalWithBootstrapButtons.fire({
      title: 'هل تريد ازالة الصورة ؟',
      type: 'warning',
      showCancelButton: true,
      heightAuto:true,
      confirmButtonText: 'نعم',
      cancelButtonText: 'لا',
    }).then((result) => {
      if (result.value) {

        var id = $(this).attr('id');
        var action = $(this).data('action');        

        $.ajax({
            type: 'GET',
            url: action,
            data: {
                'id' : id,
            },
            dataType: 'json',
            success: function(result) {

                // remove item                 
                $(".image-"+id).remove();
                
                swalWithBootstrapButtons.fire({
                  type: 'success',
                  title: 'تم الازالة الصورة بنجاح',
                  showConfirmButton: false,
                  timer: 1500
                });

                if($('.delete-image').length == 0)
                {

                    $('.append-defualt').html('<img src="'+document.location.origin+'/web'+'/images/products/add.png" alt="إصافة صورة">');

                }

            } // end of success
    
        }); // end of ajax         


  
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
       
      }
    })
  
  });