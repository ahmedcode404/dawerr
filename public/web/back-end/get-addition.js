
$('#get-addition').change(function(){

    var id = $(this).val();
    var action = $(this).data('action');

    $.ajax({
        type: 'GET',
        url: action,
        data: {
            'id' : id,
        },
        dataType: 'html',
        success: function(result) {

            $('#append-this-addition').html(result);

        } // end of success

    }); // end of ajax 

});
var action = $('#get-option-other-addition').data('action');

// $( "#get-option-other-addition" ).load(action, function() {

//         var id = $(this).val();
//         var action = $(this).data('action');

//         $.ajax({
//             type: 'GET',
//             url: action,
//             data: {
//                 'id' : id,
//             },
//             dataType: 'html',
//             success: function(result) {

//                 $('#append-this-other-addition').html(result);

//             } // end of success

//         }); // end of ajax
//   });


$('#get-option-other-addition').change(function(){

    var id = $(this).val();
    var action = $(this).data('action');

    $.ajax({
        type: 'GET',
        url: action,
        data: {
            'id' : id,
        },
        dataType: 'html',
        success: function(result) {

            $('#append-this-other-addition').html(result);

        } // end of success

    }); // end of ajax 

});

$('.check-option').change(function(){

    var id = $(this).data('id');
    $('.remove-' + id).not(this).prop("checked", false);

});