$(document).ready(function() {

    setTimeout(() => {
                    
        $("#content-meassages").scrollTop(15+80000);

    }, 3000);

    var recever_id = $('#reciver-id').val();
    var image_reciver = $('#image-reciver').val();
    var sender_id = $('#sender-id').val();
    var image_sender = $('#image-sender').val();


    $("#search-user").on("keyup", function() {

        var value = $(this).val().toLowerCase();
        $("#all-users a").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            

        });
        
    });    




    var db = firebase.firestore();

    function initIdToSide(sId, rId) {

        db.collection("messages")
        .where("idTo", "==", sId)
        .where("idFrom", "==", rId)
        .where('read', "==", false)
        .get().then(function(querySnapshot) {
            if (querySnapshot.docs.length > 0) {
                for (var i in querySnapshot.docs) {
                    db.collection("messages").doc(querySnapshot.docs[i].id).update({
                        'read': true
                    })
                }

            }
        });
    }


    // get all messages
    db.collection("messages").where("idAll", "in", [
            recever_id + '-' + sender_id,
            sender_id + '-' + recever_id
        ])
        .orderBy('timestamp')
        .onSnapshot(function(querySnapshot) {
            if(querySnapshot.docs.length != 0)
            {

                var str = '';
                var allConersionDate = {};
                for (var i in querySnapshot.docs) {


                    initIdToSide(sender_id, recever_id)

                    var class_dir = '';
                    var image = '';
                    if (querySnapshot.docs[i].data().idTo == recever_id) {
                        class_dir = '';
                        image = image_sender;
                    } else {

                        class_dir = 'reciever';

                        image = image_reciver;

                    }
                    if (querySnapshot.docs[i].data().timestamp != null) {
                        
                        const options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
                        var d = new Date(querySnapshot.docs[i].data().timestamp['seconds'] * 1000);

                        var message_date = d.toLocaleDateString('ar-EG', { year: 'numeric', month: 'long', day: 'numeric' });
                        var message_time = d.toLocaleTimeString('ar-EG', { hour: 'numeric', minute: 'numeric' });
                        
                        var isDateExist = message_date in allConersionDate;
                        if (!isDateExist) {
                            allConersionDate[message_date] = querySnapshot.docs[i].id;
                            str += '<p class="message-date" style="text-align:center">' + message_date + '</p>';
                        }


                        str += '<div class="chat-message first_color ' + class_dir + '">';
                        if (i == 0) {
                            str += '<div class="chat-img"><img src=' + image + ' alt=""></div>';
                        } else if (querySnapshot.docs[(querySnapshot.docs.length - i) - 1].data().idFrom != sender_id) {
                            if ((i > 0 && querySnapshot != null && querySnapshot.docs[(querySnapshot.docs.length - i) - 1].data().idFrom != sender_id)) {
                                str += '<div class="chat-img"><img src=' + image + ' alt=' + i + '></div>';
                            } else {
                                str += '';
                            }

                        } else if (querySnapshot.docs[(querySnapshot.docs.length - i) - 1].data().idFrom == sender_id) {
                            if ((i > 0 && querySnapshot != null && querySnapshot.docs[(querySnapshot.docs.length - i) - 1].data().idFrom == sender_id)) {
                                str += '<div class="chat-img"><img src=' + image + ' alt=""></div>';
                            } else {
                                str += '';
                            }

                        }


                        if (querySnapshot.docs[i].data().type == 1) {
                            str += '<div class="chat-bubble">' + querySnapshot.docs[i].data().content + '</div>';
                        } else if (querySnapshot.docs[i].data().type == 2) {
                            str += '<img class="chat-bubble" src=' + window.location.origin + '/storage/' + querySnapshot.docs[i].data().content + '>';
                        } else {
                            str += '<a class="chat-bubble" href=' + window.location.origin + '/storage/' + querySnapshot.docs[i].data().content + '</a>';
                        }

                        str += '<div class="timechet"><span>' + message_time + '</span></div>';
                        str += '</div>';

                    }

                }

                $("#content-meassages").scrollTop(15+80000);

                $('#content-meassages').html(str);
            } else 
            {

                $('#content-meassages').html('<div class="enquiriesContainer" style="padding-top: 1.5rem; padding-bottom: 1.5rem;text-align: center;">' + 'لا يوجد محادثات' +
                '</div>');

            }
        }); /// get message bettwen sender and reciver



// when online show icon

var db = firebase.firestore();

db.collection("users").doc("userId_"+recever_id).get().then(function(dat) {

    
    if (dat.data().status == 'online') {

        $('.main-chat-owner').addClass('online');

    } else {

        $('.main-chat-owner').removeClass('online');

    }

});        


}) // end of document


function searchPeerId(nameKey, myArray) {

for (var i = 0; i < myArray.length; i++) {
    var str = myArray[i].userId;
    var reciver = str.split("userId_");

    if (reciver[1] === nameKey) {

        return myArray[i];
    }
}
}


// get all user with first messages
var allChatUsersList1 = [];
var allMessagesList = [];
var allMessagesListReadCount = [];
var count = [];
var recever_id = $('#reciver-id').val();
var sender_id = $('#sender-id').val();

var db = firebase.firestore();

db.collection("users").orderBy("createdAt", "desc")
.onSnapshot(function(querySnapshot) {
    querySnapshot.forEach(function(doc) {

        if (doc.data().id != "userId_" + sender_id) {
            
            allChatUsersList1.push({

                'name': doc.data().name,
                'photoUrl': doc.data().photoUrl,
                'userId': doc.data().id,
                'status': doc.data().status,
                'slug': doc.data().slug,
                'lastSeen': doc.data().lastSeen,
                'typingTo': doc.data().typingTo,

            });

        }


    });

    var i = 0;

    db.collection("messages").orderBy("timestamp", "asc")
        .onSnapshot(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {

                const myId = "userId_" + sender_id;

                if (doc.data().idAll.includes(sender_id)) {

                    var peerId = doc.data().idAll.replace('-', '').replace(sender_id, '');

                    var peerData = searchPeerId(peerId, allChatUsersList1);
                    
                    const userMesIndex = allMessagesList.findIndex(user => user.userId === peerId);

                    if (userMesIndex === -1) {

                        var d = new Date(doc.data().timestamp * 1000);

                        var message_time = d.toLocaleTimeString('ar-EG', { hour: 'numeric', minute: 'numeric' });
                        var message_date = d.toLocaleDateString('ar-EG', { year: 'numeric', month: 'long', day: 'numeric' });

                        var image = 'صوره';

                        if (doc.data().unReadingCount) 
                        {
                            
                        }

                        if (doc.data().type == 2) {

                            var message = '<p>' + image + ' </p>';

                        } else {

                            var message = '<p>' + doc.data().content + '</p>'

                        }

                        allMessagesList.push({
                            'userId': peerId,
                            'name': peerData.name,
                            'userPhoto': peerData.photoUrl,
                            'slug': peerData.slug,
                            'content': message, // getContentValue(doc.data().content, doc.data().type),
                            'date': message_time, // getDateValue(doc.data().timestamp),
                            'allData': message_date, // getDateAllValue(doc.data().timestamp)
                        });

                        allMessagesListReadCount.push({
                            'userId': peerId,
                            'messageId': doc.id,
                            'status': doc.data().idTo == sender_id ? doc.data().read : true
                        });

                    } else {

                        const allMessagesListReadCountIndex = allMessagesListReadCount.findIndex(count => count.messageId === doc.id);
                        if (allMessagesListReadCountIndex === -1) {
                            allMessagesListReadCount.push({
                                'userId': peerId,
                                'messageId': doc.id,
                                'status': doc.data().idTo == sender_id ? doc.data().read : true
                            });

                        } else {
                            const olduserId = allMessagesListReadCount[allMessagesListReadCountIndex].userId;

                            const olduserMessageId = allMessagesListReadCount[allMessagesListReadCountIndex].messageId;
                            allMessagesListReadCount[allMessagesListReadCountIndex] = {
                                'userId': olduserId,
                                'messageId': olduserMessageId,
                                'status': doc.data().idTo == sender_id ? doc.data().read : true
                            }
                        }


                        var d = new Date(doc.data().timestamp * 1000);

                        var message_time = d.toLocaleTimeString('ar-EG', { hour: 'numeric', minute: 'numeric' });
                        var message_date = d.toLocaleDateString('ar-EG', { year: 'numeric', month: 'long', day: 'numeric' });


                        var image =  'صوره';

                        if (doc.data().type == 2) {

                            var message = '<p>' + image + ' </p>';

                        } else {

                            var message = '<p>' + doc.data().content + '</p>'

                        }

                        image = peerData.photoUrl;

                       
                        allMessagesList[userMesIndex] = {
                            'userId': peerId,
                            'name': peerData.name,
                            'userPhoto': image,
                            'slug': peerData.slug,
                            'content': message, //getContentValue(doc.data().content, doc.data().type),
                            'date': message_time, //getDateValue(doc.data().timestamp),
                            'allData': message_date //getDateAllValue(doc.data().timestamp)
                        };
                        //}
                    }
                }
                allMessagesList.sort(function(a, b) {
                    return new Date(b.allData) - new Date(a.allData);
                });
            });

            if (allMessagesList.length == 0) {
                var str = '';
                str += '<div class="enquiriesContainer" style="padding-top: 1.5rem; padding-bottom: 1.5rem;text-align: center;">' + 'لا يوجد اعضاء' +
                    '</div>';
                $('#all-users').html(str);
            } else {

                var str = '';
                var str1 = '';
                //str+='<tr>';

                allMessagesList.forEach(function(message) {

                    var count = allMessagesListReadCount.filter(mess => mess.userId === message.userId && mess.status === false);

                    img_user = message.userPhoto;
                    
                    str += '<a class="link-url" href=" ' +
                        window.location.origin + '/chat/' + message.slug +
                        '">' +
                        '<div class = "chat-img">' +
                        '<img src=' + img_user + ' alt="" />' +
                        '</div>' +
                        '<div class="side-contact-chat">' +
                        '<h3 class="second_color">' + message.name + '</h3>';
                    if (count.length > 0) {
                        str += '<span class="count-message">' + count.length + '</span>';
                    }
                    str += '<span class="message-time gray-color">' + message.date + '</span>'+
                        '<p>' +
                        message.content +
                        '</p>' +
                        '</div>' +
                        '</a>';
                });


                $('#all-users').html(str);

                i++;
            }
        });


}); // end of get user


// scroll down
var scroll_bottom = function() {
    $(".chat-date").scrollTop(20000);
}
// end of scroll

// start form send message
var form = $('#form-chat');
var message = form.find('#input-chat');

form.submit(function(event) {

    event.preventDefault();

    // onFormSubmit(form);

    var recever_id = $('#reciver-id').val();
    var sender_id = $('#sender-id').val();

    var db = firebase.firestore();

    db.collection("messages").add({
        'idTo': recever_id,
        'idFrom': sender_id,
        'idAll': recever_id + '-' + sender_id,
        'timestamp': firebase.firestore.FieldValue.serverTimestamp(),
        'content': message.val(),
        'type': 1,
        'read': false
    })
    .then(() => {


        // get message all
        $('#input-chat').val("");

        setTimeout(() => {
                    
            $("#content-meassages").scrollTop(15+80000);

        }, 600);

    })
    .catch((error) => {

        console.error("Error writing document: ", error);

    });

});
// end form send message

// send image chat
$("#form-chat").on("change", function() {

var recever_id = $('#reciver-id').val();
var sender_id = $('#sender-id').val();
var url = $(this).data('upload');

var image = $('#upload-chat')[0].files;

var formData = new FormData();

formData.append('image', image[0]);

$.ajax({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    url: url,
    type: "POST",
    cache: false,
    contentType: false, // you can also use multipart/form-data replace of false
    processData: false,
    data: formData,
    dataType: 'json',
    success: function(data) {

        $('#upload-chat').val('');
        $('#input-chat').val('');

        var read = false;
        // if (sender_id == recever_id) {
        //     read = true;
        // }

        // send message
        var db = firebase.firestore();

        db.collection("messages").add({
                'idTo': recever_id,
                'idFrom': sender_id,
                'idAll': recever_id + '-' + sender_id,
                'timestamp': firebase.firestore.FieldValue.serverTimestamp(),
                'content': data.image,
                'type': 2,
                'read': false
            })
            .then(function(docRef) {
                var docRef = db.collection("users").doc(recever_id);
                docRef.get().then(function(doc) {
                    if (doc.exists) {
                        var unreadcount = doc.data().unReadingCount;
                        if (!read) {
                            // reciever
                            db.collection("users").doc(recever_id).update({
                                'unReadingCount': unreadcount + 1,
                                'typingTo': null
                            });
                        } else {
                            db.collection("users").doc(recever_id).update({
                                'typingTo': null
                            });
                        }
                    } else {

                    }
                }).catch(function(error) {

                });

                setTimeout(() => {
                    
                    $("#content-meassages").scrollTop(15+80000);

                }, 600);

            })
            .catch(function(error) {

            });




    }
});
});
// end of image chat


var db = firebase.firestore();
var recever_id = $('#reciver-id').val();
var sender_id = $('#sender-id').val();

// when auth writing
$("#input-chat").keyup(function() {

if ($('#input-chat').val().trim().length > 0) {
    db.collection("users").doc("userId_" + sender_id).update({
        'typingTo': "userId_" + recever_id
    });


    // when reciver writing
    db.collection("users").doc("userId_" + recever_id).onSnapshot(function(reciverIds) {

        db.collection("users").doc("userId_" + sender_id).get().then(function(senderIds) {

            if (reciverIds.data().typingTo != null && reciverIds.data().typingTo == senderIds.data().id) {

                $('#online').html('يكب الان ....');

            } else {

                $('#online').html('نشط الأن');

            }

        });

    }); // end of when reciver writing



} else {
    db.collection("users").doc("userId_" + sender_id).update({
        'typingTo': null
    });
}
}); // end of when auth writing

