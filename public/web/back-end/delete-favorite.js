$(document).on('click', '.remove-button', function (e) {

    var $this = $(this);

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })
  
    swalWithBootstrapButtons.fire({
      title: 'هل تريد ازالة المنتج من المفضلة ؟',
      type: 'warning',
      showCancelButton: true,
      heightAuto:true,
      confirmButtonText: 'نعم',
      cancelButtonText: 'لا',
    }).then((result) => {
      if (result.value) {

        var id = $(this).data('id');
        var action = $(this).data('action');        

        $.ajax({
            type: 'GET',
            url: action,
            data: {
                'id' : id,
            },
            dataType: 'json',
            success: function(result) {

                // remove item                 
                $this.closest(".remove-product").remove();
                
                swalWithBootstrapButtons.fire({
                  type: 'success',
                  title: 'تم الازالة من المفضلة بنجاح',
                  showConfirmButton: false,
                  timer: 1500
                });

                if($('.remove-product').length == 0)
                {
                    $('.profile-data').first().html('<p style="margin: auto">لا توجد منتجات في المفضلة </p>');
                }

            } // end of success
    
        }); // end of ajax         


  
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
       
      }
    })
  
  });