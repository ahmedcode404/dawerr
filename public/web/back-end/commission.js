$('.append-url-to-button').change(function(){

    var id = $(this).data('id');

    $('#payment-now').attr('data-action' , document.location.origin + '/payment/' + id);
    $('#payment-now').attr('data-id' , id);

}); // end of change radio button and append id bank


$('#payment-now').click(function(){

    var value = $('input[name=bank_id]:checked').val();
    var price = $('#price-product').val();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
      })

    if(price == '')
    {

        swalWithBootstrapButtons.fire({
        type: 'error',
        title: 'من فضلك ادخل السعر لتتكمن من الدفع',
        showConfirmButton: true,
        });          

    }
    else if(value == undefined)
    {

        swalWithBootstrapButtons.fire({
            type: 'error',
            title: 'من فضلك اختر وسيلة الدفع',
            showConfirmButton: true,
        });          
        
    } else 
    {

        var id  = $(this).attr('data-id');
        var price_result  = $('.commission-result').text();

        if (id == 'thoyl-bnky') {
 
            sessionStorage.setItem("price", JSON.stringify(price_result));

            window.location.replace($(this).data('action'));

        } else 
        {

            swalWithBootstrapButtons.fire({
                type: 'question',
                title: 'الدفع بالفيزا او الماستر كارد',
                showConfirmButton: true,
            });             

        }

        
    } // end of else if
    
}); // end of payment now

