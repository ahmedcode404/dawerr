$('#subcategory-select').change(function(){

    var id = $(this).val();

    var action = $(this).data('action');

    $.ajax({
        type: 'GET',
        url: action,
        data: {
            'id' : id,
        },
        dataType: 'html',
        success: function(result) {

            $('.appended-div').html(result);

        } // end of success

    }); // end of ajax 

});
