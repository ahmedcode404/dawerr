
$('.add-to-favorite').click(function(){

    var id = $(this).data('id');
    var action = $(this).data('action');

    $.ajax({
        type: 'GET',
        url: action,
        data: {
            'id' : id,
        },
        dataType: 'json',
        success: function(result) {

            if(result.status == true)
            {

                swal.fire({
                    title: 'تم اضافه المنتج الي المفضلة',
                    // showConfirmButton: false,
                    type: 'success',
                    timer: 1600
                });

            } else 
            {

                swal.fire({
                    title: 'تم حذف المنتج من المفضلة',
                    // showConfirmButton: false,
                    type: 'success',
                    timer: 1600
                });

            }


        } // end of success

    }); // end of ajax 

}); // end fo add to favorite

$('.login-first').click(function() {

    var redirect_login = $(this).data('redirectlogin');

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
        title: "برجاء التسجيل اولا , هل تريد التسجيل ؟",
        type: 'question',
        showCancelButton: true,
        heightAuto:true,
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا',
    }).then((result) => {
        if (result.value) {

            window.location.href = redirect_login;

        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
        ) {
        
        }
    })


}); // end of login first

$('.category-diffrent').click(function() {

    var redirect_login = $(this).data('redirectlogin');

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
        title: "يمكنك رؤية المنتجات فقط خارج النشاط الخاص بك",
        type: 'warning',
        showCancelButton: false,
        heightAuto:true,
        confirmButtonText: 'تم',
    });


}); // end of login first




$('#code-copy').click(function(e) {
    
    e.preventDefault();
    
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
    })
    
    
    var dummy = document.createElement('input'),
    text = $(this).text();
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand('copy');
    
    swalWithBootstrapButtons.fire({
        
        type: 'success',
        title: 'تم نسخ الكود بنجاح',
        showConfirmButton: false,
        timer: 1000
        
    });
    
    document.body.removeChild(dummy);
    
})


$('#redirect-chat').click(function(e) {


    e.preventDefault();

    // var email   = $(this).data('email');
    // var name    = $(this).data('name');
    // var image   = $(this).data('image');
    // var user_id = $(this).data('id');
    // var slug    = $(this).data('slug');
    var url     = $(this).data('action');
    window.location.replace(url);
    
    // var db = firebase.firestore();

    // var docRef = db.collection("users").doc("userId_"+user_id);
    // docRef.get().then(function(doc) {
    //     if (doc.exists) { 

    //         window.location.replace(url);

    //      } else {

    //         firebase.auth().createUserWithEmailAndPassword(email, "daweer" + user_id).then((userCredential) => {

    //                 // Signed in
    //                 var user = userCredential.user;

    //                 // Add a new document in collection "users"
    //                 db.collection("users").doc('userId_' + user_id).set({

    //                         'name': name,
    //                         'email': email,
    //                         'photoUrl': image,
    //                         'id': "userId_" + user_id,
    //                         'createdAt': firebase.firestore.FieldValue.serverTimestamp(),
    //                         'chattingWith': null,
    //                         'typingTo': null,
    //                         'unReadingCount': 0,
    //                         'status': 'online',
    //                         'slug': slug,
    //                         'lastSeen': firebase.firestore.FieldValue.serverTimestamp(),
    //                     })
    //                     .then(() => {

    //                         window.location.replace(url);

    //                     })
    //                     .catch((error) => {
    //                         console.error("Error writing document: ", error);
    //                     });

    //                 })
    //                 .catch((error) => {

    //                     var errorCode = error.code;
    //                     var errorMessage = error.message;
    //                     // ..
    //                 });


    //      }  

    // }).catch(function(error) {
    //     console.log("Error getting document:", error);
    // });        

});
