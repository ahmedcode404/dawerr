$(document).on('click', '.remove-myproduct', function (e) {

    var $this = $(this);

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })
  
    swalWithBootstrapButtons.fire({
      title: 'هل تريد حذف الاعلان ؟',
      type: 'warning',
      showCancelButton: true,
      heightAuto:true,
      confirmButtonText: 'نعم',
      cancelButtonText: 'لا',
    }).then((result) => {
      if (result.value) {

        var id = $(this).data('id');
        var action = $(this).data('action');        

        $.ajax({
            type: 'GET',
            url: action,
            data: {
                'id' : id,
            },
            dataType: 'json',
            success: function(result) {

              // remove item                 
              $this.closest(".product-delete").remove();
              
              swalWithBootstrapButtons.fire({
                type: 'success',
                title: 'تم حذف الاعلان بنجاح',
                showConfirmButton: false,
                timer: 1500
              });

              if($(".product-delete").length == 0)
              {

                $('.appent-notfound').html('<p style="margin: auto">لا توجد اعلانات </p>');

              } // end of if length
                
            } // end of success
    
        }); // end of ajax         


  
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
       
      }
    })
  
  });