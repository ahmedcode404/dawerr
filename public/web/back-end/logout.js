$('#logout-user').click(function(e) {
    e.preventDefault();
    var redirect_logout = $(this).data('redirectlogout');

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
        title: "هل تريد تسجيل الخروج ؟",
        type: 'question',
        showCancelButton: true,
        heightAuto:true,
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا',
    }).then((result) => {
        if (result.value) {

            window.location.href = redirect_logout;

        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
        ) {
        
        }
    })


}); // end of login user

