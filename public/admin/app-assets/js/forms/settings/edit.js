
$(function () {


    // create phone key 
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
        preferredCountries: ["sa", "eg"],
        separateDialCode: true,
        formatOnDisplay: false,
        utilsScript: "{{url('utils.js')}}",
    });

    $(".needs-validation").on("submit", function() {

        var country_code = $(this).find('.iti__selected-dial-code').text();

        $('input[name="phone_key"]').val(country_code);

    });
    // end create phone key  


    $.validator.addMethod("letters", function (value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/) || value == value.match(/^[\u0621-\u064A ]+$/);
    });    

    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        ignore: [],
        rules: {
            // logo : {
            //     required : true
            // },
           
            about:{
                required: function() 
               {
                CKEDITOR.instances.about.updateElement();
               },

                minlength:10,
           },
            instagram : {
                required : true,
                url: true                
            },  
            facebook : {
                required : true,
                url: true                
            },
            snapchat : {
                required : true,
                url: true                
            },
            twitter : {
                required : true,
                url: true
            },
            phone : {
                required : true,
                minlength : 9,
                maxlength : 12,
            },
            tax : {
                required : true,
                min :1,             
            },
            email : {
                required : true,
                email : true,             
            },
            commission_account_text : {
                required : true,
            }            

        },

        messages: {
            about : {
                required : 'برجاء ادخال محتوي من نحن',
                minlength : 'يجب ان يكون عدد الحروف اكبر من 10 حروق'
            },
            logo : {
                required : 'برجاء ادخال الشعار'
            } ,
            facebook : {
                required : 'برجاء ادخال رابط الفيس بوك',
                url : 'صبغه الرابط غير صحيحه مثال http://domain.com'
            }, 
            instagram : {
                required : 'برجاء ادخال رابط الانستغرام',
                url : 'صبغه الرابط غير صحيحه مثال http://domain.com'
            },
            snapchat : {
                required : 'برجاء ادخال رابط سناب شات',
                url : 'صبغه الرابط غير صحيحه مثال http://domain.com'
            }, 
            twitter : {
                required : 'برجاء ادخال رابط تويتر',
                url : 'صبغه الرابط غير صحيحه مثال http://domain.com'
            },
            phone : {
                required : 'برجاء ادخال رقم  الجوال',
                minlength : 'يجب ان يكون عدد الارقام لا يقل عن 9 رقم',
                maxlength : 'يجب ان يكون عدد الارقام لا يزيد عن 12 رقم'
            },
            tax : {
                required : 'برجاء ادخال نسبه حساب العمله'
            }, 
            email : {
                required : 'برجاء ادخال البريد الالكتروني',
                email : 'برجاء ادخال بريد الكتروني صحيح مثال name@gmail.com'
            },
            commission_account_text : {
                required : 'برجاء ادخال محتوي صفحه حساب العموله',
            },                                                                                                            
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});


$('.attribute').change(function() {

    var id  = $(this).val();
    var url = $(this).data('url');

    var sub = $('#subcategory').html('');

    $.ajax({
        type: 'get',
        url: url,
        data: {'id' : id},
        dataType: 'html',
        success: function(result) {

            sub.html(result);

        } // end of success

    }); // end of ajax     

});