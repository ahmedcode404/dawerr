
$(function () {
    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        rules: {
            price_from : {
                required : true,
                digits:true
            },
            price_to : {
                required : true,
                digits:true
            },            
        },

        messages: {
            price_from : {
                required : 'برجاء ادخال سعر البداية',
                digits : 'يجب ان يكون سعر البداية ارقام'
            },
            price_to : {
                required : 'برجاء ادخال سعر النهايه',
                digits : 'يجب ان يكون سعر النهايه ارقام'

            }
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});
