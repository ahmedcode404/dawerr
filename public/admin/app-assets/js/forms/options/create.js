
$(function () {
    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        lang: 'ar',
        rules: {
            name : {
                required : true
            },
            attribute_id : {
                required : true
            },            
        },

        messages: {
            attribute_id : {
                required : 'برجاء اختيار اسم الخصائص'
            },
            name : {
                required : 'برجاء ادخال اسم الخيار'
            }
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});


$('.attribute').change(function() {

    var id  = $(this).val();
    var url = $(this).data('url');

    var sub = $('#subcategory').html('');

    $.ajax({
        type: 'get',
        url: url,
        data: {'id' : id},
        dataType: 'html',
        success: function(result) {

            sub.html(result);

        } // end of success

    }); // end of ajax     

});