
$(function () {
    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        lang: 'ar',
        rules: {
            name : {
                required : true
            },
            category_id : {
                required : true
            },            
        },

        messages: {
            category_id : {
                required : 'برجاء اختيار اسم القسم'
            },
            name : {
                required : 'برجاء ادخال اسم الخصائص'
            }
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});


$('.category').change(function() {

    var id  = $(this).val();
    var url = $(this).data('url');

    var sub = $('#subcategory').html('');

    $.ajax({
        type: 'get',
        url: url,
        data: {'id' : id},
        dataType: 'html',
        success: function(result) {

            sub.html(result);

        } // end of success

    }); // end of ajax     

});