
$(function () {

    $.validator.addMethod("letters", function (value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/) || value == value.match(/^[\u0621-\u064A ]+$/);
    });     

    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        lang: 'ar',
        rules: {
            content : {
                required : true,
            },                                     
        },

        messages: {

            content : {
                required : 'برجاء ادخال المحتوي',
            },   
                      
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});
