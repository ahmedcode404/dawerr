
$(function () {

    $.validator.addMethod("letters", function (value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/) || value == value.match(/^[\u0621-\u064A ]+$/);
    });     

    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        lang: 'ar',
        rules: {
            category_id : {
                required : true,
            },             
            title : {
                required : true,
                letters: true
            }, 
            content : {
                required : true,
            },                                    
        },

        messages: {
            category_id : {
                required : 'برجاء اختيار القسم',
            },            
            title : {
                required : 'برجاء ادخال العنوان',
                letters : 'يجب ان يكون حقل المدينه مكون من حروف'
            },
            content : {
                required : 'برجاء ادخال المحتوي',
            },                         
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});
