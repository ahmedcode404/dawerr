
$(function () {


    $.validator.addMethod("letters", function (value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/) || value == value.match(/^[\u0621-\u064A ]+$/);
    });    

    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        rules: {
            name : {
                required : true,
                letters: true                
            },
            number_account : {
                required : true,
                digits:true               
            },  
            number_eban : {
                required : true,
                digits:true               
            },                                  
        },

        messages: {
            name : {
                required : 'برجاء ادخال اسم البنك',
                letters : 'يجب ان يكون اسم البنك مكون من حروف',
            },
            number_account : {
                required : 'برجاء ادخال رقم الحساب',
                digits : 'يجب ان يكون اسم البنك مكون ارقام',
            }, 
            number_eban : {
                required : 'برجاء ادخال رقم الايبان',
                digits : 'يجب ان يكون اسم البنك مكون ارقام',
            },                        
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});
