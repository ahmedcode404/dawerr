
$(function () {

    $.validator.addMethod("letters", function (value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/) || value == value.match(/^[\u0621-\u064A ]+$/);
    });     

    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        lang: 'ar',
        rules: {
            name : {
                required : true,
                letters: true
            },           
        },

        messages: {
            name : {
                required : 'برجاء ادخال اسم الممدينة',
                letters : 'يجب ان يكون حقل المدينه مكون من حروف'
            },
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});
