
$(function () {


    $.validator.addMethod("letters", function (value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/) || value == value.match(/^[\u0621-\u064A ]+$/);
    });    

    'use strict';
    var $form = $(".needs-validation");
    $form.validate({
        rules: {
            name : {
                required : true,
                letters: true                
            }          
        },

        messages: {
            name : {
                required : 'برجاء ادخال اسم البنك',
                letters : 'يجب ان يكون اسم البنك مكون من حروف',
            },
        },
        submitHandler: function () {
            $form[0].submit();
        }
    });

});
