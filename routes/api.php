<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:jwt.verify')->get('/user', function (Request $request) {
    return $request->user();
});


////////////////////////////// BEGIN: route api ////////////////////////////////

// no authentication
Route::namespace('Api')->group(function ()
{

    // ================================ BRGIN: route auth =====================================
    
    // route get categories
    Route::get('categories' , 'CategoryController@category');
    // route get types
    Route::get('types' , 'CategoryController@typeAuth');
    // route get cities
    Route::get('cities' , 'CityController@city');
    // route register
    Route::post('register' , 'AuthenticationController@register');
    // route verify
    Route::post('verify' , 'AuthenticationController@verifyAccount');
    // route login
    Route::post('login' , 'AuthenticationController@login');
    // route reset password
    Route::post('resetpassword' , 'AuthenticationController@resetPassword');
    // route send code
    Route::post('sendcode' , 'AuthenticationController@sendCode');
    // route get all product
    Route::get('products/{category}' , 'ProductController@getAllProduct');
    // route get product
    Route::get('product/{id}/{category}' , 'ProductController@getProduct'); 
    
    // route get attribute
    Route::get('attribute/{id}' , 'AttributeController@getOption');

    // route get companies
    Route::get('company' , 'SearchController@company'); 
    
    // ================================ END: route auth =====================================


    // ================================ BRGIN: route opposed =====================================

    Route::get('opposeds/{id}' , 'OpposedController@getAllOpposed');
    Route::get('opposeds/{id}/{category}' , 'OpposedController@getOpposed');
    Route::get('opposedsproduct/{id}/{category}' , 'OpposedController@getOpposedProduct');
    Route::get('fillter/{id}/{category}' , 'OpposedController@fillter');

    // ================================ END: route opposed =======================================
    
    
    // ================================ BRGIN: route static page ======================================= 

    // route commission
    Route::get('statics/{key}' , 'StaticPageController@statics');
    // route agreement
    Route::get('agreement'  , 'StaticPageController@agreement');

    // ================================ END: route static page =====================================   
    
    // ================================ BRGIN: route payment =======================================   
    
    // route banks
    Route::get('banks'  , 'PaymentController@bank');    
    // route account banks
    Route::get('accountbanks'  , 'PaymentController@accountBank');    
    // route payment
    Route::post('payment'  , 'PaymentController@store');       

    // ================================ END: route payment =========================================   
    
    // ================================ BRGIN: route contact us ====================================   
    
    // route storecontact
    Route::post('storecontact'  , 'ContactController@store'); 
    // route data contact
    Route::get('contact'  , 'ContactController@contact'); 
    
    // ================================ END: route contact us =======================================  
    
    // ================================ BRGIN: route search simple ==================================
    
    // route search simple
    Route::post('searchsimple'  , 'SearchController@searchSimple'); 
    // route search advanced
    Route::post('searchadvanced'  , 'SearchController@searchAdvanced'); 
    // route search map
    Route::post('searchmap'  , 'SearchController@searchMap'); 
    // route fillter map
    Route::post('filltermap'  , 'SearchController@fillterMap'); 
    // route get price
    Route::get('price'  , 'SearchController@price'); 
    
    // ================================ END: route search simple ====================================  
    
    
    // ================================ BRGIN: route search simple ==================================

    Route::get('sliders/{category}'  , 'SliderController@slider'); 

    // ================================ END: route search simple ====================================  

    // route get commission
    Route::get('commission' , 'ProductController@commission');
    // route get subcategory
    Route::get('subcategory/{category}' , 'CategoryController@subCategory');

    // route get additions
    Route::get('additions' , 'CategoryController@additions');
    // route get additionsother
    Route::get('additionsother' , 'CategoryController@additionsother');
    // route get attributes
    Route::get('attributes/{category}' , 'AttributeController@attribute');
    // route direction
    Route::get('directione' , 'DirectioneController@directione');
    // route dir
    Route::get('dir' , 'DirectioneController@dir');

});

// authentication
Route::middleware('jwt.verify')->namespace('Api')->group(function ()
{

    // ========================== BEGIN: route user ========================================

    // route active notification
    Route::post('activenotification' , 'AuthenticationController@activeNotification');
    // route update user
    Route::post('update' , 'AuthenticationController@update');   
    // route new password
    Route::post('newpassword' , 'AuthenticationController@newPassword');      
    // route change password
    Route::post('changepassword' , 'AuthenticationController@changePassword');  
    // route change image
    Route::post('changeimage' , 'AuthenticationController@changeImage');  
    // route logout
    Route::get('logout' , 'AuthenticationController@logout');
    // route auth user
    Route::get('authuser' , 'AuthenticationController@getUser');  

    // ============================ END: route user ===========================================

    // ============================ BEGIN: route product ======================================
    


    // ============================= END: route product =========================================
    
    // ============================= BEGGIN: route my product ===================================

    // route my product
    Route::get('myproduct/{id}' , 'ProductController@myProduct');
    // route store
    Route::post('store' , 'ProductController@store');
    // route update product
    Route::post('updateproduct/{id}' , 'ProductController@update');
    // route delete product
    Route::get('deleteproduct/{id}/{category}' , 'ProductController@destroy');
    // route done sale product
    Route::get('donesale/{id}' , 'ProductController@doneSale');
    // route report
    Route::post('report' , 'ProductController@report');
    // route delete image
    Route::get('deleteimage/{id}' , 'ProductController@deleteImage');

    // ============================= END: route my product ======================================

    // ============================= BEGIN: route my favotrite ==================================

    // route favorite
    Route::get('favorite/{product}' , 'FavoriteController@store');
    // route my favorite
    Route::get('myfavorite/{id}' , 'FavoriteController@myFavorite');

    // ============================== END: route my favotrite ===================================

});

////////////////////////////// END: route api //////////////////////////////////