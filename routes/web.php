<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// route group auth
Route::prefix('dashboard')->namespace('Dashboard')->group(function(){

    // route login 
    Route::get('login', 'DashboardController@login')->name('login');
    Route::post('login/auth', 'DashboardController@loginAuth')->name('login.auth');
    Route::get('reset/password', 'DashboardController@resetPassword')->name('reset.password');
    Route::post('send/link', 'DashboardController@sendLink')->name('send.link');
    Route::get('link/reset/password/{code}', 'DashboardController@newPassword')->name('link.reset.password');
    Route::post('update/password', 'DashboardController@updatePassword')->name('update.password');

});

//##################################  BEGIN: route group dashboard ######################################
Route::prefix('dashboard')->middleware('admin')->name('dashboard.')->namespace('Dashboard')->group(function(){

    // route dashboard
    Route::get('/', 'DashboardController@index')->name('dashboard');
    // route sliders
    Route::resource('sliders', 'SliderController');      
    // route cities
    Route::resource('cities', 'CityController');    
    // route orders
    Route::resource('orders', 'OrderController');    
    // route sub categories
    Route::resource('categories', 'CategoryController');
    // route products
    Route::resource('products', 'ProductController');
    // route atrributes
    Route::resource('attributes', 'AttributeController');
    Route::get('append/subcategory', 'OptionController@appendSubCategory')->name('append.subcategory');
    // route options
    Route::resource('options', 'OptionController');
    // route price-fillters
    Route::resource('price-fillters', 'PriceFillterController');  
    // route banks
    Route::resource('banks', 'BankController');  
    // route agreements
    Route::resource('agreements', 'AgreementController');       
    // route bank-accounts
    Route::resource('bank-accounts', 'BankAccountController');            
    // route static-pages
    Route::resource('static-pages', 'StaticPageController')->except(['create' , 'show']); 
    // route contacts
    Route::resource('contacts', 'ContactController')->except(['create' , 'show']); 
    Route::get('contact/reply', 'ContactController@reply')->name('contact.reply'); 
    // route settings
    Route::resource('settings', 'SettingController')->except(['index' , 'show']);                
    // route logout
    Route::get('logout', 'DashboardController@logout')->name('logout');

}); // end of route prefix dashboard
//##################################  END: route group dashboard ########################################


///////////////////////////////////////  BEGIN: route group web ////////////////////////////////////////

Route::name('web.')->namespace('Web')->group(function(){

    // route get categories
    Route::get('/' , 'CategoryController@category')->name('category');
    // route home
    Route::get('home/{slug}' , 'HomeController@home')->name('home');
    Route::get('pagination' , 'HomeController@pagination')->name('pagination');

    
    ################################################## BEGIN: authentication #########################################
    // route register
    Route::get('register' , 'AuthenticationController@register')->name('register');
    // route register store
    Route::post('register/store' , 'AuthenticationController@registerStore')->name('register.store');
    // route login
    Route::get('login' , 'AuthenticationController@login')->name('login');
    // route login auth
    Route::post('login/auth' , 'AuthenticationController@loginAuth')->name('login.auth');
    
    ################################################## END: authentication #########################################
    

    ################################################## END: statics #########################################

    // route agreement
    Route::get('static/{key}' , 'StaticPageController@static')->name('static');
    // route about
    Route::get('about' , 'StaticPageController@about')->name('about');
    // route commission
    Route::get('commission' , 'StaticPageController@commission')->name('commission');
    // route contact
    Route::get('contact' , 'StaticPageController@contact')->name('contact');
    // route store contact
    Route::post('store/contact' , 'StaticPageController@store')->name('store.contact');
    // route payment 
    Route::get('payment/{slug}' , 'PaymentController@payment')->name('payment');
    // route web payment now 
    Route::post('payment/now' , 'PaymentController@paymentNow')->name('payment.now');

    ################################################## BEGIN: statics #########################################


    ################################################## BEGIN: active account #########################################
    // route verfiy
    Route::get('verfiy/{slug}' , 'AuthenticationController@verfiy')->name('verfiy');
    // route resend code
    Route::get('resend/code/{slug}' , 'AuthenticationController@resendCode')->name('resend.code');
    // route active phone
    Route::post('active/phone' , 'AuthenticationController@activePhone')->name('active.phone');

    ################################################## END: active account #########################################

    ################################################## BEGIN: reset password #########################################
    // route forget password
    Route::get('forget/password' , 'AuthenticationController@forgetPassword')->name('forget.password');
    // route send code reset password
    Route::post('send/code/reset/password' , 'AuthenticationController@sendCodeResetPassword')->name('send.code.reset.password');
    // route verfiy code reset password
    Route::get('verfiy/code/reset/password/{slug}' , 'AuthenticationController@verfiySendCodeResetPassword')->name('verfiy.code.reset.password');
    // route reset password
    Route::post('active/code/reset/password' , 'AuthenticationController@activeSendCodeResetPassword')->name('active.code.reset.password');
    // route reset password
    Route::get('reset/password/{slug}' , 'AuthenticationController@resetPassword')->name('reset.password');  
    // route reset password
    Route::post('new/password' , 'AuthenticationController@newPassword')->name('new.password');
    
    ################################################## END: reset password #########################################
    

    ################################################## BEGIN: products #########################################
    // route single product
    Route::get('product/{slug}' , 'ProductController@getProduct')->name('product');  
    
    ################################################## END: products ###########################################
    

    ################################################## BEGIN: opposed #########################################
    
    // route opposeds
    Route::get('opposeds' , 'OpposedController@getAllOpposed')->name('opposeds');  
    // route opposed
    Route::get('opposed/{slug}' , 'OpposedController@getOpposed')->name('opposed');  
    
    
    ################################################## END: opposed ###########################################
    
    
    ################################################## BEGIN: opposed #########################################
    
    // route search
    Route::get('public/search' , 'SearchController@searchPublic')->name('public.search');  

    // route search
    Route::post('search' , 'SearchController@searchSimple')->name('search');  
    
    // route search
    Route::get('search/advanced' , 'SearchController@searchAdvanced')->name('search.advanced');  
    
    // route map
    Route::get('search/map' , 'SearchController@searchMap')->name('search.map');  
     
    // route search
    Route::post('search/advanced/store' , 'SearchController@searchAdvancedStore')->name('search.advanced.store'); 

    // route get attribute
    Route::get('get/attribute/search' , 'SearchController@getAttributeSearch')->name('get.attribute.search');

    ################################################## END: opposed ###########################################

    // route get option other addition
    Route::get('get/other/addition' , 'SearchController@getOtherAddition')->name('get.other.addition');


    Route::middleware('website')->group(function(){
        
        // route logout
        Route::get('logout' , 'AuthenticationController@logout')->name('logout');
        // route profile
        Route::get('profile' , 'ProfileController@profile')->name('profile');
        // route edit profile
        Route::get('edit/profile' , 'ProfileController@edit')->name('edit.profile');
        // route update profile
        Route::put('update/profile' , 'ProfileController@update')->name('update.profile');
        // route upload image
        Route::post('upload/image' , 'ProfileController@uploadImage')->name('upload.image');
        // route add to favorite
        Route::get('add/to/favorite' , 'HomeController@addToFavorite')->name('add.to.favorite');
        // route get favorite
        Route::get('profile/favorite' , 'ProfileController@getFavorite')->name('profile.favorite');
        // route my product
        Route::get('profile/product' , 'ProfileController@myProduct')->name('profile.product');
        // route done sale
        Route::get('done/sale' , 'ProfileController@doneSale')->name('done.sale');
        
        ################################################## BEGIN: add new product ###########################################
        
        // route add product
        Route::get('add/product' , 'ProductController@addProduct')->name('add.product');
        // route edit product
        Route::get('edit/product/{slug}' , 'ProductController@editProduct')->name('edit.product');
        // route add product
        Route::put('update/product' , 'ProductController@updateProduct')->name('update.product');
        // route delete product
        Route::get('delete/product' , 'ProductController@deleteProduct')->name('delete.product');
        // route delete image
        Route::get('delete/image' , 'ProductController@deleteImage')->name('delete.image');
        // route get addition
        Route::get('get/addition' , 'ProductController@getAddition')->name('get.addition');
        // route get attribute
        Route::get('get/attribute' , 'ProductController@getAttribute')->name('get.attribute');
        // route get direction
        Route::get('get/direction' , 'ProductController@getDirection')->name('get.direction');
        // route get option other addition
        Route::get('get/option/other/addition' , 'ProductController@getOptionOtherAddition')->name('get.option.other.addition');
        // route store product
        Route::post('store/product' , 'ProductController@storeProduct')->name('store.product');
        // route report product
        Route::post('report/product' , 'ProductController@reportProduct')->name('report.product');
        
        ################################################## END: add new product ###########################################
        
        ################################################## END: add new product ###########################################
        
        // route chat 
        Route::get('chat/{slug}' , 'ChatController@chat')->name('chat');
        // route chat 
        Route::post('uploade/file/chat' , 'ChatController@uploadFile')->name('uploade.file.chat');

        ################################################## END: add new product ###########################################

    });

});

////////////////////////////////////////  END: route group web ///////////////////////////////////
