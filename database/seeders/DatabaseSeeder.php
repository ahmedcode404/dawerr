<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CategoryTableSeeder::class);
        $this->call(AttributeTableSeeder::class);
        $this->call(TypeAuthTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(StaticPageTableSeeder::class);
        $this->call(SettingTableSeeder::class);
        // $this->call(ProductTableSeeder::class);
        $this->call(DirectioneTableSeeder::class);
        $this->call(AgreementTableSeeder::class);
        $this->call(BankTableSeeder::class);
        $this->call(BankAccountTableSeeder::class);
        $this->call(SliderTableSeeder::class);
        $this->call(PriceFillterTableSeeder::class);
        
    }
}
