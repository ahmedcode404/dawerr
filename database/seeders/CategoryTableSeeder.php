<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create category
        Category::create([

            'name' => 'السيارات',
            'image' => url('storage/category/cars.png'),
            'type' => 'main'

         
        ]); // end of category
        // create category
        Category::create([

            'name' => 'العقارات',
            'image' => url('storage/category/realestate.png'),
            'type' => 'main'
            
        ]); // end of category

        // انواع العربيات
        Category::insert([

            [
                'name' => 'تويوتا',
                'parent_id' => 1
            ],
            [
                'name' => 'ميتسوبيشي',
                'parent_id' => 1
            ],
            [
                'name' => 'مرسيدس',
                'parent_id' => 1
            ],  

        ]); // انواع العربيات 

        // انواع العقارات
        Category::insert([

            [
                'name' => 'عمارة',
                'parent_id' => 2
            ],
            [
                'name' => 'شقه',
                'parent_id' => 2
            ],
            [
                'name' => 'مزرعة',
                'parent_id' => 2
            ],  

            [
                'name' => 'محطة بنزين',
                'parent_id' => 2
            ],
            [
                'name' => 'أراضى',
                'parent_id' => 2
            ],
            [
                'name' => 'مستودع',
                'parent_id' => 2
            ],  
            [
                'name' => 'استراحة',
                'parent_id' => 2
            ],
            [
                'name' => 'منتجع',
                'parent_id' => 2
            ],
            [
                'name' => 'صالة',
                'parent_id' => 2
            ],  
            
            [
                'name' => 'محل',
                'parent_id' => 2
            ],  
            [
                'name' => 'فيلا',
                'parent_id' => 2
            ],
            [
                'name' => 'بيت شعبي',
                'parent_id' => 2
            ],
            [
                'name' => 'فيلا دور + شقة وملحق',
                'parent_id' => 2
            ],                            
            [
                'name' => 'فيلا دور + شقة',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دور + شقتين',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دور + شقتين ملحق',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دور ونص',
                'parent_id' => 2
            ],
            [
                'name' => 'فيلا دور ونص وملحق',
                'parent_id' => 2
            ],                          
            [
                'name' => 'فيلا دور ونص + شقة',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دور ونص + شقتين',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دور ونص + شقه ومحلق',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دورين',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دورين + شقة',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دورين + شقتين',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دورين + شقة ومحلق',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دور ونص + شقتين وملحق',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا دورين ومحلق',
                'parent_id' => 2
            ],              
            [
                'name' => 'فيلا ثلاث ادوار',
                'parent_id' => 2
            ],              
              
             
        ]); // انواع العقارات       

        // اضافات العربيات
        Category::insert([
            [
                'name' => 'ستاندرد',
                'image' => null,
                'type' => 'adds'
            ],
            [
                'name' => 'نص فل',
                'image' => null,
                'type' => 'adds'
            ],            
            [
                'name' => 'فل',
                'image' => null,
                'type' => 'adds'
            ],            
        ]); // اضافات العربيات      

        // اضافات اخري العربيات
        Category::insert([

            [
                'name' => 'انظمة الدفع الرباعي',
                'image' => null,
                'type' => 'other_add'
            ],
          
        ]); // اضافات اخري العربيات      

    } // end of run
}
