<?php

namespace Database\Seeders;

use App\Models\Slider;
use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::insert([
            // cars
            [

                'title'       => 'اضافة اعلان',
                'content'     => 'هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى).ويُستخدم في صناعات المطابع ودور النشر',
                'image'       => 'slider/slider_car1.png',
                'category_id' => 1,

            ],
            [

                'title'       => 'اضافة اعلان',
                'content'     => 'هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى).ويُستخدم في صناعات المطابع ودور النشر',
                'image'       => 'slider/slider_car2.png',
                'category_id' => 1,

            ],
            [

                'title'       => 'اضافة اعلان',
                'content'     => 'هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى).ويُستخدم في صناعات المطابع ودور النشر',
                'image'       => 'slider/slider_car3.png',
                'category_id' => 1,

            ],
            // realestate
            [

                'title'       => 'اضافة اعلان',
                'content'     => 'هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى).ويُستخدم في صناعات المطابع ودور النشر',
                'image'       => 'slider/slider_reale1.png',
                'category_id' => 2,

            ],
            [

                'title'       => 'اضافة اعلان',
                'content'     => 'هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى).ويُستخدم في صناعات المطابع ودور النشر',
                'image'       => 'slider/slider_reale2.png',
                'category_id' => 2,

            ],
            [

                'title'       => 'اضافة اعلان',
                'content'     => 'هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى).ويُستخدم في صناعات المطابع ودور النشر',
                'image'       => 'slider/slider_reale3.png',
                'category_id' => 2,

            ],

        ]);
    }
}
