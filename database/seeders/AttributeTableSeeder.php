<?php

namespace Database\Seeders;

use App\Models\Attribute;
use Illuminate\Database\Seeder;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // add attributes
        Attribute::insert([

            [
                'name'        => 'حالة الإعلان',
                'cat'         => 'not',
                'type'        => 'radio',
                'category_id' => 1
            ],
            [
                'name'        => 'ناقل الحركة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 1               
            ],
            [
                'name'        => 'نوع الوقود',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 1               
            ],
            [
                'name'        => 'نظام الدفع',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 1                
            ],                                    
            [
                'name' => 'وارد',
                'cat'         => null,
                'type' => 'radio',
                'category_id' => 1               
            ],                                    
                                    
        ]); // end of insert attrbiute

        // ================================================ العقارات =========================================================
        // خصائص العمارة  
        Attribute::insert([

            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 6
            ],
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 6               
            ],
            [
                'name'        => 'عدد الشقق',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 6               
            ],
            [
                'name'        => 'المصعد',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 6                
            ],                                    
            [
                'name'        => 'الجهات الاصلية والفرعية',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 6                
            ],                                    
                                  
                                    
        ]); // خصائص العمارة 

        // خصائص الشقق 
        Attribute::insert([

            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 7
            ],                                                                   
            [
                'name'        => 'المدخل',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 7
            ],                                                                   
                                    
        ]); // خصائص الشقق        

        // خصائص المزرعة 
        Attribute::insert([

            [
                'name'        => 'حالة المبني',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'المدخل',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'سكن خاص',
                'cat'         => 'اضافات اخري',
                'type'        => 'radio',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'مجالس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'بئر ماء',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'بيوت',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'البلاستيكية',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'مستودع',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 8
            ],                                                                   
            [
                'name'        => 'حظيرة للماشة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 8
            ],                                                                   
                                    
        ]); // خصائص المزرعة 

        // خصائص محطة بنزين 
        Attribute::insert([

            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'عناصر الوقود',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'تموينات',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'مسجد ودورات',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'مياه',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'تغيير زيوت',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'غسيل سيارات',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'كهرباء سيارات',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'محل قهوة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'مطعم',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'صراف اٌلي',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'فندق للطرق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 9
            ],                                                                   
                                                                                          
        ]); // خصائص محطة بنزين  

        // خصائص الاراضي 
        Attribute::insert([

            [
                'name'        => 'حالة الاراضي',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'كهرباء',
                'cat'         => 'خدمات البنية التحتية المتوفرة في منطقة العقار',
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'مياه',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'هاتف',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'إنارة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'شوارع مسفلتة',
                'cat'         => 'المرافق والخدمات المحيطه',
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'تموينات',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'مدارس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'مطاعم',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                   
            [
                'name'        => 'أسواق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 10
            ],                                                                                                                                   
                                                                                          
        ]); // خصائص الاراضي    

        // خصائص المستودع 
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 11
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 11
            ],                                                                                                                                    
                                                                                          
        ]); // خصائص المستودع  

        // خصائص الاستراحة 
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 12
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 12
            ],                                                                                                                                    
            [
                'name'        => 'جلسات خارجية',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 12
            ],                                                                                                                                    
            [
                'name'        => 'مسطحات خضراء',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 12
            ],                                                                                                                                    
            [
                'name'        => 'ألعاب',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 12
            ],                                                                                                                                    
                                                                                          
        ]); // خصائص الاستراحة   

        // خصائص منتجع 
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 13
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 13
            ],                                                                                                                                    
            [
                'name'        => 'جلسات خارجية',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 13
            ],                                                                                                                                    
            [
                'name'        => 'مسطحات خضراء',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 13
            ],                                                                                                                                    
            [
                'name'        => 'ألعاب',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 13
            ],                                                                                                                                    
                                                                                          
        ]); // خصائص منتجع      

        // خصائص صالة 
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 14
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 14
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); // خصائص صالة 

        // خصائص محل 
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 15
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 15
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); // خصائص محل  

        // خصائص فلة 
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 16
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 16
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); // خصائص فلة  

        // خصائص بيت شعبي 
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 17
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 17
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); // خصائص بيت شعبي    

        // خصائص فيلا دور + شقة وملحق 
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 18
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 18
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); // خصائص فيلا دور + شقة وملحق   

        // خصائص فيلا دور + شقة  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 19
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 19
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); // خصائص فيلا دور + شقة  

        // خصائص فيلا دور + شقتين  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 20
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 20
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); // خصائص فيلا دور + شقتين    
             
        //  خصائص فيلا دور + شقتين وملحق  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 21
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 21
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دور + شقتين وملحق   

        //  خصائص فيلا دور ونص  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 22
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 22
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دور ونص      

        //  خصائص فيلا دور ونص + ملحق  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 23
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 23
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دور ونص + ملحق   
              
        //  خصائص فيلا دور ونص + شقة  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 24
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 24
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دور ونص + شقة    

        //  خصائص فيلا دور ونص + شقتين  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 25
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 25
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دور ونص + شقتين    
             
        //  خصائص فيلا دور ونص + شقة وملحق  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 26
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 26
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دور ونص + شقة وملحق    

        //  خصائص فيلا دور ونص + شقتين وملحق  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 27
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 27
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دور ونص + شقتين وملحق 

        //  خصائص فيلا دورين  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 28
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 28
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دورين   

        //  خصائص فيلا دورين + شقة  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 29
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 29
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دورين + شقة     

        //  خصائص فيلا دورين + شقتين  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 30
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 30
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دورين + شقتين  

        //  خصائص فيلا دورين + شقة وملحق  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 31
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 31
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دورين + شقة وملحق   

        //  خصائص فيلا دورين وملحق  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 32
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 32
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا دورين وملحق    

        //  خصائص فيلا ثلاث ادوار  
        Attribute::insert([
                                                                 
            [
                'name'        => 'الحالة',
                'cat'         => 'not',
                'type'        => 'select',
                'category_id' => 33
            ],                                                                   
            [
                'name'        => 'عدد الشوارع',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرف الخدم',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'صالة الطعام',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'سكن للعمال',
                'cat'         => null,
                'type'        => 'select',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مشب خارجي',
                'cat'         => 'إضافات اخري',
                'type'        => 'radio',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'حديقة',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'مسبح',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'غرفة الغسيل الملابس',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            [
                'name'        => 'مصعد',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
            [
                'name'        => 'ملحق',
                'cat'         => null,
                'type'        => 'radio',
                'category_id' => 33
            ],                                                                                                                                                                                                                                                                       
                                                                                          
        ]); //  خصائص فيلا ثلاث ادوار         

        //  اختيارات العماره 
        Attribute::insert([
            // الحالة
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 6
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 6
            ],                                   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 6
            ],   
            
            // عدد الشوارع
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 7
            ],                                   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 7
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 7
            ],                                   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 7
            ],
            // عدد الشقق
            [
                'name'        => 'شقة',
                'cat'         => null,
                'parent_id' => 8
            ],            
            [
                'name'        => 'شقتين',
                'cat'         => null,
                'parent_id' => 8
            ],            
            [
                'name'        => '3 شقق',
                'cat'         => null,
                'parent_id' => 8
            ],            
            [
                'name'        => '4 شقق',
                'cat'         => null,
                'parent_id' => 8
            ],  
            // المصعد          
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 9
            ],            
            [
                'name'        => 'لا يوجد ',
                'cat'         => null,
                'parent_id' => 9
            ],            
            // وصف الحدود          
            [
                'name'        => 'شمال',
                'cat'         => null,
                'parent_id' => 10
            ],            
            [
                'name'        => 'غرب',
                'cat'         => null,
                'parent_id' => 10
            ],            
                                    
                                    
        ]); // اختيارات العماره

        //  اختيارات الشقق 
        Attribute::insert([
            // الحالة
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 11
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 11
            ],                                   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 11
            ], 
            // المدخل  
            [
                'name'        => 'خاص',
                'cat'         => null,
                'parent_id' => 12
            ],                                   
            [
                'name'        => 'مشترك',
                'cat'         => null,
                'parent_id' => 12
            ],   
                                              
        ]); // اختيارات الشقق

        //  اختيارات المزرعة 
        Attribute::insert([
            // الحالة
            [
                'name'        => 'قائمة',
                'cat'         => null,
                'parent_id' => 13
            ],                                   
            [
                'name'        => 'مهجورة',
                'cat'         => null,
                'parent_id' => 13
            ],                                   

            // المدخل  
            [
                'name'        => 'خاص',
                'cat'         => null,
                'parent_id' => 14
            ],                                   
            [
                'name'        => 'مشترك',
                'cat'         => null,
                'parent_id' => 14
            ],   
            // سكن خاص  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 15
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 15
            ],   
            //  مجالس  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 16
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 16
            ],   
            //  سكن للعمال  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 17
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 17
            ],   
            //  بئر ماء  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 18
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 18
            ],   
            //  بيوت  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 19
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 19
            ],   
            //  البلاستيكية  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 20
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 20
            ],   
            //  مستودع  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 21
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 21
            ],   
            //  حظيرة للماشة  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 22
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 22
            ],   
                                              
        ]); // اختيارات المزرعة

        //  اختيارات محطة بنزين 
        Attribute::insert([
                                  
            // عدد الشوارع  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 23
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 23
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 23
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 23
            ],   
            // عناصر الوقود  
            [
                'name'        => 'كافة انواع البنزين',
                'cat'         => null,
                'parent_id' => 24
            ],                                   
            [
                'name'        => 'كافة انواع البنزين و ديزل',
                'cat'         => null,
                'parent_id' => 24
            ],   
            [
                'name'        => 'كافة انواع البنزين و ديزل و كيروسين',
                'cat'         => null,
                'parent_id' => 24
            ],   
            // تموينات  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 25
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 25
            ],   
            //  مسجد ودورات  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 26
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 26
            ],   
            //  مياه  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 27
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 27
            ],   
            //  تغيير زيوت  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 28
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 28
            ],   
            //  غسيل سيارات  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 29
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 29
            ],   
            //  كهرباء سيارات  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 30
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 30
            ],   
            //  محل قهوة  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 31
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 31
            ],   
            //  مطعم  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 32
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 32
            ],   
            //  صراف الي  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 33
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 33
            ],   
            //  فندق للطرق  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 34
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 34
            ],   
            //  سكن للعمال  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 35
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 35
            ],   
                                              
        ]); // اختيارات محطة بنزين
        
        //  اختيارات الاراضي 
        Attribute::insert([
                                  
            // حاله الاراض  
            [
                'name'        => 'سكنية',
                'cat'         => null,
                'parent_id' => 36
            ],                                   
            [
                'name'        => 'تجارية',
                'cat'         => null,
                'parent_id' => 36
            ],   
            [
                'name'        => 'تجارية سكنية',
                'cat'         => null,
                'parent_id' => 36
            ],                                   
            [
                'name'        => 'زراعية',
                'cat'         => null,
                'parent_id' => 36
            ],   
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 37
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 37
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 37
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 38
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 38
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 38
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 38
            ], 
            // كهرباء
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 39
            ],   
            //  كهرباء سيارات  
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 39
            ],     
            //  مياة  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 40
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 40
            ],   
            //  هاتف  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 41
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 41
            ],   
            //  انارة  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 42
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 42
            ],   
            //  شوارع مسفلته  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 43
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 43
            ],   
            //  تموينات  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 44
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 44
            ],   
            //  مدارس  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 45
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 45
            ],   
            //  مطاعم  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 46
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 46
            ],   
            //  اسواق  
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 47
            ],                                   
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 47
            ],   
                                              
        ]); // اختيارات الاراضي

        //  اختيارات المستودع 
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 48
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 48
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 48
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 49
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 49
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 49
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 49
            ], 
                                              
        ]); // اختيارات المستودع

        //  اختيارات الاستراحة 
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 50
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 50
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 50
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 51
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 51
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 51
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 51
            ], 
            // جلسات خارجية
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 52
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 52
            ], 
            // مسطحات خضراء
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 53
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 53
            ], 
            // ألعاب
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 54
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 54
            ], 
                                              
        ]); // اختيارات الاستراحة

        //  اختيارات منتجع 
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 55
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 55
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 55
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 56
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 56
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 56
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 56
            ], 
            // جلسات خارجية
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 57
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 57
            ], 
            // مسطحات خضراء
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 58
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 58
            ], 
            // ألعاب
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 59
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 59
            ], 
                                              
        ]); // اختيارات منتجع

        //  اختيارات صالة 
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 60
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 60
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 60
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 61
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 61
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 61
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 61
            ], 
                                              
        ]); // اختيارات صالة

        //  اختيارات محل 
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 62
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 62
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 62
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 63
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 63
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 63
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 63
            ], 
                                              
        ]); // اختيارات محل

        //  اختيارات فلة 
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 64
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 64
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 64
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 65
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 65
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 65
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 65
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 66
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 66
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 66
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 67
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 67
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 67
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 68
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 68
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 69
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 69
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 70
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 70
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 71
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 71
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 72
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 72
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 73
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 73
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 74
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 74
            ], 

                                              
        ]); // اختيارات فلة

        //  اختيارات بيت شعبي 
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 75
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 75
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 75
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 76
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 76
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 76
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 76
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 77
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 77
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 77
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 78
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 78
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 78
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 79
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 79
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 80
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 80
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 81
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 81
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 82
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 82
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 83
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 83
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 84
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 84
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 85
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 85
            ], 

                                              
        ]); // اختيارات بيت شعبي

        //  اختيارات فيلا دور + شقة وملحق 
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 86
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 86
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 86
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 87
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 87
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 87
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 87
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 88
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 88
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 88
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 89
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 89
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 89
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 90
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 90
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 91
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 91
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 92
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 92
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 93
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 93
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 94
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 94
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 95
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 95
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 96
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 96
            ], 

                                              
        ]); // اختيارات فيلا دور + شقة وملحق

        //  اختيارات فيلا دور + شقة  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 97
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 97
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 97
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 98
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 98
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 98
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 98
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 99
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 99
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 99
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 100
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 100
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 100
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 101
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 101
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 102
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 102
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 103
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 103
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 104
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 104
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 105
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 105
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 106
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 106
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 107
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 107
            ], 

                                              
        ]); // اختيارات فيلا دور + شقة 

        //  اختيارات فيلا دور + شقتين  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 108
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 108
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 108
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 109
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 109
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 109
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 109
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 110
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 110
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 110
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 111
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 111
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 111
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 112
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 112
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 113
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 113
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 114
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 114
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 115
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 115
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 116
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 116
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 117
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 117
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 118
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 118
            ], 

                                              
        ]); // اختيارات فيلا دور + شقتين 

        //  اختيارات فيلا دور + شقتين وملحق  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 119
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 119
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 119
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 120
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 120
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 120
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 120
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 121
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 121
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 121
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 122
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 122
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 122
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 123
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 123
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 124
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 124
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 125
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 125
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 126
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 126
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 127
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 127
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 128
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 128
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 129
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 129
            ], 

                                              
        ]); // اختيارات فيلا دور + شقتين وملحق 

        //  اختيارات فيلا دور ونص  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 130
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 130
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 130
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 131
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 131
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 131
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 131
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 132
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 132
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 132
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 133
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 133
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 133
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 134
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 134
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 135
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 135
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 136
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 136
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 137
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 137
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 138
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 138
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 139
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 139
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 140
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 140
            ], 

                                              
        ]); // اختيارات فيلا دور ونص 

        //  اختيارات فيلا دور ونص  + وملحق  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 141
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 141
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 141
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 142
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 142
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 142
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 142
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 143
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 143
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 143
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 144
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 144
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 144
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 145
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 145
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 146
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 146
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 147
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 147
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 148
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 148
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 149
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 149
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 150
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 150
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 151
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 151
            ], 

                                              
        ]); // اختيارات فيلا دور ونص  + وملحق 

        //  اختيارات فيلا دور ونص  + شقة  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 152
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 152
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 152
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 153
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 153
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 153
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 153
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 154
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 154
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 154
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 155
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 155
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 155
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 156
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 156
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 157
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 157
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 158
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 158
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 159
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 159
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 160
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 160
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 161
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 161
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 162
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 162
            ], 

                                              
        ]); // اختيارات فيلا دور ونص  + شقة 

        //  اختيارات فيلا دور ونص  + شقتين  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 163
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 163
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 163
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 164
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 164
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 164
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 164
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 165
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 165
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 165
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 166
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 166
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 166
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 167
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 167
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 168
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 168
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 169
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 169
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 170
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 170
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 171
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 171
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 172
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 172
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 173
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 173
            ], 

                                              
        ]); // اختيارات فيلا دور ونص  + شقتين 

        //  اختيارات فيلا دور ونص  + شقة وملحق  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 174
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 174
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 174
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 175
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 175
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 175
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 175
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 176
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 176
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 176
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 177
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 177
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 177
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 178
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 178
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 179
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 179
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 180
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 180
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 181
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 181
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 182
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 182
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 183
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 183
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 184
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 184
            ], 

                                              
        ]); // اختيارات فيلا دور ونص  + شقة وملحق 

        //  اختيارات فيلا دور ونص  + شقتين وملحق  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 185
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 185
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 185
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 186
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 186
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 186
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 186
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 187
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 187
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 187
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 188
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 188
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 188
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 189
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 189
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 190
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 190
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 191
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 191
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 192
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 192
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 193
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 193
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 194
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 194
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 195
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 195
            ], 

                                              
        ]); // اختيارات فيلا دور ونص  + شقتين وملحق 

        //  اختيارات فيلا دورين  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 196
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 196
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 196
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 197
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 197
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 197
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 197
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 198
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 198
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 198
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 199
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 199
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 199
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 200
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 200
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 201
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 201
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 202
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 202
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 203
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 203
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 204
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 204
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 205
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 205
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 206
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 206
            ], 

                                              
        ]); // اختيارات فيلا دورين 

        //  اختيارات فيلا دورين + شقة  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 207
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 207
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 207
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 208
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 208
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 208
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 208
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 209
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 209
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 209
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 210
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 210
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 210
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 211
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 211
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 212
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 212
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 213
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 213
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 214
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 214
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 215
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 215
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 216
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 216
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 217
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 217
            ], 

                                              
        ]); // اختيارات فيلا دورين + شقة 

        //  اختيارات فيلا دورين + شقتين  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 218
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 218
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 218
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 219
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 219
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 219
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 219
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 220
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 220
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 220
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 221
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 221
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 221
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 222
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 222
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 223
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 223
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 224
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 224
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 225
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 225
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 226
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 226
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 227
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 227
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 228
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 228
            ], 
                                
        ]); // اختيارات فيلا دورين + شقتين 

        //  اختيارات فيلا دورين + شقة وملحق  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 229
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 229
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 229
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 230
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 230
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 230
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 230
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 231
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 231
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 231
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 232
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 232
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 232
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 233
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 233
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 234
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 234
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 235
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 235
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 236
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 236
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 237
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 237
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 238
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 238
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 239
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 239
            ], 
                                
        ]); // اختيارات فيلا دورين + شقة وملحق 

        //  اختيارات فيلا دورين وملحق  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 240
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 240
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 240
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 241
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 241
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 241
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 241
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 242
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 242
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 242
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 243
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 243
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 243
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 244
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 244
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 245
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 245
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 246
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 246
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 247
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 247
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 248
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 248
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 249
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 249
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 250
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 250
            ], 
                                
        ]); // اختيارات فيلا دورين وملحق 

        //  اختيارات فيلا ثلاث ادوار  
        Attribute::insert([
                                    
            //  الحالة  
            [
                'name'        => 'للبيع',
                'cat'         => null,
                'parent_id' => 251
            ],                                   
            [
                'name'        => 'للايجار',
                'cat'         => null,
                'parent_id' => 251
            ],   
            [
                'name'        => 'للاستثمار',
                'cat'         => null,
                'parent_id' => 251
            ], 
            // عدد الشوارع                                  
            [
                'name'        => 'علي شارع',
                'cat'         => null,
                'parent_id' => 252
            ],   
            [
                'name'        => 'علي شارعين',
                'cat'         => null,
                'parent_id' => 252
            ],                                   
            [
                'name'        => 'علي 3 شوارع',
                'cat'         => null,
                'parent_id' => 252
            ],   
            [
                'name'        => 'علي 4 شوارع',
                'cat'         => null,
                'parent_id' => 252
            ], 
            // غرف الخدم
            [
                'name'        => 'غرفة للسائق وغرفة للخدامة',
                'cat'         => null,
                'parent_id' => 253
            ], 
            [
                'name'        => 'غرفة للسائق',
                'cat'         => null,
                'parent_id' => 253
            ], 
            [
                'name'        => 'غرفة للخدامة',
                'cat'         => null,
                'parent_id' => 253
            ], 
            // صالة الطعام
            [
                'name'        => 'للعائلة',
                'cat'         => null,
                'parent_id' => 254
            ], 
            [
                'name'        => 'للضيوف',
                'cat'         => null,
                'parent_id' => 254
            ], 
            [
                'name'        => 'للعائلة والضيوف',
                'cat'         => null,
                'parent_id' => 254
            ], 
            // سكن للعمال
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 255
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 255
            ], 
            // مشب خارجي
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 256
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 256
            ], 
            // حديقة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 257
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 257
            ], 
            // مسبح
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 258
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 258
            ], 
            // غرفة لغسيل الملابس
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 259
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 259
            ], 
            // مصعد
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 260
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 260
            ], 
            // ملحق
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 261
            ], 
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 261
            ], 
                                
        ]); // اختيارات فيلا ثلاث ادوار 
        // ====================================================== العقارات ==========================================

        // add options
        Attribute::insert([
            // attribute id = 1
            [
                'name'        => 'مستعمل',
                'cat'         => 'not',
                'parent_id' => 1
            ],
            [
                'name'        => 'جديد',
                'cat'         => 'not',
                'parent_id' => 1               
            ],
            // attribute id = 2
            [
                'name'        => 'أوتوماتيك',
                'cat'         => null,
                'parent_id' => 2              
            ],
            [
                'name'        => 'عادي',
                'cat'         => null,
                'parent_id' => 2               
            ],    
            // attribute id = 3                                            
            [
                'name' => 'بنزين',
                'cat'         => null,
                'parent_id' => 3               
            ],                                    
            [
                'name' => 'ديزل',
                'cat'         => null,
                'parent_id' => 3               
            ],                                    
            [
                'name' => 'هايبرد',
                'cat'         => null,
                'parent_id' => 3               
            ],                                    
            [
                'name' => 'كهرباء',
                'cat'         => null,
                'parent_id' => 3               
            ],  
            // attribute id = 4                                               
            [
                'name' => '4*2',
                'cat'         => null,
                'parent_id' => 4               
            ],                                    
            [
                'name' => '4*4',
                'cat'         => null,
                'parent_id' => 4               
            ],                                    
            [
                'name' => 'دفع رباعي مستمر',
                'cat'         => null,
                'parent_id' => 4               
            ],                                    
            [
                'name' => 'دفع رباعي كلي',
                'cat'         => null,
                'parent_id' => 4               
            ], 
            // attribute id = 5                                                  
            [
                'name' => 'السعودية',
                'cat'         => null,
                'parent_id' => 5               
            ],                                    
            [
                'name' => 'الخليج',
                'cat'         => null,
                'parent_id' => 5               
            ],                                    
            [
                'name' => 'أمريكا',
                'cat'         => null,
                'parent_id' => 5               
            ],                                    
            [
                'name' => 'أخرى',
                'cat'         => null,
                'parent_id' => 5               
            ],                                    
                                    
        ]); // end of insert options    
        
        //  خصائص الاضافات ستاندرد
        Attribute::insert([

            [
                'name'        => 'الصنف',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'select',

            ],
            [
                'name'        => 'عدد الابواب',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'select',

            ],
            [
                'name'        => 'اللون الخارجي',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'select',

            ],
            [
                'name'        => 'المقاعد',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'select',

            ],
            [
                'name'        => 'التكييف',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'select',

            ],                 
            [
                'name'        => 'مقاس الجنط',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'select',

            ],
            [
                'name'        => 'فتحة سقف',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'radio',

            ],
            [
                'name'        => 'كاميرا',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'radio',

            ],
            [
                'name'        => 'شاشة',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'radio',

            ],
            [
                'name'        => 'حساسات',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'radio',

            ],
            [
                'name'        => 'مثبت سرعة',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'radio',

            ],
            [
                'name'        => 'حساس كفارات',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'radio',

            ],
            [
                'name'        => 'فرامل ABS',
                'cat'         => null,
                'category_id' => 34,
                'type'        => 'radio',

            ],
                                
                                    
        ]); //  خصائص الاضافات ستاندرد

        //  خصائص الاضافات نص فل
        Attribute::insert([

            [
                'name'        => 'الصنف',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'select',

            ],
            [
                'name'        => 'عدد الابواب',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'select',

            ],
            [
                'name'        => 'اللون الخارجي',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'select',

            ],
            [
                'name'        => 'المقاعد',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'select',

            ],
            [
                'name'        => 'التكييف',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'select',

            ],                 
            [
                'name'        => 'مقاس الجنط',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'select',

            ],
            [
                'name'        => 'فتحة سقف',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'radio',

            ],
            [
                'name'        => 'كاميرا',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'radio',

            ],
            [
                'name'        => 'شاشة',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'radio',

            ],
            [
                'name'        => 'حساسات',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'radio',

            ],
            [
                'name'        => 'مثبت سرعة',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'radio',

            ],
            [
                'name'        => 'حساس كفارات',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'radio',

            ],
            [
                'name'        => 'فرامل ABS',
                'cat'         => null,
                'category_id' => 35,
                'type'        => 'radio',

            ],
                                
                                    
        ]); //  خصائص الاضافات نص فل

        //  خصائص الاضافات فل
        Attribute::insert([

            [
                'name'        => 'الصنف',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'select',

            ],
            [
                'name'        => 'عدد الابواب',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'select',

            ],
            [
                'name'        => 'اللون الخارجي',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'select',

            ],
            [
                'name'        => 'المقاعد',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'select',

            ],
            [
                'name'        => 'التكييف',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'select',

            ],                 
            [
                'name'        => 'مقاس الجنط',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'select',

            ],
            [
                'name'        => 'فتحة سقف',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'radio',

            ],
            [
                'name'        => 'كاميرا',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'radio',

            ],
            [
                'name'        => 'شاشة',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'radio',

            ],
            [
                'name'        => 'حساسات',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'radio',

            ],
            [
                'name'        => 'مثبت سرعة',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'radio',

            ],
            [
                'name'        => 'حساس كفارات',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'radio',

            ],
            [
                'name'        => 'فرامل ABS',
                'cat'         => null,
                'category_id' => 36,
                'type'        => 'radio',

            ],
                                
                                    
        ]); //  خصائص الاضافات فل

        // خصائص الاضافات الاخري 
        Attribute::insert([

            [
                'name'        => 'اي تراك',
                'cat'         => null,
                'category_id' => 37,
                'type'        => 'select',

            ],
            [
                'name'        => 'نظام الزحف',
                'cat'         => null,
                'category_id' => 37,
                'type'        => 'select',

            ],                   
            [
                'name'        => 'الدفلوك',
                'cat'         => null,
                'category_id' => 37,
                'type'        => 'select',

            ],                   
            [
                'name'        => 'الدبل',
                'cat'         => null,
                'category_id' => 37,
                'type'        => 'select',

            ],                   
                                    
        ]); // خصائص الاضافات الاخري    

        // خيارات الاضافات ستاندرد
        Attribute::insert([
            // الصنف
            [
                'name'        => 'بيك اب',
                'cat'         => null,
                'parent_id' => 909,

            ],
            [
                'name'        => 'اس يو في',
                'cat'         => null,
                'parent_id' => 909,

            ],
            [
                'name'        => 'كروس اوفر',
                'cat'         => null,
                'parent_id' => 909,

            ],
            [
                'name'        => 'كوبية',
                'cat'         => null,
                'parent_id' => 909,

            ],
            [
                'name'        => 'هاتشباك',
                'cat'         => null,
                'parent_id' => 909,

            ],
            [
                'name'        => 'فان',
                'cat'         => null,
                'parent_id' => 909,

            ],
            // عدد الابواب
            [
                'name'        => '2',
                'cat'         => null,
                'parent_id' => 910,

            ],
            [
                'name'        => '4',
                'cat'         => null,
                'parent_id' => 910,

            ],
            //  اللون الخارجي
            [
                'name'        => 'احمر',
                'cat'         => null,
                'parent_id' => 911,

            ],
            [
                'name'        => 'ازرق',
                'cat'         => null,
                'parent_id' => 911,

            ],
            [
                'name'        => 'رصاصي',
                'cat'         => null,
                'parent_id' => 911,

            ],
            //   المقاعد
            [
                'name'        => 'مخمل',
                'cat'         => null,
                'parent_id' => 912,

            ],
            [
                'name'        => 'جلد',
                'cat'         => null,
                'parent_id' => 912,

            ],
            [
                'name'        => 'شامواه',
                'cat'         => null,
                'parent_id' => 912,

            ],
            [
                'name'        => 'جلد/مخمل',
                'cat'         => null,
                'parent_id' => 912,

            ],
            [
                'name'        => 'جلد/شامواه',
                'cat'         => null,
                'parent_id' => 912,

            ],
            // التكييف
            [
                'name'        => 'امامي',
                'cat'         => null,
                'parent_id' => 913,

            ],
            [
                'name'        => 'خلفي',
                'cat'         => null,
                'parent_id' => 913,

            ],
            // مقاس الجنط
            [
                'name'        => '16',
                'cat'         => null,
                'parent_id' => 914,

            ],
            [
                'name'        => '18',
                'cat'         => null,
                'parent_id' => 914,

            ],
            [
                'name'        => '20',
                'cat'         => null,
                'parent_id' => 914,

            ],
            //  فتحة سقف
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 915,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 915,

            ],
            [
                'name'        => 'بانوراما',
                'cat'         => null,
                'parent_id' => 915,

            ],
            // كاميرا
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 916,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 916,

            ],
            // شاشة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 917,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 917,

            ],
            // كاميرا
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 918,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 918,

            ],
            // مثبت سررعة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 919,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 919,

            ],
            // حساس كفارات
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 920,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 920,

            ],
            // فرامل ABS 
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 921,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 921,

            ],
                                              
        ]); // خيارات الاضافات ستاندرد 

        // خيارات الاضافات نص فل
        Attribute::insert([
            // الصنف
            [
                'name'        => 'بيك اب',
                'cat'         => null,
                'parent_id' => 922,

            ],
            [
                'name'        => 'اس يو في',
                'cat'         => null,
                'parent_id' => 922,

            ],
            [
                'name'        => 'كروس اوفر',
                'cat'         => null,
                'parent_id' => 922,

            ],
            [
                'name'        => 'كوبية',
                'cat'         => null,
                'parent_id' => 922,

            ],
            [
                'name'        => 'هاتشباك',
                'cat'         => null,
                'parent_id' => 922,

            ],
            [
                'name'        => 'فان',
                'cat'         => null,
                'parent_id' => 922,

            ],
            // عدد الابواب
            [
                'name'        => '2',
                'cat'         => null,
                'parent_id' => 923,

            ],
            [
                'name'        => '4',
                'cat'         => null,
                'parent_id' => 923,

            ],
            //  اللون الخارجي
            [
                'name'        => 'احمر',
                'cat'         => null,
                'parent_id' => 924,

            ],
            [
                'name'        => 'ازرق',
                'cat'         => null,
                'parent_id' => 924,

            ],
            [
                'name'        => 'رصاصي',
                'cat'         => null,
                'parent_id' => 924,

            ],
            //   المقاعد
            [
                'name'        => 'مخمل',
                'cat'         => null,
                'parent_id' => 925,

            ],
            [
                'name'        => 'جلد',
                'cat'         => null,
                'parent_id' => 925,

            ],
            [
                'name'        => 'شامواه',
                'cat'         => null,
                'parent_id' => 925,

            ],
            [
                'name'        => 'جلد/مخمل',
                'cat'         => null,
                'parent_id' => 925,

            ],
            [
                'name'        => 'جلد/شامواه',
                'cat'         => null,
                'parent_id' => 925,

            ],
            // التكييف
            [
                'name'        => 'امامي',
                'cat'         => null,
                'parent_id' => 926,

            ],
            [
                'name'        => 'خلفي',
                'cat'         => null,
                'parent_id' => 926,

            ],
            // مقاس الجنط
            [
                'name'        => '16',
                'cat'         => null,
                'parent_id' => 927,

            ],
            [
                'name'        => '18',
                'cat'         => null,
                'parent_id' => 927,

            ],
            [
                'name'        => '20',
                'cat'         => null,
                'parent_id' => 927,

            ],
            //  فتحة سقف
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 928,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 928,

            ],
            [
                'name'        => 'بانوراما',
                'cat'         => null,
                'parent_id' => 928,

            ],
            // كاميرا
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 929,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 929,

            ],
            // شاشة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 930,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 930,

            ],
            // كاميرا
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 931,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 931,

            ],
            // مثبت سررعة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 932,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 932,

            ],
            // حساس كفارات
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 933,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 933,

            ],
            // فرامل ABS 
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 934,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 934,

            ],
                                              
        ]); // خيارات الاضافات نص فل

        // خيارات الاضافات  فل
        Attribute::insert([
            // الصنف
            [
                'name'        => 'بيك اب',
                'cat'         => null,
                'parent_id' => 935,

            ],
            [
                'name'        => 'اس يو في',
                'cat'         => null,
                'parent_id' => 935,

            ],
            [
                'name'        => 'كروس اوفر',
                'cat'         => null,
                'parent_id' => 935,

            ],
            [
                'name'        => 'كوبية',
                'cat'         => null,
                'parent_id' => 935,

            ],
            [
                'name'        => 'هاتشباك',
                'cat'         => null,
                'parent_id' => 935,

            ],
            [
                'name'        => 'فان',
                'cat'         => null,
                'parent_id' => 935,

            ],
            // عدد الابواب
            [
                'name'        => '2',
                'cat'         => null,
                'parent_id' => 936,

            ],
            [
                'name'        => '4',
                'cat'         => null,
                'parent_id' => 936,

            ],
            //  اللون الخارجي
            [
                'name'        => 'احمر',
                'cat'         => null,
                'parent_id' => 937,

            ],
            [
                'name'        => 'ازرق',
                'cat'         => null,
                'parent_id' => 937,

            ],
            [
                'name'        => 'رصاصي',
                'cat'         => null,
                'parent_id' => 937,

            ],
            //   المقاعد
            [
                'name'        => 'مخمل',
                'cat'         => null,
                'parent_id' => 938,

            ],
            [
                'name'        => 'جلد',
                'cat'         => null,
                'parent_id' => 938,

            ],
            [
                'name'        => 'شامواه',
                'cat'         => null,
                'parent_id' => 938,

            ],
            [
                'name'        => 'جلد/مخمل',
                'cat'         => null,
                'parent_id' => 938,

            ],
            [
                'name'        => 'جلد/شامواه',
                'cat'         => null,
                'parent_id' => 938,

            ],
            // التكييف
            [
                'name'        => 'امامي',
                'cat'         => null,
                'parent_id' => 939,

            ],
            [
                'name'        => 'خلفي',
                'cat'         => null,
                'parent_id' => 939,

            ],
            // مقاس الجنط
            [
                'name'        => '16',
                'cat'         => null,
                'parent_id' => 940,

            ],
            [
                'name'        => '18',
                'cat'         => null,
                'parent_id' => 940,

            ],
            [
                'name'        => '20',
                'cat'         => null,
                'parent_id' => 940,

            ],
            //  فتحة سقف
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 941,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 941,

            ],
            [
                'name'        => 'بانوراما',
                'cat'         => null,
                'parent_id' => 941,

            ],
            // كاميرا
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 942,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 942,

            ],
            // شاشة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 943,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 943,

            ],
            // كاميرا
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 944,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 944,

            ],
            // مثبت سررعة
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 945,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 945,

            ],
            // حساس كفارات
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 946,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 946,

            ],
            // فرامل ABS 
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 947,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 947,

            ],
                                              
        ]); // خيارات الاضافات فل

        // خيارات الاضافات الاخري 
        Attribute::insert([
            // اي تراك
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 948,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 948,

            ],
            // نظام الزحف
            [
                'name'        => 'يوجد',
                'cat'         => null,
                'parent_id' => 949,

            ],
            [
                'name'        => 'لا يوجد',
                'cat'         => null,
                'parent_id' => 949,

            ],
            // الدفلوك
            [
                'name'        => 'لا',
                'cat'         => null,
                'parent_id' => 950,

            ],
            [
                'name'        => 'خلفي',
                'cat'         => null,
                'parent_id' => 950,

            ],
            [
                'name'        => 'امامي',
                'cat'         => null,
                'parent_id' => 950,

            ],
            [
                'name'        => 'امامي وخلفي',
                'cat'         => null,
                'parent_id' => 950,

            ],
            // الدبل
            [
                'name'        => 'لا',
                'cat'         => null,
                'parent_id' => 951,

            ],
            [
                'name'        => 'خلفي',
                'cat'         => null,
                'parent_id' => 951,

            ],
            [
                'name'        => 'امامي',
                'cat'         => null,
                'parent_id' => 951,

            ],
            [
                'name'        => 'امامي وخلفي',
                'cat'         => null,
                'parent_id' => 951,

            ],

                                              
        ]); // خيارات الاضافات الاخري        

    }
}
