<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::insert([

            [
                'key'      => 'logo',
                'neckname' => 'الشعار',
                'type'     => 'file',
                'value'    => 'logo.png',
            ],
            [
                'key'      => 'about',
                'neckname' => 'محتوي من نحن',
                'type'     => 'textarea',
                'value'    => 'محتوي من نحن',
            ],   
            [
                'key'      => 'instagram',
                'neckname' => 'رابط الانستغرام',
                'type'     => 'url',
                'value'    => 'https://www.instagram.com/',
            ],
            [
                'key'      => 'snapchat',
                'neckname' => 'رابط سناب شات',
                'type'     => 'url',
                'value'    => 'https://www.snapchat.com/',
            ],
            [
                'key'      => 'facebook',
                'neckname' => 'رابط الفيس بوك',
                'type'     => 'url',
                'value'    => 'https://www.facebook.com/',
            ], 
            [
                'key'      => 'twitter',
                'neckname' => 'رابط تويتر',
                'type'     => 'url',
                'value'    => 'https://www.twitter.com/',
            ],  
            [
                'key'      => 'phone',
                'neckname' => 'رقم الجوال',
                'type'     => 'number',
                'value'    => '+96685874558',
            ], 
            [
                'key'      => 'email',
                'neckname' => 'البريد الالكتروني',
                'type'     => 'email',
                'value'    => 'daweer@gmail.com',
            ],                                                                                   
            [
                'key'      => 'tax',
                'neckname' => 'نسبه حساب العموله',
                'type'     => 'number',
                'value'    => '2',
            ],
            [
                'key'      => 'tax2',
                'neckname' => 'نسبه حساب الضريبة',
                'type'     => 'number',
                'value'    => '15',
            ],
            [
                'key'      => 'day',
                'neckname' => 'فتره دفع العمولة',
                'type'     => 'text',
                'value'    => '10 أيام',
            ],                                                                                                                                                                       
            [
                'key'      => 'lat',
                'neckname' => 'lat',
                'type'     => 'text',
                'value'    => '96.2232',
            ],
            [
                'key'      => 'lng',
                'neckname' => 'lng',
                'type'     => 'text',
                'value'    => '47.5446225',
            ],
            [
                'key'      => 'address',
                'neckname' => 'العنوان',
                'type'     => 'text',
                'value'    => 'المملكة العربيه السعودية',
            ],             
            
                                              
        ]);
    }
}
