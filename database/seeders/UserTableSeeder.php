<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // admin
        $user = User::create([
            
            'first_name'          => 'admin',
            'second_name'         => 'admin',
            'last_name'           => 'admin',
            'account_verify'      => 1,
            'email'               => 'admin@admin.com',
            'password'            => bcrypt('admin'),
            'phone'               => '+96636363636',
            'city_id'             => 1,
            'type_id'             => 1,
            'category_id'         => 1,
            'lat'                 => '31.0465121',
            'lng'                 => '31.3801538',
            'address'             => 'المنصورة (قسم 2)، المنصورة',            
            'role'                => 'admin',

        ]);
        
        // user car
        User::create([
            
            'first_name'          => 'car',
            'account_verify'      => 1,
            'email'               => 'car@demo.com',
            'password'            => bcrypt('car'),
            'phone'               => '+96636363656',
            'city_id'             => 2,
            'type_id'             => 1,
            'category_id'         => 1,
            'lat'                 => '31.0465121',
            'lng'                 => '31.3801538',
            'address'             => 'المنصورة (قسم 2)، المنصورة',            

        ]);

        // user realestate
        User::create([
            
            'first_name'          => 'realestate',
            'account_verify'      => 1,
            'email'               => 'realestate@demo.com',
            'password'            => bcrypt('realestate'),
            'phone'               => '+96636363688',
            'city_id'             => 3,
            'type_id'             => 1,
            'lat'                 => '31.0465121',
            'lng'                 => '31.3801538',
            'address'             => 'المنصورة (قسم 2)، المنصورة',            
            'role'                => 'admin',

        ]);

        $user->category()->create([
           'category_id' => 1,
           'user_id' => 1,
        ]);

        $user->category()->create([
           'category_id' => 2,
           'user_id' => 1,
        ]);
        $user->category()->create([
           'category_id' => 3,
           'user_id' => 1,
        ]);
//
//        // user car
//        User::create([
//
//            'first_name'          => 'car',
//            'account_verify'      => 1,
//            'email'               => 'car@demo.com',
//            'password'            => bcrypt('car'),
//            'phone'               => '+96636363656',
//            'city_id'             => 2,
//            'type_id'             => 1,
//            'lat'                 => '31.0465121',
//            'lng'                 => '31.3801538',
//            'address'             => 'المنصورة (قسم 2)، المنصورة',
//
//        ]);
//
//        // user realestate
//        User::create([
//
//            'first_name'          => 'realestate',
//            'account_verify'      => 1,
//            'email'               => 'realestate@demo.com',
//            'password'            => bcrypt('realestate'),
//            'phone'               => '+96636363688',
//            'city_id'             => 3,
//            'type_id'             => 1,
//            'lat'                 => '31.0465121',
//            'lng'                 => '31.3801538',
//            'address'             => 'المنصورة (قسم 2)، المنصورة',
//
//        ]);
//
//
//        // opposeds cars
//        User::create([
//
//            'first_name'          => 'opposed car 1',
//            'account_verify'      => 1,
//            'email'               => 'opposedcar1@demo.com',
//            'password'            => bcrypt('opposedcar'),
//            'phone'               => '+96636363674',
//            'city_id'             => 4,
//            'type_id'             => 2,
//            'commercial_register' => 889556251542,
//            'tax_number'          => 453453453453,
//            'name_bank'           => 'بنك مصر',
//            'name_account_bank'   => 'احمد محمد علي',
//            'number_account_bank' => 5465445654,
//            'number_eban_bank'    => 123,
//            'lat'                 => '31.0465121',
//            'lng'                 => '31.3801538',
//            'address'             => 'المنصورة (قسم 2)، المنصورة',
//
//        ]);
//
//        User::create([
//
//            'first_name'          => 'opposed car 2',
//            'account_verify'      => 1,
//            'email'               => 'opposedcar2@demo.com',
//            'password'            => bcrypt('opposedcar'),
//            'phone'               => '+96636363897',
//            'city_id'             => 4,
//            'type_id'             => 2,
//            'commercial_register' => 889556251542,
//            'tax_number'          => 453453453453,
//            'name_bank'           => 'بنك مصر',
//            'name_account_bank'   => 'احمد محمد علي',
//            'number_account_bank' => 5465445654,
//            'number_eban_bank'    => 123,
//            'lat'                 => '31.0465121',
//            'lng'                 => '31.3801538',
//            'address'             => 'المنصورة (قسم 2)، المنصورة',
//
//        ]);
//
//        User::create([
//
//            'first_name'          => 'opposed car 3',
//            'account_verify'      => 1,
//            'email'               => 'opposedcar3@demo.com',
//            'password'            => bcrypt('opposedcar'),
//            'phone'               => '+96636368659',
//            'city_id'             => 1,
//            'type_id'             => 2,
//            'commercial_register' => 889556251542,
//            'tax_number'          => 453453453453,
//            'name_bank'           => 'بنك مصر',
//            'name_account_bank'   => 'احمد محمد علي',
//            'number_account_bank' => 5465445654,
//            'number_eban_bank'    => 123,
//            'lat'                 => '31.0465121',
//            'lng'                 => '31.3801538',
//            'address'             => 'المنصورة (قسم 2)، المنصورة',
//
//        ]);
//
//        // opposeds realestate
//        User::create([
//
//            'first_name'          => 'opposed realstate 1',
//            'account_verify'      => 1,
//            'email'               => 'opposedrealstate1@demo.com',
//            'password'            => bcrypt('opposedrealstate'),
//            'phone'               => '+96636364236',
//            'city_id'             => 2,
//            'type_id'             => 2,
//            'commercial_register' => 889556251542,
//            'tax_number'          => 453453453453,
//            'name_bank'           => 'بنك مصر',
//            'name_account_bank'   => 'احمد محمد علي',
//            'number_account_bank' => 5465445654,
//            'number_eban_bank'    => 123,
//            'lat'                 => '31.0465121',
//            'lng'                 => '31.3801538',
//            'address'             => 'المنصورة (قسم 2)، المنصورة',
//
//        ]);
//
//        User::create([
//
//            'first_name'          => 'opposed realstate 2',
//            'account_verify'      => 1,
//            'email'               => 'opposedrealstate2@demo.com',
//            'password'            => bcrypt('opposedrealstate'),
//            'phone'               => '+96636367789',
//            'city_id'             => 3,
//            'type_id'             => 2,
//            'commercial_register' => 889556251542,
//            'tax_number'          => 453453453453,
//            'name_bank'           => 'بنك مصر',
//            'name_account_bank'   => 'احمد محمد علي',
//            'number_account_bank' => 5465445654,
//            'number_eban_bank'    => 123,
//            'lat'                 => '31.0465121',
//            'lng'                 => '31.3801538',
//            'address'             => 'المنصورة (قسم 2)، المنصورة',
//
//        ]);
//
//        User::create([
//
//            'first_name'          => 'opposed realstate 3',
//            'account_verify'      => 1,
//            'email'               => 'opposedrealstate3@demo.com',
//            'password'            => bcrypt('opposedrealstate'),
//            'phone'               => '+96636363329',
//            'city_id'             => 3,
//            'type_id'             => 2,
//            'commercial_register' => 889556251542,
//            'tax_number'          => 453453453453,
//            'name_bank'           => 'بنك مصر',
//            'name_account_bank'   => 'احمد محمد علي',
//            'number_account_bank' => 5465445654,
//            'number_eban_bank'    => 123,
//            'lat'                 => '31.0465121',
//            'lng'                 => '31.3801538',
//            'address'             => 'المنصورة (قسم 2)، المنصورة',
//
//        ]);
//
    }
}
