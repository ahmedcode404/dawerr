<?php

namespace Database\Seeders;

use App\Models\Agreement;
use Illuminate\Database\Seeder;

class AgreementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Agreement::insert([

            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],
            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],
            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],
            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],
            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],
            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],
            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],
            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],
            [
               'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
               المساحة .' 
            ],

        ]);
        
    }
}
