<?php

namespace Database\Seeders;

use App\Models\TypeAuth;
use Illuminate\Database\Seeder;

class TypeAuthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeAuth::insert([
            [
                'name'  => 'حساب افراد',
                'type' => 1,                
                'image' => url('storage/type/user.png'),
            ],
            [
                'name'  => 'حساب تجاري',
                'type' => 1,                
                'image' => url('storage/type/commercial.png'),
            ],
            [
                'name' => 'admin',
                'type' => 0,
                'image' => null,
            ]            
        ]);
    }
}
