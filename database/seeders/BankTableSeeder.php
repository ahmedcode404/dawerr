<?php

namespace Database\Seeders;

use App\Models\Bank;
use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Bank::create([
            
            'name' => 'فيسا',
            'logo' => 'bank/visa.png',
            
        ]);

        Bank::create([

            'name' => 'مدي',
            'logo' => 'bank/mada.png',

        ]);

        Bank::create([

            'name' => 'ماستر كارت',
            'logo' => 'bank/master.png',

        ]);   
        
        Bank::create([

            'name' => 'تحويل بنكي',
            'logo' => 'bank/transfer.png',

        ]);        

    }
}
