<?php

namespace Database\Seeders;

use App\Models\BankAcount;
use Illuminate\Database\Seeder;

class BankAccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        BankAcount::insert([

            [
                'name' => 'مؤسسة موقع دور',
                'number_account' => 565656464646,
                'number_eban' => 12323,
                'logo' => 'bankaccount/raghy.png',
            ],
            [
                'name' => 'مؤسسة موقع دور',
                'number_account' => 565656464646,
                'number_eban' => 12323,
                'logo' => 'bankaccount/ahly.png',
            ],

        ]);

    }
}
