<?php

namespace Database\Seeders;

use App\Models\Dir;
use App\Models\Directione;
use Illuminate\Database\Seeder;

class DirectioneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Directione::insert([
            
            [
                'name' => 'جار'
            ],
            [
                'name' => 'فضاء'
            ],
            [
                'name' => 'شارع بعرض'
            ],
            [
                'name' => 'ممر مشاه'
            ],
            [
                'name' => 'مواقف'
            ],
            [
                'name' => 'حديقه'
            ],
            [
                'name' => 'مسجد'
            ],
            [
                'name' => 'مرافق عامة'
            ],

        ]);

        Dir::insert([

            [
                'name' => 'شمال'
            ],
            [
                'name' => 'شرق'
            ],
            [
                'name' => 'غرب'
            ],
            [
                'name' => 'جنوب'
            ]
        ]);
    }
}
