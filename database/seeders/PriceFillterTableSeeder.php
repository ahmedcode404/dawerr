<?php

namespace Database\Seeders;

use App\Models\PriceFillter;
use Illuminate\Database\Seeder;

class PriceFillterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // create category
        PriceFillter::insert([

            [            
                'price_from' => 3000,
                'price_to' => 6000,
            ],
            [            
                'price_from' => 2000,
                'price_to' => 4000,
            ],
            [            
                'price_from' => 1000,
                'price_to' => 1500,
            ],
            
        ]); // end of category

    }
}
