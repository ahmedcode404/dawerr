<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ================================ السيارات =============================
        // cars
        $product_car = Product::create([
            
            'code'            => '#1234567',
            'lat'             => '31.0465121',
            'lng'             => '31.3801538',
            'address'         => 'المنصورة (قسم 2)، المنصورة',
            'name'            => 'عربيه هوندا',
            'name_company'    => 'شركة هوندا',
            'status'          => 'جديد',
            'category'        => 'فئه جديده',
            'price'           => '5000',
            'category_id'     => 1,
            'city_id'         => 1,
            'subcategory_id'  => 3,
            'show_phone'      => 1,
            'user_id'         => 2,
            'email'           => 'user@demo.com',
            'the_walker'      => 500,
            'make_year'       => 2021,
            'capacity'       => 1.9,
            'image_one'       => 'product/car1.png',
            'image_two'       => 'product/car2.png',
            'image_three'     => 'product/car3.png',
            'details'         => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص
            من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص
            .الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
            .هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص',

        ]);

        $product_car->attributeandoption()->insert([
            
            [

                'product_id'   => 1,
                'attribute_id' => 2,
                'option_id'    => 895,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 3,
                'option_id'    => 897,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 4,
                'option_id'    => 901,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 5,
                'option_id'    => 905,

            ],

            [

                'product_id'   => 1,
                'attribute_id' => 922,
                'option_id'    => 988,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 923,
                'option_id'    => 994,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 924,
                'option_id'    => 997,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 925,
                'option_id'    => 999,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 926,
                'option_id'    => 1004,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 927,
                'option_id'    => 1007,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 928,
                'option_id'    => 1010,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 929,
                'option_id'    => 1013,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 930,
                'option_id'    => 1014,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 931,
                'option_id'    => 1016,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 932,
                'option_id'    => 1018,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 933,
                'option_id'    => 1020,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 934,
                'option_id'    => 1023,

            ],
            [

                'product_id'   => 1,
                'attribute_id' => 948,
                'option_id'    => 1060,

            ],

        ]); // end of insert

        $product_car->images()->insert([

            [
                'product_id' => 1,
                'path'       => 'product/c1.png',                
            ],
            [
                'product_id' => 1,
                'path'       => 'product/c2.png',                
            ],
            [
                'product_id' => 1,
                'path'       => 'product/c3.png',                
            ],

        ]); // end if create images       
        
        
        $product_car1 = Product::create([
            
            'code'            => '#1234567',
            'lat'             => '31.0465121',
            'lng'             => '31.3801538',
            'address'         => 'المنصورة (قسم 2)، المنصورة',
            'name'            => 'عربيه مرسيدس',
            'name_company'    => 'شركة مرسيدس',
            'status'          => 'جديد',
            'category'        => 'فئه جديده',
            'price'           => '5000',
            'category_id'     => 1,
            'city_id'         => 1,
            'subcategory_id'  => 3,
            'show_phone'      => 1,
            'user_id'         => 2,
            'email'           => 'user@demo.com',
            'the_walker'      => 500,
            'make_year'       => 2021,
            'image_one'       => 'product/car11.png',
            'image_two'       => 'product/car12.png',
            'image_three'     => 'product/car13.png',
            'details'         => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص
            من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص
            .الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
            .هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص',

        ]);

        $product_car1->attributeandoption()->insert([
            
            [

                'product_id'   => 2,
                'attribute_id' => 2,
                'option_id'    => 895,

            ],
            [

                'product_id'   => 2,
                'attribute_id' => 3,
                'option_id'    => 897,

            ],
            [

                'product_id'   => 2,
                'attribute_id' => 4,
                'option_id'    => 901,

            ],
            [

                'product_id'   => 2,
                'attribute_id' => 5,
                'option_id'    => 905,

            ],


        ]); // end of insert

        $product_car1->images()->insert([

            [
                'product_id' => 1,
                'path'       => 'product/c11.png',                
            ],
            [
                'product_id' => 1,
                'path'       => 'product/c12.png',                
            ],
            [
                'product_id' => 1,
                'path'       => 'product/c13.png',                
            ],

        ]); // end if create images        

        // ================================ السيارات =============================


        
        // ================================ العقارات =============================

        // realestate
         $product_realestate = Product::create([

            'code'            => '#9856948',
            'lat'             => '31.0465121',
            'lng'             => '31.3801538',
            'address'         => 'المنصورة (قسم 2)، المنصورة',
            'name'            => 'شقه للبيع',
            'status'          => 'للبيع',
            'price'           => '60000',
            'category_id'     => 2,
            'city_id'         => 2,
            'subcategory_id'  => 6,
            'show_phone'      => 1,
            'user_id'         => 3,
            'email'           => 'user@demo.com',
            'image_one'       => 'product/reale1.png',
            'image_two'       => 'product/reale2.png',
            'image_three'     => 'product/reale3.png',
            'details'         => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص
            من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص
            .الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
            .هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص',
            'number_of_shops'            => 2,
            'neighborhood'               => 'حي الجامعه',
            'street_view'                => 10,
            'border'                     => 3,
            'number_bedroom'             => 1,
            'number_of_roles'            => 3,
            'space'                      => 300,
            
        ]);

        $product_realestate->attributeandoption()->insert([
            
            [

                'product_id'   => 3,
                'attribute_id' => 7,
                'option_id'    => 266,

            ],
            [

                'product_id'   => 3,
                'attribute_id' => 8,
                'option_id'    => 270,

            ],
            [

                'product_id'   => 3,
                'attribute_id' => 9,
                'option_id'    => 273,

            ],
            [

                'product_id'   => 3,
                'attribute_id' => 10,
                'option_id'    => 275,

            ],


        ]); // end of insert

        // ================================ العقارات =============================

    }
}

