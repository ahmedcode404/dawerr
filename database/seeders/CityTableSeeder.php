<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\Cities;
use Maatwebsite\Excel\Facades\Excel;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Excel::import(new Cities, base_path('public/cities.xlsx'));

    }
}
