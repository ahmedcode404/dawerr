<?php

namespace Database\Seeders;

use App\Models\StaticPage;
use Illuminate\Database\Seeder;

class StaticPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StaticPage::insert([
            // about
            [
                'key'     => 'about',
                'title'   => 'من نحن',
                'title2'   => 'من نحن',
                'content' => 'محتوي من نحن',
                'content2' => 'محتوي من نحن',
                'image'   => 'image-one.png',
                'image2'   => 'image-one.png',
            ],
            // help center
            [
                'key'     => 'help_center',
                'title'   => 'مركز المساعده',
                'content' => 'محتوي مركز المساعده',
                'title2'   => null,
                'content2'  => null,                
                'image'   => null,
                'image2'   => null,
            ],  
            // terms_and_conditions
            [
                'key'      => 'terms_and_conditions',
                'title'    => 'الشروط والاحكام',
                'title2'   => null,
                'content2'  => null,
                'content' => 'محتوي الشروط والاحكام',
                'image'    => null,                
                'image2'    => null,                
            ],  
            // User Agreement
            [
                'key'     => 'user_agreement',
                'title'   => 'اتافية المستخدم',
                'content' => 'محتوي اتافية المستخدم',
                'title2'   => null,
                'content2'  => null,                
                'image'   => null,                
                'image2'   => null,                
            ],
            // commission account text
            [
                'key'      => 'commission_account_text',
                'title'    => 'سياسة دفع العمولة',
                'content'  => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص
                العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة
                عدد الحروف التى يولدها التطبيق',
                'title2'   => null,
                'content2' => null,                
                'image'    => null,                
                'image2'   => null,                 
            ],   
            // about application          
            [
                'key'      => 'about_application',
                'title'    => 'عن التطبيق',
                'content'  => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس
                المساحة، لقد تم توليد هذا النص من مولد النص العربى،
                حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص
                .الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
                إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد
                النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو
                مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد
                لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل
                فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم
                الموقع. ومن هنا وجب على المصمم أن يضع نصوصا
                مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور
                مولد النص العربى أن يوفر على المصمم عناء البحث عن
                نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم
                فيظهر بشكل لا يليق. هذا النص هو مثال لنص يمكن أن
                يستبدل في نفس المساحة، لقد تم توليد هذا النص من
                مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو
                العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف
                التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من
                الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما
                تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد
                النص العربى مفيد لمصممي المواقع على وجه
                الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع
                على صورة حقيقية لتصميم الموقع. ومن هنا وجب على
                المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر',
                'title2'   => null,
                'content2' => null,                
                'image'    => 'logo.png',                
                'image2'   => null,                 
            ],             
            // block one car
            [
                'key'     => 'block_one_car',
                'title'   => 'اتافية المستخدم',
                'content' => 'محتوي اتافية المستخدم',
                'title2'   => null,
                'content2'  => null,                
                'image'   => null,                
                'image2'   => null,                
            ],            

        ]);
    }
}
