<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectioneProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directione_products', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_id')->index()->nullable(); // id product
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');            

            $table->unsignedBigInteger('directione_id')->index()->nullable(); // id directione
            $table->foreign('directione_id')->references('id')->on('directiones')->onDelete('cascade');            

            $table->string('value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directione_products');
    }
}
