<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_sub_categories', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('subcategory_id')->index()->nullable(); // id subcategory where option add option to it only
            $table->foreign('subcategory_id')->references('id')->on('categories')->onDelete('cascade');

            $table->unsignedBigInteger('attribute_id')->index()->nullable(); // id option of attribute
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_sub_categories');
    }
}
