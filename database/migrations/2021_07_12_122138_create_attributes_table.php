<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->id();
            $table->string('name'); // name attribute
            $table->string('cat')->nullable();
            $table->enum('type' , ['select' , 'radio'])->nullable(); // type attribute select or radio
            $table->integer('parent_id')->nullable(); // paretn is use when add option take id attribute

            $table->unsignedBigInteger('category_id')->index()->nullable(); // id category 
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
