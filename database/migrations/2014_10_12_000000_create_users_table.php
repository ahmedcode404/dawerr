<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('second_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->unique();
            $table->string('image')->default('users/avatar.png');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('slug')->nullable();
            $table->string('address')->nullable();
            $table->string('name_bank')->nullable();
            $table->string('name_account_bank')->nullable();
            $table->bigInteger('number_account_bank')->nullable();
            $table->bigInteger('number_eban_bank')->nullable();
            $table->string('code')->nullable();
            $table->boolean('account_verify')->default(0);
            $table->boolean('is_notify')->default(1);
            $table->bigInteger('commercial_register')->nullable();
            $table->bigInteger('tax_number')->nullable();
            $table->longText('device_token')->nullable();
            $table->string('role')->default('user');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
