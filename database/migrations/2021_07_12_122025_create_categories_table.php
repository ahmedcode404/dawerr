<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name'); // name category
            $table->string('slug')->nullable(); // name category
            $table->string('image')->nullable(); // image category
            $table->integer('parent_id')->nullable(); // parent id user when add subcategory take id category
            $table->string('type')->nullable(); // type category or other 
            $table->timestamps();
        });

        // Schema::table('users', function (Blueprint $table){

        //     $table->unsignedBigInteger('category_id')->index()->nullable(); // id category 
        //     $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

        // });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
