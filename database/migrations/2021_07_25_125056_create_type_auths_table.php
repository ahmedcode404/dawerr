<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeAuthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_auths', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->boolean('type')->default(1);
            $table->string('image')->nullable();
            $table->timestamps();
        });
        
        Schema::table('users', function (Blueprint $table){

            $table->unsignedBigInteger('type_id')->index()->nullable(); // id type auth 
            $table->foreign('type_id')->references('id')->on('type_auths')->onDelete('cascade');

        });           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_auths');
    }
}
