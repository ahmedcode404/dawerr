<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionAttributeProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_attribute_products', function (Blueprint $table) {
            $table->id();

            $table->string('type')->nullable();

            $table->unsignedBigInteger('product_id')->index()->nullable(); // id product
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');            

            $table->unsignedBigInteger('attribute_id')->index()->nullable(); // id option and attribute
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');

            $table->unsignedBigInteger('option_id')->index()->nullable(); // id option and option
            $table->foreign('option_id')->references('id')->on('attributes')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_attribute_products');
    }
}
