<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('name_user');
            $table->float('commission_amount');
            $table->string('numebr_product');
            $table->date('transfer_date');
            $table->string('transfer_name');
            $table->string('phone');
            $table->longText('note')->nullable();
            $table->string('image');

            $table->unsignedBigInteger('bank_id')->index()->nullable(); // id bank
            $table->foreign('bank_id')->references('id')->on('bank_acounts')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
