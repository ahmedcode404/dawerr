<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = [

        'slug',
        'name',
        'name_company',
        'price',
        'space',
        'phone',
        'email',
        'image_one',
        'image_two',
        'image_three',
        'number_halls',
        'number_bedroom',
        'details',
        'show_phone',
        'category',
        'addition_id',
        'other_addition_id',
        'lat',
        'lng',
        'address',
        'code',
        'border',
        'street_view',
        'district',
        'status',
        'number_roles',
        'height',
        'city_id',
        'category_id',
        'user_id',
        'fuel_id',
        'the_walker',
        'make_year',
        'capacity',        
        'subcategory_id',

        'number_of_shops',
        'neighborhood',
        'street_view',
        'border',
        'number_halls_for_two',
        'women_boards_for_two',
        'men_boards_for_two',
        'number_of_bedrooms_for_two',
        'number_halls_for_one',
        'women_boards_for_one',
        'men_boards_for_one',
        'number_of_bedrooms_for_one',
        'shops',
        'number_of_sections',
        'inside_height',
        'street_or_lane_width',
        'fuel_tank_capacity',
        'number_pumps',
        'on_highway',
        'storehouse',
        'number_bathrooms',
        'number_of_swimming_pools',
        'number_kitchens',
        'number_halls',
        'women_councils',
        'men_boards',
        'number_of_master_bedrooms',
        'number_bedroom',
        'number_of_roles',
        'building_erea',
        'space',     
        'status',        
        'date_sale'        

    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['name' , 'id'],
                'separator' => '-'
            ]
        ];
    }    


    protected $casts  = ['date_sale' => 'date'];

    public function imageone()
    {
        return url('storage/' . $this->image_one);
    } // end of image one
    
    public function imagetwo()
    {
        return url('storage/' . $this->image_two);
    } // end of image one

    public function imagethree()
    {
        return url('storage/' . $this->image_three);
    } // end of image one

    public function images()
    {

        return $this->hasMany(Image::class);

    } // end of images

    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function maincategory()
    {
        return $this->belongsTo(Category::class , 'category_id' , 'id');
    } // end of main category

    public function type()
    {
        return $this->belongsTo(Category::class , 'subcategory_id' , 'id');
    } // end of type

    public function addition()
    {
        return $this->belongsTo(Category::class , 'addition_id' , 'id');
    } // end of addition

    public function user()
    {

        return $this->belongsTo(User::class , 'user_id' , 'id' );

    } // end of user

    public function attributeandoption()
    {
        return $this->hasMany(OptionAttributeProduct::class);
    } // end of type    

    public function directione()
    {

        return $this->hasMany(DirectioneProduct::class);

    } // end of type 
    
    public function reports()
    {

        return $this->hasMany(Report::class);
        
    } // end of report

    public function city()
    {
        return $this->belongsTo(City::class );
    } // end of city


} // end of model

