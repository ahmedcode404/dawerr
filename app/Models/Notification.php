<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = ['url' , 'send_from' , 'send_to' , 'type' , 'title' ,'seen' , 'message'];

}
