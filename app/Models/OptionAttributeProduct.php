<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionAttributeProduct extends Model
{
    use HasFactory;

    protected $fillable = ['product_id' , 'attribute_id' , 'option_id'];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class , 'attribute_id' , 'id');
    } // end of attribute

    public function option()
    {
        return $this->belongsTo(Attribute::class , 'option_id' , 'id');
    } // end of option

    

}
