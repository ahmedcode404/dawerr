<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = ['name' , 'slug' , 'parent_id' , 'type' , 'image'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['name' , 'id'],
                'separator' => '-'
            ]
        ];
    }    

    public function subCategory()
    {

        return $this->hasMany(Self::class , 'parent_id' , 'id');

    } // end of get sub categories

    public function attributes()
    {

        return $this->hasMany(Attribute::class , 'category_id' , 'id');
        
    } // end of get sub categories 
    
    public function optionsubcategory()
    {

        return $this->belongsToMany(Attribute::class , 'option_sub_categories')->where('parent_id' , null);

    }    

} // end of model
