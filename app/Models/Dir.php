<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dir extends Model
{
    use HasFactory;

    protected $fillable = ['name'];


    public function directs()
    {
        return $this->hasMany(DirectioneProduct::class , 'id' , 'dire_id');
    }    

}
