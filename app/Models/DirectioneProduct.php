<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DirectioneProduct extends Model
{
    use HasFactory;

    protected $fillable = ['directione_id' , 'product_id' , 'value' , 'dire_id'];

    public function dire()
    {
        return $this->belongsTo(Directione::class , 'directione_id' , 'id');
    }

    public function getNameAttribute()
    {
        return $this->value;
    }

    public function direct()
    {
        return $this->belongsTo(Dir::class , 'dire_id' , 'id');
    }

}
