<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;
    
    protected $fillable = ['report' , 'user_id' , 'product_id'];

    public function reports()
    {
    
        return $this->hasMany(Report::class , 'project_id'  , 'id');
        
    } // end of reports

} // end of model
