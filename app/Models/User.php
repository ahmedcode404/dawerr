<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Cviebrock\EloquentSluggable\Sluggable;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable , Sluggable;


    protected $fillable = [

        'first_name',
        'second_name',
        'last_name',
        'email',
        'password',
        'phone',
        'type_id',
        'category_id',
        'code',
        'image',
        'city_id',
        'account_verify',
        'is_notify',
        'commercial_register',
        'tax_number',
        'lat',
        'lng',
        'address',
        'name_bank',
        'name_account_bank',
        'number_account_bank',
        'number_eban_bank',
        'slug',

    ];  

    protected $hidden = [
        // 'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    } 

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['first_name' , 'id'],
                'separator' => '-'
            ]
        ];
    }

    public function ImageUser()
    {
        return url('storage/' . $this->image);
    }

    // get firebase token 
    public function devices()
    {
        return $this->hasMany(DeviceToken::class);
    } // end of  devices

    public function city()
    {
        return $this->belongsTo(City::class );
    } // end of city

    public function typeauth()
    {
        return $this->belongsTo(TypeAuth::class , 'type_id' , 'id');
    } // end of city

    public function products()
    {
        
        return $this->hasMany(Product::class , 'user_id' , 'id');

    } // end of product

    public function favorites()
    {

        return $this->hasMany(Favorite::class);
        
    } // end of favorite 

    public function category()
    {
        
        return $this->hasMany(UserCategory::class , 'user_id' , 'id');

    } // end of product    

} // end of model
