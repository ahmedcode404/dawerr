<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankAcount extends Model
{
    use HasFactory;

    protected $fillable = ['name' , 'number_account' , 'number_eban' , 'logo'];

    public function logoOne()
    {
        return url('storage/' . $this->logo);
    }    

}
