<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = ['name' , 'type' , 'cat' , 'parent_id' , 'category_id'];

    public function attribute()
    {

        return $this->belongsTo(self::class , 'parent_id' , 'id');

    } // end of attribute 
    
    public function options()
    {

        return $this->hasMany(self::class , 'parent_id' , 'id');

    } // end of options       

    public function subcategory()
    {

        return $this->belongsTo(Category::class , 'category_id' , 'id');

    } // end of subcategory

    public function optionsubcategory()
    {

        return $this->belongsToMany(Category::class , 'option_sub_categories' , 'attribute_id' , 'subcategory_id');

    } // end of optionsubcategory

} // end fo model
