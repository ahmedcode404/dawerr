<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceFillter extends Model
{
    use HasFactory;

    protected $fillable = ['price_from' , 'price_to'];

} // end of model
