<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Order extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = [

        'slug',
        'name_user',
        'commission_amount',
        'numebr_product',
        'transfer_date',
        'transfer_name',
        'phone',
        'note',
        'image',
        'bank_id',

    ];

    protected $casts  = ['transfer_date' => 'date'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['name_user' , 'id'],
                'separator' => '-'
            ]
        ];
    }       

    public function imageone()
    {
        return url('storage/' . $this->image);
    } // end of image one

    public function bank()
    {
        return $this->belongsTo(BankAcount::class);
    } // end of bank
}
