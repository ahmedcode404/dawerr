<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeAuth extends Model
{
    use HasFactory;

    protected $table = 'type_auths';

    protected $fillable = ['name', 'image' , 'type'];

} // end of model
