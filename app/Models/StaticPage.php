<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaticPage extends Model
{
    use HasFactory;

    protected $fillable = [
        'key' ,
        'parent_id' ,
        'title_ar' ,
        'title_en' ,
        'content_ar' ,
        'content_en' ,
        'image',
        'link'
    ];

} // end of model
