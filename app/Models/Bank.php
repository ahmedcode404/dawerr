<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Bank extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = ['name' , 'logo' , 'slug'];

    public function logoOne()
    {
        return url('storage/' . $this->logo);
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['name' , 'id'],
                'separator' => '-'
            ]
        ];
    }     

}
