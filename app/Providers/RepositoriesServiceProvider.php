<?php

namespace App\Providers;

// interface

use App\Repositories\AgreementRepositoryInterface;
use App\Repositories\AttributeRepositoryInterface;
use App\Repositories\AuthenticationRepositoryInterface;
use App\Repositories\BankAccountRepositoryInterface;
use App\Repositories\BankRepositoryInterface;
use App\Repositories\BaseRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\DirectioneRepositoryInterface;
use App\Repositories\DirRepositoryInterface;
use App\Repositories\Eloquent\DirRepository;
use App\Repositories\OptionSubCategoryRepositoryInterface;
use App\Repositories\PriceFillterRepositoryInterface;
use App\Repositories\StaticPageRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Repositories\SliderRepositoryInterface;
use App\Repositories\TypeAuthRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\OrderRepositoryInterface;


// repository

use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\Eloquent\CategoryRepository;
use App\Repositories\Eloquent\AgreementRepository;
use App\Repositories\Eloquent\AttributeRepository;
use App\Repositories\Eloquent\AuthenticationRepository;
use App\Repositories\Eloquent\BankAccountRepository;
use App\Repositories\Eloquent\BankRepository;
use App\Repositories\Eloquent\CityRepository;
use App\Repositories\Eloquent\ContactRepository;
use App\Repositories\Eloquent\DirectioneRepository;
use App\Repositories\Eloquent\ImageRepository;
use App\Repositories\Eloquent\NotificationRepository;
use App\Repositories\Eloquent\OptionSubCategoryRepository;
use App\Repositories\Eloquent\OrderRepository;
use App\Repositories\Eloquent\PriceFillterRepository;
use App\Repositories\Eloquent\ProductRepository;
use App\Repositories\Eloquent\ReportRepository;
use App\Repositories\Eloquent\SettingRepository;
use App\Repositories\Eloquent\SliderRepository;
use App\Repositories\Eloquent\StaticPageRepository;
use App\Repositories\Eloquent\TypeAuthRepository;
use App\Repositories\ImageRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{

    public function register()
    {

        $this->app->bind(BaseRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(AttributeRepositoryInterface::class, AttributeRepository::class);
        $this->app->bind(OptionSubCategoryRepositoryInterface::class, OptionSubCategoryRepository::class);
        $this->app->bind(PriceFillterRepositoryInterface::class, PriceFillterRepository::class);
        $this->app->bind(BankRepositoryInterface::class, BankRepository::class);
        $this->app->bind(BankAccountRepositoryInterface::class, BankAccountRepository::class);
        $this->app->bind(StaticPageRepositoryInterface::class, StaticPageRepository::class);
        $this->app->bind(SettingRepositoryInterface::class, SettingRepository::class);
        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);
        $this->app->bind(SliderRepositoryInterface::class, SliderRepository::class);
        $this->app->bind(AgreementRepositoryInterface::class, AgreementRepository::class);
        $this->app->bind(AuthenticationRepositoryInterface::class, AuthenticationRepository::class);
        $this->app->bind(NotificationRepositoryInterface::class, NotificationRepository::class);
        $this->app->bind(TypeAuthRepositoryInterface::class, TypeAuthRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(DirectioneRepositoryInterface::class, DirectioneRepository::class);
        $this->app->bind(DirRepositoryInterface::class, DirRepository::class);
        $this->app->bind(ContactRepositoryInterface::class, ContactRepository::class);
        $this->app->bind(ReportRepositoryInterface::class, ReportRepository::class);
        $this->app->bind(ImageRepositoryInterface::class, ImageRepository::class);

    }

    public function boot()
    {
        //
    }
}
