<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Paginator::useBootstrap();        

        view()->composer('*', function ($view) {

            $categoryrepository = \App::make('App\Repositories\CategoryRepositoryInterface');
            $settingrepository = \App::make('App\Repositories\SettingRepositoryInterface');

            $instagram = $settingrepository->getwhere(['key' => 'instagram'])->first();
            $snapchat  = $settingrepository->getwhere(['key' => 'snapchat'])->first();
            $facebook  = $settingrepository->getwhere(['key' => 'facebook'])->first();
            $twitter   = $settingrepository->getwhere(['key' => 'twitter'])->first();
            $about2	   = $settingrepository->getwhere(['key' => 'about'])->first();
            $logo	   = $settingrepository->getwhere(['key' => 'logo'])->first();

            $categories = $categoryrepository->getAll();

            view()->share([

                'categories' => $categories,
                'instagram'  => $instagram,
                'snapchat'   => $snapchat,
                'facebook'   => $facebook,
                'twitter'    => $twitter,
                'about2'      => $about2,
                'logo'       => $logo,

            ]);  // end of view share

        });  // end of view composer

    }
}
