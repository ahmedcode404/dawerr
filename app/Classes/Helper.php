<?php

use App\Jobs\SendMultiMail;
use App\Mail\SendMailMarkting;
// use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\Mail;
use App\Mail\WeHelp;
use App\Models\Notification;
use App\Servicies\Notify;
use Carbon\Carbon;
use AmrShawky\LaravelCurrency\Facade\Currency;
use App\Jobs\SendEmailGifts;
use App\Jobs\sendMailSubscribe;
use App\Mail\BondMail;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

/*curr
|--------------------------------------------------------------------------
| Detect Active Routes Function
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/

function isActiveRoute($route, $output = "active"){
    if (\Route::currentRouteName() == $route) return $output;
}

function areActiveRoutes(Array $routes, $output = "active show-sub"){

    foreach ($routes as $route){
        if (\Route::currentRouteName() == $route) return $output;
    }
}

function areActiveMainRoutes(Array $routes, $output = "active"){

    foreach ($routes as $route){
        if (\Route::currentRouteName() == $route) return $output;
    }
}

function NotificationAdmin($type , $send_to = null , $user_send_to = null)
{

    $userrepository         = \App::make('App\Repositories\AuthenticationRepositoryInterface');
    $notificationrepository = \App::make('App\Repositories\NotificationRepositoryInterface');

    $admins = $userrepository->getWhere([['type' , 'admin'] , ['is_notify' , 1]]);

    if ($type = 'user') {

        $message = 'تم التسجيل من قبل مستخدم جديد';
        $title   = 'مستخدم جديد';

    } else 
    {
        $message = 'تم التسجيل من قبل حساب تجاري';
        $title   = 'حساب  تجاري جديد';        
    }

    foreach ($admins as $admin) 
    {

        $notificationrepository->create([

            'title'   => $title,
            'message' => $message,
            'type'    => 'new_user',
            'send_to' => $admin->id,

        ]); // end of create

    } // end of foreach

    Notify::NotifyWeb($message , $send_to , $title);

} // end of notification admin

function UploadeMulipleImage($files , $product)
{

    foreach ($files as $file) {

        $path = $file->store('product');

        $product->images()->create([

            'product_id' => $product->id,
            'path'       => $path,

        ]); // end if create images


    }  // end of foreach

} // end of uploade multiple images


function AttributeAndOptionCreate($attr , $product)
{
    
    foreach (json_decode($attr) as $att) {

        $product->attributeandoption()->create([
            
            'product_id'   => $product->id,
            'attribute_id' => $att->attribute_id,
            'option_id'    => $att->option_id,

        ]); // end of create

    } // end of foreach
  
} // end of attribute and option

function DirectioneCreate($dire , $product)
{
    
    foreach (json_decode($dire) as $att) {

        $product->directione()->create([
            
            'product_id'    => $product->id,
            'dire_id'        => $att->dir_id,
            'directione_id' => $att->directione_id,
            'value'         => $att->value,

        ]); // end of create

    } // end of foreach
  
} // end of attribute and option

function attributeExcept($request)
{

    if ($request->subcategory_id == 6) {
  
        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 7) {

        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'street_view',
            'border',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'number_of_swimming_pools',
            'number_of_master_bedrooms',
            'building_erea'

        );

    } elseif ($request->subcategory_id == 8) {

        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'street_view',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two' ,
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms',
            'number_bedroom',
            'number_of_roles',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 9) {

        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops'     ,
            'border',
            'number_halls_for_two'    ,
            'women_boards_for_two'    ,
            'men_boards_for_two'     ,
            'number_of_bedrooms_for_two' ,
            'number_halls_for_one',
            'women_boards_for_one' ,
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections'    ,
            'inside_height'      ,
            'street_or_lane_width'   ,
            'storehouse'  ,
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens' ,
            'number_halls',
            'women_councils' ,
            'men_boards' ,
            'number_of_master_bedrooms' ,
            'number_bedroom' ,
            'number_of_roles' ,
            'building_erea'
        );

    } elseif ($request->subcategory_id == 10) {

        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops'     ,
            'number_halls_for_two'    ,
            'women_boards_for_two'      ,
            'men_boards_for_two'         ,
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one'  ,
            'men_boards_for_one'       ,
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections'   ,
            'inside_height'       ,
            'street_or_lane_width' ,
            'fuel_tank_capacity'   ,
            'number_pumps',
            'on_highway'        ,
            'storehouse'           ,
            'number_bathrooms',
            'number_of_swimming_pools' ,
            'number_kitchens'     ,
            'number_halls'   ,
            'women_councils' ,
            'men_boards' ,
            'number_of_master_bedrooms' ,
            'number_bedroom',
            'number_of_roles',
            'building_erea'

        );

    } elseif ($request->subcategory_id == 11) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_bathrooms' ,
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms' ,
            'number_bedroom',
            'number_of_roles',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 12) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops' ,
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_bathrooms',
            'number_of_master_bedrooms',
            'number_of_roles',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 13) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops' ,
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'inside_height' ,
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps' ,
            'on_highway' ,
            'storehouse' ,
            'number_bathrooms',
            'number_halls',
            'number_of_master_bedrooms',
            'number_of_roles',
            'building_erea'

        );

    } elseif ($request->subcategory_id == 14) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',


            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two' ,
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway' ,
            'storehouse',
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms',
            'number_bedroom',
            'building_erea'


        );

    } elseif ($request->subcategory_id == 15) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway' ,
            'storehouse',
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms',
            'number_bedroom',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 16) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two' ,
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 17) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',



        );

    } elseif ($request->subcategory_id == 18) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two'  ,
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );


    } elseif ($request->subcategory_id == 19) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 20) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 21) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 22) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 23) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 24) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',



        );

    } elseif ($request->subcategory_id == 25) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 26) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway' ,
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 27) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 28) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 29) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 30) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 31) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',
            

        );

    } elseif ($request->subcategory_id == 32) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one'  ,
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',



        );

    } elseif ($request->subcategory_id == 33) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles'

        );

    }

    return $attrubute;
    
} // end of attribute except

function attributeExceptSearch($request)
{

    if ($request->subcategory_id == 6) {
  
        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 7) {

        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'street_view',
            'border',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'number_of_swimming_pools',
            'number_of_master_bedrooms',
            'building_erea'

        );

    } elseif ($request->subcategory_id == 8) {

        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'street_view',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two' ,
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms',
            'number_bedroom',
            'number_of_roles',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 9) {

        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops'     ,
            'border',
            'number_halls_for_two'    ,
            'women_boards_for_two'    ,
            'men_boards_for_two'     ,
            'number_of_bedrooms_for_two' ,
            'number_halls_for_one',
            'women_boards_for_one' ,
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections'    ,
            'inside_height'      ,
            'street_or_lane_width'   ,
            'storehouse'  ,
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens' ,
            'number_halls',
            'women_councils' ,
            'men_boards' ,
            'number_of_master_bedrooms' ,
            'number_bedroom' ,
            'number_of_roles' ,
            'building_erea'
        );

    } elseif ($request->subcategory_id == 10) {

        $attrubute = $request->except(

            'images' ,
            'attr',
            'directione',
            'image_one' ,
            'image_two' ,
            'image_three' ,
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops'     ,
            'number_halls_for_two'    ,
            'women_boards_for_two'      ,
            'men_boards_for_two'         ,
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one'  ,
            'men_boards_for_one'       ,
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections'   ,
            'inside_height'       ,
            'street_or_lane_width' ,
            'fuel_tank_capacity'   ,
            'number_pumps',
            'on_highway'        ,
            'storehouse'           ,
            'number_bathrooms',
            'number_of_swimming_pools' ,
            'number_kitchens'     ,
            'number_halls'   ,
            'women_councils' ,
            'men_boards' ,
            'number_of_master_bedrooms' ,
            'number_bedroom',
            'number_of_roles',
            'building_erea'

        );

    } elseif ($request->subcategory_id == 11) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_bathrooms' ,
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms' ,
            'number_bedroom',
            'number_of_roles',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 12) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops' ,
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_bathrooms',
            'number_of_master_bedrooms',
            'number_of_roles',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 13) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops' ,
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'inside_height' ,
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps' ,
            'on_highway' ,
            'storehouse' ,
            'number_bathrooms',
            'number_halls',
            'number_of_master_bedrooms',
            'number_of_roles',
            'building_erea'

        );

    } elseif ($request->subcategory_id == 14) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',


            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two' ,
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway' ,
            'storehouse',
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms',
            'number_bedroom',
            'building_erea'


        );

    } elseif ($request->subcategory_id == 15) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'shops',
            'number_of_sections',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway' ,
            'storehouse',
            'number_bathrooms',
            'number_of_swimming_pools',
            'number_kitchens',
            'number_halls',
            'women_councils',
            'men_boards',
            'number_of_master_bedrooms',
            'number_bedroom',
            'building_erea'
        );

    } elseif ($request->subcategory_id == 16) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two' ,
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 17) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',



        );

    } elseif ($request->subcategory_id == 18) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two'  ,
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );


    } elseif ($request->subcategory_id == 19) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 20) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 21) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 22) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 23) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 24) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',



        );

    } elseif ($request->subcategory_id == 25) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 26) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway' ,
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 27) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 28) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 29) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',

        );

    } elseif ($request->subcategory_id == 30) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',


        );

    } elseif ($request->subcategory_id == 31) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',
            

        );

    } elseif ($request->subcategory_id == 32) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one'  ,
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles',



        );

    } elseif ($request->subcategory_id == 33) {

        $attrubute = $request->except(

            'images',
            'attr',
            'directione',
            'image_one',
            'image_two',
            'image_three',
            'name_comapny',
            'type',
            'category',
            'the_walker',
            'make_year',
            'capacity',

            'number_of_shops',
            'number_halls_for_two',
            'women_boards_for_two',
            'men_boards_for_two',
            'number_of_bedrooms_for_two',
            'number_halls_for_one',
            'women_boards_for_one',
            'men_boards_for_one',
            'number_of_bedrooms_for_one',
            'number_of_sections',
            'inside_height',
            'street_or_lane_width',
            'fuel_tank_capacity',
            'number_pumps',
            'on_highway',
            'storehouse',
            'number_of_swimming_pools',
            'number_of_roles'

        );

    }

    return $attrubute;
    
} // end of attribute except search
