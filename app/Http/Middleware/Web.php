<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Web
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(auth()->check() AND auth()->user()->type_id == 1 || auth()->user()->type_id == 2)
        {

            return $next($request);

        } else 
        {

            return redirect('login');

        }    
    }
}
