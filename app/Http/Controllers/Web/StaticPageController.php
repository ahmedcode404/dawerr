<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\ContactRequest;
use App\Repositories\AgreementRepositoryInterface;
use App\Repositories\BankRepositoryInterface;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\StaticPageRepositoryInterface;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{

    public $staticrepository;
    public $settingrepository;
    public $cotntactrepository;
    public $agreementrepository;
    public $bankrepository;

    public function __construct(StaticPageRepositoryInterface $staticrepository,
                                SettingRepositoryInterface $settingrepository,
                                ContactRepositoryInterface $cotntactrepository,
                                AgreementRepositoryInterface $agreementrepository,
                                BankRepositoryInterface $bankrepository
    )
    {

        $this->staticrepository    = $staticrepository;
        $this->settingrepository   = $settingrepository;
        $this->cotntactrepository  = $cotntactrepository;
        $this->agreementrepository = $agreementrepository;
        $this->bankrepository      = $bankrepository;

    } // end of construct    

    public function static($key)
    {

        $static = $this->staticrepository->getwhere(['key' => $key])->first();

        return view('website.static_page' , compact('static'));
        
    } // end of static  

    public function about()
    {

        $about = $this->staticrepository->getwhere(['key' => 'about'])->first();

        return view('website.statics.about' , compact('about'));

    } // end of about  

    public function contact()
    {

        $phone     = $this->settingrepository->getwhere(['key' => 'phone'])->first();
        $email     = $this->settingrepository->getwhere(['key' => 'email'])->first();
        $address   = $this->settingrepository->getwhere(['key' => 'address'])->first();
        $lat       = $this->settingrepository->getwhere(['key' => 'lat'])->first();
        $lng       = $this->settingrepository->getwhere(['key' => 'lng'])->first();
        $instagram = $this->settingrepository->getwhere(['key' => 'instagram'])->first();
        $snapchat  = $this->settingrepository->getwhere(['key' => 'snapchat'])->first();
        $facebook  = $this->settingrepository->getwhere(['key' => 'facebook'])->first();
        $twitter   = $this->settingrepository->getwhere(['key' => 'twitter'])->first();

        return view('website.statics.contact' , compact(
            'phone',
            'email',
            'address',
            'lat',
            'lng',
            'instagram',
            'snapchat',
            'facebook',
            'twitter',
        ));

    } // end of contact  

    public function store(ContactRequest $request)
    {
        

        $contact = $this->cotntactrepository->create($request->all());

        if($contact)
        {

            return redirect()->back()->with('success' , 'تم ارسال الرسالة بنجاح , سيتم الرد علي البريد الالكتروني الخاص بكم حين المراجعة');
            
        } else 
        {
            
            return redirect()->back()->with('error' , 'هناك مشكلة برجاء المحاولة مره اخري');

        } // end of else if

    } // end of store

    public function commission()
    {

        $commission = $this->staticrepository->getwhere(['key' => 'commission_account_text'])->first();
        $tax        = $this->settingrepository->getwhere(['key' => 'tax'])->first();
        $agreements =  $this->agreementrepository->getAll();
        $banks =  $this->bankrepository->getAll();

        return view('website.statics.commission' , compact('commission' , 'agreements' , 'tax' , 'banks'));

    } // end of commission

} // end of class
