<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\PaymentRequest;
use App\Repositories\BankAccountRepositoryInterface;
use App\Repositories\OrderRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public $orderrepository;
    public $bankaccountrepository;
    public $productrepository;

    public function __construct(OrderRepositoryInterface $orderrepository,
                                BankAccountRepositoryInterface $bankaccountrepository,
                                ProductRepositoryInterface $productrepository
    )
    {

        $this->orderrepository       = $orderrepository;
        $this->bankaccountrepository = $bankaccountrepository;
        $this->productrepository     = $productrepository;

    } // end of construct

    public function payment($slug)
    {

        $banks = $this->bankaccountrepository->getAll();

        return view('website.payment.transform' , compact('banks'));

    } // end of payment

    public function paymentNow(PaymentRequest $request)
    {

        $attribute = $request->except('image' , 'numebr_product' , 'transfer_date');

        // uploade one image
        if($request->has('image'))
        {

            // Upload new image
            $attribute['image'] = $request->file('image')->store('product');
        
        } // end of has image   
        
        $attribute['numebr_product'] = '#'.$request->numebr_product;
        $attribute['transfer_date'] = Carbon::createFromFormat('d/m/Y', $request->transfer_date)->format('d-m-Y');

        $this->orderrepository->create($attribute);

        return redirect()->route('web.commission')->with('success' , 'تم الدفع بنجاح');
        


    } // end of payment

} // end of class
