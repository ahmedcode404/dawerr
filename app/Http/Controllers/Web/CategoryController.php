<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepositoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    public $categoryrepository;

    public function __construct(CategoryRepositoryInterface $categoryrepository)
    {
        $this->categoryrepository = $categoryrepository;
    } // end of construct

    public function category()
    {

        $categories = $this->categoryrepository->getAll();

        return view('website.category' , compact('categories'));

    } // end of category
    
}
