<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\RegisterRequest;
use App\Repositories\AuthenticationRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\StaticPageRepositoryInterface;
use Illuminate\Http\Request;
use Auth;
class AuthenticationController extends Controller
{

    public $authenticationrepository;
    public $cityrepository;
    public $staticrepository;

    public function __construct(AuthenticationRepositoryInterface $authenticationrepository,
                                CityRepositoryInterface $cityrepository,
                                StaticPageRepositoryInterface $staticrepository
    )
    {

        $this->authenticationrepository = $authenticationrepository;
        $this->cityrepository           = $cityrepository;
        $this->staticrepository         = $staticrepository;

    } // end of construct

    ##################################################### BEGIN: authenticateion ###########################

    public function register()
    {
        
        if (session('categoryId')) {

            $cities = $this->cityrepository->getAll();
    
            return view('website.auth.register' , compact('cities'));

        } else 
        {

            return redirect()->route('web.category');

        }

    } // end of category
 
    public function registerStore(RegisterRequest $request)
    {

        if ($request->type_id == 1) {

            $attribute = $request->except(

                    'commercial_register' ,
                    'tax_number' ,
                    'password' ,
                    'confirm_password',
                    'name_bank',
                    'name_account_bank',
                    'number_account_bank',
                    'number_eban_bank',
                    'company_name',
                    'category_id'
                    
            );

            $attribute['first_name']  = $request->first_name;


        } else  
        {

            $attribute = $request->except(

                    'password' ,
                    'confirm_password' ,
                    'second_name' ,
                    'last_name',
                    'first_name',
                    'category_id'

            );
            
            $attribute['first_name']  = $request->company_name;
        }

        $code = rand(1111,9999);

        $attribute['password'] = bcrypt($request->password);
        $attribute['image']    = 'users/avatar.png';
        $attribute['code']     = $code;      

        $user = $this->authenticationrepository->create($attribute);

        if ($user) {

            if($user->type_id == 1)
            {

                $user->category()->create([
    
                    'user_id' => $user->id,
                    'category_id' => 1,
    
                ]);

                $user->category()->create([
    
                    'user_id' => $user->id,
                    'category_id' => 2,
    
                ]);

            } else 
            {

                $user->category()->create([
    
                    'user_id' => $user->id,
                    'category_id' => session('categoryId'),
    
                ]);                

            }

            // send code to mobile
            
            // send code to mobile              

            return redirect()->route('web.verfiy' , $user->slug)->with('success' , 'تم انشاء الحساب بنجاح , برجاء تفعيل الحساب الخاص بنك من خلال الكود المرسل علي رقم الجوال');

        } else 
        {
            
            return redirect()->back()->with('error' , 'لقد وجدنا مشكله اثناء تسجيل الحساب , برجاء اعادة المحاولة');

        } // end of user

    } // end of register store

    public function login()
    {

        if (session('categoryId')) {
    
            return view('website.auth.login');

        } else 
        {

            return redirect()->route('web.category');

        }

    } // end of login    
    
    public function loginAuth(Request $request)
    {

        $email = $this->authenticationrepository->getWhere(['email' => $request->email])->first();
        
        if ($email) {
            
            $check_category = $this->authenticationrepository->whereHas('category' , ['category_id' => session('categoryId')] , ['email' => $request->email] , ['column' => 'id', 'dir' => 'asc'])->first();

            if ($check_category) {
 
                $active = $this->authenticationrepository->getWhere([['email' , $request->email] , ['account_verify' , 1]])->first();
    
                if ($active) {
    
                    if (Auth::attempt(request(['email','password'])) == false)
                    {
                        
                        return redirect()->back()->with('error' , 'بيانات الدخول غير صحيحة');
            
                    } else 
                    {
    
                        return redirect()->route('web.home' , session('categorySlug'))->with('success' , 'تم تسجيل الدخول بنجاح');
    
                    } // end of if auth attempt
    
                }else 
                {
    
                    return redirect()->route('web.verfiy' , $email->slug)->with('note' , 'يجب عليك تفعيل الحساب اولا');
                    
                } // end of else if active
                
            } else 
            {
                
                if ($email->category_id == 1) 
                {

                    return redirect()->back()->with('home' , 'هذا الحساب يمكنك الدخول به في قسم السيارات فقط , اذا كنت تريد الدخول الي قسم العقارات برجاء انشاء حساب جديد خاص بالعقارات , هل تريد العوده لاختيار قسم اخر ؟');
                    
                } else 
                {
                    
                    return redirect()->back()->with('home' , 'هذا الحساب يمكنك الدخول به في قسم العقارات فقط , اذا كنت تريد الدخول الي قسم السيارات برجاء انشاء حساب جديد خاص بالسيارات , هل تريد العوده لاختيار قسم اخر ؟');

                } // end of if category


            } // end of check category

        } else 
        {

            return redirect()->back()->with('error' , 'البريد الالكتروني غير صحيح برجاء المحاولة مره اخري');

        } // end of else if email

    } // end of login auth  

    ##################################################### END: authenticateion ###########################

    ##################################################### BEGIN: active account ###########################

    public function verfiy($slug)
    {
        
        // dd($slug);
        
        $user = $this->authenticationrepository->getWhere(['slug' => $slug]);
        
        if ($user) {

            return view('website.auth.verfiy' , compact('slug'));

        } else 
        {

            return view('website.errors.notfound');

        } // end of else if
        

    } // end of verfiy
    
    public function resendCode($slug)
    {

        $user = $this->authenticationrepository->getWhere([['slug' , $slug]])->first();

        if ($user) {
            
            $code = rand(1111,9999);

            $user->update(['code' => $code]);

            // send code to mobile

            // send code to mobile

            return redirect()->route('web.verfiy' , $user->slug)->with('success' , 'تم ارسال كود التفعيل مره اخري');

        } else 
        {

            return view('website.errors.notfound');

        } // end of else if
        

    } // end of verfiy

    public function activePhone(Request $request)
    {

        $user = $this->authenticationrepository->getWhere([['code' , $request->code] , ['slug' , $request->slug]])->first();
        
        if ($user) {

            // update user to active
            $user->update(['code' => null , 'account_verify' => 1]);

            // login user
            auth()->login($user);

            // rediret to category of session
            return redirect()->route('web.home' , session('categorySlug'))->with('success' , 'تم تفعيل الحساب بنجاح');
            
        } else 
        {

            return redirect()->back()->with('error' , 'كود التفعيل غير صحيح برجاء ادخال الكود مره اخري');

        } // end of user

    } // end of active phone    

    ##################################################### END: active account ###########################


    ##################################################### BEGIN: reset password ###########################
    public function forgetPassword()
    {

        return view('website.auth.forget_password');

    } // end of forget password
    
    public function sendCodeResetPassword(Request $request)
    {

        $phone = $this->authenticationrepository->getWhere(['phone' => $request->phone])->first();
        if ($phone) {

            
            $check_category = $this->authenticationrepository->whereHas('category' , ['category_id' => session('categoryId')] , ['phone' , $request->phone] , ['column' => 'id', 'dir' => 'asc'])->first();

            if ($check_category) {

                $code = rand(1111,9999);
                $phone->update(['code' => $code]);

                return redirect()->route('web.verfiy.code.reset.password' , $phone->slug)->with('success' , 'تم ارسال كود اعادة كلمة المرور الي رقم الجوال الخاص بك ');
                
            } else 
            {
                
                if ($phone->category_id == 1) 
                {

                    return redirect()->back()->with('home' , 'يمكنك اعاده كلمة المرور في قسم السيارات فقط لان هذا الحساب في السيارات , هل تريد العوده لاختيار قسم السيارات ؟');
                    
                } else 
                {
                    
                    return redirect()->back()->with('home' , 'يمكنك اعاده كلمة المرور في قسم العقارات فقط لان هذا الحساب في العقارات , هل تريد العوده لاختيار قسم العقارات ؟');

                } // end of if category
            } // end of check category

        } else 
        {
            
            return redirect()->back()->with('error' , 'رقم الجوال غير صحيح , برجاء اضافة رقم جوال صحيح');


        } // end of phone


    } // /end of send code forget password

    public function verfiySendCodeResetPassword($slug)
    {

        return view('website.auth.verfiy_reset_password' , compact('slug'));

    } // end of verfiy Send Code Reset Password  

    public function activeSendCodeResetPassword(Request $request)
    {

        $user = $this->authenticationrepository->getWhere([['code' , $request->code] , ['slug' , $request->slug]])->first();
        
        if ($user) {

            // update user to active
            $user->update(['code' => null , 'account_verify' => 1]);

            // login user
            auth()->login($user);

            // rediret to category of session
            return redirect()->route('web.reset.password' , $user->slug)->with('success' , 'تم تأكيد الحساب بنجاح , برجاء ادخال كلمة مرور جديدة ');
            
        } else 
        {

            return redirect()->back()->with('error' , 'كود التفعيل غير صحيح برجاء ادخال الكود مره اخري');

        } // end of user        

    } // end of active Send Code Reset Password    

    public function resetPassword()
    {
        return view('website.auth.reset_password');
    } // end of reset password
    
    public function newPassword(Request $request)
    {

        if (auth()->check()) {

            auth()->user()->update(['password' => bcrypt($request->password)]);

            return redirect()->route('web.home' , session('categorySlug'))->with('success' , 'تم تغيير كلمة المرور  بنجاح');

        } else 
        {

            return redirect('web/category');

        } // end of auth user check

    } // end of new password

    ##################################################### END: reset password ###########################

    public function logout ()
    {

        if (auth()->check()) {

            // auth user logout
            auth()->logout();

            return redirect()->route('web.home' , session('categorySlug'))->with('success' , 'تم تسجيل الخروج بنجاح');

        } else 
        {

            return view('website.errors.notfound'); 

        } // end of auth check

    } // end of logout

} // end of class
