<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\AuthenticationRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use Illuminate\Http\Request;

class OpposedController extends Controller
{

    public $authrepository;
    public $cityrepository;

    public function __construct(AuthenticationRepositoryInterface $authrepository,
                                CityRepositoryInterface $cityrepository
    )
    {

        $this->authrepository  = $authrepository;
        $this->cityrepository  = $cityrepository;

    } // end of construct

    public function getAllOpposed(Request $request)
    {
        
        $cities  =  $this->cityrepository->getAll();        

        if($request->city_id)
        {

            $opposeds = $this->authrepository->paginateWhereHas('category' , ['category_id' => session('categoryId')] , 2 , [['type_id' , 2] , ['city_id' , $request->city_id]] , ['column' => 'id', 'dir' => 'asc']);

            return view('website.opposeds.fillter' , compact('opposeds'));

        } else 
        {

            $opposeds = $this->authrepository->paginateWhereHas('category' , ['category_id' => session('categoryId')] , 2 , ['type_id' => 2] , ['column' => 'id', 'dir' => 'asc']);

            return view('website.opposeds.opposeds' , compact('opposeds' , 'cities'));

        }    

    } // end of get all opposed

    public function getOpposed($slug)
    {
        
        $opposed = $this->authrepository->whereHas('category' , ['category_id' => session('categoryId')] , ['type_id' => 2] , ['column' => 'id', 'dir' => 'asc'])->first();

        if($opposed)
        {

            return view('website.opposeds.opposed' , compact('opposed'));

        } else 
        {

            return view('website.errors.notfound'); 

        } // end of opposed

    } // end of get opposed

} // end of class
