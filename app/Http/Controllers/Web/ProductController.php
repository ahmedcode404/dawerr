<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\ProductRequest;
use App\Http\Requests\Web\ProductUpdateRequest;
use App\Repositories\AttributeRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\DirectioneRepositoryInterface;
use App\Repositories\DirRepositoryInterface;
use App\Repositories\ImageRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public $productrepository;
    public $categoryrepository;
    public $attributerepository;
    public $cityrepository;
    public $settingrepository;
    public $imagerepository;
    public $directionrepository;
    public $dirrepository;

    public function __construct(ProductRepositoryInterface $productrepository,
                                CategoryRepositoryInterface $categoryrepository,
                                AttributeRepositoryInterface $attributerepository,
                                CityRepositoryInterface $cityrepository,
                                SettingRepositoryInterface $settingrepository,
                                ImageRepositoryInterface $imagerepository,
                                DirectioneRepositoryInterface $directionrepository,
                                DirRepositoryInterface $dirrepository
    )
    {

        $this->productrepository   = $productrepository;
        $this->categoryrepository  = $categoryrepository;
        $this->attributerepository = $attributerepository;
        $this->cityrepository      = $cityrepository;
        $this->settingrepository   = $settingrepository;
        $this->imagerepository     = $imagerepository;
        $this->directionrepository = $directionrepository;
        $this->dirrepository       = $dirrepository;

    } // end of construct

    public function getProduct($slug)
    {

        $product = $this->productrepository->findOneSlug($slug);
        
        $category = $this->categoryrepository->findOne($product->category_id);
        
        // session category id
        session()->put('categoryId' , $category->id);
        // session category slug
        session()->put('categorySlug' , $category->name);

        return view('website.products.single_product' , compact('product'));

    } // end of get product

    public function addProduct()
    {
        if (auth()->user() &&  in_array(session('categoryId') , auth()->user()->category()->pluck('category_id')->toArray()))
        {
            
            $attributes     = $this->attributerepository->getOrWhere([['category_id' , session('categoryId')] , ['cat'  , '!=' , 'not']] , [['category_id' , session('categoryId')] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);
            $subcategories  = $this->categoryrepository->getWhere(['parent_id' => session('categoryId')] , ['column' => 'id', 'dir' => 'asc']);
            $additions      = $this->categoryrepository->getWhere(['type' => 'adds'] , ['column' => 'id', 'dir' => 'asc']);
            $otheradditions = $this->categoryrepository->getWhere(['type' => 'other_add'] , ['column' => 'id', 'dir' => 'asc'])->first();
            $cities         = $this->cityrepository->getAll();
            $commission     = $this->settingrepository->getWhere(['key' => 'tax'])->first();
            $day            = $this->settingrepository->getWhere(['key' => 'day'])->first();
    
            return view('website.products.add-product' , compact('commission' , 'day' , 'cities' , 'attributes' , 'subcategories' , 'additions' , 'otheradditions'));

        } else
        {

            return view('website.errors.notfound');

        } // end of else if

    } // end of add product

    public function getAddition(Request $request)
    {

        $attributes = $this->attributerepository->getOrWhere([['category_id' , $request->id] , ['cat'  , '!=' , 'not']] , [['category_id' , $request->id] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);

        return view('website.cars.append-addition' , compact('attributes'))->render();

    } // end of get addtion

    public function getAttribute(Request $request)
    {

        $attributes = $this->attributerepository->getOrWhere([['category_id' , $request->id] , ['cat'  , '!=' , 'not']] , [['category_id' , $request->id] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);

        $subcategory = $request->id;

        $directions = $this->directionrepository->getAll();
        $dirs       = $this->dirrepository->getAll();


        return view('website.buildings.append-attribute' , compact('directions' , 'attributes' , 'subcategory' , 'dirs'))->render();

    } // end get attribute

    public function getDirection(Request $request)
    {

        $dir_id = $request->id;

        $directions = $this->directionrepository->getAll();

        return view('website.buildings.append-direction' , compact('directions' , 'dir_id'))->render();

    } // end get direction

    public function getOptionOtherAddition(Request $request)
    {

        $options = $this->attributerepository->getOrWhere([['parent_id' , $request->id] , ['cat'  , '!=' , 'not']] , [['parent_id' , $request->id] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);
        $attribute = $request->id;
        return view('website.cars.append-option-other-addition' , compact('options' , 'attribute'))->render();

    } // end of get option other addition

    public function storeProduct(ProductRequest $request)
    {

            if (session('categoryId') == 1) {
                // ================================ السيارات ==================================
                $attrubute = $request->except(

                    'images' ,
                    'image_one' ,
                    'image_two' ,
                    'image_three'

                );
    
                // uploade one image_one
                if($request->has('image_one'))
                {
        
                    // Upload new image_one
                    $attrubute['image_one'] = $request->file('image_one')->store('product');
                
                } // end of has image_one  
    
                // uploade one image_two
                if($request->has('image_two'))
                {

                    // Upload new image_two
                    $attrubute['image_two'] = $request->file('image_two')->store('product');
                
                } // end of has image_two  
                
                // uploade one image_three
                if($request->has('image_three'))
                {
        
                    // Upload new image_three
                    $attrubute['image_three'] = $request->file('image_three')->store('product');
                
                } // end of has image_three  
        
                $attrubute['code']        = '#' . rand(0000000,9999999);
                $attrubute['user_id']     = auth()->user()->id;
                $attrubute['category_id'] = session('categoryId');
                $attrubute['show_phone']  =  $request->show_phone ? true : false;
        
                $product = $this->productrepository->create($attrubute);
        
                // insert attribute and options
                if ($request->attribute) {
                    
                    
                    foreach($request->attribute as $key=>$attr)
                    {

                        $product->attributeandoption()->create([

                            'product_id'   => $product->id,
                            'attribute_id' => $attr,
                            'option_id'    => $request->input('option_'.$attr),

                        ]); // end of create

                    } // end of foreach attribute and option
        
                } // end of if insert attributes and options
        
                // uploade multiple images 
                if($request->hasFile('images'))
                {
        
                    $files = $request->file('images');
                    
                    UploadeMulipleImage($files , $product);
        
                } // end of has files 
                
                return redirect()->route('web.profile.product')->with('success' , 'تم اضافة الاعلان بنجاح');
                // ================================ السيارات ==================================
        
            } else 
            {

                // ================================ العقارات ==================================
                
                // except key from request 
                $attribute = $request->except('token' , 'image_one' , 'image_two' , 'image_three' , 'images' , 'dir_id');

                $attribute = [
                    
                    'address'           => $request->address ? $request->address : null,
                    'lat'               => $request->lat ? $request->lat : null,
                    'lng'               => $request->lng ? $request->lng : null,
                    'name'              => $request->name ? $request->name : null,
                    'subcategory_id'    => $request->subcategory_id ? $request->subcategory_id : null,
                    'neighborhood'      => $request->neighborhood ? $request->neighborhood : null,
                    'street_view'       => $request->street_view ? $request->street_view : null,
                    'number_of_roles'   => $request->number_of_roles ? $request->number_of_roles : null,
                    'number_of_shops'   => $request->number_of_shops ? $request->number_of_shops : null,
                    'space'             => $request->space ? $request->space : null,
                    'price'             => $request->price ? $request->price : null,
                    'city_id'           => $request->city_id ? $request->city_id : null,
                    'phone'             => $request->phone ? $request->phone : null,
                    'show_phone'        =>  $request->show_phone ? true : false,
                    'email'             => $request->email ? $request->email : null,
                    'status'            => $request->status ? $request->status : null,
                    'details'           => $request->details ? $request->details : null,
                    'image_one'         => $request->file('image_one')->store('product'),
                    'image_two'         => $request->file('image_two')->store('product'),
                    'image_three'       => $request->file('image_three')->store('product'),
                    'code'              => '#' . rand(0000000,9999999),
                    'user_id'           => auth()->user()->id,
                    'category_id'       => session('categoryId'),
                    
                    'number_bedroom'    => $request->number_bedroom ? $request->number_bedroom : null,
                    'men_boards'        => $request->men_boards ? $request->men_boards : null,
                    'women_councils'    => $request->women_councils ? $request->women_councils : null,
                    'number_halls'      => $request->number_halls ? $request->number_halls : null,
                    'number_kitchens'   => $request->number_kitchens ? $request->number_kitchens : null,
                    'number_bathrooms'  => $request->number_bathrooms ? $request->number_bathrooms : null,
                    'storehouse'        => $request->storehouse ? $request->storehouse : null,
                    
                    'fuel_id'           => $request->fuel_id ? $request->fuel_id : null,
                    'number_pumps'      => $request->number_pumps ? $request->number_pumps : null,
                    'fuel_tank_capacity'=> $request->fuel_tank_capacity ? $request->fuel_tank_capacity : null,
                    'on_highway'        => $request->on_highway ? $request->on_highway : null,
                    
                    
                    'street_or_lane_width' => $request->street_or_lane_width ? $request->street_or_lane_width : null,
                    'inside_height'        => $request->inside_height ? $request->inside_height : null,


                    'number_of_sections'        => $request->number_of_sections ? $request->number_of_sections : null,
                    'number_of_swimming_pools'  => $request->number_of_swimming_pools ? $request->number_of_swimming_pools : null,
                    
                    
                    'building_erea'  => $request->building_erea ? $request->building_erea : null,
                    
                    'number_of_master_bedrooms'  => $request->number_of_master_bedrooms ? $request->number_of_master_bedrooms : null,
                    
                    'number_of_bedrooms_for_one'  => $request->number_of_bedrooms_for_one ? $request->number_of_bedrooms_for_one : null,
                    'men_boards_for_one'  => $request->men_boards_for_one ? $request->men_boards_for_one : null,
                    'women_boards_for_one'  => $request->women_boards_for_one ? $request->women_boards_for_one : null,
                    'number_halls_for_one'  => $request->number_halls_for_one ? $request->number_halls_for_one : null,
                    
                    'number_of_bedrooms_for_two'  => $request->number_of_bedrooms_for_two ? $request->number_of_bedrooms_for_two : null,
                    'men_boards_for_two'  => $request->men_boards_for_two ? $request->men_boards_for_two : null,
                    'women_boards_for_two'  => $request->women_boards_for_two ? $request->women_boards_for_two : null,
                    'number_halls_for_two'  => $request->number_halls_for_two ? $request->number_halls_for_two : null,
                    'shops'                 => $request->shops ? $request->shops : null,
                
                ];                

                // dd($attribute);
                $product = $this->productrepository->create($attribute);

                // insert attribute and options
                if ($request->attribute) {
                    
                    foreach($request->attribute as $key=>$attr)
                    {

                        $product->attributeandoption()->create([

                            'product_id'   => $product->id,
                            'attribute_id' => $attr,
                            'option_id'    => $request->input('option_'.$attr),

                        ]); // end of create

                    } // end of foreach attribute and option
        
                } // end of if insert attributes and options

                if ($request->dir_id) {

                   foreach ($request->dir_id as $ddir)
                   {
                        for ($i = 0; $i < 8; $i++)
                        {

                            $product->directione()->create([

                                'directione_id' => $request->direction_id[$i],
                                'dire_id'        => $ddir,
                                'product_id'    => $product->id,
                                'value'         => $request->input('direction_'.$request->direction_id[$i].'_'.$ddir),

                            ]); // end of create

                        }

                    } // end foreach

                } // end of dir id


                // uploade multiple images 
                if($request->hasFile('images'))
                {
                    
                    $files = $request->file('images');
        
                    UploadeMulipleImage($files , $product);
        
                } // end of has files 
                
                return redirect()->route('web.profile.product')->with('success' , 'تم اضافة الاعلان بنجاح');
                // ================================ العقارات ==================================
        
            }


    } // end of store product

    public function updateProduct(ProductUpdateRequest $request)
    {

            if (session('categoryId') == 1) {
                // ================================ السيارات ==================================
                $attrubute = [];

                $old_pro = $this->productrepository->findOne($request->item);
    
                // uploade one image_one
                if($request->has('image_one'))
                {
        
                    // Upload new image_one
                    $img_one = $request->file('image_one')->store('product');
                
                } // end of has image_one
                
                $attrubute['image_one'] = isset($img_one) ? $img_one : $old_pro->image_one;
    
                // uploade one image_two
                if($request->has('image_two'))
                {

                    // Upload new image_two
                    $img_two = $request->file('image_two')->store('product');
                
                } // end of has image_two  

                
                // uploade one image_three
                if($request->has('image_three'))
                {
        
                    // Upload new image_three
                    $img_three = $request->file('image_three')->store('product');
                
                } // end of has image_three  

                $attrubute['image_one']    = isset($img_one) ? $img_one : $old_pro->image_one;
                $attrubute['image_three']  = isset($img_three) ? $img_three : $old_pro->image_three;
                $attrubute['image_two']    = isset($img_two) ? $img_two : $old_pro->image_two;
                $attrubute['code']         = $old_pro->code;
                $attrubute['user_id']      = auth()->user()->id;
                $attrubute['category_id']  = session('categoryId');
                $attrubute['show_phone']   =  $request->show_phone ? true : false;
                $attrubute['the_walker']   =  $request->the_walker;
                $attrubute['make_year']    =  $request->make_year;
                $attrubute['category']     =  $request->category;
                $attrubute['price']        =  $request->price;
                $attrubute['city_id']      =  $request->city_id;
                $attrubute['phone']        =  $request->phone;
                $attrubute['email']        =  $request->email;
                $attrubute['name']         =  $request->name;
                $attrubute['name_company'] =  $request->name_company;
                $attrubute['capacity']     =  $request->capacity;
                $attrubute['status']       =  $request->status;
                $attrubute['address']      =  $request->address;
                $attrubute['lng']          =  $request->lng;
                $attrubute['lat']          =  $request->lat;
                $attrubute['addition_id']  =  $request->addition_id;
                $attrubute['details']      =  $request->details;
        
                $product = $this->productrepository->update($attrubute , $old_pro->id);
        
                // insert attribute and options
                if ($request->attribute) {
                    
                    $product->attributeandoption()->delete();
                    
                    foreach($request->attribute as $key=>$attr)
                    {


                        $product->attributeandoption()->create([

                            'product_id'   => $product->id,
                            'attribute_id' => $attr,
                            'option_id'    => $request->input('option_'.$attr),

                        ]); // end of create

                    } // end of foreach attribute and option
        
                } // end of if insert attributes and options
        
                // uploade multiple images 
                if($request->hasFile('images'))
                {

                    $files = $request->file('images');
                    
                    UploadeMulipleImage($files , $product);
        
                } // end of has files 
                
                return redirect()->route('web.profile.product')->with('success' , 'تم تعديل الاعلان بنجاح');                
                // ================================ السيارات ==================================
        
            } else 
            {

                // ================================ العقارات ==================================
                // dd($request->all());

                $attribute = $request->except('token' , 'image_one' , 'image_two' , 'image_three' , 'images');

                $old_pro = $this->productrepository->findOne($request->item);

                $attribute = [
                    
                    'address'           => $request->address ? $request->address : null,
                    'lat'               => $request->lat ? $request->lat : null,
                    'lng'               => $request->lng ? $request->lng : null,
                    'name'              => $request->name ? $request->name : null,
                    'subcategory_id'    => $request->subcategory_id ? $request->subcategory_id : null,
                    'neighborhood'      => $request->neighborhood ? $request->neighborhood : null,
                    'border'            => $request->border ? $request->border : null,
                    'street_view'       => $request->street_view ? $request->street_view : null,
                    'number_of_roles'   => $request->number_of_roles ? $request->number_of_roles : null,
                    'number_of_shops'   => $request->number_of_shops ? $request->number_of_shops : null,
                    'space'             => $request->space ? $request->space : null,
                    'price'             => $request->price ? $request->price : null,
                    'city_id'           => $request->city_id ? $request->city_id : null,
                    'phone'             => $request->phone ? $request->phone : null,
                    'show_phone'        =>  $request->show_phone ? true : false,
                    'email'             => $request->email ? $request->email : null,
                    'status'            => $request->status ? $request->status : null,
                    'details'           => $request->details ? $request->details : null,
                    'image_one'         => $request->image_one ? $request->file('image_one')->store('product') : $old_pro->image_one,
                    'image_two'         => $request->image_two ? $request->file('image_two')->store('product') : $old_pro->image_two,
                    'image_three'       => $request->image_three ? $request->file('image_three')->store('product') : $old_pro->image_three,
                    'code'              => $old_pro->code,
                    'user_id'           => auth()->user()->id,
                    'category_id'       => session('categoryId'),
                    
                    'number_bedroom'    => $request->number_bedroom ? $request->number_bedroom : null,
                    'men_boards'        => $request->men_boards ? $request->men_boards : null,
                    'women_councils'    => $request->women_councils ? $request->women_councils : null,
                    'number_halls'      => $request->number_halls ? $request->number_halls : null,
                    'number_kitchens'   => $request->number_kitchens ? $request->number_kitchens : null,
                    'number_bathrooms'  => $request->number_bathrooms ? $request->number_bathrooms : null,
                    'storehouse'        => $request->storehouse ? $request->storehouse : null,
                    
                    'fuel_id'           => $request->fuel_id ? $request->fuel_id : null,
                    'number_pumps'      => $request->number_pumps ? $request->number_pumps : null,
                    'fuel_tank_capacity'=> $request->fuel_tank_capacity ? $request->fuel_tank_capacity : null,
                    'on_highway'        => $request->on_highway ? $request->on_highway : null,
                    
                    
                    'street_or_lane_width' => $request->street_or_lane_width ? $request->street_or_lane_width : null,
                    'inside_height'        => $request->inside_height ? $request->inside_height : null,


                    'number_of_sections'        => $request->number_of_sections ? $request->number_of_sections : null,
                    'number_of_swimming_pools'  => $request->number_of_swimming_pools ? $request->number_of_swimming_pools : null,
                    
                    
                    'building_erea'  => $request->building_erea ? $request->building_erea : null,
                    
                    'number_of_master_bedrooms'  => $request->number_of_master_bedrooms ? $request->number_of_master_bedrooms : null,
                    
                    'number_of_bedrooms_for_one' => $request->number_of_bedrooms_for_one ? $request->number_of_bedrooms_for_one : null,
                    'men_boards_for_one'         => $request->men_boards_for_one ? $request->men_boards_for_one : null,
                    'women_boards_for_one'       => $request->women_boards_for_one ? $request->women_boards_for_one : null,
                    'number_halls_for_one'       => $request->number_halls_for_one ? $request->number_halls_for_one : null,
                    
                    'number_of_bedrooms_for_two' => $request->number_of_bedrooms_for_two ? $request->number_of_bedrooms_for_two : null,
                    'men_boards_for_two'         => $request->men_boards_for_two ? $request->men_boards_for_two : null,
                    'women_boards_for_two'       => $request->women_boards_for_two ? $request->women_boards_for_two : null,
                    'number_halls_for_two'       => $request->number_halls_for_two ? $request->number_halls_for_two : null,
                    'shops'                      => $request->shops ? $request->shops : null,
                
                ];   

                $product = $this->productrepository->update($attribute , $request->item);

                // insert attribute and options
                if ($request->attribute) {
                    
                    // delete attribute
                    $product->attributeandoption()->delete();

                    foreach($request->attribute as $key=>$attr)
                    {

                        $product->attributeandoption()->create([

                            'product_id'   => $product->id,
                            'attribute_id' => $attr,
                            'option_id'    => $request->input('option_'.$attr),

                        ]); // end of create

                    } // end of foreach attribute and option
        
                } // end of if insert attributes and options




                if ($request->dir_id) {

                    // delete directione 
                    $product->directione()->delete();                    

                    foreach ($request->dir_id as $ddir)
                    {
                         for ($i = 0; $i < 8; $i++)
                         {
 
                             $product->directione()->create([
 
                                 'directione_id' => $request->direction_id[$i],
                                 'dire_id'        => $ddir,
                                 'product_id'    => $product->id,
                                 'value'         => $request->input('direction_'.$request->direction_id[$i].'_'.$ddir),
 
                             ]); // end of create
 
                         }
 
                     } // end foreach
 
                 } // end of dir id                


                // uploade multiple images 
                if($request->hasFile('images'))
                {
                    
                    $files = $request->file('images');
        
                    UploadeMulipleImage($files , $product);
        
                } // end of has files 
                
                return redirect()->route('web.profile.product')->with('success' , 'تم تعديل الاعلان بنجاح');                
                // ================================ العقارات ==================================
        
            }


    } // end of store product
    
    public function editProduct($slug)
    {

        $product        = $this->productrepository->getOrWhereAndWhereIn([['slug' , $slug]] , [] , 'category_id' , auth()->user()->category()->pluck('category_id')->toArray())->first();
        $attributes     = $this->attributerepository->getOrWhereAndWhereIn([['cat'  , '!=' , 'not']] , [['cat' , Null]] , 'category_id' , auth()->user()->category()->pluck('category_id')->toArray() , ['column' => 'id', 'dir' => 'asc']);
        $subcategories  = $this->categoryrepository->getWhere(['parent_id' => $product->category_id] , ['column' => 'id', 'dir' => 'asc']);
        $additions      = $this->categoryrepository->getWhere(['type' => 'adds'] , ['column' => 'id', 'dir' => 'asc']);
        $otheradditions = $this->categoryrepository->getWhere(['type' => 'other_add'] , ['column' => 'id', 'dir' => 'asc'])->first();
        $cities         = $this->cityrepository->getAll();
        $commission     = $this->settingrepository->getWhere(['key' => 'tax'])->first();
        $day            = $this->settingrepository->getWhere(['key' => 'day'])->first();
        $directions     = $this->directionrepository->getAll();
        $dirs           = $this->dirrepository->getAll();

        return view('website.products.edit-product' , compact('directions' , 'commission' , 'day' , 'product' , 'cities' , 'attributes' , 'subcategories' , 'additions' , 'otheradditions' , 'dirs'));

    } // end of edit product

    public function deleteProduct(Request $request)
    {

        $product = $this->productrepository->findOne($request->id);

        $product->delete();

        return response()->json();

    } // end of destory

    public function deleteImage(Request $request)
    {

        $image = $this->imagerepository->findOne($request->id);

        $image->delete();

        return response()->json();

    } // end of delete image

    public function reportProduct(Request $request)
    {
        // dd($request->all());
        $product = $this->productrepository->findOne($request->id);

        if($product)
        {

            $check_favorite = $product->reports->contains('user_id' , auth()->user()->id);

            if(! $check_favorite)
            {
                
                $product->reports()->create([
                    'user_id'    => auth()->user()->id,
                    'product_id' => $request->id,
                    'report' => $request->report,
                ]);

                return redirect()->back()->with('success' , 'تم الابلاغ بنجاح');

            }

        } else 
        {

            return view('website.errors.notfound');

        } // end of else if product
    }

} // end of class
