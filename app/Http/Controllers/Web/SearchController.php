<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Repositories\AttributeRepositoryInterface;
use App\Repositories\AuthenticationRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\DirectioneRepositoryInterface;
use App\Repositories\PriceFillterRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public $productrepository;
    public $categoryrepository;
    public $authrepository;
    public $attributerepository;
    public $cityrepository;
    public $pricerepository;
    public $directionrepository;

    public function __construct(ProductRepositoryInterface $productrepository,
                                CategoryRepositoryInterface $categoryrepository,
                                AuthenticationRepositoryInterface $authrepository,
                                AttributeRepositoryInterface $attributerepository,
                                CityRepositoryInterface $cityrepository,
                                PriceFillterRepositoryInterface $pricerepository,
                                DirectioneRepositoryInterface $directionrepository
    )
    {

        $this->productrepository   = $productrepository;
        $this->categoryrepository  = $categoryrepository;
        $this->authrepository      = $authrepository;
        $this->attributerepository = $attributerepository;
        $this->cityrepository      = $cityrepository;
        $this->pricerepository     = $pricerepository;
        $this->directionrepository = $directionrepository;

    } // end of construct
    
    public function searchSimple(Request $request)
    {

        if (session('categoryId') == 1) {

            $all = [
                'company'   => $request->company_name ? $request->company_name : "''",
                'type'      => $request->type ? $request->type : "''",
                'category'  => $request->category ? $request->category : "''",
                'make_year' => $request->make_year ? $request->make_year : "''",
            ];
            
            //======================================== Cars ==========================================
            $products = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);
            
            $products
            ->where(function($q) use ($all)
            {

                $q->where('name_company'     , 'LIKE', '%'.$all['company'].'%');
                $q->orWhere('subcategory_id' , $all['type']);
                $q->orWhere('category'       , 'LIKE', '%'.$all['category'].'%');
                $q->orWhere('make_year'      , 'LIKE', '%'.$all['make_year'].'%');

            })->where('category_id' , 1);

            $products = $products->orderBy('id' , 'asc')->get();          
            
            return view('website.search.search-result' , compact('products'));

            //======================================== Cars ==========================================

        } else
        {

            //================================= realestate ==========================================
            $all = [
                'space'          => $request->space ? $request->space : "''",
                'type'           => $request->type ? $request->type : "''",
                'number_street'  => $request->number_street ? $request->number_street : "''",
                'street_view'    => $request->street_view ? $request->street_view : "''",
            ];
            // dd($all);
            
            //======================================== Cars ==========================================
            $products = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);
            
            $products
            ->where(function($q) use ($all)
            {

                $q->where('space'            , 'LIKE', '%'.$all['space'].'%');
                $q->orWhere('subcategory_id' , $all['type']);
                $q->orWhere('street_view'    , $all['street_view'])
                ->orWhereHas('attributeandoption.option' , function ($e) use ($all)
                {
                    
                    $e->where('name' , $all['number_street']);
                    
                });               

            })->where('category_id' , 2);

            $products = $products->orderBy('id' , 'asc')->get();          
            
            return view('website.search.search-result' , compact('products'));

            //================================= realestate ==========================================

        } // end of category id == 1

    } // end of search simple

    public function searchAdvanced()
    {

        $attributes     = $this->attributerepository->getOrWhere([['category_id' , session('categoryId')] , ['cat'  , '!=' , 'not']] , [['category_id' , session('categoryId')] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);
        $subcategories  = $this->categoryrepository->getWhere(['parent_id' => session('categoryId')] , ['column' => 'id', 'dir' => 'asc']);
        $additions      = $this->categoryrepository->getWhere(['type' => 'adds'] , ['column' => 'id', 'dir' => 'asc']);
        $otheradditions = $this->categoryrepository->getWhere(['type' => 'other_add'] , ['column' => 'id', 'dir' => 'asc'])->first();
        $cities         = $this->cityrepository->getAll();
        $prices         = $this->pricerepository->getAll();
        $companies      = $this->productrepository->getWhere([['category_id' , 1]]);
        
        return view('website.search.search-advanced' , compact(

            'prices',
            'companies',
            'attributes',
            'subcategories',
            'additions',
            'otheradditions',
            'cities'

        ));

    } // end of search advanced

    public function getAttributeSearch(Request $request)
    {
        $attributes = $this->attributerepository->getOrWhere([['category_id' , $request->id] , ['cat'  , '!=' , 'not']] , [['category_id' , $request->id] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);

        $subcategory = $request->id;

        $directions     = $this->directionrepository->getAll();

        $prices         = $this->pricerepository->getAll();

        $cities         = $this->cityrepository->getAll();       

        return view('website.buildings.append-attribute-search' , compact('cities' , 'prices' ,'directions' , 'attributes' , 'subcategory'))->render();

    } // get attribute search

    public function searchMap(Request $request)
    {
        
        if (session('categoryId') == 2) {

            // dd($request->all());
            //================================= realestate ==========================================
            $all = [

                'space'          => $request->space ? $request->space : "''",
                'type'           => $request->type ? $request->type : "''",
                'city_id'        => $request->city_id ? $request->city_id : "''",

            ];
            
            
            $price = '';
            $from  = '';
            $to    = '';

            if ($request->price_id > 0) 
            {

                $price = $this->pricerepository->getWhere(['id' => $request->price_id])->first();

                if ($price) {

                   $from =  $price->price_from;
                   $to   =  $price->price_to;

                } 

            } // end of if request price_id            

            //======================================== Cars ==========================================
            $products = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);
            
            $products
            ->where(function($q) use ($all , $from , $to)
            {

                $q->orWhere('subcategory_id' , $all['type']);
                $q->orWhere('city_id'        , $all['city_id']);
                $q->orWhere('space'        , $all['space']);
                $q->orWhereBetween('price'   , array($from , $to));
              

            })->where('category_id' , 2);

            $products = $products->orderBy('id' , 'asc')->get();          

            //================================= realestate ==========================================

            $subcategories  = $this->categoryrepository->getWhere(['parent_id' => session('categoryId')] , ['column' => 'id', 'dir' => 'asc']);
            $cities         = $this->cityrepository->getAll();
            $prices         = $this->pricerepository->getAll();
            
            return view('website.search.map' , compact(
    
                'prices',
                'subcategories',
                'cities',
                'products'

            ));

        } else 
        {

            return view('website.errors.notfound');

        } // end of else if

    } // end of search map    

    public function getOtherAddition(Request $request)
    {

        $options = $this->attributerepository->getOrWhere([['parent_id' , $request->id] , ['cat'  , '!=' , 'not']] , [['parent_id' , $request->id] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);

        return view('website.cars.append-other-addition' , compact('options'))->render();

    } // get other addition

    public function searchAdvancedStore(Request $request)
    {
        // dd($request->all());
        if (session('categoryId') == 1) {

            //======================================== Cars ==========================================

            $all = [
                'name_company'   => $request->name_company ? array_map('intval', $request->name_company) : "''",
                'make_year'   => $request->make_year ? $request->make_year : "''",
                'type'      => $request->type ? $request->type : "''",
                'category'  => $request->category ? $request->category : "''",
                'city_id' => $request->city_id ? $request->city_id : "''",
                'addition_id' => $request->addition_id ? $request->addition_id : "''",
                'status' => $request->status ? $request->status : "''",
            ];             

            $price = '';
            $from  = '';
            $to    = '';

            if ($request->price_id > 0) 
            {

                $price = $this->pricerepository->getWhere(['id' => $request->price_id])->first();

                if ($price) {

                   $from =  $price->price_from;
                   $to   =  $price->price_to;

                } 

            } // end of if request price_id

            $products = Product::with(['attributeandoption.option']);

            $products
            ->where(function($q) use ($request , $from , $to , $all)
            {

                $q->where('make_year'        , $all['make_year']);
                $q->orWhere('category'       , 'LIKE' , '%'. $all['category'].'%');
                $q->orWhere('subcategory_id' , $all['type']);
                $q->orWhere('city_id'        , $all['city_id']);
                $q->orWhere('addition_id'    , $all['addition_id']);
                $q->orWhere('status'         , $all['status']);
                $q->orWhereBetween('price'   , array($from , $to));
                $q->orWhereIn('id'           , [$all['name_company']])
                ->orWhereHas('attributeandoption.option' , function ($q) use ($request)
                {

                    $q->whereIn('id' , $request->option);
        
                });

            })->where('category_id' , 1);

            $products = $products->orderBy('id' , 'asc')->get();          

            return view('website.search.search-result' , compact('products'));
    
            //======================================== Cars ==========================================

        } else
        {

            //================================= realestate ==========================================
            
            #/////////////////////////////////////// BEGIN: all request /////////////////////////////

            $all = [];
            if ($request->neighborhood) {

                $all['neighborhood'] = $request->neighborhood ? $request->neighborhood : '';
                
            } 
            if($request->border)
            {
                $all['border'] = $request->border ? $request->border : '';
                
            } 
            if($request->street_view)
            {
                $all['street_view'] = $request->street_view ? $request->street_view : '';
                
            } 
            if($request->number_of_roles)
            {
                $all['number_of_roles'] = $request->number_of_roles ? $request->number_of_roles : '';
                
            } 
            if($request->number_of_shops)
            {
                $all['number_of_shops'] = $request->number_of_shops ? $request->number_of_shops : '';
                
            } 
            if($request->space)
            {
                $all['space'] = $request->space ? $request->space : '';
                
            } 
            if($request->city_id)
            {
                $all['city_id'] = $request->city_id ? $request->city_id : '';
                
            } 
            if($request->number_bedroom)
            {
                $all['number_bedroom'] = $request->number_bedroom ? $request->number_bedroom : '';
                
            } 
            if($request->men_boards)
            {
                $all['men_boards'] = $request->men_boards ? $request->men_boards : '';
                
            } 
            if($request->women_councils)
            {
                $all['women_councils'] = $request->women_councils ? $request->women_councils : '';
                
            } 
            if($request->number_halls)
            {
                $all['number_halls'] = $request->number_halls ? $request->number_halls : '';
                
            } 
            if($request->number_kitchens)
            {
                $all['number_kitchens'] = $request->number_kitchens ? $request->number_kitchens : '';
                
            } 
            if($request->number_bathrooms)
            {
                $all['number_bathrooms'] = $request->number_bathrooms ? $request->number_bathrooms : '';
                
            } 
            if($request->storehouse)
            {
                $all['storehouse'] = $request->storehouse ? $request->storehouse : '';
                
            } 
            if($request->fuel_id)
            {
                $all['fuel_id'] = $request->fuel_id ? $request->fuel_id : '';
                
            } 
            if($request->number_pumps)
            {
                $all['number_pumps'] = $request->number_pumps ? $request->number_pumps : '';
                
            } 
            if($request->fuel_tank_capacity)
            {
                $all['fuel_tank_capacity'] = $request->fuel_tank_capacity ? $request->fuel_tank_capacity : '';
                
            } 
            if($request->on_highway)
            {
                $all['on_highway'] = $request->on_highway ? $request->on_highway : '';
                
            } 
            if($request->street_or_lane_width)
            {
                $all['street_or_lane_width'] = $request->street_or_lane_width ? $request->street_or_lane_width : '';
                
            } 
            if($request->inside_height)
            {
                $all['inside_height'] = $request->inside_height ? $request->inside_height : '';
                
            } 
            if($request->number_of_sections)
            {
                $all['number_of_sections'] = $request->number_of_sections ? $request->number_of_sections : '';
                
            } 
            if($request->number_of_swimming_pools)
            {
                $all['number_of_swimming_pools'] = $request->number_of_swimming_pools ? $request->number_of_swimming_pools : '';
                
            } 
            if($request->building_erea)
            {
                $all['building_erea'] = $request->building_erea ? $request->building_erea : '';
                
            } 
            if($request->number_of_master_bedrooms)
            {
                $all['number_of_master_bedrooms'] = $request->number_of_master_bedrooms ? $request->number_of_master_bedrooms : '';
                
            } 
            if($request->number_of_bedrooms_for_one)
            {
                $all['number_of_bedrooms_for_one'] = $request->number_of_bedrooms_for_one ? $request->number_of_bedrooms_for_one : '';
                
            } 
            if($request->men_boards_for_one)
            {
                $all['men_boards_for_one'] = $request->men_boards_for_one ? $request->men_boards_for_one : '';
                
            } 
            if($request->women_boards_for_one)
            {
                $all['women_boards_for_one'] = $request->women_boards_for_one ? $request->women_boards_for_one : '';
                
            } 
            if($request->number_halls_for_one)
            {
                $all['number_halls_for_one'] = $request->number_halls_for_one ? $request->number_halls_for_one : '';
                
            } 
            if($request->men_boards_for_two)
            {
                $all['men_boards_for_two'] = $request->men_boards_for_two ? $request->men_boards_for_two : '';
                
            } 
            if($request->women_boards_for_two)
            {
                $all['women_boards_for_two'] = $request->women_boards_for_two ? $request->women_boards_for_two : '';
                
            } 
            if($request->number_halls_for_two)
            {
                $all['number_halls_for_two'] = $request->number_halls_for_two ? $request->number_halls_for_two : '';
                
            } 
            if($request->shops)
            {
                $all['shops'] = $request->shops ? $request->shops : '';
                
            }
            if($request->city_id)
            {
                $all['city_id'] = $request->city_id ? $request->city_id : '';
                
            }


            #/////////////////////////////////////// END: all request /////////////////////////////
            

            #/////////////////////////////////////// BEGIN: attribute and option /////////////////////////////
            
            $optionProduct  = [];
            
            if ($request->option) 
            {
                                
                foreach ($request->option as $value) {
                    
                    $optionProduct[]   = $value;
                    
                } // end of foreach
                
            } // end of if request attr


            #/////////////////////////////////////// END: attribute and option /////////////////////////////
            

            #/////////////////////////////////////// BEGIN: directione on category id = 6 /////////////////////////////
            
            $valueProduct = [];
            
            if ($request->direction) 
            {
                
                foreach ($request->direction as $value) {

                    if($value)
                    {

                        $valueProduct[] = $value;
                        
                    }
                    
                } // end of foreach
                
            } // end of if request attr

            #/////////////////////////////////////// END: deirection on category id = 6 /////////////////////////////
            

            #/////////////////////////////////////// BEGIN: get price from to from to  /////////////////////////////
            
            $price = '';
            $from  = '';
            $to    = '';
            
                
            $price = $this->pricerepository->getWhere(['id' => $request->price_id])->first();
            
            if ($price) {
                
                $from =  $price->price_from;
                $to   =  $price->price_to;
                
            }  else 
            {
                
                $from =  0;
                $to   =  0;
            }
                
            
            // dd($from , $to  , $valueProduct , $optionProduct , $all);

            #/////////////////////////////////////// END: get price from to from to  /////////////////////////////
            
            #/////////////////////////////////////// END: get price from to from to  /////////////////////////////
            
            $products = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' , 'directione']);

            $products
            ->where(function($q) use ($request , $all , $optionProduct , $from , $to , $valueProduct)
            {
                                
                foreach ($all as $key => $value) {

                    $q->orWhere($key   ,  $value);
                    
                }
                $q->orWhereBetween('price' , array($from , $to));
                $q->orWhereHas('attributeandoption.option' , function ($q) use ($optionProduct)
                {
                    
                    $q->whereIn('id' , $optionProduct);

                })->whereHas('directione' , function ($q) use ($valueProduct)
                {
                    
                    foreach ($valueProduct as $valu) {
                        
                        $q->where('value' , $valu);
                        
                    }
                    
                }); // end of where

            })->where([['category_id' , 2] , ['subcategory_id' , $request->subcategory_id]]);

            $products = $products->orderBy('id' , 'asc')->get();

            // dd($products);

            return view('website.search.search-result' , compact('products'));

            //================================= realestate ==========================================

        } // end of category id == 1        

    } // search advanced store

    public function searchPublic(Request $request)
    {
<<<<<<< HEAD
        
=======
>>>>>>> d7d1904f47785f02320bccfa496a93519b1548e7
        if (session('categoryId') == 1) {

 
            //======================================== Cars ==========================================
            $products = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);
            
            $products
            ->where(function($q) use ($request)
            {

                $q->where('name_company' , 'LIKE', '%'.$request->search.'%');
                $q->orWhere('name'       , 'LIKE', '%'.$request->search.'%');
                $q->orWhere('category'   , 'LIKE', '%'.$request->search.'%');
                $q->orWhere('make_year'  , 'LIKE', '%'.$request->search.'%');

            })->where('category_id' , 1);

            $products = $products->orderBy('id' , 'asc')->get();          
            
            return view('website.search.search-result' , compact('products'));

            //======================================== Cars ==========================================

        } else
        {

            //================================= realestate ==========================================
            
            $products = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);
            
            $products
            ->where(function($q) use ($request)
            {

                $q->where('space'         , 'LIKE', '%'.$request->search.'%');
                $q->orWhere('name'        , 'LIKE', '%'.$request->search.'%');
                $q->orWhere('street_view' , $request->search)
                ->orWhereHas('attributeandoption.option' , function ($e) use ($request)
                {
                    
                    $e->where('name' , $request->search);
                    
                });               

            })->where('category_id' , 2);

            $products = $products->orderBy('id' , 'asc')->get();          
            
            return view('website.search.search-result' , compact('products'));

            //================================= realestate ==========================================

        } // end of category id == 1        
    }

} // end of class
