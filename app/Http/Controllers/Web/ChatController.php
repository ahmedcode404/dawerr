<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\AuthenticationRepositoryInterface;
use Illuminate\Http\Request;

class ChatController extends Controller
{

    public $authrepository;

    public function __construct(AuthenticationRepositoryInterface $authrepository)
    {

        $this->authrepository = $authrepository;

    } // end of construct
    
    public function chat($slug)
    {

        $user = $this->authrepository->findOneSlug($slug);

        if($user && auth()->user() && $user->id != auth()->user()->id)
        {

            return view('website.chat' , compact('user'));

        } else 
        {

            return view('website.errors.notfound');

        }

    } // end of chat

    public function uploadFile(Request $request)
    {

        if ($request->hasFile('image')) {

                    // Upload new image_two
            $image = $request->file('image')->store('chat');
            
            return response()->json(['image' => $image]);

        } // end of if image           

    } // end of upload file

} // end of controller 
