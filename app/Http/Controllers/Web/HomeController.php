<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Repositories\AttributeRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\SliderRepositoryInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public $categoryrepository;
    public $sliderrepository;
    public $productrepository;
    public $attributerepository;

    public function __construct(CategoryRepositoryInterface $categoryrepository,
                                SliderRepositoryInterface $sliderrepository,
                                ProductRepositoryInterface $productrepository,
                                AttributeRepositoryInterface $attributerepository
                                
    )
    {

        $this->categoryrepository  = $categoryrepository;
        $this->sliderrepository    = $sliderrepository;
        $this->productrepository   = $productrepository;
        $this->attributerepository = $attributerepository;

    } // end of construct

    public function home($cat)
    {

        $category = $this->categoryrepository->getWhere(['name' => $cat])->first();

        if ($category) {

            // session category id
            session()->put('categoryId' , $category->id);
            // session category slug
            session()->put('categorySlug' , $category->name);

            // get sliders
            $sliders  = $this->sliderrepository->getWhere(['category_id' => session('categoryId')]);
            // get products
            $products = $this->productrepository->paginateWhereWith(['category_id' => session('categoryId')] , [] ,  ['column' => 'id', 'dir' => 'asc'] , 5);

            $types =  $this->categoryrepository->getWhere(['parent_id' => session('categoryId')] , ['column' => 'id', 'dir' => 'asc']);
            
            return view('website.index' , compact('category' , 'sliders' , 'products' , 'types'));

        } else 
        {

            return view('website.errors.notfound');

        } // end of category


    } // end of home

    public function addToFavorite(Request $request)
    {

        $project = $this->productrepository->findOne($request->id);

        if($project)
        {

            $check_favorite = $project->favorites->contains('user_id' , auth()->user()->id);

            if(! $check_favorite)
            {
                
                $project->favorites()->create([
                    'user_id'    => auth()->user()->id,
                    'product_id' => $request->id,
                ]);

                return response()->json(['status' => true]);

            } else 
            {

                $favorite = $project->favorites()->where([
                    'user_id'    => auth()->user()->id,
                    'product_id' => $request->id,
                ]);

                $favorite->delete();

                return response()->json(['status' => false]);

            } // end of else if chech favorite     

        } else 
        {

            return view('website.errors.notfound');

        } // end of else if product

    } // end of add to favorite

    public function pagination(Request $request) 
    {

        if(session('categoryId') == 1)
        {

            $products = $this->productrepository->paginateWhereWith(['category_id' => session('categoryId')] , [] ,  ['column' => 'id', 'dir' => 'asc'] , 5);
            
            return view('website.cars.pagination', compact('products'))->render();
            
        } else 
        {

            $products = $this->productrepository->paginateWhereWith(['category_id' => session('categoryId')] , [] ,  ['column' => 'id', 'dir' => 'asc'] , 5);
            
            return view('website.buildings.pagination', compact('products'))->render();

        }

    }

} // en of class
