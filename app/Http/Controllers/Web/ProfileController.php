<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\RegisterRequest;
use App\Repositories\AuthenticationRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\This;

class ProfileController extends Controller
{

    public $authenticationrepository;
    public $cityrepository;
    public $productrepository;

    public function __construct(AuthenticationRepositoryInterface $authenticationrepository,
                                CityRepositoryInterface $cityrepository,
                                ProductRepositoryInterface $productrepository
    )
    {
        $this->authenticationrepository = $authenticationrepository;
        $this->cityrepository           = $cityrepository;
        $this->productrepository        = $productrepository;
    } // end of construct

    public function profile()
    {

        return view('website.profile.profile');

    } // end of category
    
    public function edit()
    {

        $cities = $this->cityrepository->getAll();

        return view('website.profile.profile' , compact('cities'));

    } // end of category

    public function update(RegisterRequest $request)
    {

        if (auth()->user()->type_id == 1) {

            $attribute = $request->except(

                    'commercial_register' ,
                    'tax_number' ,
                    'password' ,
                    'confirm_password',
                    'name_bank',
                    'name_account_bank',
                    'number_account_bank',
                    'number_eban_bank',
                    'company_name'
                    
            );

            $attribute['first_name']  = $request->first_name;


        } else  
        {

            $attribute = $request->except(

                    'password' ,
                    'confirm_password' ,
                    'second_name' ,
                    'last_name',
                    'first_name'

            );
            
            $attribute['first_name']  = $request->company_name;
            
        } // end of else if auth user

        if($request->password) 
        {

            $attribute['password']  = bcrypt($request->password);

        }

        auth()->user()->update($attribute);

        session()->put('edit-user' , true);

        return redirect()->route('web.profile')->with('success' , 'تم تحديث البيانات بنجاح');

    } // end of update

    public function uploadImage(Request $request)
    {

        // uploade file image
        if ($request->hasFile('image')) {
        
            // Delete old internal_image
            Storage::delete(auth()->user()->id);

            // Upload new internal_image
            $attribute['image'] = $request->file('image')->store('user');

        } // end of if image

        auth()->user()->update($attribute);            

        return response()->json([

            'image'   => url('storage/' . auth()->user()->image),
            'user_id' => auth()->user()->id,
            
        ]);

    } // end of upload image

    public function getFavorite()
    {
        return view('website.profile.profile');
    }

    public function myProduct()
    {
        
        $products = auth()->user()->products()->paginate(1);



        return view('website.profile.profile' , compact('products'));

    } // end of my product

    public function doneSale(Request $request)
    {

        $product = $this->productrepository->findOne($request->id);

        $product->update(['status' => 'تم البيع' , 'date_sale' => now()]);

        return response()->json();

    } // end of done sale

} // end of class
