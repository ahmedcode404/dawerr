<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\StaticPageResource;
use App\Repositories\AgreementRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\StaticPageRepositoryInterface;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{

    use ApiResponseTrait;

    public $staticpagerepostiory;
    public $settingrepository;
    public $agreementrepository;

    public function __construct(StaticPageRepositoryInterface $staticpagerepostiory ,
                                SettingRepositoryInterface $settingrepository,
                                AgreementRepositoryInterface $agreementrepository
    )
    {

        $this->staticpagerepostiory = $staticpagerepostiory;
        $this->settingrepository    = $settingrepository;
        $this->agreementrepository  = $agreementrepository;
        
    } // end of construct

    public function statics($key)
    {
        
        $static = $this->staticpagerepostiory->getWhere(['key' => $key])->first();

        if ($static) {
            
            return $this->ApiResponse(new StaticPageResource($static) , 'تم' , 200);

        } else 
        {

            return $this->notFoundResponse();

        } // end of else static

    } // end of static

    public function agreement()
    {
        
        $agreement = $this->agreementrepository->getAll();

        if ($agreement->isNotEmpty()) {

            return $this->ApiResponse($agreement , 'تم' , 200);

        } else 
        {

            return $this->notFoundResponse();

        } // end of else 

    } // end of agreement

} // end of class
