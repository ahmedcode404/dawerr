<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PaymentRequest;
use App\Http\Resources\Api\BankAccountResource;
use App\Http\Resources\Api\BankResource;
use App\Http\Resources\Api\BankTransferResource;
use App\Repositories\BankAccountRepositoryInterface;
use App\Repositories\BankRepositoryInterface;
use App\Repositories\OrderRepositoryInterface;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    use ApiResponseTrait;

    public $bankrepository;
    public $bankaccountrepository;
    public $orderrepository;

    public function __construct(BankRepositoryInterface $bankrepository,
                                BankAccountRepositoryInterface $bankaccountrepository,
                                OrderRepositoryInterface $orderrepository
    )
    {

        $this->bankrepository        = $bankrepository;
        $this->bankaccountrepository = $bankaccountrepository;
        $this->orderrepository       = $orderrepository;
        
    } // end of construct

    public function bank()
    {

        $banks = $this->bankrepository->getAll();

        if ($banks->isNotEmpty()) {

            return $this->ApiResponse(BankResource::collection($banks) , 'تم' , 200 );

        } else 
        {

            return $this->notFoundResponse();

        } // end of else if banks
        
    }  // end of bank

    public function accountBank()
    {

        $accountbanks = $this->bankaccountrepository->getAll();

        if ($accountbanks->isNotEmpty()) {

            return $this->ApiResponse(BankAccountResource::collection($accountbanks) , 'تم' , 200 );

        } else 
        {

            return $this->notFoundResponse();

        } // end of else if banks
        
    }  // end of account Bank

    public function store(PaymentRequest $request)
    {

        $attribute = $request->except('image');

        // uploade one image
        if($request->has('image'))
        {

            // Upload new image
            $attribute['image'] = $request->file('image')->store('transfer');
        
        } // end of has image 

        $transfer = $this->orderrepository->create($attribute);

        return $this->ApiResponse(new BankTransferResource($transfer) , 'تم اتمام العمليه بنجاح' , 200);

    } // end of store
    
} // end of class
