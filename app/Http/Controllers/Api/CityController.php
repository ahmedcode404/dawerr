<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\CityResource;
use App\Repositories\CityRepositoryInterface;
use Illuminate\Http\Request;

class CityController extends Controller
{

    use ApiResponseTrait;

    public $cityrepository;

    public function __construct(CityRepositoryInterface $cityrepository)
    {

        $this->cityrepository = $cityrepository;

    } // end of construct

    public function city()
    {

        $cities = $this->cityrepository->getAll();

        if ($cities->isNotEmpty()) {

            return $this->ApiResponse(CityResource::collection($cities) , 'تم' , 200);
            
        } else 
        {

            return $this->notFoundResponse();

        } // end of esle if cities

    } // end of get city

} // end of class
