<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\OpposedResource;
use App\Http\Resources\Api\ProductAllResource;
use App\Http\Resources\Api\ProductResource;
use App\Http\Resources\Api\ProductSingelResource;
use App\Http\Resources\Api\UserResource;
use App\Repositories\AuthenticationRepositoryInterface;
use Illuminate\Http\Request;

class OpposedController extends Controller
{

    use ApiResponseTrait;
    
    public $authenticationrepository;

    public function __construct(AuthenticationRepositoryInterface $authenticationrepository)
    {
        
        $this->authenticationrepository = $authenticationrepository;

    } // end of construct

    public function getAllOpposed($id)
    {

        $opposeds = $this->authenticationrepository->whereHas('category' , ['category_id' => $id] , ['type_id' => 2] , ['column' => 'id', 'dir' => 'asc']);

        if ($opposeds->isNotEmpty()) {
            
            return $this->ApiResponse(OpposedResource::collection($opposeds) , 'تم' , 200 );

        } else 
        {

            return $this->notFoundResponse();

        } // end of if is not empty opposeds

    } // end of get all opposed

    public function getOpposed($id , $category)
    {

        $opposed = $this->authenticationrepository->whereHas('category' , ['category_id' => $category] , [['type_id' , 2] , ['id' , $id]] , ['column' => 'id', 'dir' => 'asc'])->first();

        if (isset($opposed)) {
            
            return $this->ApiResponse(new UserResource($opposed) , 'تم' , 200 );

        } else 
        {

            return $this->notFoundResponse();

        } // end of if is not empty opposeds

    } // end of get opposed

    public function getOpposedProduct($id , $category)
    {

        $opposed = $this->authenticationrepository->whereHas('category' , ['category_id' => $category] , [['type_id' , 2] , ['id' , $id]] , ['column' => 'id', 'dir' => 'asc'])->first();

        if ($opposed && $opposed->products->isNotEmpty()) {
            
            return $this->ApiResponse(ProductSingelResource::collection($opposed->products) , 'تم' , 200 );

        } else 
        {

            return $this->notFoundResponse();

        } // end of if is not empty opposeds

    } // end of get opposed

    public function fillter($city , $category)
    {
        
        $opposeds_fillter = $this->authenticationrepository->whereHas('category' , ['category_id' => $category] , [['type_id' , 2] , ['city_id' , $city]] , ['column' => 'id', 'dir' => 'asc']);

        if ($opposeds_fillter) {
            
            return $this->ApiResponse(OpposedResource::collection($opposeds_fillter) , 'تم' , 200 );

        } else 
        {

            return $this->notFoundResponse();

        } // end of else opposeds fillter

    } // end of fillter

} // end of class
