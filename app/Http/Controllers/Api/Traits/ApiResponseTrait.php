<?php

namespace App\Http\Controllers\Api\Traits;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait ApiResponseTrait
{

    public $paginateNumber = 10;

    public function ApiResponse($data = [] , $message = null, $code = 200)
    {
            $array =
            [
                'data'    => $data,
                'message' => $message,
                'status'  => in_array($code ,$this->successCode())? true : false
            ];

            return response()->json($array,$code);
    } // end of api response

    public function successCode()
    {

        return [
            200 ,201 ,202
        ];

    } // end of success code

    public function notFoundResponse()
    {

        return $this->ApiResponse(null , 'لا توجد بيانات' , 404);

    } // end of not found response

    public function notUser()
    {

        return $this->ApiResponse(null , 'لا يوجد مستخدم' , 302);

    }

    public function apiValidation($request , $array)
    {

        $validate = Validator::make($request->all() , $array);

        if ($validate->fails())
        {
            return $this->ApiResponse(null , $validate->errors() , 422);
        }

    } // end of api validate

}
