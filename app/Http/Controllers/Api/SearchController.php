<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SearchMapRequest;
use App\Http\Requests\Api\SearchSimpleRequest;
use App\Http\Resources\Api\PriceFillterResource;
use App\Http\Resources\Api\ProductAllResource;
use App\Http\Resources\Api\ProductResource;
use App\Http\Resources\Api\SearchMapResource;
use App\Models\Product;
use App\Repositories\PriceFillterRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    use ApiResponseTrait;
    
    public $productrepository;
    public $pricerepository;

    public function __construct(ProductRepositoryInterface $productrepository,
                                PriceFillterRepositoryInterface $pricerepository
    )
    {

        $this->productrepository = $productrepository;
        $this->pricerepository   = $pricerepository;
        
    } // end of construct

    public function searchSimple(SearchSimpleRequest $request)
    {

        if ($request->category_id == 1) {

            //======================================== Cars ==========================================
            $searchsimple = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);

            $searchsimple
            ->where(function($q) use ($request)
            {

                $q->where('name'         , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('price'      , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('make_year'  , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('the_walker' , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('details'    , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('address'    , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('status'     , 'LIKE', '%'.$request->search.'%')
//                ->orWhereHas('attributeandoption.option' , function ($q) use ($request)
//                {
//
//                    $q->where('name' , 'LIKE', '%'.$request->search.'%');
//
//                })->orWhereHas('attributeandoption.attribute' , function ($q) use ($request)
//                {
//
//                    $q->where('name' , 'LIKE', '%'.$request->search.'%');
//
//                })->orWhereHas('user' , function ($q) use ($request)
//                {
//
//                    $q->where('first_name'  , 'LIKE', '%'.$request->search.'%')
//                    ->orWhere('second_name' , 'LIKE', '%'.$request->search.'%')
//                    ->orWhere('last_name'   , 'LIKE', '%'.$request->search.'%');
//
//                });

            })->where('category_id' , 1);

            $searchsimple = $searchsimple->orderBy('id' , 'asc')->get();

            if ($searchsimple->isNotEmpty()) {

                return $this->ApiResponse(ProductResource::collection($searchsimple) , 'تم' , 200);

            } else
            {
    
                return $this->notFoundResponse();
    
            } // end of else if search simple

            //======================================== Cars ==========================================

        } else
        {

            //================================= realestate ==========================================
           
            $searchsimple = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);

            $searchsimple
            ->where(function($q) use ($request)
            {

                $q->where('name'         , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('price'      , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('details'    , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('address'    , 'LIKE', '%'.$request->search.'%');
//                $q->orWhere('status'     , 'LIKE', '%'.$request->search.'%')
//                ->orWhereHas('attributeandoption.option' , function ($q) use ($request)
//                {
//
//                    $q->where('name' , 'LIKE', '%'.$request->search.'%');
//
//                })->orWhereHas('attributeandoption.attribute' , function ($q) use ($request)
//                {
//
//                    $q->where('name' , 'LIKE', '%'.$request->search.'%');
//
//                })->orWhereHas('user' , function ($q) use ($request)
//                {
//
//                    $q->where('first_name'  , 'LIKE', '%'.$request->search.'%')
//                    ->orWhere('second_name' , 'LIKE', '%'.$request->search.'%')
//                    ->orWhere('last_name'   , 'LIKE', '%'.$request->search.'%');
//
//                });

            })->where('category_id' , 2);

            $searchsimple = $searchsimple->orderBy('id' , 'asc')->get();

            if ($searchsimple->isNotEmpty()) {
    
                return $this->ApiResponse(ProductResource::collection($searchsimple) , 'تم' , 200);
    
            } else 
            {
    
                return $this->notFoundResponse();
    
            } // end of else if search simple

            //================================= realestate ==========================================

        } // end of category id == 1

    }  // end of search simple

    public function searchAdvanced(Request $request)
    {

        if ($request->category_id == 1) {
//            dd($request->all());
            //======================================== Cars ==========================================
            $attributeProduct = [];
            $optionProduct    = [];

            if ($request->attr) 
            {

                $attr = json_decode($request->attr);

                foreach ($attr as $value) {
    
                    $optionProduct[]    = $value->option_id;
                    $attributeProduct[] = $value->attribute_id;
    
                } // end of foreach

            } // end of if request attr

            $price = '';
            $from  = '';
            $to    = '';

            if ($request->price_id > 0) 
            {

                $price = $this->pricerepository->getWhere(['id' => $request->price_id])->first();

                if ($price) {

                   $from =  $price->price_from;
                   $to   =  $price->price_to;

                } 

            } // end of if request price_id
//            dd($request->name_comapny);
            $searchsimple = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user' , 'type']);

            $searchsimple
            ->where(function($q) use ($request , $optionProduct , $attributeProduct , $from , $to)
            {

                $q->where('make_year'    , 'LIKE' , '%'. $request->make_year.'%');
                $q->orWhere('the_walker' , 'LIKE' , '%'. $request->the_walker.'%');
                $q->orWhere('category'   , 'LIKE' , '%'. $request->category.'%');
                $q->orWhere('city_id'    , $request->city_id);
                $q->orWhere('status'     , 'LIKE' , '%'. $request->status.'%');
                $q->orWhere('id'         , $request->name_comapny);
                $q->orWhereBetween('price' , array($from , $to))
                ->orWhereHas('attributeandoption.option' , function ($q) use ($optionProduct)
                {

                    $q->whereIn('id' , $optionProduct);
        
                })->orWhereHas('attributeandoption.attribute' , function ($q) use ($request , $attributeProduct)
                {

                    $q->whereIn('id' , $attributeProduct);
        
                })->orWhereHas('type' , function ($q) use ($request)
                {
        
                    $q->where('id'  , $request->type);
        
                });

            })->where('category_id' , 1);

            $searchsimple = $searchsimple->orderBy('id' , 'asc')->get();

            if ($searchsimple->isNotEmpty()) {
    
                return $this->ApiResponse(ProductResource::collection($searchsimple) , 'تم' , 200);
    
            } else 
            {
    
                return $this->notFoundResponse();
    
            } // end of else if search advanced car

            //======================================== Cars ==========================================

        } else
        {

            //================================= realestate ==========================================
            
            
            #/////////////////////////////////////// BEGIN: except /////////////////////////////
            
            $attrubute = attributeExceptSearch($request);
            unset($attrubute['attr']);
            unset($attrubute['category_id']);
            unset($attrubute['price_id']);
            unset($attrubute['directione']);
            unset($attrubute['status']);
            unset($attrubute['city_id']);

            #/////////////////////////////////////// END: except /////////////////////////////
            

            #/////////////////////////////////////// BEGIN: columns /////////////////////////////

            $keys    = [];
            $values  = [];
            if ($attrubute) {
                
                foreach ($attrubute as $key => $value) {
                    $keys[]    = $key;
                    $values[] = $value;
                }
                
            } // end of if attribute

            #/////////////////////////////////////// END: columns /////////////////////////////
            
            
            #/////////////////////////////////////// BEGIN: attribute and option /////////////////////////////
            
            $attributeProduct = [];
            $optionProduct    = [];
            
            if ($request->attr) 
            {
                
                $attr = json_decode($request->attr);
                
                foreach ($attr as $value) {
                    
                    $optionProduct[]    = $value->option_id;
                    $attributeProduct[] = $value->attribute_id;
                    
                } // end of foreach
                
            } // end of if request attr

            #/////////////////////////////////////// END: attribute and option /////////////////////////////
            
            #/////////////////////////////////////// BEGIN: directione on category id = 6 /////////////////////////////
            
            $directioneProduct = [];
            $valueProduct      = [];
            
            if ($request->directione) 
            {
                
                $attr = json_decode($request->directione);
                
                foreach ($attr as $value) {
                    
                    $directioneProduct[] = $value->directione_id;
                    $valueProduct[]      = $value->value;
                    
                } // end of foreach
                
            } // end of if request attr

            #/////////////////////////////////////// END: deirection on category id = 6 /////////////////////////////
            

            #/////////////////////////////////////// BEGIN: get price from to from to  /////////////////////////////
            
            $price = '';
            $from  = '';
            $to    = '';
            
            if ($request->price_id > 0) 
            {
                
                $price = $this->pricerepository->getWhere(['id' => $request->price_id])->first();
                
                if ($price) {
                    
                    $from =  $price->price_from;
                    $to   =  $price->price_to;
                    
                } 
                
            } // end of if request price_id

            #/////////////////////////////////////// END: get price from to from to  /////////////////////////////
            

            #/////////////////////////////////////// END: get price from to from to  /////////////////////////////
            
            $searchsimple = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' , 'type' , 'directione']);

            $searchsimple
            ->where(function($q) use ($request , $optionProduct , $attributeProduct , $from , $to , $keys , $values , $directioneProduct , $valueProduct)
            {

                foreach ($keys as $val => $key) {

                    $q->orWhere($key  , 'LIKE' , '%'. $values[$val].'%');

                } // end of foreach

                $q->orWhere('city_id'    , $request->city_id);
                $q->orWhereBetween('price' , array($from , $to))
                ->orWhereHas('attributeandoption.option' , function ($q) use ($optionProduct)
                {

                    $q->whereIn('id' , $optionProduct);
        
                })->orWhereHas('attributeandoption.attribute' , function ($q) use ($request , $attributeProduct)
                {

                    $q->whereIn('id' , $attributeProduct);
        
                })->orWhereHas('type' , function ($q) use ($request)
                {
        
                    $q->where('name'  , 'LIKE' , '%'. $request->type .'%');
        
                })->orWhereHas('directione' , function ($q) use ($request , $directioneProduct , $valueProduct)
                {

                    foreach ($valueProduct as $valu) {

                        $q->where('value' , 'LIKE' , '%'. $valu .'%');

                    }
        
                }); // end of where

            })->where([['category_id' , 2] , ['subcategory_id' , $request->subcategory_id]]);

            $searchsimple = $searchsimple->get();

            if ($searchsimple->isNotEmpty()) {
    
                return $this->ApiResponse(ProductResource::collection($searchsimple) , 'تم' , 200);
    
            } else 
            {
    
                return $this->notFoundResponse();
    
            } // end of else if search advanced realestate

            //================================= realestate ==========================================

        } // end of category id == 1

    } // end of search advanced

    public function searchMap(SearchMapRequest $request)
    {

        //================================= realestate ==========================================
        
        $searchsimple = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);

        $searchsimple
        ->where(function($q) use ($request)
        {

            $q->where('name'         , 'LIKE', '%'.$request->search.'%');
            $q->orWhere('price'      , 'LIKE', '%'.$request->search.'%');
            $q->orWhere('details'    , 'LIKE', '%'.$request->search.'%');
            $q->orWhere('address'    , 'LIKE', '%'.$request->search.'%');
            $q->orWhere('status'     , 'LIKE', '%'.$request->search.'%')
            ->orWhereHas('attributeandoption.option' , function ($q) use ($request)
            {
    
                $q->where('name' , 'LIKE', '%'.$request->search.'%');
    
            })->orWhereHas('attributeandoption.attribute' , function ($q) use ($request)
            {
    
                $q->where('name' , 'LIKE', '%'.$request->search.'%');
    
            })->orWhereHas('user' , function ($q) use ($request)
            {
    
                $q->where('first_name'  , 'LIKE', '%'.$request->search.'%')
                ->orWhere('second_name' , 'LIKE', '%'.$request->search.'%')
                ->orWhere('last_name'   , 'LIKE', '%'.$request->search.'%');
    
            });

        })->where('category_id' , 2);

        $searchsimple = $searchsimple->orderBy('id' , 'asc')->get();

        if ($searchsimple->isNotEmpty()) {

            return $this->ApiResponse(ProductResource::collection($searchsimple) , 'تم' , 200);

        } else 
        {

            return $this->notFoundResponse();

        } // end of else if search simple

        //================================= realestate ==========================================

    } // end of search map

    public function fillterMap(Request $request)
    {

        //================================= realestate ==========================================
        
        $price = '';
        $from  = '';
        $to    = '';

        if ($request->price_id > 0) 
        {

            $price = $this->pricerepository->getWhere(['id' => $request->price_id])->first();

            if ($price) {

               $from =  $price->price_from;
               $to   =  $price->price_to;

            } 

        } // end of if request price_id        

        $searchsimple = Product::with(['attributeandoption.attribute' , 'attributeandoption.option' ,  'user']);

        $searchsimple
        ->where(function($q) use ($request , $from , $to)
        {

            $q->where('space'         , 'LIKE', '%'.$request->space.'%');
            $q->orWhere('address'    , 'LIKE', '%'.$request->address.'%');
            $q->orWhere('status'     , $request->status);
            $q->orWhereBetween('price' , array($from , $to));

        })->where('category_id' , 2);

        $searchsimple = $searchsimple->orderBy('id' , 'asc')->get();

        if ($searchsimple->isNotEmpty()) {

            return $this->ApiResponse(ProductResource::collection($searchsimple) , 'تم' , 200);

        } else 
        {

            return $this->notFoundResponse();

        } // end of else if search simple

        //================================= realestate ==========================================

    } // end of fillter map

    public function price()
    {
        
        $prices = $this->pricerepository->getAll();

        if ($prices) {

            return $this->ApiResponse(PriceFillterResource::collection($prices) , 'تم' , 200);

        } else 
        {

            return $this->notFoundResponse();

        } // end of if prices or no

    } // end of get prices

    public function company()
    {

        $companies = $this->productrepository->getWhere([['category_id' , 1]]);

        if($companies->isNotEmpty())
        {

            return $this->ApiResponse(ProductResource::collection($companies) , 'تم ' , 200 );
            
        } else 
        {

            return $this->ApiResponse(null , 'تم ' , 404 );

        }


    }

} // end of class
