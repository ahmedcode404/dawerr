<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ContactRequest;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    use ApiResponseTrait;

    public $contactrepository;
    public $settingrepository;

    public function __construct(ContactRepositoryInterface $contactrepository,
                                SettingRepositoryInterface $settingrepository
    )
    {

        $this->contactrepository = $contactrepository;
        $this->settingrepository = $settingrepository;

    } // end of construct
    
    public function store(ContactRequest $request)
    {
        
        $this->contactrepository->create($request->all());

        return $this->ApiResponse(null , 'تم ارسال الرسالة بنجاح' , 200);

    } // end of store

    public function contact()
    {

        $insta   = $this->settingrepository->getWhere(['key' => 'instagram'])->first();
        $face    = $this->settingrepository->getWhere(['key' => 'facebook'])->first();
        $snab    = $this->settingrepository->getWhere(['key' => 'snapchat'])->first();
        $twit    = $this->settingrepository->getWhere(['key' => 'twitter'])->first();
        $phone   = $this->settingrepository->getWhere(['key' => 'phone'])->first();
        $email   = $this->settingrepository->getWhere(['key' => 'email'])->first();
        $address = $this->settingrepository->getWhere(['key' => 'address'])->first();

        $array = [

            'instagram' => $insta->value,
            'facebook'  => $face->value,
            'snapchat'  => $snab->value,
            'twitter'   => $twit->value,
            'phone'     => $phone->value,
            'email'     => $email->value,
            'address'   => $address->value,

        ];

        return $this->ApiResponse($array , 'تم ' , 200);

    } // end of contact

} // end of class
