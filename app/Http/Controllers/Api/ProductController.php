<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ProductRequest;
use App\Http\Requests\Api\ReportRequest;
use App\Http\Resources\Api\ProductAllResource;
use App\Http\Resources\Api\ProductResource;
use App\Http\Resources\Api\ProductSingelResource;
use App\Repositories\ImageRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{

    use ApiResponseTrait;

    public $productrepository;
    public $settingrepository;
    public $imagerepository;

    public function __construct(ProductRepositoryInterface $productrepository ,
                                SettingRepositoryInterface $settingrepository,
                                ImageRepositoryInterface $imagerepository
    )
    {

        $this->productrepository = $productrepository;
        $this->settingrepository = $settingrepository;
        $this->imagerepository   = $imagerepository;

    } // end of construct

    public function getAllProduct($category)
    {
        
        $product_cars = $this->productrepository->paginateGetWhere(['category_id' => $category] , 10 , ['column' => 'id', 'dir' => 'desc']);

        if ($product_cars->isNotEmpty()) {
            
            return $this->ApiResponse(new ProductAllResource($product_cars) , 'تم' , 200);

        } else 
        {

            return $this->notFoundResponse();

        } // end of else if product cars

    } // end of get all product

    public function getProduct($id , $category)
    {
        
        $product = $this->productrepository->getWhere([['id' , $id] , ['category_id' , $category]])->first();

        if (isset($product)) {
            
            return $this->ApiResponse(new ProductSingelResource($product) , 'تم' , 200);

        } else 
        {

            return $this->notFoundResponse();

        } // end of else if product cars

    } // end of get product

    public function store(ProductRequest $request)
    {

        $user = JWTAuth::parseToken()->authenticate();
        
        if ($user) {

            if ($request->category_id == 1) {
                // ================================ السيارات ==================================
                $attrubute = $request->except(

                    'images' ,
                    'image_one' ,
                    'image_two' ,
                    'image_three' ,
                    'phone',
                    'number_of_shops',
                    'neighborhood',
                    'street_view',
                    'border',
                    'number_halls_for_two',
                    'women_boards_for_two',
                    'Men_boards_for_two',
                    'number_of_bedrooms_for_two',
                    'number_halls_for_one',
                    'women_boards_for_one',
                    'men_boards_for_one',
                    'number_of_bedrooms_for_one',
                    'shops',
                    'number_of_sections',
                    'inside_height',
                    'street_or_lane_width',
                    'fuel_tank_capacity',
                    'number_pumps',
                    'on_highway',
                    'storehouse',
                    'number_bathrooms',
                    'number_of_swimming_pools',
                    'number_Kitchens',
                    'number_halls',
                    'women_councils',
                    'men_boards',
                    'number_of_master_bedrooms',
                    'number_bedroom',
                    'number_of_roles',
                    'building_erea',
                    'space'

                );
    
                // uploade one image_one
                if($request->has('image_one'))
                {
        
                    // Upload new image_one
                    $attrubute['image_one'] = $request->file('image_one')->store('product');
                
                } // end of has image_one  
    
                // uploade one image_two
                if($request->has('image_two'))
                {

                    // Upload new image_two
                    $attrubute['image_two'] = $request->file('image_two')->store('product');
                
                } // end of has image_two  
                
                // uploade one image_three
                if($request->has('image_three'))
                {
        
                    // Upload new image_three
                    $attrubute['image_three'] = $request->file('image_three')->store('product');
                
                } // end of has image_three  
        
                $attrubute['code']    = '#' . rand(0000000,9999999);
                $attrubute['user_id'] = $user->id;
                $attrubute['category_id'] = $request->category_id;
                $attrubute['show_phone']   = $request->show_phone == 1 ? true : false;
        
                $product = $this->productrepository->create($attrubute);
        
                // insert attribute and options
                if ($request->attr) {
        
                    $attr = $request->attr;
        
                    AttributeAndOptionCreate($attr , $product);
        
                } // end of if insert attributes and options
        
                // uploade multiple images 
                if($request->hasFile('images'))
                {
        
                    $files = $request->file('images');
        
                    UploadeMulipleImage($files , $product);
        
                } // end of has files 
                
                return $this->ApiResponse(new ProductResource($product) , 'تم اضافه الاعلان بنجاح' , 200 );
                // ================================ السيارات ==================================
        
            } else 
            {

                // ================================ العقارات ==================================
                
                // except key from request 
                $attrubute = attributeExcept($request);
                
                // uploade one image_one
                if($request->has('image_one'))
                {
        
                    // Upload new image_one
                    $attrubute['image_one'] = $request->file('image_one')->store('product');
                
                } // end of has image_one  
    
                // uploade one image_two
                if($request->has('image_two'))
                {

                    // Upload new image_two
                    $attrubute['image_two'] = $request->file('image_two')->store('product');
                
                } // end of has image_two  
                
                // uploade one image_three
                if($request->has('image_three'))
                {
        
                    // Upload new image_three
                    $attrubute['image_three'] = $request->file('image_three')->store('product');
                
                } // end of has image_three  
        
                $attrubute['code']        = '#' . rand(0000000,9999999);
                $attrubute['user_id']     = $user->id;
                $attrubute['category_id'] = $request->category_id;
                $attrubute['show_phone']       = $request->show_phone == 1 ? true : false;
        
                $product = $this->productrepository->create($attrubute);

                // insert attribute and options
                if ($request->attr) {
        
                    $attr = $request->attr;
                    
                    AttributeAndOptionCreate($attr , $product);
        
                } // end of if insert attributes and options


                if ($request->directione) {

                    $dire = $request->directione;

                    DirectioneCreate($dire , $product);

                } // end of if insert attributes and options


                // uploade multiple images 
                if($request->hasFile('images'))
                {
        
                    $files = $request->file('images');
        
                    UploadeMulipleImage($files , $product);
        
                } // end of has files 
                
                return $this->ApiResponse(new ProductResource($product) , 'تم اضافه الاعلان بنجاح' , 200 );
                
                // ================================ العقارات ==================================
        
            }

        } else 
        {

           return $this->notUser();

        } // end of user

    } // end of store cars

    public function update(ProductRequest $request , $id)
    {
        
        $user = JWTAuth::parseToken()->authenticate();
        
        if ($user) {

            if ($request->category_id == 1) {
                // ================================ السيارات ==================================
                $attrubute = $request->except(

                    'images' ,
                    'image_one' ,
                    'image_two' ,
                    'image_three' ,
                    'phone',
                    'number_of_shops',
                    'neighborhood',
                    'street_view',
                    'border',
                    'number_halls_for_two',
                    'women_boards_for_two',
                    'Men_boards_for_two',
                    'number_of_bedrooms_for_two',
                    'number_halls_for_one',
                    'women_boards_for_one',
                    'men_boards_for_one',
                    'number_of_bedrooms_for_one',
                    'shops',
                    'number_of_sections',
                    'inside_height',
                    'street_or_lane_width',
                    'fuel_tank_capacity',
                    'number_pumps',
                    'on_highway',
                    'storehouse',
                    'number_bathrooms',
                    'number_of_swimming_pools',
                    'number_Kitchens',
                    'number_halls',
                    'women_councils',
                    'men_boards',
                    'number_of_master_bedrooms',
                    'number_bedroom',
                    'number_of_roles',
                    'building_erea',
                    'space',
                    'attr',
                    'directione'

                );

                $product = $this->productrepository->findOne($id);
    
                // uploade one image_one
                if($request->image_one)
                {
        
                    // delete old image
                    Storage::delete($product->image);
                    // Upload new image_one
                    $attrubute['image_one'] = $request->file('image_one')->store('product');
                
                } // end of has image_one  
    
                // uploade one image_two
                if($request->image_two)
                {

                    // delete old image
                    Storage::delete($product->image);
                    // Upload new image_two
                    $attrubute['image_two'] = $request->file('image_two')->store('product');
                
                } // end of has image_two  
                
                // uploade one image_three
                if($request->image_three)
                {

                    // delete old image
                    Storage::delete($product->image);
                    // Upload new image_three
                    $attrubute['image_three'] = $request->file('image_three')->store('product');
                
                } // end of has image_three  
        
                $attrubute['code']    = $product->code;
                $attrubute['user_id'] = $user->id;
                $attrubute['category_id'] = $request->category_id;
                $attrubute['show_phone']   = $request->show_phone == 1 ? true : false;
        
                $productup = $this->productrepository->update($attrubute , $id);
        
                // insert attribute and options
                if ($request->attr) {
        
                    $attr = $request->attr;
                    // delete attribute and option
                    $product->attributeandoption()->delete();
                    AttributeAndOptionCreate($attr , $product);
        
                } // end of if insert attributes and options
        
                // uploade multiple images 
                if($request->hasFile('images'))
                {
        
                    $files = $request->file('images');
                    UploadeMulipleImage($files , $product);
        
                } // end of has files 
                
                return $this->ApiResponse(new ProductResource($productup) , 'تم تعديل الاعلان بنجاح' , 200 );
                // ================================ السيارات ==================================
        
            } else 
            {

                // ================================ العقارات ==================================
                
                // except key from request 
                $attrubute = attributeExcept($request);
                
                $product = $this->productrepository->findOne($id);

                // uploade one image_one
                if($request->image_one != null)
                {

                    // delete old image
                    Storage::delete($product->image);        
                    // Upload new image_one
                    $attrubute['image_one'] = $request->file('image_one')->store('product');
                
                } // end of has image_one  
    
                // uploade one image_two
                if($request->image_two != null)
                {

                    // delete old image
                    Storage::delete($product->image);
                    // Upload new image_two
                    $attrubute['image_two'] = $request->file('image_two')->store('product');
                
                } // end of has image_two  
                
                // uploade one image_three
                if($request->image_three != null)
                {

                    // delete old image
                    Storage::delete($product->image);
                    // Upload new image_three
                    $attrubute['image_three'] = $request->file('image_three')->store('product');
                
                } // end of has image_three  
        
                $attrubute['code']        = $product->code;
                $attrubute['user_id']     = $user->id;
                $attrubute['category_id'] = $request->category_id;
                $attrubute['show_phone']  = $request->show_phone == 1 ? true : false;
        
                $productup = $this->productrepository->update($attrubute , $id);
        
                // insert attribute and options
                if ($request->attr) {
        
                    $attr = $request->attr;

                    $product->attributeandoption()->delete();

                    AttributeAndOptionCreate($attr , $product);
        
                } // end of if insert attributes and options

                if ($request->subcategory_id == 6) {

                    if ($request->directione) {
            
                        $dire = $request->directione;

                        $product->directione()->delete();

                        DirectioneCreate($dire , $product);
            
                    } // end of if insert attributes and options
            
                } // end of subcategory id == 6                
        
                // uploade multiple images 
                if($request->hasFile('images'))
                {
        
                    $files = $request->file('images');
        
                    UploadeMulipleImage($files , $product);
        
                } // end of has files 
                
                return $this->ApiResponse(new ProductResource($productup) , 'تم تعديل الاعلان بنجاح' , 200 );
                
                // ================================ العقارات ==================================
                
            }

        } else 
        {

           return $this->notUser();

        } // end of user

    } // end of store cars

    public function destroy($id , $category_id)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {

            $product = $this->productrepository->getWhere([['user_id' , $user->id] , ['category_id' , $category_id] , ['id' , $id]])->first();
    
            if (isset($product)) {
                
                $product->delete();
    
                return $this->ApiResponse(null , 'تم حذف المنتج ينجاح' , 200);
                
            } else 
            {

                return $this->notFoundResponse();

            } // end of product

        } else 
        {

            return $this->notUser();

        } // end if user


    } // end of destory

    public function myProduct($id)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {
            
            if ($user->products->where('category_id' , $id)->isNotEmpty()) {

                return $this->ApiResponse(ProductResource::collection($user->products()->where('category_id' , $id)->get()));

            } else 
            {

                return $this->notFoundResponse();

            } // end of user products

        } else  
        {

            return $this->notUser();

        } // end of user

    } // end of my product

    public function doneSale($id)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {
            
            $product = $user->products()->where('id' , $id)->first();

            if ($product) {

                $product->update(['status' => 'تم البيع' , 'date_sale' => now()]);

                return $this->ApiResponse(null , 'تم البيع بنجاح ' , 200);

            } else 
            {

                return $this->notFoundResponse();

            } // end of product
            
        } else 
        {

            return $this->notUser();

        } // end of user

    } // end of done sale

    public function commission()
    {
        $commission  = $this->settingrepository->getWhere(['key' => 'tax'])->first();
        $commission2 = $this->settingrepository->getWhere(['key' => 'tax2'])->first();
        $day         = $this->settingrepository->getWhere(['key' => 'day'])->first();

        if ($commission) {
            
            $array = [
                'commission'    => $commission->value,
                'tax'           => $commission2->value,
                'condition_one' => '* اتعهد واقسم بالله أنا المعلن أن ادفع عمولة الموقع وهى %' . $commission->value . ' من قيمة البيع سواء تم البيع عن طريق الموقع أو بسببه .',
                'condition_two' => '* كما اتعهد بدفع العمولة خلال ' . $day->value .' من استلام مبلغ المبايعة .',
            ];

            return $this->ApiResponse($array , 'تم' , 200);
            
        } else 
        {

            return $this->notFoundResponse();

        } // end of else if commission 

    } // end of commission

    public function report(ReportRequest $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {

            $product = $this->productrepository->findOne($request->product_id);
            
            $check_report = $product->reports->contains('user_id' , $user->id);

            if (! $check_report) {

                $product->reports()->create([
    
                    'product_id' => $request->product_id,
                    'user_id'    => $user->id,
                    'report'     => $request->report,
    
                ]);

            } else 
            {

                return $this->ApiResponse(null , 'تم الابلاغ علي المنتج من قبل' , 200);

            } // end of else if chack report

    
            return $this->ApiResponse(null , 'تم الابلاغ بنجاح' , 200);

        } else 
        {

            return $this->notUser();

        } // end of if user
        

    } // end of report

    public function deleteImage($id)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {

            $image = $this->imagerepository->findOne($id);

            if ($image) {

                $image->delete();

                return $this->ApiResponse(null , 'تم حذف الصوره بنجاح' , 200);

            } else 
            {

                return $this->notFoundResponse();

            } // end of if image

        } else 
        {

            return $this->notUser();

        } // end of user

    } // end of delete image

} // end of class
