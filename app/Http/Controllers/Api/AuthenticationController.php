<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ActiveNotificationRequest;
use App\Http\Requests\Api\ChangePasswordRequest;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\NewPasswordRequest;
use App\Http\Requests\Api\RegisterRequest;
use App\Http\Requests\Api\ResetPasswordRequest;
use App\Http\Requests\Api\SendCodeRequest;
use App\Http\Requests\Api\UpdateUserRequest;
use App\Http\Requests\Api\VerifyAccountRequest;
use App\Http\Resources\Api\UserResource;
use App\Repositories\AuthenticationRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Servicies\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthenticationController extends Controller
{

    use ApiResponseTrait;    

    public $authenticationrepository;
    public $notificationrepository;

    public function __construct(AuthenticationRepositoryInterface $authenticationrepository ,
                                NotificationRepositoryInterface $notificationrepository
    )
    {
        
        $this->authenticationrepository = $authenticationrepository;
        $this->notificationrepository   = $notificationrepository;

    } // end of construct

    public function register(RegisterRequest $request)
    {
//        dd($request->all());
        if ($request->type_id == 1) {

            $attribute = $request->except(

                    'commercial_register' ,
                    'tax_number' ,
                    'password' ,
                    'confirm_password',
                    'name_bank',
                    'name_account_bank',
                    'number_account_bank',
                    'number_eban_bank',
                    'category_id'

            );

        } else  
        {

            $attribute = $request->except(

                    'password' ,
                    'confirm_password' ,
                    'second_name' ,
                    'last_name',
                    'category_id'

            );
            
        }

        $attribute['password'] = bcrypt($request->password);
        $attribute['code'] = rand(1111,9999);
        $attribute['image'] = 'users/avatar.png';

        $user = $this->authenticationrepository->create($attribute);

        if ($request->type_id == 1)
        {

            $user->category()->create([

                'user_id'     => $user->id,
                'category_id' => 1

            ]); // end of create

            $user->category()->create([

                'user_id'     => $user->id,
                'category_id' => 2

            ]); // end of create

        } else
        {

            $user->category()->create([

                'user_id'     => $user->id,
                'category_id' => $request->category_id

            ]); // end of create

        }


        $user->devices()->create([

            'user_id'   => $user->id,
            'token'     => $request->firebase_token,
            'device_id' => $request->device_id

        ]); // end of create        



        return $this->ApiResponse(new UserResource($user) , 'تم انشاء الحساب بنجاح , برجاء ادخال كود التفعيل المرسل علي رقم الجوال الخاص بكم' , 200 );

    } // end of register

    public function verifyAccount(VerifyAccountRequest $request)
    {
        
        $user = $this->authenticationrepository->getWhere([['code' , $request->code]])->first();
//        dd($user->category()->pluck('category_id'));
        if($user && in_array($request->category_id , $user->category()->pluck('category_id')->toArray()))
        {

            $phone = $this->authenticationrepository->getWhere([['code' , $request->code] , ['phone' , $request->phone]])->first();

            if ($phone) {

                $user->update(['code' => null , 'account_verify' => 1]);
    
                $token = JWTAuth::fromUser($user);
    
                return $this->ApiResponse( [ 'user' => new UserResource($user) , 'token' => 'Bearer ' . $token] , 'تم تفعيل الحساب الخاص بكم بنجاح' , 200);
            
            } else 
            {

                return $this->ApiResponse( null , 'رقم الجوال غير صحيح' , 404);

            } // end of else if

        }else
        {

            return $this->ApiResponse( null , 'كود التفعيل غير صحيح' , 404);

        } // end of else        

    } // end of verify account

    public function sendCode(SendCodeRequest $request)
    {

        $user = $this->authenticationrepository->getWhere([['phone' , $request->phone]])->first();

        if ($user)
        {

            $code = rand(1111,9999);

            $user->update(['code' => $code]);

            return $this->ApiResponse($code , 'تم ارسال الكود بنجاح' , 200);

        } else
        {

            return $this->ApiResponse(null , 'رقم الهاتف غير صحيح' , 404);

        }




    } // end of send code

    public function login(LoginRequest $request)
    {
        
        $user = $this->authenticationrepository->WhereOrWhere([['email' , $request->email]] , ['phone' => $request->email]);
        
        if ($user && in_array($request->category_id , $user->category()->pluck('category_id')->toArray())) {

            $verify = $this->authenticationrepository->WhereOrWhere([['email' , $request->email] , ['account_verify' , 1]] , [['phone' , $request->email] , ['account_verify' , 1]]);

            if (isset($verify)) {
            
                $login_type = filter_var( $request['email'], FILTER_VALIDATE_EMAIL ) ? 'email' : 'phone';

                try {   

                        $credential = [$login_type => $request['email'], 'password' => $request['password']];
                        // dd($credential);
                        if (! $token = JWTAuth::attempt($credential)) {
                            
                            return $this->ApiResponse(null , 'بيانات الدخول غير صحيحة' , 302);

                        }

                    } catch (JWTException $e) {

                        return $this->ApiResponse(null , 'لا تستطيع انشاء توكن' , 500);

                    }

                    $device = $user->devices()->where('device_id' , $request->device_id)->get();

                    if($device->isNotEmpty())
                    {

                    } else
                    {
                        $user->devices()->create([

                            'user_id'   => $user->id,
                            'token'     => $request->firebase_token,
                            'device_id' => $request->device_id

                        ]); // end of create
                    }


                    return $this->ApiResponse([ 'user' => new UserResource($user) , 'token' => 'Bearer ' . $token ] , 'تم تسجيل الدخول بنجاح' , 200);
                
            } else 
            {

                return response()->json([

                    'data'    => 'unactive',
                    'code'    => $user->code,
                    'message' => 'برجاء تفعيل الحساب اولا',
                    'status'  => false

                ] , 404);
                
            }
        } else {

            return $this->ApiResponse(null , 'البريد الالكتروني او رقم الجوال غير موجود' , 404);

        } // end of if user
        
    } // end of login

    public function resetPassword(ResetPasswordRequest $request)
    {

        $user = $this->authenticationrepository->getWhere([['phone' , $request->phone]])->first();

        if ($user) {

            $code = rand(1111,9999);

            $this->authenticationrepository->update(['code' => $code] , $user->id)->first();

            return $this->ApiResponse($code , 'تم ارسال كود نسيت كلمه المرور الي رقم الجوال الخاص بكم' , 200);
        
        } else 
        {

            return $this->ApiResponse(null , 'رقم الجوال غير صحيح' , 404);

        } // end of else if user

    } // end of reset password

    public function newPassword(NewPasswordRequest $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {
            
            $user->update(['password' => bcrypt($request->new_password) ]);

            return $this->ApiResponse(null , 'تم تغير كلمة المرور بنجاح' , 200);

        } else 
        {
            return $this->notUser();
        }

    } // end of new password

    public function changePassword(ChangePasswordRequest $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {

            if (Hash::check($request->old_password , $user->password)) {

                $user->update(['password' => bcrypt($request->new_password) ]);
    
                return $this->ApiResponse(null , 'تم تغير كلمة المرور بنجاح' , 200);

            } else 
            {

                return $this->ApiResponse(null , 'كلمة المرور القديمة غير صحيحة' , 404);

            }  // end of else user auth password != request pld password
            
        } else 
        {
            return $this->notUser();
        }

    } // end of change password

    public function activeNotification(ActiveNotificationRequest $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user && $user->category_id == $request->category_id) {

            if ($user->is_notify == 1) {

                $user->update(['is_notify' => 0]);
                return $this->ApiResponse(null , 'تم ايقاف الاشعارات بنجاح' , 200);

            } else 
            {

                $user->update(['is_notify' => 1]);
                return $this->ApiResponse(null , 'تم تفعيل الاشعارات بنجاح' , 200);

            } // end of else if user is notify or no

        } else 
        {

            return $this->notUser();

        } // end of if user


    } // end of active notification

    public function update(UpdateUserRequest $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user && in_array($request->category_id , $user->category()->pluck('category_id')->toArray())) {

            if ($user->type_id == 1) {

                $attribute = $request->except(

                        'commercial_register' ,
                        'tax_number' ,
                        'name_bank' ,
                        'name_account_bank',
                        'number_account_bank',
                        'number_eban_bank',
                        'category_id'

                    ); // end of request

            } else 
            {

                $attribute = $request->except(

                    'password' ,
                    'confirm_password' ,
                    'second_name' ,
                    'last_name',
                    'category_id'

                );                

            
            } // end of else if

            if ($request->phone != $user->phone) {

                $code = rand(1111,9999);

                $attribute['account_verify'] = 0;
                $attribute['code'] = $code;

                $user->update($attribute);

                return $this->ApiResponse(['data' => 'unactive' , 'code' => $code] , 'تم تحديث البيانات بنجاح , برجاء تفعيل الحساب بأستخدام الكود المرسل علي رقم الجوال الخاص بكم' , 200);

            } else 
            {

                $user->update($attribute);
                
                return $this->ApiResponse(new UserResource($user) , 'تم تحديث البيانات بنجاح' , 200);

            } // end of else if request phone != auth user phone
        


        } else 
        {
            return $this->notUser();
        }

    } // end of update

    public function changeImage(Request $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {

            $attribute = $request->except('image');

            // uploade one image
            if($request->has('image'))
            {

                if ($user->image != 'users/avatar.png') {

                    Storage::delete($user->image);

                } // end of if user image != users/avatar.png
                
                // Upload new image
                $attrubute['image'] = $request->file('image')->store('users');
            
            } // end of has file              
            
            $user->update($attrubute);

            return $this->ApiResponse($user->ImageUser() , 'تم تغيير الصورة بنجاح' , 200);

        } else 
        {
            return $this->notUser();
        }

    }  // end of change image

    public function getUser()
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {
            
            return $this->ApiResponse(new UserResource($user) , 'تم' , 200 );

        } else 
        {

            return $this->notUser();

        } // end of else if auth user

    } // end of get user

    public function logout()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {
            
            JWTAuth::invalidate(JWTAuth::getToken());

            return $this->ApiResponse(null , 'تم تسجيل الخروج بنجاح' , 200);

        } else 
        {

            return $this->notUser();

        } // ebnd of else if user auth

    } // end of logout

} // end of class
