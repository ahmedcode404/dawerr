<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\SliderResource;
use App\Repositories\SliderRepositoryInterface;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    
    use ApiResponseTrait;

    public $sliderrepository;

    public function __construct(SliderRepositoryInterface $sliderrepository)
    {
        
        $this->sliderrepository = $sliderrepository;

    } // end of construct

    public function slider($category)
    {
        
        $sliders = $this->sliderrepository->getWhere(['category_id' => $category] , ['column' => 'id', 'dir' => 'asc']);

        if ($sliders->isNotEmpty()) {
            
            return $this->ApiResponse(SliderResource::collection($sliders) , 'تم' , 200);

        } else 
        {

            return $this->notFoundResponse();

        } // end of else if sliders

    } // end of slider

} // end of class
