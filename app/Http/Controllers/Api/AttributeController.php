<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\AttributeResource;
use App\Models\Attribute;
use App\Repositories\AttributeRepositoryInterface;
use Illuminate\Http\Request;

class AttributeController extends Controller
{

    use ApiResponseTrait;

    public $attrbibuterepository;

    public function __construct(AttributeRepositoryInterface $attrbibuterepository)
    {

        $this->attrbibuterepository = $attrbibuterepository;
        
    } // end of construct

    public function attribute($category)
    {

        $attributes = $this->attrbibuterepository->getOrWhere([['category_id' , $category] , ['cat'  , '!=' , 'not']] , [['category_id' , $category] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);
        
        if ($attributes->isNotEmpty()) {

            return $this->ApiResponse(AttributeResource::collection($attributes) , 'تم' , 200);
            
        } else 
        {

            return $this->notFoundResponse();

        } // end of else if

    } // end of attribute

    public function getOption($id)
    {

        $attributes = $this->attrbibuterepository->getOrWhere([['parent_id' , $id] , ['cat'  , '!=' , 'not']] , [['parent_id' , $id] , ['cat' , Null]] , ['column' => 'id', 'dir' => 'asc']);
        
        if ($attributes->isNotEmpty()) {

            return $this->ApiResponse(AttributeResource::collection($attributes) , 'تم' , 200);
            
        } else 
        {

            return $this->notFoundResponse();

        } // end of else if
    }

} // end of class
