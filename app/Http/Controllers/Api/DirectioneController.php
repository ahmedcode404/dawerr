<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\CityResource;
use App\Repositories\DirectioneRepositoryInterface;
use App\Repositories\DirRepositoryInterface;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

use function Ramsey\Uuid\v1;

class DirectioneController extends Controller
{

    use ApiResponseTrait;

    public $directionrepository;
    public $dirrepository;

    public function __construct(DirectioneRepositoryInterface $directionrepository,
                                DirRepositoryInterface $dirrepository
    )
    {
        
        $this->directionrepository = $directionrepository;
        $this->dirrepository       = $dirrepository;

    } // end of conatruct

    public function directione()
    {
        
        $user = JWTAuth::parseToken()->authenticate();
        
        if ($user) {

            $dreictiones = $this->directionrepository->getAll();
    
            if ($dreictiones->isNotEmpty()) {
                
                return $this->ApiResponse(CityResource::collection($dreictiones) , 'تم' , 200);
    
            } else 
            {
    
                return $this->notFoundResponse();
    
            } // end of directione
            
        } else 
        {

            return $this->notUser();

        } // end of user

    } // end of directione

    public function dir()
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {

            $dreictiones = $this->dirrepository->getAll();

            if ($dreictiones->isNotEmpty()) {

                return $this->ApiResponse(CityResource::collection($dreictiones) , 'تم' , 200);

            } else
            {

                return $this->notFoundResponse();

            } // end of directione

        } else
        {

            return $this->notUser();

        } // end of user

    } // end of directione

} // end of class
