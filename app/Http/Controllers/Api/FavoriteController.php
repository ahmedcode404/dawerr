<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\ProductAllResource;
use App\Http\Resources\Api\ProductResource;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class FavoriteController extends Controller
{

    use ApiResponseTrait;

    public $productrepository;

    public function __construct(ProductRepositoryInterface $productrepository)
    {

        $this->productrepository = $productrepository;

    } // end of construct
    

    public function store($id)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {

            $product = $this->productrepository->findOne($id);

            if ($user->id == $product->user_id) {

                return $this->ApiResponse(null , 'هذا المنتج خاص بك لا يمكن اضافة في المفضله' , 404);

            } else 
            {
                
                $check_favorite = $product->favorites->contains('user_id' , $user->id);
    
                if ($check_favorite == true) {
    
                    $product->favorites()->where('product_id' , $product->id)->delete();
    
                    return $this->ApiResponse(null , 'تم الحذف من المفضله بنجاح' , 200);
    
                } else 
                {
                    
                    $product->favorites()->create([
                        'user_id'    => $user->id,
                        'product_id' => $product->id,
                    ]);
    
                } // end of else check product

                return $this->ApiResponse(null , 'تم الاضافه الي المفضله بنجاح' , 200);

            } // end of else if user id == product user id

        } else 
        {

            return $this->notUser();

        }


    } // end of my favorite

    public function myFavorite($id)
    {
        
        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {
            
            $favorites = $this->productrepository->WhereHas('favorites' , ['user_id' => $user->id] , ['category_id' => $id] , ['column' => 'id', 'dir' => 'desc']);

            return $this->ApiResponse(ProductResource::collection($favorites) , 'تم' , 200);

        } else 
        {

            return $this->notUser();

        } // end of user auth 

    } // end of my favorite

} // end of class
