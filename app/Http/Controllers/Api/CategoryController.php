<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\CategoryResource;
use App\Http\Resources\Api\TypeAuthResource;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\TypeAuthRepositoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    use ApiResponseTrait;

    public $categoryrepository;
    public $typeauthrepository;

    public function __construct(CategoryRepositoryInterface $categoryrepository,
                                TypeAuthRepositoryInterface $typeauthrepository
    )
    {

        $this->categoryrepository = $categoryrepository;;
        $this->typeauthrepository = $typeauthrepository;;
        
    } // end of construct

    public function category()
    {
        
        $categories = $this->categoryrepository->getWhere(['type' => 'main'] , ['column' => 'id', 'dir' => 'asc']);

        return $this->ApiResponse(CategoryResource::collection($categories) , 'تم' , 200);

    } // end of get categories

    public function subCategory($category)
    {
        
        $subcategories = $this->categoryrepository->getWhere(['parent_id' => $category] , ['column' => 'id', 'dir' => 'asc']);

        if ($subcategories->isNotEmpty()) {

            return $this->ApiResponse(CategoryResource::collection($subcategories) , 'تم' , 200);

        } else 
        {
            return $this->notFoundResponse();
        }

    } // end of get categories    

    public function typeAuth()
    {
        
        $types = $this->typeauthrepository->getWhere(['type' => 1] , ['column' => 'id', 'dir' => 'asc']);

        return $this->ApiResponse(TypeAuthResource::collection($types) , 'تم' , 200);

    } // end of get type auth   
    
    public function additions()
    {

        $additions = $this->categoryrepository->getWhere(['type' => 'adds'] , ['column' => 'id', 'dir' => 'asc']);

        if ($additions->isNotEmpty()) {

            return $this->ApiResponse(CategoryResource::collection($additions) , 'تم' , 200);

        } else 
        {
            return $this->notFoundResponse();
        }
    } // end of additions

    public function additionsother()
    {

        $additions = $this->categoryrepository->getWhere(['type' => 'other_add'] , ['column' => 'id', 'dir' => 'asc']);

        if ($additions->isNotEmpty()) {

            return $this->ApiResponse(CategoryResource::collection($additions) , 'تم' , 200);

        } else 
        {
            return $this->notFoundResponse();
        }
    } // end of additionsother

} // end of class
