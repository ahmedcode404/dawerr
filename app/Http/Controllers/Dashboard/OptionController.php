<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OptionRequest;
use App\Repositories\AttributeRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\OptionSubCategoryRepositoryInterface;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public $categoryrepository;
    public $attributerepository;
    public $optionsubcategoryrepository;

    public function __construct(CategoryRepositoryInterface $categoryrepository,
                                AttributeRepositoryInterface $attributerepository,
                                OptionSubCategoryRepositoryInterface $optionsubcategoryrepository
    )
    {

        $this->categoryrepository          = $categoryrepository;
        $this->attributerepository         = $attributerepository;
        $this->optionsubcategoryrepository = $optionsubcategoryrepository;
        
    } // end of construct

    public function index()
    {

        $options = $this->attributerepository->option();
        
        return view('dashboard.options.index' , compact('options'));

    } // end of index

    public function create()
    {

        $attributes = $this->attributerepository->attribute();

        return view('dashboard.options.create' , compact('attributes'));

    } // end of create

    public function store(OptionRequest $request)
    {

        $attribute = $request->except('_token' , 'attribute_id');

        $attribute['parent_id'] = $request->attribute_id;

        $opton = $this->attributerepository->create($attribute);
        
        if ($request->subcategory_id) {

            foreach ($request->subcategory_id as $subcategory) {
                
                $this->optionsubcategoryrepository->create([
                    'subcategory_id' => $subcategory,
                    'attribute_id'   => $opton->id,
                ]);
    
            } // end of foreach

        } // end of if request subcategory id

        return redirect()->route('dashboard.options.index')->with('success' , 'تم اضافه الخيارات بنجاح');

    } // end of store

    public function edit($optoin)
    {
        
        $one_option = $this->attributerepository->findOne($optoin);

        $attributes = $this->attributerepository->attribute();

        $subcategories = $this->categoryrepository->whereHas('attributes' , ['id' => $one_option->parent_id])->first();

        return view('dashboard.options.edit' , compact('subcategories' , 'attributes' , 'one_option'));
        
    } // end of edit

    public function update(OptionRequest $request , $option)
    {

        $attribute = $request->except('_token' , '_method' , 'subcategory_id' , 'attribute_id');

        $attribute['parent_id'] = $request->attribute_id;

        $one_option = $this->attributerepository->update($attribute , $option);

        $one_option->optionsubcategory()->detach();

        if ($request->subcategory_id) {

            foreach ($request->subcategory_id as $subcategory) {
                
                $this->optionsubcategoryrepository->create([
                    'subcategory_id' => $subcategory,
                    'attribute_id'   => $one_option->id,
                ]);
    
            } // end of foreach

        } // end of if request subcategory id        

        return redirect()->route('dashboard.options.index')->with('success' , 'تم تعديل الخيارات بنجاح');

    } // end of update

    public function destroy($attr)
    {

        $this->attributerepository->delete($attr);

        return response()->json();
        
    } // end of destroy

    public function appendSubCategory(Request $request)
    {

        $subcategories = $this->categoryrepository->whereHas('attributes' , ['id' => $request->id])->first();

        return view('dashboard.options.append-subcategory' , compact('subcategories'))->render();

    } // end of append subcategory

} // end of class
