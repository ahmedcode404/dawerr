<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SettingRequest;
use App\Repositories\SettingRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    public $settingrepository;

    public function __construct(SettingRepositoryInterface $settingrepository)
    {

        $this->settingrepository = $settingrepository;
        
    } // end of construct

    public function create()
    {

        $settings = $this->settingrepository->getAll();
        
        return view('dashboard.settings.edit' , compact('settings'));

    } // end of index


    public function store(SettingRequest $request)
    {
        // dd($request->all());
        $attribute = $request->except('_token' , '_method' , 'logo');

        $setting = $this->settingrepository->getWhere([['key' , 'logo']])->first();      

        if($request->has('logo')){

            // Delete old internal_image
            Storage::delete($setting->image);

            // Upload new internal_image
            $attribute['logo'] = $request->file('logo')->store('setting');

        }      
        
        $attribute['phone'] = $request->phone_key . $request->phone; 

        $this->settingrepository->updateSetting($attribute);      

        return redirect()->back()->with('success' , 'تم تعديل البيانات بنجاح');

    } // end of update

    public function destroy($bank)
    {

        $this->settingrepository->delete($bank);

        return response()->json();
        
    } // end of destroy
}
