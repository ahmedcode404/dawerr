<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SliderRequest;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\SliderRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public $sliderrepository;
    public $categoryrepository;

    public function __construct(SliderRepositoryInterface $sliderrepository,
                                CategoryRepositoryInterface $categoryrepository
    )
    {

        $this->sliderrepository   = $sliderrepository;
        $this->categoryrepository = $categoryrepository;
        
    } // end of construct

    public function index()
    {

        $sliders = $this->sliderrepository->getAll();
        
        return view('dashboard.sliders.index' , compact('sliders'));

    } // end of index    

    public function create()
    {

        $categories = $this->categoryrepository->getWhere(['type' => 'main']);
        
        return view('dashboard.sliders.create' , compact('categories'));

    } // end of create


    public function store(SliderRequest $request)
    {

        $attribute = $request->except('_token' , '_method' , 'image');

        if($request->has('image')){

            // Upload new internal_image
            $attribute['image'] = $request->file('image')->store('slider');

        }          

        $this->sliderrepository->create($attribute); // end of create
        
        return redirect()->route('dashboard.sliders.index')->with('success' , 'تم تعديل السلايدر بنجاح');

    } // end of update

    public function edit($slider)
    {
        $slider_one = $this->sliderrepository->findOne($slider);

        $categories = $this->categoryrepository->getWhere(['type' => 'main']);

        return view('dashboard.sliders.edit' , compact('slider_one' , 'categories'));

    } // end of edit

    public function update(SliderRequest $request , $slider)
    {

        $attribute = $request->except('_token' , '_method' , 'image');

        $slider_one = $this->sliderrepository->findOne($slider);      

        if($request->has('image')){

            // Delete old internal_image
            Storage::delete($slider_one->image);

            // Upload new internal_image
            $attribute['image'] = $request->file('image')->store('slider');

        }          

        $this->sliderrepository->update($attribute , $slider);      

        return redirect()->route('dashboard.sliders.index')->with('success' , 'تم تعديل البنك بنجاح');

    } // end of update


    public function destroy($bank)
    {


        $this->sliderrepository->delete($bank);
    
        return response()->json();

        
    } // end of destroy
}
