<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public$productrepository;

    public function __construct(ProductRepositoryInterface $productrepository)
    {

        $this->productrepository = $productrepository;
        
    } // end of construct

    public function index()
    {

        $products = $this->productrepository->getAll();
        
        return view('dashboard.products.index' , compact('products'));

    } // end of index

    public function show($product)
    {

        $product = $this->productrepository->findOne($product);

        return view('dashboard.products.show' , compact('product'));

    } // end of destroy



    public function destroy($product)
    {

        $this->productrepository->delete($product);

        return response()->json();

    } // end of destroy

}
