<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AgreementRequest;
use App\Repositories\AgreementRepositoryInterface;
use Illuminate\Http\Request;

class AgreementController extends Controller
{
    public $agreementrepository;

    public function __construct(AgreementRepositoryInterface $agreementrepository)
    {

        $this->agreementrepository = $agreementrepository;
        
    } // end of construct

    public function index()
    {

        $agreements = $this->agreementrepository->getAll();
        
        return view('dashboard.agreements.index' , compact('agreements'));

    } // end of index    

    public function create()
    {

        
        return view('dashboard.agreements.create');

    } // end of create


    public function store(AgreementRequest $request)
    {

        $attribute = $request->except('_token' , '_method');        

        $this->agreementrepository->create($attribute); // end of create
        
        return redirect()->route('dashboard.agreements.index')->with('success' , 'تم اضافة البند بنجاح');

    } // end of update

    public function edit($agreement)
    {
        $agreement_one = $this->agreementrepository->findOne($agreement);

        return view('dashboard.agreements.edit' , compact('agreement_one'));

    } // end of edit

    public function update(AgreementRequest $request , $slider)
    {

        $attribute = $request->except('_token' , '_method');

        $this->agreementrepository->update($attribute , $slider);      

        return redirect()->route('dashboard.agreements.index')->with('success' , 'تم تعديل البند بنجاح');

    } // end of update


    public function destroy($bank)
    {


        $this->agreementrepository->delete($bank);
    
        return response()->json();

        
    } // end of destroy
}
