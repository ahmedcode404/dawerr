<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StaticPageRequest;
use App\Repositories\StaticPageRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StaticPageController extends Controller
{
    public $staticpagerepository;

    public function __construct(StaticPageRepositoryInterface $staticpagerepository)
    {

        $this->staticpagerepository = $staticpagerepository;
        
    } // end of construct

    public function index()
    {

        $static_pages = $this->staticpagerepository->getAll();
        
        return view('dashboard.static_pages.index' , compact('static_pages'));

    } // end of index

    public function edit($bank)
    {
        
        $static_page = $this->staticpagerepository->findOne($bank);

        return view('dashboard.static_pages.edit' , compact('static_page'));
        
    } // end of edit

    public function update(StaticPageRequest $request , $static)
    {

        $attribute = $request->except('_token' , '_method' , 'logo' , 'logo2');

        $static_one = $this->staticpagerepository->findOne($static);      

        if($static_one->key == 'about')
        {

            if($request->has('logo')){

                // Delete old internal_image
                Storage::delete($static_one->image);
    
                // Upload new internal_image
                $attribute['image'] = $request->file('logo')->store('staticpage');
    
            } // end of logo

            if($request->has('logo2')){

                // Delete old internal_image
                Storage::delete($static_one->image);
    
                // Upload new internal_image
                $attribute['image2'] = $request->file('logo2')->store('staticpage');
    
            } // end of logo two            

        } // end of key about

        if($static_one->key == 'about_application')
        {

            if($request->has('logo')){
    
                // Delete old internal_image
                Storage::delete($static_one->image);
    
                // Upload new internal_image
                $attribute['image'] = $request->file('logo')->store('staticpage');
    
            } // end of logo 

        } // end of key 

        $this->staticpagerepository->update($attribute , $static);      

        return redirect()->route('dashboard.static-pages.index')->with('success' , 'تم تعديل البيانات بنجاح');

    } // end of update

    public function destroy($bank)
    {

        $this->staticpagerepository->delete($bank);

        return response()->json();
        
    } // end of destroy

}
