<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AttributeRequest;
use App\Repositories\AttributeRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\OptionSubCategoryRepositoryInterface;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public $categoryrepository;
    public $attributerepository;

    public function __construct(CategoryRepositoryInterface $categoryrepository,
                                AttributeRepositoryInterface $attributerepository                      
    )
    {

        $this->categoryrepository          = $categoryrepository;
        $this->attributerepository         = $attributerepository;
        
    } // end of construct

    public function index()
    {

        $attributes = $this->attributerepository->attribute();
        
        return view('dashboard.attributes.index' , compact('attributes'));

    } // end of index

    public function create()
    {

        $subcategories = $this->categoryrepository->subcategory();
        $categories = $this->categoryrepository->category();

        return view('dashboard.attributes.create' , compact('subcategories' , 'categories'));

    } // end of create

    public function store(AttributeRequest $request)
    {

        $attribute = $request->except('_token');

        $this->attributerepository->create($attribute);
        
        return redirect()->route('dashboard.attributes.index')->with('success' , 'تم اضافه الخصائص بنجاح');

    } // end of store

    public function edit($attr)
    {
        
        $attribute = $this->attributerepository->findOne($attr);
    
        $categories = $this->categoryrepository->category();

        $subcategories = $this->categoryrepository->getwhere([['parent_id' , $attribute->category_id]]);

        return view('dashboard.attributes.edit' , compact('categories' , 'subcategories' , 'attribute'));
        
    } // end of edit

    public function update(AttributeRequest $request , $attr)
    {

        $attribute = $request->except('_token' , '_method');

        $this->attributerepository->update($attribute , $attr);

        return redirect()->route('dashboard.attributes.index')->with('success' , 'تم تعديل الخصائص بنجاح');

    } // end of update

    public function destroy($attr)
    {

        $this->attributerepository->delete($attr);

        return response()->json();
        
    } // end of destroy


} // end of class
