<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BankRequest;
use App\Repositories\BankRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BankController extends Controller
{
    public $bankrepository;

    public function __construct(BankRepositoryInterface $bankrepository)
    {

        $this->bankrepository = $bankrepository;
        
    } // end of construct

    public function index()
    {

        $banks = $this->bankrepository->getAll();
        
        return view('dashboard.banks.index' , compact('banks'));

    } // end of index

    public function create()
    {


        return view('dashboard.banks.create');

    } // end of create

    public function store(BankRequest $request)
    {

        $attribute = $request->except('_token' , 'logo');

        if($request->has('logo')){

            // Upload new logo
            $attribute['logo'] = $request->file('logo')->store('bank');
        
        }        

        $this->bankrepository->create($attribute);
        
        return redirect()->route('dashboard.banks.index')->with('success' , 'تم اضافه البنك بنجاح');

    } // end of store

    public function edit($bank)
    {
        
        $one_bank = $this->bankrepository->findOne($bank);

        return view('dashboard.banks.edit' , compact('one_bank'));
        
    } // end of edit

    public function update(BankRequest $request , $ban)
    {

        $attribute = $request->except('_token' , '_method' , 'logo');

        $bank = $this->bankrepository->findOne($ban);      

        if($request->has('logo')){

            // Delete old internal_image
            Storage::delete($bank->logo);

            // Upload new internal_image
            $attribute['logo'] = $request->file('logo')->store('bank');

        }          

        $this->bankrepository->update($attribute , $ban);      

        return redirect()->route('dashboard.banks.index')->with('success' , 'تم تعديل البنك بنجاح');

    } // end of update

    public function destroy($bank)
    {

        $this->bankrepository->delete($bank);

        return response()->json();
        
    } // end of destroy

}
