<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CityRequest;
use App\Repositories\CityRepositoryInterface;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public $cityrepository;

    public function __construct(CityRepositoryInterface $cityrepository)
    {

        $this->cityrepository = $cityrepository;
        
    } // end of construct

    public function index()
    {

        $cities = $this->cityrepository->getAll();
        
        return view('dashboard.cities.index' , compact('cities'));

    } // end of index

    public function create()
    {

        return view('dashboard.cities.create');

    } // end of create

    public function store(CityRequest $request)
    {

        $attribute = $request->except('_token');

        $this->cityrepository->create($attribute);

        return redirect()->route('dashboard.cities.index')->with('succcess' , 'تم اضافه المدينة بنجاح');

    } // end of store

    public function edit($city)
    {

        $city_one = $this->cityrepository->findOne($city);

        return view('dashboard.cities.edit' , compact('city_one'));

    } // end of edit

    public function update(CityRequest $request , $city)
    {

        $attribute = $request->except('_token' , '_method' , 'category_id');

        $this->cityrepository->update($attribute , $city);

        return redirect()->route('dashboard.cities.index')->with('success' , 'تم تعديل المدينة بنجاح');
    } // end of update

    public function destroy($city)
    {

        $this->cityrepository->delete($city);

        return response()->json();
        
    } // end of destroy
}
