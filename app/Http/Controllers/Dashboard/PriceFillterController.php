<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PriceFillterRequest;
use App\Repositories\PriceFillterRepositoryInterface;
use Illuminate\Http\Request;

class PriceFillterController extends Controller
{

    public $pricefillterrepository;

    public function __construct(PriceFillterRepositoryInterface $pricefillterrepository)
    {

        $this->pricefillterrepository = $pricefillterrepository;
        
    } // end of construct

    public function index()
    {

        $price_fillters = $this->pricefillterrepository->getAll();
        
        return view('dashboard.price_fillters.index' , compact('price_fillters'));

    } // end of index

    public function create()
    {


        return view('dashboard.price_fillters.create');

    } // end of create

    public function store(PriceFillterRequest $request)
    {

        $attribute = $request->except('_token');


        $this->pricefillterrepository->create($attribute);
        

        return redirect()->route('dashboard.price-fillters.index')->with('success' , 'تم الاضافه بنجاح');

    } // end of store

    public function edit($price)
    {
        
        $price_fillter = $this->pricefillterrepository->findOne($price);

        return view('dashboard.price_fillters.edit' , compact('price_fillter'));
        
    } // end of edit

    public function update(PriceFillterRequest $request , $option)
    {

        $attribute = $request->except('_token' , '_method');

        $this->pricefillterrepository->update($attribute , $option);      

        return redirect()->route('dashboard.price-fillters.index')->with('success' , 'تم التعديل بنجاح');

    } // end of update

    public function destroy($attr)
    {

        $this->pricefillterrepository->delete($attr);

        return response()->json();
        
    } // end of destroy

}
