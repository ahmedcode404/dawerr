<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Loginrequest;
use App\Mail\SendMail;
use App\Mail\SendMailWeb;
use App\Repositories\AuthenticationRepositoryInterface;
use App\Repositories\BankAccountRepositoryInterface;
use App\Repositories\OrderRepositoryInterface;
use App\Repositories\SliderRepositoryInterface;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{

    public $bankaccountrepository;
    public $sliderrepository;
    public $orderrepository;
    public $authrepository;

    public function __construct(BankAccountRepositoryInterface $bankaccountrepository,
                                SliderRepositoryInterface $sliderrepository,
                                OrderRepositoryInterface $orderrepository,
                                AuthenticationRepositoryInterface $authrepository
    )
    {

        $this->bankaccountrepository = $bankaccountrepository;
        $this->sliderrepository      = $sliderrepository;
        $this->orderrepository       = $orderrepository;
        $this->authrepository        = $authrepository;
        
    } // end of construct 

    public function index()
    {

        $bank_account = $this->bankaccountrepository->getAll();
        $sliders      = $this->sliderrepository->getAll();
        $orders       = $this->orderrepository->getAll();

        return view('dashboard.dashboard' , compact('bank_account' , 'sliders' , 'orders'));
    } // end of index

    public function login()
    {
        return view('dashboard.auth.login');
    } // end of login

    public function loginAuth(Loginrequest $request)
    {
        
        $email = $this->authrepository->getWhere(['email' => $request->email])->first();

        if ($email) {

            $type = $this->authrepository->getWhere([[['email' => $request->email]] , ['role' , 'admin']])->first();

            if ($type) {

                if(auth()->attempt(request(['email' , 'password'])) == false)
                {
        
                    return redirect()->back()->with('error' , 'بيانات الدخول غير صحيحة');
        
                } else 
                {
        
                    return redirect('dashboard/')->with('success' , 'تم تسجيل الدخول بنجاح');
        
                } // end of else if
            } else 
            {

                return redirect()->back()->with('error' , 'البريد الالكتروني غير صحيح');

            }

        } else  
        {

            return redirect()->back()->with('error' , 'البريد الالكتروني غير صحيح');

        }


    } // end of login auth

    public function logout()
    {

        auth()->logout();
        return redirect('dashboard/login');

    } // end of logout

    public function resetPassword()
    {
        return view('dashboard.auth.reset_password');
    } // end of reset password

    public function sendLink(Request $request)
    {
 
        $user = $this->authrepository->getWhere(['email' => $request->email])->first();

        if ($user) {
            
            $code = rand(1111 , 9999);

            $user->update(['code' => $code]);

            $data = [
                'link' => url('dashboard/link/reset/password/' . $code)
            ];

            Mail::to($user->email)->send(new SendMailWeb($data));

            return redirect()->back()->with('success' , 'تم ارسال رابط اعادة كلمة المرور الي البريد الالكتروني الخاص بك ');

        } else 
        {

            return redirect()->back()->with('error' , 'البريد الالكتروني غير موجود');

        } // end of else if not user

    } // end of send link

    public function newPassword($code)
    {

        $user = $this->authrepository->getWhere(['code' => $code])->first();

        if ($user) {

            auth()->login($user);

            return view('dashboard.auth.new_pasword');

        } else 
        {
            return redirect('reset/password')->with('error' , 'يوجد مشكله اثناء اعاده كلمة المرور  الخاصه  بك برجاء المحاولة مره اخري');
        }
    } // end of new password

    public function updatePassword(Request $request)
    {

        if (auth()->user()) {

            auth()->user()->update(['password' => bcrypt($request->new_password) , 'code' => null]);

            return redirect('dashboard')->with('success' , 'تم اعادة كلمة المرور بنجاح');

        } else 
        {
            return redirect('reset/password')->with('error' , 'يوجد مشكله اثناء اعاده كلمة المرور  الخاصه  بك برجاء المحاولة مره اخري');
        }
    }
    
} // end of class
