<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Repositories\CategoryRepositoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public$categoryrepository;

    public function __construct(CategoryRepositoryInterface $categoryrepository)
    {

        $this->categoryrepository = $categoryrepository;
        
    } // end of construct

    public function index()
    {

        $subcategories = $this->categoryrepository->getWhere(['parent_id' => 1]);
        
        return view('dashboard.categories.index' , compact('subcategories'));

    } // end of index

    public function create()
    {

        $categories = $this->categoryrepository->category();

        return view('dashboard.categories.create' , compact('categories'));

    } // end of create

    public function store(CategoryRequest $request)
    {

        $attribute = $request->except('_token');

        $attribute['parent_id'] = 1;

        $this->categoryrepository->create($attribute);

        return redirect()->route('dashboard.categories.index')->with('succcess' , 'تم اضافه النوع بنجاح');

    } // end of store

    public function edit($subcat)
    {

        $categories = $this->categoryrepository->category();

        $subcategory = $this->categoryrepository->findOne($subcat);

        return view('dashboard.categories.edit' , compact('subcategory' , 'categories'));
    } // end of edit

    public function update(CategoryRequest $request , $subcat)
    {
        $attribute = $request->except('_token' , '_method');

        $attribute['parent_id'] = 1;

        $this->categoryrepository->update($attribute , $subcat);

        return redirect()->route('dashboard.categories.index')->with('success' , 'تم تعديل النوع بنجاح');
    } // end of update

    public function destroy($subcat)
    {

        $this->categoryrepository->delete($subcat);

        return response()->json();
        
    } // end of destroy

} // end if class
