<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\OrderRepositoryInterface;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public $orderrepository;

    public function __construct(OrderRepositoryInterface $orderrepository)
    {

        $this->orderrepository = $orderrepository;

    } // end of construct

    public function index()
    {

        $orders = $this->orderrepository->getAll();

        return view('dashboard.orders.index' , compact('orders'));

    } // end of index

    public function show($id)
    {

        $order = $this->orderrepository->findOne($id);

        return view('dashboard.orders.show' , compact('order'));

    } // end of show
    
} // end of class
