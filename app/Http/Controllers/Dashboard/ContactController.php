<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Api\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Repositories\ContactRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    
    public $contactrepostiory;

    public function __construct(ContactRepositoryInterface $contactrepostiory)
    {
        
        $this->contactrepostiory = $contactrepostiory;

    } // end of construct

    public function index()
    {

        $contacts = $this->contactrepostiory->getAll();

        return view('dashboard.contacts.index' , compact('contacts'));

    } // end of index

    public function destroy($id)
    {
 
        $contact = $this->contactrepostiory->findOne($id);

        $contact->delete();

        return response()->json();

    } //  end of destroy

    public function reply(Request $request)
    {
        
        $data = [
            'message' => $request->message
        ];

        Mail::to($request->email)->send(new SendMail($data));

        return response()->json();

    } // end of reply

} // end of index
