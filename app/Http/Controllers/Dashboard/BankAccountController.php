<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BankAccountRequest;
use App\Repositories\BankAccountRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BankAccountController extends Controller
{

    public $bankaccountrepository;

    public function __construct(BankAccountRepositoryInterface $bankaccountrepository)
    {

        $this->bankaccountrepository = $bankaccountrepository;
        
    } // end of construct

    public function index()
    {

        $bank_accounts = $this->bankaccountrepository->getAll();
        
        return view('dashboard.bank_accounts.index' , compact('bank_accounts'));

    } // end of index

    public function create()
    {


        return view('dashboard.bank_accounts.create');

    } // end of create

    public function store(BankAccountRequest $request)
    {

        $attribute = $request->except('_token' , 'logo');

        if($request->has('logo')){

            // Upload new logo
            $attribute['logo'] = $request->file('logo')->store('bankaccount');
        
        }        

        $this->bankaccountrepository->create($attribute);
        
        return redirect()->route('dashboard.bank-accounts.index')->with('success' , 'تم اضافه الحساب البنكي بنجاح');

    } // end of store

    public function edit($bank)
    {
        
        $bank_account = $this->bankaccountrepository->findOne($bank);

        return view('dashboard.bank_accounts.edit' , compact('bank_account'));
        
    } // end of edit

    public function update(BankAccountRequest $request , $ban)
    {

        $attribute = $request->except('_token' , '_method' , 'logo');

        $bank = $this->bankaccountrepository->findOne($ban);      

        if($request->has('logo')){

            // Delete old internal_image
            Storage::delete($bank->logo);

            // Upload new internal_image
            $attribute['logo'] = $request->file('logo')->store('bankaccount');

        }          

        $this->bankaccountrepository->update($attribute , $ban);      

        return redirect()->route('dashboard.bank-accounts.index')->with('success' , 'تم تعديل الحساب البنكي بنجاح');

    } // end of update

    public function destroy($bank)
    {

        $this->bankaccountrepository->delete($bank);

        return response()->json();
        
    } // end of destroy
}
