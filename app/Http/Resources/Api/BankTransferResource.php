<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class BankTransferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'commission_amount' => $this->commission_amount,
            'numebr_product'    => $this->numebr_product,
            'name_user'         => $this->name_user,
            'transfer_date'     => $this->transfer_date,
            'transfer_name'     => $this->transfer_name,
            'phone'             => $this->phone,
            'notes'             => $this->notes,
            'image'             => $this->imageone(),

        ];
    }
}
