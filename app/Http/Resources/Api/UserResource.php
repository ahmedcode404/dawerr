<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->type_id == 1) {

            return [
                
                'id'                  => $this->id,
                'first_name'          => $this->first_name,
                'second_name'         => $this->second_name,
                'last_name'           => $this->last_name,
                'email'               => $this->email,
                'phone'               => $this->phone,
                'image'               => $this->imageuser(),
                'city'                => $this->city->name,
                'code'                => $this->code,
                'type_id'             => $this->type_id,
                'category'            => $this->category,
                'lat'                 => $this->lat,
                'lng'                 => $this->lng,
                'address'             => $this->address,
                'active'              => $this->account_verify == 1 ? $this->account_verify : 0 
    
            ];

        } else 
        {

            return [
                
                'id'                  => $this->id,
                'first_name'          => $this->first_name,
                'second_name'         => $this->second_name,
                'last_name'           => $this->last_name,
                'email'               => $this->email,
                'phone'               => $this->phone,
                'image'               => $this->imageuser(),
                'commercial_register' => $this->commercial_register,
                'tax_number'          => $this->tax_number,
                'lat'                 => $this->lat,
                'lng'                 => $this->lng,
                'address'             => $this->address,
                'name_bank'           => $this->name_bank,
                'name_account_bank'   => $this->name_account_bank,
                'number_account_bank' => $this->number_account_bank,
                'number_eban_bank'    => $this->number_eban_bank,
                'city'                => $this->city->name,
                'code'                => $this->code,
                'type_id'             => $this->type_id,
                'category'            => $this->category,
                'active'              => $this->account_verify == 1 ? $this->account_verify : 0

            ];

        } // end  of else if request type == user
    }
}
