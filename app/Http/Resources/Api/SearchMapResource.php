<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchMapResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id'      =>  $this->id,
            'lat'     =>  $this->lat,
            'lng'     =>  $this->lng,
            'address' =>  $this->address,
            'name' =>  $this->name,
            'type' =>  $this->type->name,
            'city' =>  $this->city->name,
            'space' =>  $this->space,
            'image'   =>  $this->imageone(),
            'price'   =>  $this->price . ' رس',
            'attributeAndOption' => AttributeAndOptionProduct::collection($this->attributeandoption),

        ];
    }
}
