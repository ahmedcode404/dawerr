<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        // $user = JWTAuth::parseToken()->authenticate();

        // if ($user) {

            try {

                $user = JWTAuth::parseToken()->authenticate();
                $id = $user->id;
                
            } catch (\Throwable $th) {

                $user = null;
                $id = Null;

            }            

            # code...
            if ($this->category_id == 1) {
    
                // ===================================== السيارات ==========================
                return [
        
                    'id'                 => $this->id,
                    'name'               => $this->name,
                    'company'            => $this->name_company,
                    'code'               => $this->code,
                    'lat'                => $this->lat,
                    'lng'                => $this->lng,
                    'address'            => $this->address,
                    'type'               => $this->type ? $this->type->name : '',
                    'type_id'            => $this->type->id,
                    'main_category'      => $this->category_id,
                    'category'           => $this->category,
                    'image_one'          => $this->imageone(),
                    'image_two'          => $this->imagetwo(),
                    'image_three'        => $this->imagethree(),
                    'price'              => $this->price,
                    'phone'              => $this->phone,
                    'show_phone'         => $this->show_phone,
                    'email'              => $this->email,
                    'the_walker'         => $this->the_walker . ' كم ',
                    'capacity'          => $this->capacity,
                    'addition_id'        => $this->addition_id,
                    'make_year'          => $this->make_year,
                    'capacity'           => $this->capacity,
                    'status'             => $this->status,
                    'date_sale'          => $this->date_sale,
                    'favorite'           => $this->favorites->contains('user_id' , $id) ? true : false,
                    'user_name'          => $this->user ? $this->user->first_name . ' ' . $this->user->second_name . ' ' . $this->user->last_name : '',
                    'content'            => $this->details,
                    'phone'              => $this->user->phone,
                    'attributeAndOption' => AttributeAndOptionProduct::collection($this->attributeandoption()->limit(2)->get()),
                    'images' => ImageResource::collection($this->images)
                ];
                // ===================================== السيارات ==========================
                
            } else  
            {
    
                // ===================================== العقارات ==========================
                return [
        
                    'id'                         => $this->id,
                    'name'                       => $this->name,
                    'code'                       => $this->code,
                    'lat'                        => $this->lat,
                    'lng'                        => $this->lng,
                    'address'                    => $this->address,
                    'type'                       => $this->type ? $this->type->name : '',
                    'type_id'                    => $this->type->id,
                    'main_category'              => $this->category_id,
                    'image_one'                  => $this->imageone(),
                    'image_two'                  => $this->imagetwo(),
                    'image_three'                => $this->imagethree(),
                    'price'                      => $this->price . ' رس',
                    'phone'                      => $this->phone,
                    'show_phone'                 => $this->show_phone,
                    'email'                      => $this->email,
                    'status'                     => $this->status,
                    'user_name'                  => $this->user ? $this->user->first_name . ' ' . $this->user->second_name . ' ' . $this->user->last_name : '',
                    'number_of_shops'            => $this->number_of_shops,
                    'neighborhood'               => $this->neighborhood,
                    'street_view'                => $this->street_view,
                    'border'                     => $this->border,
                    'number_halls_for_two'       => $this->number_halls_for_two,
                    'women_boards_for_two'       => $this->women_boards_for_two,
                    'men_boards_for_two'         => $this->men_boards_for_two,
                    'number_of_bedrooms_for_two' => $this->number_of_bedrooms_for_two,
                    'number_halls_for_one'       => $this->number_halls_for_one,
                    'women_boards_for_one'       => $this->women_boards_for_one,
                    'men_boards_for_one'         => $this->men_boards_for_one,
                    'number_of_bedrooms_for_one' => $this->number_of_bedrooms_for_one,
                    'shops'                      => $this->shops,
                    'number_of_sections'         => $this->number_of_sections,
                    'inside_height'              => $this->inside_height,
                    'street_or_lane_width'       => $this->street_or_lane_width,
                    'fuel_tank_capacity'         => $this->fuel_tank_capacity,
                    'number_pumps'               => $this->number_pumps,
                    'on_highway'                 => $this->on_highway,
                    'storehouse'                 => $this->storehouse,
                    'number_bathrooms'           => $this->number_bathrooms,
                    'number_of_swimming_pools'   => $this->number_of_swimming_pools,
                    'number_kitchens'            => $this->number_kitchens,
                    'number_halls'               => $this->number_halls,
                    'women_councils'             => $this->women_councils,
                    'men_boards'                 => $this->men_boards,
                    'number_of_master_bedrooms'  => $this->number_of_master_bedrooms,
                    'number_bedroom'             => $this->number_bedroom,
                    'number_of_roles'            => $this->number_of_roles,
                    'building_erea'              => $this->building_erea,
                    'space'                      => $this->space,
                    'details'                    => $this->details,
                    'phone'                      => $this->user->phone,
                    'user_name'                  => $this->user ? $this->user->first_name . ' ' . $this->user->second_name . ' ' . $this->user->last_name : '',                        
                    'favorite'                   => $this->favorites->contains('user_id' , $id) ? true : false,
                    'attributeAndOption'         => AttributeAndOptionProduct::collection($this->attributeandoption()->limit(2)->get()),
                    'images' => ImageResource::collection($this->images),
                    'directione' => CityResource::collection($this->directione)
                ];
                
                // ===================================== العقارات ==========================
    
            } // end of else category id == 2

        // } else 
        // {

        //     return [
        //         'error' => 'لا يوجد مستخدم'
        //     ];

        // } // end of else if not user

    }
}
