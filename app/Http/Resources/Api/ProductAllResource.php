<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductAllResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        // ===================================== السيارات ==========================
        return [
            
            'data' => $this->collection->transform(function($product){

                try {

                    $user = JWTAuth::parseToken()->authenticate();
                    $id = $user->id;
                    
                } catch (\Throwable $th) {

                    $user = null;
                    $id = Null;

                }

                if ($product->category_id == 1)
                {

                    return [                
        
                        'id'                 => $product->id,
                        'name'               => $product->name,
                        'company'            => $product->name_company,
                        'code'               => $product->code,
                        'lat'                => $product->lat,
                        'lng'                => $product->lng,
                        'address'            => $product->address,
                        'type'               => $product->type->name,
                        'type_id'            => $product->type->id,
                        'type_account'       => $product->user->typeauth->name,
                        'main_category'      => $product->category_id,
                        'category'           => $product->category,
                        'image_one'          => $product->imageone(),
                        'image_two'          => $product->imagetwo(),
                        'image_three'        => $product->imagethree(),
                        'price'              => $product->price . ' رس',
                        'phone'              => $product->phone,
                        'show_phone'         => $product->show_phone,
                        'email'              => $product->email,
                        'the_walker'         => $product->the_walker . '  كم  ',
                        'make_year'          => $product->make_year,
                        'capacity'          => $product->capacity,
                        'addition_id'        => $product->addition_id,
                        'status'             => $product->status,
                        'date_sale'          => $product->date_sale,
                        'favorite'           => $product->favorites->contains('user_id' , $id) ? true : false,
                        'user_name'          => $product->user->first_name . ' ' . $product->user->second_name . ' ' . $product->user->last_name,
                        'content'            => $product->details,
                        'phone'              => $product->user->phone,
                        'attributeAndOption' => AttributeAndOptionProduct::collection($product->attributeandoption()->limit(2)->get()),
                        'images' => ImageResource::collection($product->images)
                    ];
                    
                }else {

                    // ===================================== العقارات ==========================
                    return [
            
                        'id'                         => $product->id,
                        'name'                       => $product->name,
                        'code'                       => $product->code,
                        'lat'                        => $product->lat,
                        'lng'                        => $product->lng,
                        'address'                    => $product->address,
                        'type'                       => $product->type->name,
                        'type_id'                    => $product->type->id,
                        'type_account'               => $product->user->typeauth->name,
                        'main_category'              => $product->category_id,
                        'image_one'                  => $product->imageone(),
                        'image_two'                  => $product->imagetwo(),
                        'image_three'                => $product->imagethree(),
                        'price'                      => $product->price . ' رس',
                        'phone'                      => $product->phone,
                        'show_phone'                 => $product->show_phone,
                        'email'                      => $product->email,
                        'status'                     => $product->status,
                        'favorite'                   => $product->favorites->contains('user_id' , $id) ? true : false,
                        'user_name'                  => $product->user->first_name . ' ' . $product->user->second_name . ' ' . $product->user->last_name,
                        'number_of_shops'            => $product->number_of_shops,
                        'neighborhood'               => $product->neighborhood,
                        'street_view'                => $product->street_view,
                        'border'                     => $product->border,
                        'number_halls_for_two'       => $product->number_halls_for_two,
                        'women_boards_for_two'       => $product->women_boards_for_two,
                        'men_boards_for_two'         => $product->men_boards_for_two,
                        'number_of_bedrooms_for_two' => $product->number_of_bedrooms_for_two,
                        'number_halls_for_one'       => $product->number_halls_for_one,
                        'women_boards_for_one'       => $product->women_boards_for_one,
                        'men_boards_for_one'         => $product->men_boards_for_one,
                        'number_of_bedrooms_for_one' => $product->number_of_bedrooms_for_one,
                        'shops'                      => $product->shops,
                        'number_of_sections'         => $product->number_of_sections,
                        'inside_height'              => $product->inside_height,
                        'street_or_lane_width'       => $product->street_or_lane_width,
                        'fuel_tank_capacity'         => $product->fuel_tank_capacity,
                        'number_pumps'               => $product->number_pumps,
                        'on_highway'                 => $product->on_highway,
                        'storehouse'                 => $product->storehouse,
                        'number_bathrooms'           => $product->number_bathrooms,
                        'number_of_swimming_pools'   => $product->number_of_swimming_pools,
                        'number_kitchens'            => $product->number_kitchens,
                        'number_halls'               => $product->number_halls,
                        'women_councils'             => $product->women_councils,
                        'men_boards'                 => $product->men_boards,
                        'number_of_master_bedrooms'  => $product->number_of_master_bedrooms,
                        'number_bedroom'             => $product->number_bedroom,
                        'number_of_roles'            => $product->number_of_roles,
                        'building_erea'              => $product->building_erea,
                        'space'                      => $product->space,
                        'details'                    => $product->details,
                        'phone'                      => $product->user->phone,
                        'user_name'                  => $product->user->first_name . ' ' . $product->user->second_name . ' ' . $product->user->last_name,                        
                        'favorite'                   => $product->favorites->contains('user_id' , $id) ? true : false,
                        'attributeAndOption'         => AttributeAndOptionProduct::collection($product->attributeandoption()->limit(2)->get()),
                        'images' => ImageResource::collection($product->images),
                        'directione' => CityResource::collection($product->directione)

                    ];
                    // ===================================== العقارات ==========================
                } // end of else product category = 2

            }),

            "links" => [
                "prev" => $this->previousPageUrl(),
                "next" => $this->nextPageUrl(),
            ],

            "meta" => [
                "current_page" => $this->currentPage(),
                "from" => $this->firstItem(),
                "to" => $this->lastItem(),
                "last_page" => $this->lastPage(), // not For Simple
                "per_page" => $this->perPage(),
                'count' => $this->count(), //count of items at current page
                "total" => $this->total() // not For Simple
            ],

            
        ];
        // ===================================== السيارات ==========================
            
    }
}
