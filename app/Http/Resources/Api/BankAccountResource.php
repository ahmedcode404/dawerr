<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class BankAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [

            'id'             => $this->id,
            'name'           => $this->name,
            'number_account' => $this->number_account,
            'number_eban'    => $this->number_eban,
            'logo'           => $this->logoOne(),

        ];

    }
}
