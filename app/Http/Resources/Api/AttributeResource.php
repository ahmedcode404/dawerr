<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id'      => $this->id,
            'cat'     => $this->cat,
            'name'    => $this->name,
            'type'    => $this->type,
            'options' => OptionResource::collection($this->options)
        ];
    }
}
