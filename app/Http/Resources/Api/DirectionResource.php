<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class DirectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id'   => $this->id,
            'value' => $this->name,
            'name' => $this->dire->name,
            'direction' => $this->direct->name,

        ];
    }
}
