<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use tidy;

class StaticPageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->key == 'about_application') {


            return [
    
                'title'    => $this->title,
                'logo'     => $this->image,
                'content'  => $this->content,
    
            ];
            
        } else 
        {

            return [
    
                'title'   => $this->title,
                'content' => $this->content,
    
            ];

        }

    }
}
