<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributeAndOptionProduct extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id'           => $this->option ? $this->option->id : '',
            'attribute_id' => $this->attribute ? $this->attribute->id : '',
            'attribute'    => $this->attribute ? $this->attribute->name : '',
            'option'       => $this->option ? $this->option->name : '',
            'type'         => $this->attribute ? $this->attribute->type : '',

        ];
    }
}


