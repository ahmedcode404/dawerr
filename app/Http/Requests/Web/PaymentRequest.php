<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        $productrepository =  \App::make('App\Repositories\ProductRepositoryInterface');

        $product = $productrepository->getWhere([['code' , '#'.$request->numebr_product] , ['category_id' , session('categoryId')]])->first();
        
        if($product)
        {

            return [

                'commission_amount' => 'required|numeric',
                'name_user'         => 'required|string',
                'transfer_date'     => 'required',
                'transfer_name'     => 'required|string',
                'bank_id'           => 'required|string',
                'phone'             => ['required' , 'regex:/^5/'],
                'image'             => 'required|mimes:png,jpg,jpeg',
                
            ];

        } else 
        {
            
            return [

                'numebr_product'    => 'required|exists:products,code',
                
            ]; 

        } // end of else if product

    }
}
