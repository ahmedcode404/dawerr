<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
            
            if (auth()->user()->category_id == 1) {

                // ================================== السيارات ======================
                // validation add cars
                return [
                    
                    'lat'             => 'required',
                    'lng'             => 'required',
                    'address'         => 'required',
                    'name'            => 'required',
                    'status'          => 'required|string',
                    'category'        => 'required',
                    'agreemnt_1'        => 'required',
                    'agreemnt_2'        => 'required',
                    'price'           => 'required',
                    'city_id'         => 'required|exists:cities,id',
                    'subcategory_id'  => 'required|exists:categories,id,parent_id,' . session('categoryId'),
                    'email'           => 'required|email',
                    'the_walker'      => 'required|numeric',
                    'make_year'       => 'required|numeric',
                    'capacity'        => 'required|numeric',
                    'attribute'       => 'required',
                    'images.*'        => 'mimes:png,jpg,jpeg',
                    'image_one'       => 'required|mimes:png,jpg,jpeg',
                    'image_two'       => 'required|mimes:png,jpg,jpeg',
                    'image_three'     => 'required|mimes:png,jpg,jpeg',
        
                ];
                // ================================== السيارات ======================

            } else 
            {

                // ================================== العقارات ======================

                // validation add realestate
                return [

                    'lat'             => 'required',
                    'lng'             => 'required',
                    'address'         => 'required',
                    'name'            => 'required',
                    'status'          => 'required|string',
                    'price'           => 'required',
                    'city_id'         => 'required|exists:cities,id',
                    'subcategory_id'  => 'required|exists:categories,id,parent_id,' . session('categoryId'),
                    // 'show_phone'      => 'required',
                    'email'           => 'required|email',
                    // 'details'    => 'required|min:10',
                    'attribute'       => 'required',
                    'images.*'        => 'mimes:png,jpg,jpeg',
                    'image_one'       => 'required|mimes:png,jpg,jpeg',
                    'image_two'       => 'required|mimes:png,jpg,jpeg',
                    'image_three'     => 'required|mimes:png,jpg,jpeg',

                    // documentation categories realestate
                    // 6  = عمارة
                    // 7  = شقه
                    // 8  = مزرعة
                    // 9  = محطه بنزين
                    // 10 = اراضي 
                    // 11 = مستودع
                    // 12 = استراحه
                    // 13 = منتجع
                    // 14 = صالة
                    // 15 = محل
                    // 16 = فيلا
                    // 17 = بيت شعبي
                    // 18 = فيلا دور + شقة وملحق
                    // 19 = فيلا دور + شقة
                    // 20 = فيلا دور + شقتين
                    // 21 = فيلا دور + شقتين وملحق
                    // 22 = فيلا دور ونص
                    // 23 = فيلا دور ونص وملحق
                    // 24 = فيلا دور ونص + شقة
                    // 25 = فيلا دور ونص + شقتين
                    // 26 = فيلا دور ونص + شقه وملحق
                    // 27 = فيلا دور ونص + شقتين وملحق
                    // 28 = فيلا دورين 
                    // 29 = فيلا دورين + شقة 
                    // 30 = فيلا دورين + شقتين
                    // 31 = فيلا دورين + شقق وملحق
                    // 32 = فيلا دورين وملحق
                    // 33 = فيلا ثلاث ادوار

                    // realestate
                   'number_of_shops'            => 'required_if:subcategory_id,6|numeric',
                   'neighborhood'               => 'required_if:subcategory_id,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|',
                   'street_view'                => 'required_if:subcategory_id,6,9,10,12,13,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
//                   'border'                     => 'required_if:subcategory_id,6,8,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'number_halls_for_two'       => 'required_if:subcategory_id,20,21,25|numeric',
                   'women_boards_for_two'       => 'required_if:subcategory_id,20,21,25|numeric',
                   'men_boards_for_two'         => 'required_if:subcategory_id,20,21,25|numeric',
                   'number_of_bedrooms_for_two' => 'required_if:subcategory_id,20,21,25|numeric',
                   'number_halls_for_one'       => 'required_if:subcategory_id,18,19,20,21,25,26,29,30,31|numeric',
                   'women_boards_for_one'       => 'required_if:subcategory_id,18,19,20,21,25,26,29,30,31|numeric',
                   'men_boards_for_one'         => 'required_if:subcategory_id,18,19,20,21,25,26,29,30,31|numeric',
                   'number_of_bedrooms_for_one' => 'required_if:subcategory_id,18,19,20,21,25,26,29,30,31|numeric',
                   'shops'                      => 'required_if:subcategory_id,12,13|numeric',
                   'number_of_sections'         => 'required_if:subcategory_id,12,13|numeric',
                   'inside_height'              => 'required_if:subcategory_id,11,14,15|numeric',
                   'street_or_lane_width'       => 'required_if:subcategory_id,11,14,15|numeric',
                   'fuel_tank_capacity'         => 'required_if:subcategory_id,9|numeric',
                   'number_pumps'               => 'required_if:subcategory_id,9|numeric',
                   'on_highway'                 => 'required_if:subcategory_id,9|numeric',
                   'storehouse'                 => 'required_if:subcategory_id,7|numeric',
                   'number_bathrooms'           => 'required_if:subcategory_id,7,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'number_of_swimming_pools'   => 'required_if:subcategory_id,12,13|numeric',
                   'number_kitchens'            => 'required_if:subcategory_id,7,12,13,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'number_halls'               => 'required_if:subcategory_id,7,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'women_councils'             => 'required_if:subcategory_id,7,12,13,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'men_boards'                 => 'required_if:subcategory_id,7,12,13,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'number_of_master_bedrooms'  => 'required_if:subcategory_id,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'number_bedroom'             => 'required_if:subcategory_id,7,12,13,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'number_of_roles'            => 'required_if:subcategory_id,6,7,14,15|numeric',
                   'building_erea'              => 'required_if:subcategory_id,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',
                   'space'                      => 'required_if:subcategory_id,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33|numeric',

                ];  

                // ================================== العقارات ======================

            }


    }
}
