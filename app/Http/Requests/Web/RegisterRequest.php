<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {


        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {

                    return [

                        'company_name'        => 'required_if:type_id,2',
                        'first_name'          => 'required_if:type_id,1',
                        'second_name'         => 'required_if:type_id,1',
                        'last_name'           => 'required_if:type_id,1',
                        'email'               => 'required|email|unique:users,email',
                        'phone'               => ['required' , 'unique:users,phone'],
                        'city_id'             => 'required|exists:cities,id',
                        'password'            => 'required|min:6',
                        'confirm_password'    => 'required|same:password',
                        'type_id'             => 'required|exists:type_auths,id',
                        'commercial_register' => 'required_if:type_id,2',
                        'tax_number'          => 'required_if:type_id,2',
                        'name_bank'           => 'required_if:type_id,2',
                        'name_account_bank'   => 'required_if:type_id,2',
                        'number_account_bank' => 'required_if:type_id,2',
                        'number_eban_bank'    => 'required_if:type_id,2',
            
                    ];

                }
                case 'PUT':
                case 'PATCH':
                {
                    
                    return [

                        'company_name'        => 'required_if:type_id,2',
                        'first_name'          => 'required_if:type_id,1',
                        'second_name'         => 'required_if:type_id,1',
                        'last_name'           => 'required_if:type_id,1',
                        'email'               => 'required|email|unique:users,email,' . $this->id,
                        'phone'               => ['required' , 'unique:users,phone,' . $this->id],
                        'city_id'             => 'required|exists:cities,id',
                        'password'            =>  $this->password ? 'min:6' : '',
                        'confirm_password'    =>  $this->password ? 'required|same:password' : '',
                        'commercial_register' => 'required_if:type_id,2',
                        'tax_number'          => 'required_if:type_id,2',
                        'name_bank'           => 'required_if:type_id,2',
                        'name_account_bank'   => 'required_if:type_id,2',
                        'number_account_bank' => 'required_if:type_id,2',
                        'number_eban_bank'    => 'required_if:type_id,2',

                    ];

                }
                default:break;

        } // end of switch

    }
}
