<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateUserRequest extends MasterApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {

            $request->merge(array('type_id' => $user->type_id));
    
            return [
    
                'first_name'          => 'required',
                'second_name'         => $request->type_id == 1 ? 'required' : '',
                'last_name'           => $request->type_id == 1 ? 'required' : '',
                'email'               => ['required' , Rule::unique('users')->ignore($user->id)],
                'phone'               => ['required' , Rule::unique('users')->ignore($user->id) , 'regex:/^5/'],
                'city_id'             => 'required|exists:cities,id',
                'commercial_register' => $request->type_id == 2 ? 'required|numeric' : '',
                'tax_number'          => $request->type_id == 2 ? 'required|numeric' : '',
                'name_bank'           => $request->type_id == 2 ? 'required' : '',
                'name_account_bank'   => $request->type_id == 2 ? 'required' : '',
                'number_account_bank' => $request->type_id == 2 ? 'required|numeric' : '',
                'number_eban_bank'    => $request->type_id == 2 ? 'required|numeric' : '',            
                'category_id'         => 'required|exists:categories,id',
    
            ];

        } else 
        {
            return [
                'user' => 'required'
            ];
        }
        
        

    }
}
