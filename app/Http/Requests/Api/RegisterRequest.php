<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends MasterApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

            'first_name'          => 'required',
            'second_name'         => 'required_if:type_id,1',
            'last_name'           => 'required_if:type_id,1',
            'email'               => 'required|email|unique:users,email',
            'phone'               => ['required' , 'unique:users,phone' , 'regex:/^5/'],
            'city_id'             => 'required|exists:cities,id',
            'password'            => 'required|min:6',
            'confirm_password'    => 'required|same:password',
            'type_id'             => 'required|exists:type_auths,id',
            'commercial_register' => 'required_if:type_id,2|numeric',
            'tax_number'          => 'required_if:type_id,2|numeric',
            'name_bank'           => 'required_if:type_id,2',
            'name_account_bank'   => 'required_if:type_id,2',
            'number_account_bank' => 'required_if:type_id,2|numeric',
            'number_eban_bank'    => 'required_if:type_id,2|numeric',
            'device_id'           => 'required',
            'firebase_token'      => 'required',
            'category_id'         => 'required|exists:categories,id',
            
        ];

    }
}
