<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends MasterApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'commission_amount' => 'required|numeric',
            'numebr_product'    => 'required|exists:products,code',
            'name_user'         => 'required|string',
            'transfer_date'     => 'required',
            'transfer_name'     => 'required|string',
            'bank_id'           => 'required',
            'phone'             => ['required' , 'regex:/^5/'],
            'image'             => 'required',
            
        ];
    }
}
