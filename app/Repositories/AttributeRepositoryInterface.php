<?php

namespace App\Repositories;

interface AttributeRepositoryInterface
{
    public function attribute();
    public function option();
}
