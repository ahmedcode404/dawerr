<?php

namespace App\Repositories;

interface CategoryRepositoryInterface
{
    public function subcategory();
    public function category();
}
