<?php

namespace App\Repositories\Eloquent;

use App\Models\Agreement;
use App\Repositories\AgreementRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class AgreementRepository extends BaseRepository implements AgreementRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Agreement();
        
    } // end of constuct 
    
}
