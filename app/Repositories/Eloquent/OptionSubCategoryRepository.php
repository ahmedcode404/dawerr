<?php

namespace App\Repositories\Eloquent;

use App\Models\OptionSubCategory;
use App\Repositories\OptionSubCategoryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class OptionSubCategoryRepository extends BaseRepository implements OptionSubCategoryRepositoryInterface
{

    public function __construct()
    {
        $this->model = new OptionSubCategory();
        
    } // end of constuct

}
