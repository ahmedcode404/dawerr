<?php

namespace App\Repositories\Eloquent;

use App\Models\Slider;
use App\Repositories\SliderRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class SliderRepository extends BaseRepository implements SliderRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Slider();
        
    } // end of constuct 
    
}
