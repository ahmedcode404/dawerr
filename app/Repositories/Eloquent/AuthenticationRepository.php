<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\AuthenticationRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class AuthenticationRepository extends BaseRepository implements AuthenticationRepositoryInterface
{

    public function __construct()
    {
        $this->model = new User();
        
    } // end of constuct
    
    public function WhereOrWhere($data1 , $data2 = null)
    {
        return $this->model->where($data1)->orWhere($data2)->first();
    }
    
}
