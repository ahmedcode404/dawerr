<?php

namespace App\Repositories\Eloquent;

use App\Models\PriceFillter;
use App\Repositories\PriceFillterRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class PriceFillterRepository extends BaseRepository implements PriceFillterRepositoryInterface
{

    public function __construct()
    {
        $this->model = new PriceFillter();
        
    } // end of constuct
    
}
