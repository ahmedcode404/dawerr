<?php

namespace App\Repositories\Eloquent;

use App\Models\Directione;
use App\Repositories\DirectioneRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class DirectioneRepository extends BaseRepository implements DirectioneRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Directione();
        
    } // end of constuct 
    
}
