<?php

namespace App\Repositories\Eloquent;

use App\Models\Contact;
use App\Repositories\ContactRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ContactRepository extends BaseRepository implements ContactRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Contact();
        
    } // end of constuct 
    
}
