<?php

namespace App\Repositories\Eloquent;

use App\Models\Report;
use App\Repositories\ReportRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ReportRepository extends BaseRepository implements ReportRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Report();
        
    } // end of constuct 
    
}
