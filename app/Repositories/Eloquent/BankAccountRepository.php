<?php

namespace App\Repositories\Eloquent;

use App\Models\BankAcount;
use App\Repositories\BankAccountRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class BankAccountRepository extends BaseRepository implements BankAccountRepositoryInterface
{

    public function __construct()
    {
        $this->model = new BankAcount();
        
    } // end of constuct
    
}
