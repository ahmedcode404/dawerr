<?php

namespace App\Repositories\Eloquent;

use App\Models\Dir;
use App\Repositories\DirRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class DirRepository extends BaseRepository implements DirRepositoryInterface
{

    public function __construct()
    {

        $this->model = new Dir();
        
    } // end of constuct 
    
}
