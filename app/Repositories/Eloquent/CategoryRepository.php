<?php

namespace App\Repositories\Eloquent;

use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Category();
        
    } // end of constuct

    public function subcategory()
    {

        return $this->model->where('parent_id', '!=' ,  null)->get();

    } // end of sub category

    public function category()
    {

        return $this->model->where('type' , 'main')->get();

    } // end of category 
    
}
