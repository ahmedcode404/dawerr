<?php

namespace App\Repositories\Eloquent;

use App\Models\TypeAuth;
use App\Repositories\TypeAuthRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class TypeAuthRepository extends BaseRepository implements TypeAuthRepositoryInterface
{

    public function __construct()
    {
        $this->model = new TypeAuth();
        
    } // end of constuct 
    
}
