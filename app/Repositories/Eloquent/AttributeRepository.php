<?php

namespace App\Repositories\Eloquent;

use App\Models\Attribute;
use App\Repositories\AttributeRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class AttributeRepository extends BaseRepository implements AttributeRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Attribute();
        
    } // end of constuct
    
    public function attribute()
    {

        return $this->model->where('category_id' , '!=' , null)->get();

    } // end of attribute

    public function option()
    {

        return $this->model->where('category_id'  , null)->get();
        
    } // end of option    
}
