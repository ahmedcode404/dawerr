<?php

namespace App\Servicies;

use App\Models\Charity;
use App\Models\DeviceToken;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App;

class Notify{


    // GET TOKEN OF SEND TO NOTIFICATION 
    public static function getTokens($send_to, $id = null)
    {

        if($send_to == "new_user"){

            return User::where([['is_notify' , 1] , ['type' , 'admin']])->pluck('device_token');

        }
        elseif($send_to == "charity") {

            return Charity::where([ ['is_notify' , 1] , ['id' , $id]])->pluck('device_token');

        } 
        elseif($send_to == "user"){

            return User::where('is_notify' , 1)->pluck('device_token');
        }
        elseif($send_to == "adminOrEmployee") {

            return User::where('is_notify' , 1)->pluck('device_token');

        } // end of if send to users        

    } // end of function get tokens


    // SEND NOTIFICATION TO USER IN MOBILE
    public static function NotifyMobile($message, $title , $send_to , $user_send_to , $user_id = null)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $tokenList;

        if($send_to == 'userOne'){
            $tokenList = SELF::getTokens($send_to, $user_send_to);
        }
        else{
            $tokenList = SELF::getTokens($send_to);
        }

        $notification = [
            'title' => $title,
            'body' => \App::getLocale() == 'ar' ? $message->message_ar : $message->message_en,
            'user_id' => $user_id,
            'image' => '/assets/images/logo/logo.png',
            'sound' => true,
        ];

        $extraNotificationData = [
            'user_id' => $user_id,
            "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
            "sound"=> "default",
            "badge"=> "8",
            "color"=> "#ffffff",
            "priority" => "high",
        ];

        $fcmNotification = [
            'registration_ids' => $tokenList, //multple token array
            'notification' => $notification,
            'data' => $extraNotificationData,
        ];

        $headers = [
            'Authorization: key=AAAAukz4Ov4:APA91bGNsdT1F1wRM_9uw0Sid1tv4IODW18KMBUb4s-pYYm605g1Gjcz2zMujkhvJgGANe0pT2aPevzFC5OAkVTh80XEPWoZH241a9azAKyncdNPnqzvBfnabvlOdjE-4xXPC9zy8UB0',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);

        curl_close($ch);

    } // end of NotifyMobile


    // SEND NOTIFICATION TO  USER WEB
    public static function NotifyWeb($message , $send_to , $title = null , $send_user_id = null)
    {

        $tokenList;

        if($send_to == 'new_user'){

            $tokenList = SELF::getTokens($send_to);

        }
        elseif ($send_to == 'charity'){

            $tokenList = SELF::getTokens($send_to , $send_user_id);

        } 
        elseif($send_to == "user"){
            
            $tokenList = SELF::getTokens($send_to , $send_user_id);
        }
        elseif ($send_to == 'adminOrEmployee'){

            $tokenList = SELF::getTokens($send_to , $send_user_id);

        }         

        $SERVER_API_KEY = 'AAAAxYxN9tI:APA91bFwl1LMDcuXfDa6PePpwxo_CEcrmhmBLHC4XpArkk_IbYyAKzlx-0Dc8gqhkXyL0DjPK6WRsp-l7Mhpn_hgY95VukH4cNdoo-Q0uuBXbeNP_3bIpyRJetBjbgNx4kh8sTsGI2Hw';
                
        $data = [
            
            "registration_ids" => $tokenList,
            "notification" => [

                'title' => $title,
                'body'  => $message,
                "icon"  => url('dashboard/images/avatar.png'),

            ]
        ];
        
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);

    } // end of notify web


} // end of class

