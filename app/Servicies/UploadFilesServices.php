<?php

namespace App\Servicies;
use Illuminate\Http\Request;
use App\Classes\FileOperations;
use App\Models\Category;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\User;
use App\Repositories\ProjectRepositoryInterface;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UploadFilesServices
{


    public function __construct()
    {



    } // end of constract

    public function uploadfile($img,$dir){

        $pathAfterUpload = FileOperations::StoreFileAs($dir, $img, $dir.'_img_'.Str::random(6));

        return $pathAfterUpload;
    } // end of uploade file

    public function updateUploadfile($id, $img,$dir)
    {

        $pathAfterUpload = FileOperations::StoreFileAs($dir, $img, $dir.'_img_'.Str::random(10));

         return $pathAfterUpload;

    } // end of ypdate file

} // end of class
