@extends('dashboard.layout.app')
@section('title', 'الاعدادات')
@section('content')
@push('js')
<link rel="stylesheet" href="{{url('intlTelInput.min.css')}}" type="text/css" />
@endpush
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.static-pages.index') }}">الاعدادات</a>
                                    </li>
                                    <li class="breadcrumb-item active">تعديل
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">الاعدادات</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('dashboard.settings.store') }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                                        @csrf

                                        <div class="row">
                                            @foreach($settings as $setting)

                                                @if($setting->type == 'file')
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="price_from">الشعار </label>
                                                        <input type="file" id="logo" name="logo" accept="image/png, image/jpeg, image/jpg" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" require/>
                                                        <div class="">
                                                            @if ($errors->has('logo'))
                                                                <span class="help-block">
                                                                    <strong style="color: red;">{{ $errors->first('logo') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>                                          
                                                    </div>  
                                                    <div class="form-group prev">
                                                        <img src="{{ url('storage/' . $setting->value) }}" style="width: 100px" class="img-thumbnail preview-logo" alt="">
                                                    </div> 
                                                </div>  
                                                                                        
                                                @elseif($setting->type == 'textarea')

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="title">{{ $setting->neckname }} </label>
                                                            <textarea name="{{ $setting->key }}" class="form-control" name="{{ $setting->key }}" id="{{ $setting->key }}" cols="30" rows="10" require>{{ $setting->value }}</textarea>
                                                            <div class="">
                                                                @if ($errors->has('title'))
                                                                    <span class="help-block">
                                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>                                          
                                                        </div>
                                                    </div>

                                                @elseif($setting->type == 'url')

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="title">{{ $setting->neckname }} </label>
                                                            <input type="{{ $setting->type }}" id="{{ $setting->key }}" name="{{ $setting->key }}" value="{{ $setting->value }}" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" require/>
                                                            <div class="">
                                                                @if ($errors->has('title'))
                                                                    <span class="help-block">
                                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>                                          
                                                        </div>
                                                    </div>
                                                        
                                                @elseif($setting->type == 'number')

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="title">{{ $setting->neckname }} </label>
                                                            <input type="{{ $setting->key == 'phone' ? 'text' : $setting->type }}" id="{{ $setting->key }}" name="{{ $setting->key }}" value="{{ $setting->value }}" class="form-control {{ $setting->key }}" aria-label="Name" aria-describedby="basic-addon-name" require/>
                                                            @if($setting->key == 'phone')
                                                            <input type="hidden" name="phone_key" id="{{ $setting->key }}">
                                                            @endif
                                                            <div class="">
                                                                @if ($errors->has('title'))
                                                                    <span class="help-block">
                                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>                                          
                                                        </div>
                                                    </div>

                                                @elseif($setting->type == 'email')

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="title">{{ $setting->neckname }} </label>
                                                            <input type="{{ $setting->type }}" id="{{ $setting->key }}" name="{{ $setting->key }}" value="{{ $setting->value }}" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" require/>
                                                            <div class="">
                                                                @if ($errors->has('title'))
                                                                    <span class="help-block">
                                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>                                          
                                                        </div>
                                                    </div> 

                                                @elseif($setting->key == 'commission_account_text')

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="title">{{ $setting->neckname }} </label>
                                                            <textarea name="{{ $setting->key }}" class="form-control" name="{{ $setting->key }}" id="{{ $setting->key }}" cols="30" rows="10" require>{{ $setting->value }}</textarea>
                                                            <div class="">
                                                                @if ($errors->has('title'))
                                                                    <span class="help-block">
                                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>                                          
                                                        </div>
                                                    </div>
                                                     
                                                    
                                                @elseif($setting->key == 'address')

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="title">{{ $setting->neckname }} </label>
                                                            <input type="{{ $setting->type }}" id="{{ $setting->key }}" name="{{ $setting->key }}" value="{{ $setting->value }}" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" require/>
                                                            <div id="map"></div>
                                                            <div class="">
                                                                @if ($errors->has('title'))
                                                                    <span class="help-block">
                                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>                                          
                                                        </div>
                                                    </div> 
                                                    
                                                @elseif($setting->key == 'lat')

                                                    <input type="hidden" name="{{$setting->key}}" id="{{ $setting->key }}" value="{{$setting->value}}">
                                                @elseif($setting->key == 'lng')

                                                    <input type="hidden" name="{{$setting->key}}" id="{{ $setting->key }}" value="{{$setting->value}}">
                                                   
                                                @endif
                                                    
                                            @endforeach       
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">تحديث <i data-feather="edit"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')

    <script src="{{ url('admin') }}/app-assets/js/forms/settings/edit.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/custom/preview-image.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/custom/map.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <script src="{{ url('intlTelInput.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('utils.js') }}" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdarVlRZOccFIGWJiJ2cFY8-Sr26ibiyY&libraries=places&callback=initAutocomplete&language=<?php echo e('ar'); ?>"
    defer></script>    
    <script>
            CKEDITOR.replace( 'about' , {
                language: 'ar'
            });
    </script>    

@endpush

@endsection