@extends('dashboard.layout.app')
@section('title', 'الاعدادات')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="#">Datatable</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">الاعدادات
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>               
            </div>
            <div class="content-body">

                <!-- BEGIN: section -->
                <section id="dashboard-analytics">

                    <!-- List DataTable -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <table class="datatables-basic table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> العنوان</th>
                                            <th> المحتوي</th>
                                            <th> الصوره</th>
                                            <th>الاختيارات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($static_pages as $static_page)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $static_page->title }}</td>
                                            <td>{{ $static_page->content }}</td>
                                            <td><img src="{{ url('storage/' . $static_page->image) }}" width="50px" alt=""></td>
                                            <td>

                                                <a href="{{route('dashboard.static-pages.edit', $static_page->id)}}" class="action-edit"><i class="fa fa-edit"></i></a>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/ List DataTable -->

                </section>
                <!-- END: section -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


@endsection