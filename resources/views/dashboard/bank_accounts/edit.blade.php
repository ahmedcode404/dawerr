@extends('dashboard.layout.app')
@section('title', 'الحسابات البنكيه')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.price-fillters.index') }}">الحسابات البنكيه</a>
                                    </li>
                                    <li class="breadcrumb-item active">تعديل
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">الخيارات</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('dashboard.bank-accounts.update' , $bank_account->id) }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                                        @method('PUT')
                                        @csrf

                                        <div class="form-group">
                                            <label class="form-label" for="price_from">اسم البنك </label>
                                            <input type="text" id="name" name="name" value="{{ $bank_account->name ? $bank_account->name : old('name') }}" class="form-control" placeholder="اسم البنك" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="number_account"> رقم الحساب </label>
                                            <input type="text" id="number_account" name="number_account" value="{{ $bank_account->number_account ? $bank_account->number_account : old('number_account') }}" class="form-control" placeholder="رقم الحساب" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('number_account'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('number_account') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>     
                                        
                                        <div class="form-group">
                                            <label class="form-label" for="number_eban"> رقم الايبان </label>
                                            <input type="text" id="number_eban" name="number_eban" value="{{ $bank_account->number_eban ? $bank_account->number_eban : old('number_eban') }}" class="form-control number_eban" placeholder="رقم الايبان" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('number_eban'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('number_eban') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>                                             

                                        <div class="form-group">
                                            <label class="form-label" for="price_from">الشعار </label>
                                            <input type="file" id="logo" name="logo" accept="image/png, image/jpeg, image/jpg" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" />
                                            <div class="">
                                                @if ($errors->has('logo'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('logo') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>                                        
                                        <div class="form-group prev">
                                            <img src="{{ url('storage/' . $bank_account->logo) }}" style="width: 100px" class="img-thumbnail preview-logo" alt="">
                                        </div>    

                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">تعديل <i data-feather="edit"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')

    <script src="{{ url('admin') }}/app-assets/js/forms/bank_accounts/edit.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/custom/preview-image.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

@endpush

@endsection