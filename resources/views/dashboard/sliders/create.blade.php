@extends('dashboard.layout.app')
@section('title', 'اضافة مدينه')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.cities.index') }}">المدن</a>
                                    </li>
                                    <li class="breadcrumb-item active">اضافة
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">المدن</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('dashboard.sliders.store') }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                                        @csrf

                                        <div class="form-group">
                                            <label for="select-country1">التصنيف</label>
                                            <select class="form-control" id="select-country1" name="category_id" required>
                                                <option value="">اختر التصنيف</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="">
                                                @if ($errors->has('category_id'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('category_id') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                              
                                        </div>                                        

                                        <div class="form-group">
                                            <label class="form-label" for="title"> العنوان</label>
                                            <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control" placeholder="العنوان" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('title'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                        
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="content">المحتوي</label>
                                            <textarea id="content" class="form-control" name="content" cols="30" rows="10">{{ old('content') }}</textarea>
                                            <div class="">
                                                @if ($errors->has('content'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('content') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                        
                                        </div> 
                                        
                                        <div class="form-group">
                                            <label class="form-label" for="price_from">الشعار </label>
                                            <input type="file" id="image" name="image" accept="image/png, image/jpeg, image/jpg" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" />
                                            <div class="">
                                                @if ($errors->has('image'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('image') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>  
                                                                              
                                        <div class="form-group prev" style="display: none;">
                                            <img src="" style="width: 100px" class="img-thumbnail preview-image" alt="">
                                        </div>    
                                        

                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">اضافة <i data-feather="plus"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')
    <script src="{{ url('admin') }}/app-assets/js/forms/sliders/create.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/custom/preview-image.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

@endpush

@endsection