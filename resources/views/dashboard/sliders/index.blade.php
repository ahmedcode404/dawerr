@extends('dashboard.layout.app')
@section('title', 'السلايدر')
@section('content')


    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="#">Datatable</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">السلايدر
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{ route('dashboard.sliders.create') }}" title="اضافة سلايدر"><i class="mr-1" data-feather="plus"></i><span class="align-middle">اضافه سلايدر</span></a>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="content-body">

                <!-- BEGIN: section -->
                <section id="dashboard-analytics">

                    <!-- List DataTable -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <table class="datatables-basic table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>العنوان</th>
                                            <th>المحتوي</th>
                                            <th>الصورة</th>
                                            <th>القسم</th>
                                            <th>الاختيارات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($sliders as $slider)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $slider->title }}</td>
                                            <td>{{ $slider->content }}</td>
                                            <td><img src="{{ url('storage/' . $slider->image) }}" width="50px" alt=""></td>
                                            <td>{{ $slider->category->name }}</td>
                                            <td>

                                                <a href="{{route('dashboard.sliders.edit', $slider->id)}}" class="action-edit"><i class="fa fa-edit"></i></a>
                                                <a href="{{route('dashboard.sliders.destroy', $slider->id)}}" class="action-delete item-delete"><i class="fa fa-trash"></i></a>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/ List DataTable -->

                </section>
                <!-- END: section -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


@endsection