@extends('dashboard.layout.app')
@section('title', 'الصفحات الثابته')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.static-pages.index') }}">الصفحات الثابته</a>
                                    </li>
                                    <li class="breadcrumb-item active">تعديل
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">الصفحات الثابته</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('dashboard.static-pages.update' , $static_page->id) }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                                        @method('PUT')
                                        @csrf

                                        <div class="form-group">
                                            <label class="form-label" for="title">العنوان </label>
                                            <input type="text" id="title" name="title" value="{{ $static_page->title }}" class="form-control" placeholder="اسم البنك" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('title'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="title">المحتوي </label>
                                            <textarea id="content" name="content" class="form-control" cols="30" rows="10">{{ $static_page->content }}</textarea>
                                            <div class="">
                                                @if ($errors->has('title'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>    

                                        @if($static_page->key == 'about')
                                        

                                        <div class="form-group">
                                            <label class="form-label" for="price_from">الشعار </label>
                                            <input type="file" id="logo" name="logo" accept="image/png, image/jpeg, image/jpg" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" />
                                            <div class="">
                                                @if ($errors->has('logo'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('logo') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>  
                                                    
                                        
                                        <div class="form-group prev">
                                            <img src="{{ url('storage/' . $static_page->image) }}" style="width: 100px" class="img-thumbnail preview-logo" alt="">
                                        </div>                                         


                                        <div class="form-group">
                                            <label class="form-label" for="title2">العنوان الثاني </label>
                                            <input type="text" id="title2" name="title2" value="{{ $static_page->title2 }}" class="form-control" placeholder="اسم البنك" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('title2'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('title2') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="title">المحتوي الثاني </label>
                                            <textarea id="content" name="content2" class="form-control" cols="30" rows="10">{{ $static_page->content }}</textarea>
                                            <div class="">
                                                @if ($errors->has('content2'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('content2') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>  

                                        <div class="form-group">
                                            <label class="form-label" for="price_from">الشعار </label>
                                            <input type="file" id="logo2" name="logo2" accept="image/png, image/jpeg, image/jpg" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" />
                                            <div class="">
                                                @if ($errors->has('logo2'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('logo2') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                          
                                        </div>  
                                                    
                                        
                                        <div class="form-group prev">
                                            <img src="{{ url('storage/' . $static_page->image2) }}" style="width: 100px" class="img-thumbnail preview-logo2" alt="">
                                        </div> 

                                        @endif


                                        @if($static_page->key == 'about_application')

                                            <div class="form-group">
                                                <label class="form-label" for="price_from">الشعار </label>
                                                <input type="file" id="logo" name="logo" accept="image/png, image/jpeg, image/jpg" class="form-control image" aria-label="Name" aria-describedby="basic-addon-name" />
                                                <div class="">
                                                    @if ($errors->has('logo'))
                                                        <span class="help-block">
                                                            <strong style="color: red;">{{ $errors->first('logo') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>                                          
                                            </div>  
                                                        
                                            
                                            <div class="form-group prev">
                                                <img src="{{ url('storage/' . $static_page->image) }}" style="width: 100px" class="img-thumbnail preview-logo" alt="">
                                            </div> 
                                        @endif
                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">تحديث <i data-feather="edit"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')

    <script src="{{ url('admin') }}/app-assets/js/forms/banks/edit.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/custom/preview-image.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

@endpush

@endsection