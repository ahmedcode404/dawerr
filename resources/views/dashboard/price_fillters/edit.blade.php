@extends('dashboard.layout.app')
@section('title', 'تعديل')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.price-fillters.index') }}">بيانات البحث بالسعر</a>
                                    </li>
                                    <li class="breadcrumb-item active">تعديل
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">بيانات البحث بالسعر</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('dashboard.price-fillters.update' , $price_fillter->id) }}" method="POST" class="needs-validation" novalidate>
                                        @method('PUT')
                                        @csrf

                                        <div class="form-group">
                                            <label class="form-label" for="price_from">السعر يبدأ من </label>
                                            <input type="text" id="price_from" name="price_from" value="{{ $price_fillter->price_from }}" class="form-control" placeholder="السعر يبدأ من" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('price_from'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('price_from') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                         
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="price_to">السعر ينتهي الي </label>
                                            <input type="text" id="price_to" name="price_to" value="{{ $price_fillter->price_to }}" class="form-control" placeholder="السعر ينتهي الي" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('price_to'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('price_to') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                         
                                        </div>   

                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">تعديل <i data-feather="edit"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')
    <script src="{{ url('admin') }}/app-assets/js/forms/price_fillters/create.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

@endpush

@endsection