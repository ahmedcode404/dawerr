@extends('dashboard.layout.app')
@section('title', 'اضافة مدينه')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.cities.index') }}">المدن</a>
                                    </li>
                                    <li class="breadcrumb-item active">اضافة
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">المدن</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('dashboard.cities.store') }}" method="POST" class="needs-validation" novalidate>
                                        @csrf

                                        <div class="form-group">
                                            <label class="form-label" for="basic-addon-name">اسم المدينة</label>
                                            <input type="text" id="basic-addon-name" name="name" value="{{ old('name') }}" class="form-control" placeholder="اسم المدينة" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>                                        
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">اضافة <i data-feather="plus"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')
    <script src="{{ url('admin') }}/app-assets/js/forms/cities/create.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

@endpush

@endsection