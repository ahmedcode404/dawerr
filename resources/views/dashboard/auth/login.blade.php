@extends('dashboard.layout-login.app')
@section('title_login', 'تسجيل الدخول')
@section('content')


    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-v1 px-2">
                    <div class="auth-inner py-2">
                        <!-- Login v1 -->
                        <div class="card mb-0">
                            <div class="card-body">
                                <a href="javascript:void(0);" class="brand-logo">
                                    <h2 class="brand-text text-primary ml-1"><img src="{{ url('admin') }}/app-assets/images/logo/logo.png" alt="logo"/></h2>
                                </a>

                                <h4 class="card-title mb-1">مرحبا بك في حراج دور 👋</h4>
                                <p class="card-text mb-2">الرجاء تسجيل الدخول إلى حسابك وبدء المغامرة</p>

                                <form class="auth-login-form mt-2" action="{{ route('login.auth') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="login-email" class="form-label">البريد الالكتروني</label>
                                        <input type="text" class="form-control" id="login-email" name="email" placeholder="john@example.com" aria-describedby="login-email" tabindex="1" autofocus />
                                        <div class="">
                                            @if ($errors->has('login-email'))
                                                <span class="help-block">
                                                    <strong style="color: red;">{{ $errors->first('login-email') }}</strong>
                                                </span>
                                            @endif
                                        </div>                                    
                                    </div>

                                    <div class="form-group">
                                        <div class="d-flex justify-content-between mb-1">
                                            <label for="login-password">كلمة المرور</label>
                                            <a href="{{ route('reset.password') }}">
                                                <small>هل نسيت كلمه المرور ؟</small>
                                            </a>
                                        </div>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            <input type="password" class="form-control form-control-merge" id="login-password" name="password" tabindex="2" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="login-password" />
                                            <div class="input-group-append">
                                                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                            </div>
                                            <div class="invalid-tooltip">
                                            @if ($errors->has('login-password'))
                                                <span class="help-block">
                                                    <strong style="color: red;">{{ $errors->first('login-password') }}</strong>
                                                </span>
                                            @endif
                                        </div>                                              
                                        </div>
                                    </div>

                                    <button class="btn btn-primary btn-block" tabindex="4">تسجبل الدخول</button>
                                </form>


                            </div>
                        </div>
                        <!-- /Login v1 -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Content-->


@endsection