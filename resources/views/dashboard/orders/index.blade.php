@extends('dashboard.layout.app')
@section('title', 'الطلبات')
@section('content')


    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="#">Datatable</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">الطلبات
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- BEGIN: section -->
                <section id="dashboard-analytics">

                    <!-- List DataTable -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <table class="datatables-basic table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>اسم المستخدم</th>
                                            <th>مبلغ العمولة</th>
                                            <th>رقم الاعلان</th>
                                            <th>تاريخ التحويل</th>
                                            <th>اسم المحول</th>
                                            <th>رقم الجوال</th>
                                            <th>صورة الايصال</th>
                                            <th>الاختيارات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($orders as $order)
                                        <tr>                                         
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $order->name_user }}</td>
                                            <td>{{ $order->commission_amount }}</td>
                                            <td>{{ $order->numebr_product }}</td>
                                            <td>{{ $order->transfer_date->isoFormat('ll') }}</td>
                                            <td>{{ $order->transfer_name }}</td>
                                            <td>{{ $order->phone }}</td>
                                            <td><img src="{{ $order->imageone() }}" width="70px" alt=""></td>
                                            <td>

                                                <a href="{{route('dashboard.orders.show', $order->id)}}" class="action-edit"><i data-feather="eye"></i></a>

                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/ List DataTable -->

                </section>
                <!-- END: section -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


@endsection