@extends('dashboard.layout.app')
@section('title', 'الطلبات')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.orders.index') }}">الطلبات</a>
                                    </li>
                                    <li class="breadcrumb-item active">عرض
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- search header -->
                <section id="faq-search-filter">
                    <div class="card faq-search" style="background-image: url({{ url('admin/app-assets/images/banner/banner.png') }})">
                        <div class="card-body text-center">
                            <!-- main title -->
                            <!-- <h2 class="text-primary">Let's answer some questions</h2> -->

                            <!-- subtitle -->
                            <!-- <p class="card-text mb-2">or choose a category to quickly find the help you need</p> -->

                        </div>
                    </div>
                </section>
                <!-- /search header -->

                <!-- frequently asked questions tabs pills -->
                <section id="faq-tabs">
                    <!-- vertical tab pill -->
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <div class="faq-navigation d-flex justify-content-between flex-column mb-2 mb-md-0">
                                <!-- pill tabs navigation -->
                                <ul class="nav nav-pills nav-left flex-column" role="tablist">

                                    <!-- payment -->
                                    <li class="nav-item">
                                        <a class="nav-link active" id="payment" data-toggle="pill" href="#faq-payment" aria-expanded="true" role="tab">
                                            <i data-feather="credit-card" class="font-medium-3 mr-1"></i>
                                            <span class="font-weight-bold">بيانات المستخدم</span>
                                        </a>
                                    </li>

                                    <!-- delivery -->
                                    <li class="nav-item">
                                        <a class="nav-link" id="delivery" data-toggle="pill" href="#faq-delivery" aria-expanded="false" role="tab">
                                            <i data-feather="shopping-bag" class="font-medium-3 mr-1"></i>
                                            <span class="font-weight-bold">بيانات البنك</span>
                                        </a>
                                    </li>

                                </ul>

                                <!-- FAQ image -->
                                <img src="{{ url('admin') }}/app-assets/images/illustration/faq-illustrations.svg" class="img-fluid d-none d-md-block" alt="demand img" />
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <!-- pill tabs tab content -->
                            <div class="tab-content">
                                <!-- payment panel -->
                                <div role="tabpanel" class="tab-pane active" id="faq-payment" aria-labelledby="payment" aria-expanded="true">
                                    <!-- icon and header -->
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-tag bg-light-primary mr-1">
                                            <i data-feather="credit-card" class="font-medium-4"></i>
                                        </div>
                                        <div>
                                            <h4 class="mb-0">بيانات المستخدم</h4>
                                        </div>
                                    </div>

                                    <!-- frequent answer and question  collapse  -->
                                    <div class="collapse-margin collapse-icon mt-2" id="faq-payment-qna">
                                        <div class="card">
                                            <div class="card-header" id="paymentOne" data-toggle="collapse" role="button" data-target="#faq-payment-one" aria-expanded="false" aria-controls="faq-payment-one">
                                                <span class="lead collapse-title">اسم المستخدم</span>
                                            </div>

                                            <div id="faq-payment-one" class="collapse" aria-labelledby="paymentOne" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    {{ $order->name_user }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentTwo" data-toggle="collapse" role="button" data-target="#faq-payment-two" aria-expanded="true" aria-controls="faq-payment-two">
                                                <span class="lead collapse-title">مبلغ العمولة</span>
                                            </div>
                                            <div id="faq-payment-two" class="collapse show" aria-labelledby="paymentTwo" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    {{ $order->commission_amount }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentThree" data-toggle="collapse" role="button" data-target="#faq-payment-three" aria-expanded="false" aria-controls="faq-payment-three">
                                                <span class="lead collapse-title">رقم الاعلان</span>
                                            </div>
                                            <div id="faq-payment-three" class="collapse" aria-labelledby="paymentThree" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    {{ $order->numebr_product }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentFour" data-toggle="collapse" role="button" data-target="#faq-payment-four" aria-expanded="false" aria-controls="faq-payment-four">
                                                <span class="lead collapse-title">تاريخ التحويل</span>
                                            </div>
                                            <div id="faq-payment-four" class="collapse" aria-labelledby="paymentFour" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    {{ $order->transfer_date->isoFormat('ll') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentFive" data-toggle="collapse" role="button" data-target="#faq-payment-five" aria-expanded="false" aria-controls="faq-payment-five">
                                                <span class="lead collapse-title">اسم المحول</span>
                                            </div>
                                            <div id="faq-payment-five" class="collapse" aria-labelledby="paymentFive" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    {{ $order->transfer_name }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentSix" data-toggle="collapse" role="button" data-target="#faq-payment-six" aria-expanded="false" aria-controls="faq-payment-six">
                                                <span class="lead collapse-title">رقم الجوال</span>
                                            </div>
                                            <div id="faq-payment-six" class="collapse" aria-labelledby="paymentSix" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    {{ $order->phone }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentSeven" data-toggle="collapse" role="button" data-target="#faq-payment-seven" aria-expanded="false" aria-controls="faq-payment-seven">
                                                <span class="lead collapse-title">ملاحظات</span>
                                            </div>
                                            <div id="faq-payment-seven" class="collapse" aria-labelledby="paymentSeven" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    {{ $order->note }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-header" id="paymentSevenimage" data-toggle="collapse" role="button" data-target="#faq-payment-seven-image" aria-expanded="false" aria-controls="faq-payment-seven">
                                                <span class="lead collapse-title">صوره الايصال</span>
                                            </div>
                                            <div id="faq-payment-seven-image" class="collapse" aria-labelledby="paymentSevenimage" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    <img src="{{ $order->imageone() }}" width="797px" alt="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!-- delivery panel -->
                                <div class="tab-pane" id="faq-delivery" role="tabpanel" aria-labelledby="delivery" aria-expanded="false">
                                    <!-- icon and header -->
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-tag bg-light-primary mr-1">
                                            <i data-feather="shopping-bag" class="font-medium-4"></i>
                                        </div>
                                        <div>
                                            <h4 class="mb-0">بيانات البنك</h4>
                                        </div>
                                    </div>

                                    <!-- frequent answer and question  collapse  -->
                                    <div class="collapse-margin collapse-icon mt-2" id="faq-delivery-qna">
                                        <div class="card">
                                            <div class="card-header" id="deliveryOne" data-toggle="collapse" role="button" data-target="#faq-delivery-one" aria-expanded="false" aria-controls="faq-delivery-one">
                                                <span class="lead collapse-title">اسم البنك</span>
                                            </div>

                                            <div id="faq-delivery-one" class="collapse" aria-labelledby="deliveryOne" data-parent="#faq-delivery-qna">
                                                <div class="card-body">
                                                    {{ $order->bank->name }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="deliveryTwo" data-toggle="collapse" role="button" data-target="#faq-delivery-two" aria-expanded="false" aria-controls="faq-delivery-two">
                                                <span class="lead collapse-title">شعار البنك</span>
                                            </div>
                                            <div id="faq-delivery-two" class="collapse" aria-labelledby="deliveryTwo" data-parent="#faq-delivery-qna">
                                                <div class="card-body">
                                                    <img src="{{ url('storage/' . $order->bank->logo) }}" width="796px" alt="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
                <!-- / frequently asked questions tabs pills -->


            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')
    <script src="{{ url('admin') }}/app-assets/js/forms/cities/create.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

@endpush

@endsection