@extends('dashboard.layout.app')
@section('title', 'تعديل الخيارات')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.options.index') }}">الخيارات</a>
                                    </li>
                                    <li class="breadcrumb-item active">تعديل
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">الخيارات</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('dashboard.options.update' , $one_option->id) }}" method="POST" class="needs-validation" novalidate>
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group">
                                            <label for="select-country1">الخصائص</label>
                                            <select class="form-control attribute" id="select-country1" data-url="{{ route('dashboard.append.subcategory') }}" name="attribute_id" required>
                                                <option value="">اختر الخصائص</option>
                                                @foreach($attributes as $attribute)
                                                    <option value="{{ $attribute->id }}" {{ $attribute->id == $one_option->parent_id ? 'selected' : '' }}>{{ $attribute->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group" id="subcategory">
                                            <p class="text-hint">اذا كنت تريد اضافه هذا الخيار في اقسام فرعيه معينه برجاء اختيارها من هنا <i class="feather feather plus"></i></p> 
                                            <label for="select-country1">الاقسام الفرعيه</label>
                                            <select class="form-control" id="select-country1" name="subcategory_id[]" multiple>
                                                @foreach($subcategories->subcategory as $category)
                                                    <option value="{{ $category->id }}" {{ in_array($category->id , $one_option->optionsubcategory->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>                                            
                                        </div>
                                      
                                        <div class="form-group">
                                            <label class="form-label" for="basic-addon-name">اسم الخيار</label>
                                            <input type="text" id="basic-addon-name" name="name" value="{{ $one_option->name }}" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">تعديل <i data-feather="edit"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')
@push('js')
    <script src="{{ url('admin') }}/app-assets/js/forms/options/create.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

@endpush
@endpush

@endsection