    



        <p class="text-hint">اذا كنت تريد اضافه هذا الخيار في اقسام فرعيه معينه برجاء اختيارها من هنا <i class="feather feather plus"></i></p> 
        <label for="select-country1">الاقسام الفرعيه</label>
        <select class="form-control" id="select-country1" name="subcategory_id[]" multiple>
            @foreach($subcategories->subcategory as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
