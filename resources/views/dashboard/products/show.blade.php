@extends('dashboard.layout.app')
@section('title', 'المنتجات')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.products.index') }}">المنتجات</a>
                                    </li>
                                    <li class="breadcrumb-item active">عرض
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- app e-commerce details start -->
                <section class="app-ecommerce-details">
                    <div class="card">
                        <!-- Product Details starts -->
                        <div class="card-body">
                            <div class="row my-2">
                                <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img src="{{ $product->imageone() }}" class="img-fluid product-img" alt="product image" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-7">
                                    <h4>

                                        {{ $product->type->name }} |
                                        @if($product->category_id == 1)
                                        {{ $product->make_year }}
                                        @else

                                            {{ $product->space }} م

                                        @endif                                    

                                    </h4>
                                    <span class="card-text item-company">
                                        بواسطه  
                                        <a href="javascript:void(0)" class="company-name">
                                        {{ $product->user->first_name }} 
                                        {{ $product->user->second_name }} 
                                        {{ $product->user->last_name }} 
                                        </a>
                                    </span>
                                    <div class="ecommerce-details-price d-flex flex-wrap mt-1">
                                        <h4 class="item-price mr-1">$ {{ $product->price }}</h4>
                                    </div>
                                    <p class="card-text">
                                    {{ $product->details }}
                                    </p>
                                    <ul class="product-features list-unstyled">
                                    
                                        @if($product->category_id == 1)

                                        <li>سنه الصنع : {{ $product->make_year }}</li>
                                        <li>الممشي : {{ $product->the_walker }} كم/ساعة</li>
                                        <li>السعة : {{ $product->capacity }}</li>

                                        @else

                                        @if($product->number_of_shops)<li>عدد المحلات: {{ $product->number_of_shops }} </li>@endif
                                        @if($product->neighborhood)<li>الحي: {{ $product->neighborhood }} </li>@endif
                                        @if($product->street_view)<li>عرض الواجهه: {{ $product->street_view }} م</li>@endif

                                        @if($product->number_halls_for_two)<li>عدد الصالات للشقه رقم 2: {{ $product->number_halls_for_two }} </li>@endif
                                        @if($product->women_boards_for_two)<li>عدد مجالس النساء للشقة رقم 2: {{ $product->women_boards_for_two }} </li>@endif
                                        @if($product->men_boards_for_two)<li>عدد مجالس الرجال للشقة رقم 2: {{ $product->men_boards_for_two }} </li>@endif
                                        @if($product->number_of_bedrooms_for_two)<li>عدد عرف النوم للشقه رقم 2: {{ $product->number_of_bedrooms_for_two }} </li>@endif
                                        @if($product->number_halls_for_one)<li>عدد الصالات للشقه رقم 1: {{ $product->number_halls_for_one }} </li>@endif
                                        @if($product->women_boards_for_one)<li>عدد مجالس النساء للشقة 1: {{ $product->women_boards_for_one }} </li>@endif
                                        @if($product->men_boards_for_one)<li>عدد مجالس الرجال للشقة 1: {{ $product->men_boards_for_one }} </li>@endif
                                        @if($product->number_of_bedrooms_for_one)<li>عدد عرف النوم للشقه 1: {{ $product->number_of_bedrooms_for_one }} </li>@endif

                
                                        @if($product->shops)<li> محلات تجارية: {{ $product->shops }} </li>@endif
                                        @if($product->number_of_sections)<li>عدد الاقسام: {{ $product->number_of_sections }} </li>@endif
                                        @if($product->inside_height)<li>الارتفاع من الداخل: {{ $product->inside_height }} م</li>@endif
                                        @if($product->street_or_lane_width)<li>عرض شارع او ممر: {{ $product->street_or_lane_width }} م</li>@endif
                                        @if($product->fuel_tank_capacity)<li>سعة خزانات الموقود: {{ $product->fuel_tank_capacity }} لتر</li>@endif
                                        @if($product->number_pumps)<li>عدد المضخات: {{ $product->number_pumps }} </li>@endif
                                        @if($product->on_highway)<li>علي طريق سريع : {{ $product->on_highway }} </li>@endif
                                        @if($product->storehouse)<li>عدد المستودع: {{ $product->storehouse }} </li>@endif
                                        @if($product->number_bathrooms)<li>عدد دورات المياة: {{ $product->number_bathrooms }} </li>@endif
                                        @if($product->number_of_swimming_pools)<li>عدد المسابح: {{ $product->number_of_swimming_pools }} </li>@endif
                                        @if($product->number_kitchens)<li>عدد المطابخ: {{ $product->number_kitchens }} </li>@endif
                                        @if($product->men_boards)<li>عدد مجالس الرجال: {{ $product->men_boards }} </li>@endif
                                        @if($product->women_councils)<li>عدد مجالس النساء: {{ $product->women_councils }} </li>@endif
                                        @if($product->number_of_master_bedrooms)<li>عدد غرف النوم الماستر: {{ $product->number_of_master_bedrooms }} </li>@endif
                                        @if($product->number_of_roles)<li>عدد الادوار: {{ $product->number_of_roles }} </li>@endif
                                        @if($product->space)<li>المساحة: {{ $product->space }} م</li>@endif
                                        @if($product->building_erea)<li>مسطح البناء: {{ $product->building_erea }} م</li>@endif
                                        @if($product->number_halls)<li>عدد الصالات: {{ $product->number_halls }} </li>@endif
                                        @if($product->number_bedroom)<li>عدد غرف النوم: {{ $product->number_bedroom }} </li>@endif

                                        @endif

                                        @foreach($product->attributeandoption as $attributewithoption)
                                            <li>{{ $attributewithoption->attribute->name }} : {{ $attributewithoption->option->name }}</li>
                                        @endforeach
                                        <hr>
                                        
                                        @foreach($product->directione()->groupBy('dire_id')->get() as $dire)

                                        @php 


                                        $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $dire->dire_id] , ['product_id' , $product->id]])->get();

                                        @endphp                                        

                                        @if($dire->dire_id == 1)
                                                    <hr>
                                            <p>شمال</p>
                                            <hr>
                                        @elseif($dire->dire_id == 2)
                                                    <hr>
                                            <p>شرق</p>
                                            <hr>
                                        @elseif($dire->dire_id == 3)
                                                    <hr>
                                            <p>غرب</p>
                                            <hr>
                                        @elseif($dire->dire_id == 4)
                                                    <hr>
                                            <p>جنوب</p>
                                            <hr>
                                        @endif

                                        @foreach($new_directions as $directione)


                                            <li>{{ $directione->dire->name }} : {{ $directione->value }}</li>

                                        @endforeach

                                        @endforeach

                                        <!-- END: attribute and opttion -->                                    

                                    </ul>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- Product Details ends -->


                    </div>
                </section>
                <!-- app e-commerce details end -->


            </div>
        </div>
    </div>
    <!-- END: Content-->
@push('js')
    <script src="{{ url('admin') }}/app-assets/js/forms/cities/create.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

@endpush

@endsection