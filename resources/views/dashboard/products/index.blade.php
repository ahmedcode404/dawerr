@extends('dashboard.layout.app')
@section('title', 'المنتجات')
@section('content')


    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="#">Datatable</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">المنتجات
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="content-body">

                <!-- BEGIN: section -->
                <section id="dashboard-analytics">

                    <!-- List DataTable -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <table class="datatables-basic table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الاسم</th>
                                            <th>النوع</th>
                                            <th>الصورة</th>
                                            <th>البريد الالكتروني</th>
                                            <th> رقم الجوال </th>
                                            <th>الاختيارات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($products as $product)
                                        <tr>                                         
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->type->name }}</td>
                                            <td><img src="{{ $product->imageone() }}" width="70px" alt=""></td>
                                            <td>{{ $product->email }}</td>
                                            <td>{{ $product->phone }}</td>
                                            <td>
                                                <a href="{{route('dashboard.products.destroy', $product->id)}}" class="action-edit item-delete"><i class="fa fa-trash"></i></a>
                                                <a href="{{route('dashboard.products.show', $product->id)}}" class="action-edit"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/ List DataTable -->

                </section>
                <!-- END: section -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection