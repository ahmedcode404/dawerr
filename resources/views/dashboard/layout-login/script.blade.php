    <!-- BEGIN: Vendor JS-->
    <script src="{{ url('admin') }}/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ url('admin') }}/app-assets/js/core/app-menu.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ url('admin') }}/app-assets/js/scripts/pages/page-auth-login.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/scripts/pages/page-auth-forgot-password.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/scripts/pages/page-auth-reset-password.js"></script>

    <!-- END: Page JS-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> -->

{{-- Sweet Alerts Start --}}
@if(session()->has('success'))
    <script>
        Swal.fire({
            title: "{{ session()->get('success') }}",
            html: "{{ session()->get('html') }}",
            icon: 'success',
            showCancelButton: false,
            showConfirmButton: true,
        });
        '{{session()->forget("success")}}';
    </script>
@endif

@if(session()->has('error'))
    <script>
        Swal.fire({
            title: "{{ session()->get('error')}}",
            html: "{!! session()->get('html') !!}",
            icon: 'error',
            showCancelButton: false,
            showConfirmButton: true,
        });
        '{{session()->forget("error")}}';
    </script>
@endif    



    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>