<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="author" content="Daweer">
    <title>دور | @yield('title_login')</title>


    <!-- BEGIN: style -->
        @include('dashboard.layout-login.style')
    <!-- END: style -->

    
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
    


    
    <!-- BEGIN: content -->
        @yield('content')
    <!-- END: content -->
    

    
    <!-- BEGIN: style -->
        @include('dashboard.layout-login.script')
    <!-- END: style -->

</body>
<!-- END: Body-->

</html>