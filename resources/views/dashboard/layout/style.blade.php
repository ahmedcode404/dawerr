
    <!-- BEGIN: Vendor CSS-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">    
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/vendors/css/vendors-rtl.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/vendors/css/extensions/toastr.min.css">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ url('admin/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('admin/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
      <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/colors.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/components.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/themes/semi-dark-layout.css">
    <!-- <link rel="stylesheet" type="text/css" href="{{ url('admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}"> -->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/plugins/charts/chart-apex.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/plugins/extensions/ext-component-toastr.css">
    <!-- <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/pages/app-invoice-list.css"> -->
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/app-assets/css-rtl/custom-rtl.css">
    <link rel="stylesheet" type="text/css" href="{{ url('admin') }}/assets/css/style-rtl.css">
    <!-- END: Custom CSS-->


    @stack('css')