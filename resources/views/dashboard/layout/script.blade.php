    <!-- BEGIN: Vendor JS-->
    <script src="{{ url('admin') }}/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- <script src="{{ url('admin') }}/app-assets/vendors/js/charts/apexcharts.min.js"></script> -->
    <script src="{{ url('admin') }}/app-assets/vendors/js/extensions/toastr.min.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/extensions/moment.min.js"></script>
    
    <script src="{{url('admin/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('admin/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
    <script src="{{url('admin/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{url('admin/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js')}}"></script>
    <!-- <script src="{{url('admin/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script> -->
    {{-- <!-- <script src="{{url('admin/app-assets/js/scripts/tables/table-datatables-advanced.js')}}"></script> --> --}}

    <!-- BEGIN: Theme JS-->
    <script src="{{ url('admin') }}/app-assets/js/core/app-menu.js"></script>
    <script src="{{ url('admin') }}/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>

    <!-- BEGIN: Page JS-->
    <!-- <script src="{{ url('admin') }}/app-assets/js/scripts/pages/dashboard-analytics.js"></script> -->
    <script src="{{ url('admin') }}/app-assets/js/scripts/pages/app-invoice-list.js"></script>
    <!-- END: Page JS-->
    <script src="{{ url('admin') }}/app-assets/js/custom/custom-delete.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    @stack('js')
    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
        
        $('table').DataTable({
            "language": {
            //for arabic language
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
        }            
        });

    </script>
    

{{-- Sweet Alerts Start --}}
@if(session()->has('success'))
    <script>
        Swal.fire({
            title: "{{ session()->get('success') }}",
            html: "{{ session()->get('html') }}",
            icon: 'success',
            showCancelButton: false,
            showConfirmButton: true,
        });
        '{{session()->forget("success")}}';
    </script>
@endif

@if(session()->has('error'))
    <script>
        Swal.fire({
            title: "{{ session()->get('error')}}",
            html: "{!! session()->get('html') !!}",
            icon: 'error',
            showCancelButton: false,
            showConfirmButton: true,
        });
        '{{session()->forget("error")}}';
    </script>
@endif    