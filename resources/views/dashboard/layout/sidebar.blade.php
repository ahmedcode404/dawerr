<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('web.category') }}"><span class="brand-logo">
                    <img src="{{ url('admin') }}/app-assets/images/logo/logo2.png" height="30" alt="logo"/></span>
                        <h2 class="brand-text"> دور</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item {{ areActiveRoutes(['dashboard.dashboard']) }}"><a class="d-flex align-items-center" href="{{ route('dashboard.dashboard') }}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Email">الرئيسية</span></a>
                </li>

                <!-- BEGIN: sliders -->

                <li class=" nav-item {{ areActiveRoutes(['dashboard.sliders.create' , 'dashboard.sliders.index' , 'dashboard.sliders.edit']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">السلايدر</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.sliders.index') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">السلايدر</span></a>
                        </li>   
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.sliders.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه سلايدر</span></a>
                        </li>
                    </ul>
                </li>
                <!-- END: sliders -->                

                <!-- BEGIN: cities -->
                <li class=" nav-item {{ areActiveRoutes(['dashboard.cities.index' , 'dashboard.cities.edit' , 'dashboard.cities.create']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">المدن</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.cities.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">المدن</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.cities.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه مدينة</span></a>
                        </li>
                    </ul>
                </li>
                <!-- END: cities -->   

                <!-- BEGIN: orders -->
                <li class=" nav-item {{ areActiveRoutes(['dashboard.orders.index' , 'dashboard.orders.edit' , 'dashboard.orders.create']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">الطلبات</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.orders.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">الطلبات</span></a>
                        </li>
                    </ul>
                </li>
                <!-- END: orders -->                

                <!-- BEGIN: categories -->
                <li class=" nav-item {{ areActiveRoutes(['dashboard.categories.index' , 'dashboard.categories.edit' , 'dashboard.categories.create']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">انواع السيارات</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.categories.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">انواع السيارات</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.categories.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه نوع جديد</span></a>
                        </li>
                    </ul>
                </li>
                <!-- END: categories -->


                <!-- BEGIN: attributes -->

                    <li><a class="d-flex align-items-center" href="{{ route('dashboard.products.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">المنتجات</span></a>
                    </li>

                <!-- END: attributes -->


            <!-- BEGIN: attributes -->
            {{--<li class=" nav-item {{ areActiveRoutes(['dashboard.attributes.index' , 'dashboard.attributes.edit' , 'dashboard.attributes.create']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">الخصائص</span></a>
                <ul class="menu-content">
                    <li><a class="d-flex align-items-center" href="{{ route('dashboard.attributes.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">الخصائص</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href="{{ route('dashboard.attributes.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه خصائص</span></a>
                    </li>
                </ul>
            </li>
            <!-- END: attributes -->


            <!-- BEGIN: options -->
            <li class=" nav-item {{ areActiveRoutes(['dashboard.options.index' , 'dashboard.options.edit' , 'dashboard.options.create']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">الخيارات</span></a>
                <ul class="menu-content">
                    <li><a class="d-flex align-items-center" href="{{ route('dashboard.options.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">الخيارات</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href="{{ route('dashboard.options.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه خيار</span></a>
                    </li>
                </ul>
            </li>--}}
                <!-- END: options -->  
                
                <!-- BEGIN: price fillter -->
                <li class=" nav-item {{ areActiveRoutes(['dashboard.price-fillters.index' , 'dashboard.price-fillters.edit' , 'dashboard.price-fillters.create']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">بيانات البحث بالسعر</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.price-fillters.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">بيانات البحث بالسعر</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.price-fillters.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه </span></a>
                        </li>
                    </ul>
                </li>                
                <!-- END: price fillter -->   
                
                <!-- BEGIN: bank -->
                {{--<li class=" nav-item {{ areActiveRoutes(['dashboard.banks.index' , 'dashboard.banks.edit' , 'dashboard.banks.create']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">البنوك</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.banks.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">البنوك</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.banks.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه بنك</span></a>
                        </li>
                    </ul>
                </li>--}}               
                <!-- END: bank -->   
                
                <!-- BEGIN: bank account -->
                <li class=" nav-item {{ areActiveRoutes(['dashboard.bank-accounts.index' , 'dashboard.bank-accounts.edit' , 'dashboard.bank-accounts.create']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">الحسابات البنكيه</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.bank-accounts.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">الحسابات البنكيه</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.bank-accounts.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه حساب</span></a>
                        </li>
                    </ul>
                </li>                
                <!-- END: bank account -->
                
                <!-- BEGIN: agreements -->

                <li class=" nav-item {{ areActiveRoutes(['dashboard.agreements.create' , 'dashboard.agreements.index' , 'dashboard.agreements.edit']) }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">الاتفاقيات</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.agreements.index') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">الاتفاقيات</span></a>
                        </li>   
                        <li><a class="d-flex align-items-center" href="{{ route('dashboard.agreements.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">اضافه بند</span></a>
                        </li>
                    </ul>
                </li>
                <!-- END: agreements --> 
                
                <!-- BEGIN: contacct us -->
                <li class=" nav-item {{ areActiveRoutes(['dashboard.contacts.index'])}}"><a class="d-flex align-items-center" href="{{ route('dashboard.contacts.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">رسائل التواصل</span></a></li>                
                <!-- END: contacct us -->

                <!-- BEGIN: static page -->
                <li class=" nav-item {{ areActiveRoutes(['dashboard.static-pages.index'])}}"><a class="d-flex align-items-center" href="{{ route('dashboard.static-pages.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">الصفحات الثابته</span></a></li>
                <!-- END: static page -->   
                
                <!-- BEGIN: settings -->
                <li class=" nav-item {{ areActiveRoutes(['dashboard.settings.create'])}}"><a class="d-flex align-items-center" href="{{ route('dashboard.settings.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview"> الاعدادات</span></a></li>
                <!-- END: settings -->                   

            </ul>
        </div>
    </div>