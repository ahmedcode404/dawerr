<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="author" content="Daweer">

    <title> دور | @yield('title')</title>


    <!-- BEGIN: stype -->
        @include('dashboard.layout.style')
    <!-- END: stype -->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="">

    <!-- BEGIN: Header-->
        @include('dashboard.layout.header')
    <!-- END: Header-->

    <!-- BEGIN: Main Menu-->
        @include('dashboard.layout.sidebar')
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
        @yield('content')
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->


    <!-- BEGIN: script-->
        @include('dashboard.layout.script')
    <!-- END: script-->


</body>
<!-- END: Body-->

</html>