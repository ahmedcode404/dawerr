@extends('dashboard.layout.app')
@section('title', 'رسائل التواصل')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.dashboard') }}">الرئيسية</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="#">Datatable</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">رسائل التواصل
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
            <div class="content-body">

                <!-- BEGIN: section -->
                <section id="dashboard-analytics">

                    <!-- List DataTable -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <table class="datatables-basic table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> الاسم الاول </th>
                                            <th> الاسم الاخير </th>
                                            <th>  البريد الالكتروني </th>
                                            <th>  الرساله </th>
                                            <th>الاختيارات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($contacts as $contact)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $contact->first_name }}</td>
                                            <td>{{ $contact->last_name }}</td>
                                            <td>{{ $contact->email }}</td>
                                            <td>{{ $contact->message }}</td>
                                            <td>

                                                <a href="{{route('dashboard.contacts.destroy', $contact->id)}}" class="action-delete item-delete"><i class="fa fa-trash"></i></a>
                                                <a href="" class="action-edit show-modal" data-id="{{$contact->id}}" data-email="{{ $contact->email }}" data-toggle="modal" data-target="#primary"><i class="fa fa-reply"></i></a>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/ List DataTable -->

                </section>
                <!-- END: section -->

            </div>
        </div>
    </div>
    <!-- END: Content-->  


    <div class="modal fade text-left modal-primary" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">

         <div class="loding_index">
             <div class="spinner-grow text-warning mr-1" role="status">
                 <span class="sr-only">Loading...</span>
             </div>            
        </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel160">الرد</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" class="needs-validation">
                        @csrf

                        <input type="hidden" name="item" id="item" value="">
                        <input type="hidden" name="email" id="email" value="">
                        <div class="form-group">
                            <label class="form-label" for="basic-addon-name">قم بكتابة الرد هنا </label>
                            <textarea name="message" id="message" cols="30" rows="10" class="form-control" ></textarea>
                        </div>
                        <div id="append-valid">
                            
                        </div>
                        

                        <div class="modal-footer">
                            <button type="button" id="reply-message" data-url="{{route('dashboard.contact.reply')}}" class="btn btn-primary">ارسال <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>      

@push('js')

    <script src="{{ url('admin') }}/app-assets/js/forms/contacts/reply.js"></script>
    <script src="{{ url('admin') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>


@endpush
@endsection