<div class="profile-main-grid col-lg-9 col-md-8">
    <!--start profile-main-grid-->
    <div class="main-profile-div sm-raduis gray-bg">
        <h2 class="circle-item second_color">البيانات الشخصية :  </h2>

        <div class="row">

            <!--start div-->
            <div class="col-12 profile-data">
                <form class="row profile-form orange-form" action="{{ route('web.update.profile' , auth()->user()->id) }}" method="post">
                    @csrf
                    @method('PUT')

                    <input type="hidden" name="id" value="{{ auth()->user()->id }}">

                    @if(auth()->user()->type_id == 1)

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">الاسم الأول</label>
                                <input type="text" class="form-control" name="first_name" value="{{ auth()->user()->first_name }}" >
                                @if($errors->has('first_name'))
                                    <div class="error">{{ $errors->first('first_name') }}</div>
                                @endif                              
                            </div>
                        </div>

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">الاسم الثاني</label>
                                <input type="text" class="form-control" name="second_name" value="{{ auth()->user()->second_name }}" >
                                @if($errors->has('second_name'))
                                    <div class="error">{{ $errors->first('second_name') }}</div>
                                @endif                               
                            </div>
                        </div>

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">اسم العائلة</label>
                                <input type="text" class="form-control" name="last_name" value="{{ auth()->user()->last_name }}" >
                                @if($errors->has('last_name'))
                                    <div class="error">{{ $errors->first('last_name') }}</div>
                                @endif                                
                            </div>
                        </div>

                    @else

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">اسم المؤسسة</label>
                                <input type="text" name="company_name" class="form-control"
                                    value="{{ auth()->user()->first_name }}" >
                                    @if($errors->has('company_name'))
                                        <div class="error">{{ $errors->first('company_name') }}</div>
                                    @endif                               
                            </div>
                        </div>

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">السجل التجاري</label>
                                <input type="number" min="1" class="form-control" name="commercial_register" value="{{ auth()->user()->commercial_register }}" >
                                @if($errors->has('commercial_register'))
                                    <div class="error">{{ $errors->first('commercial_register') }}</div>
                                @endif                               
                            </div>
                        </div>

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">الرقم الضريبي</label>
                                <input type="number" min="1" class="form-control" name="tax_number" value="{{ auth()->user()->tax_number }}" >
                                @if($errors->has('tax_number'))
                                    <div class="error">{{ $errors->first('tax_number') }}</div>
                                @endif                               
                            </div>
                        </div> 

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">اسم البنك</label>
                                <input type="text"  class="form-control" name="name_bank" value="{{ auth()->user()->name_bank }}" >
                                @if($errors->has('name_bank'))
                                    <div class="error">{{ $errors->first('name_bank') }}</div>
                                @endif                             
                            </div>
                        </div>                                    

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color"> اسم الحساب </label>
                                <input type="text" class="form-control" name="name_account_bank" value="{{ auth()->user()->name_account_bank }}" >
                                @if($errors->has('name_account_bank'))
                                    <div class="error">{{ $errors->first('name_account_bank') }}</div>
                                @endif                              
                            </div>
                        </div>                                    

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color"> رقم الحساب </label>
                                <input type="text" min="1" class="form-control" name="number_account_bank" value="{{ auth()->user()->number_account_bank }}" >
                                @if($errors->has('number_account_bank'))
                                    <div class="error">{{ $errors->first('number_account_bank') }}</div>
                                @endif                                 
                            </div>
                        </div>     

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color"> رقم الايبان </label>
                                <input type="number" min="1" class="form-control" name="number_eban_bank" value="{{ auth()->user()->number_eban_bank }}" >
                                @if($errors->has('number_eban_bank'))
                                    <div class="error">{{ $errors->first('number_eban_bank') }}</div>
                                @endif                              
                            </div>
                        </div>                                    

                    @endif

                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color"> </label>
                            <select id="" class="form-control select-input" name="city_id">
                                <option selected disabled>اختر المدينة</option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" {{ auth()->user()->city_id == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                                @endforeach                                
                            </select>
                                @if($errors->has('city_id'))
                                    <div class="error">{{ $errors->first('city_id') }}</div>
                                @endif                         
                        </div>
                    </div>

                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color">رقم الجوال</label>
                            <input type="tel" class="form-control" name="phone" value="{{ auth()->user()->phone }}"
                                minlength="11" maxlength="14" >
                                @if($errors->has('phone'))
                                    <div class="error">{{ $errors->first('phone') }}</div>
                                @endif                         
                        </div>
                    </div>


                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color">البريد الإلكتروني</label>
                            <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}"
                                >
                                @if($errors->has('email'))
                                    <div class="error">{{ $errors->first('email') }}</div>
                                @endif                          
                        </div>
                    </div>

                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color">كلمة المرور</label>
                            <input type="password" name="password" class="form-control" id="password" >
                                @if($errors->has('password'))
                                    <div class="error">{{ $errors->first('password') }}</div>
                                @endif                         
                        </div>
                    </div>


                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color">تأكيد كلمة المرور</label>
                            <input type="password" name="confirm_password" class="form-control" >
                                @if($errors->has('confirm_password'))
                                    <div class="error">{{ $errors->first('confirm_password') }}</div>
                                @endif                          
                        </div>
                    </div>

                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="border-btn"><span>حفظ </span></button>
                        </div>
                    </div>


                </form>
            </div>
            <!--end div-->
        </div>
    </div>
</div>