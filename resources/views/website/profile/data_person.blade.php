<div class="profile-main-grid col-lg-9 col-md-8">
    <!--start profile-main-grid-->
    <div class="main-profile-div sm-raduis gray-bg">
        <h2 class="circle-item second_color">البيانات الشخصية : 

            <a href="{{ route('web.edit.profile') }}" class="simple-btn open-edit-form"><i class="fa fa-edit"></i></a>
            
        </h2>


        <div class="row">

            <!--start div-->
            <div class="col-12 profile-data">
                <form class="row profile-form orange-form" action="{{ route('web.update.profile' , auth()->user()->id) }}" method="post">
                    @csrf
                    @method('PUT')

                    @if(auth()->user()->type_id == 1)

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">الاسم الأول</label>
                                <input type="text" class="form-control" name="first_name" value="{{ auth()->user()->first_name }}" disabled>
                            </div>
                        </div>

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">الاسم الثاني</label>
                                <input type="text" class="form-control" name="second_name" value="{{ auth()->user()->second_name }}" disabled>
                            </div>
                        </div>

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">اسم العائلة</label>
                                <input type="text" class="form-control" name="last_name" value="{{ auth()->user()->last_name }}" disabled>
                            </div>
                        </div>

                    @else

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">اسم المؤسسة</label>
                                <input type="text" name="compnay_name" class="form-control"
                                    value="{{ auth()->user()->first_name }}" disabled>
                            </div>
                        </div>

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">السجل التجاري</label>
                                <input type="number" class="form-control" name="commercial_register" value="{{ auth()->user()->commercial_register }}" disabled>
                            </div>
                        </div>

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">الرقم الضريبي</label>
                                <input type="number" class="form-control" name="tax_number" value="{{ auth()->user()->tax_number }}" disabled>
                            </div>
                        </div> 

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color">اسم البنك</label>
                                <input type="text" class="form-control" name="name_bank" value="{{ auth()->user()->name_bank }}" disabled>
                            </div>
                        </div>                                    

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color"> اسم الحساب </label>
                                <input type="text" class="form-control" name="name_account_bank" value="{{ auth()->user()->name_account_bank }}" disabled>
                            </div>
                        </div>                                    

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color"> رقم الحساب </label>
                                <input type="text" class="form-control" name="number_account_bank" value="{{ auth()->user()->number_account_bank }}" disabled>
                            </div>
                        </div>     

                        <div class="col-12" data-aos="fade-in">
                            <div class="form-group">
                                <label class="moving-label second_color"> رقم الايبان </label>
                                <input type="text" class="form-control" name="number_eban_bank" value="{{ auth()->user()->number_eban_bank }}" disabled>
                            </div>
                        </div>                                    

                    @endif

                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color"> المدينة</label>
                            <input type="tel" class="form-control" name="phone" value="{{ auth()->user()->city->name }}"
                                minlength="11" maxlength="14" disabled>
                        </div>
                    </div>

                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color">رقم الجوال</label>
                            <input type="tel" class="form-control" name="phone" value="{{ auth()->user()->phone }}"
                                minlength="11" maxlength="14" disabled>
                        </div>
                    </div>

                    
                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color">البريد الإلكتروني</label>
                            <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}"
                                disabled>
                        </div>
                    </div>

                    {{--<div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color">كلمة المرور</label>
                            <input type="password" class="form-control" id="password"
                                value="" disabled>
                        </div>
                    </div>


                    <div class="col-12" data-aos="fade-in">
                        <div class="form-group">
                            <label class="moving-label second_color">تأكيد كلمة المرور</label>
                            <input type="password" class="form-control" name="password_confirm"
                                value="" disabled>
                        </div>
                    </div>--}}

                    <div class="col-lg-12 text-center hide-element">
                        <div class="form-group">
                            <button type="submit" class="border-btn"><span>حفظ </span></button>
                        </div>
                    </div>


                </form>
            </div>
            <!--end div-->
        </div>
    </div>
</div>