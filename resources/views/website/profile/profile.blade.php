@extends('website.layouts.app')

@if(\Request::route()->getName() == 'web.profile')

    @section('title' , 'حسابي')

@elseif(\Request::route()->getName() == 'web.edit.profile')

    @section('title' , 'تعديل البيانات الشخصية')

@elseif(\Request::route()->getName() == 'web.profile.favorite')

    @section('title' , 'المفضلة')

@elseif(\Request::route()->getName() == 'web.profile.product')

    @section('title' , 'أعلاناتي')

@endif

@push('css')



@endpush
@section('content')
<!-- start pages-header
 ================ -->
<section class="pages-header text-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="page-header-content" data-aos="zoom-out">
                    <h2 class="white-text">حسابي</h2>
                    <div class="breadcrumbes second_color">
                        <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                        
                            @if(\Request::route()->getName() == 'web.profile')

                                البيانات الشخصية

                            @elseif(\Request::route()->getName() == 'web.edit.profile')

                                تعديل البيانات الشخصية

                            @elseif(\Request::route()->getName() == 'web.profile.favorite')

                                المفضلة

                            @elseif(\Request::route()->getName() == 'web.profile.product')

                               أعلاناتي 

                            @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end pages-header-->


<!-- start profile-pg
 ================ -->
<section class="profile-pg margin-div" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3 class="shadow-title first_color">حسابي </h3>
            </div>
        </div>


        <div class="profile-content">
            <div class="row">
                <!--start profile-list-grid-->
                <div class="profile-list-grid col-lg-3 col-md-4">
                    <!--start div-->
                    <div class="profile-img text-center first_color sm-raduis" data-aos="fade-in">
                        <form action="">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type="file" id="imageUpload" data-url="{{ route('web.upload.image') }}" class="hidden-input"
                                        accept=".png, .jpg, .jpeg">
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview"
                                        style="background-image: url({{ url('storage/' . auth()->user()->image) }});">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <h3 class="second_color">{{ auth()->user()->first_name }} {{ auth()->user()->second_name }} {{ auth()->user()->last_name }}</h3>

                        {{ auth()->user()->email }}

                    </div>
                    <!--end div-->

                    <!--start div-->
                    <div class="profile-list-grid" data-aos="fade-in">
                        <div class="profile-list has_seudo gray-bg">
                            <ul class="list-unstyled">
                                <li class="{{ areActiveRoutes(['web.profile']) }}">
                                    <a href="{{ route('web.profile') }}">
                                        <i class="far fa-user-circle"></i>
                                        البيانات الشخصية
                                    </a>
                                </li>

                                <li class="{{ areActiveRoutes(['web.profile.favorite']) }}">
                                    <a href="{{ route('web.profile.favorite') }}">
                                        <i class="far fa-heart"></i>
                                        المفضلة
                                    </a>
                                </li>

                                <li class="{{ areActiveRoutes(['web.profile.product']) }}">
                                    <a href="{{ route('web.profile.product') }}">
                                        <i class="fa fa-bullhorn"></i>
                                        إعلاناتي
                                    </a>
                                </li>

                                {{--<li>
                                    <a href="{{ route('web.profile') }}">
                                        <i class="fa fa-cog"></i>
                                        الإعدادات
                                    </a>
                                </li>--}}

                                <li>
                                    <a href="" id="logout-user" data-redirectlogout="{{ route('web.logout') }}">
                                        <i class="fa fa-power-off"></i>
                                        تسجيل خروج
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end div-->
                </div>
                <!--end profile-list-grid-->

                <!-- data person -->
                @if(\Request::route()->getName() == 'web.profile')
                
                    @include('website.profile.data_person')
                
                @endif
                <!-- data person -->
                
                <!-- edit data person -->
                
                @if(\Request::route()->getName() == 'web.edit.profile')
                
                    @include('website.profile.edit')
                
                @endif

                <!-- edit data person -->
                
                <!-- favorite -->
                
                @if(\Request::route()->getName() == 'web.profile.favorite')
                
                    @include('website.profile.favorite')
                
                @endif

                <!-- favorite -->

                <!-- myproduct -->
                
                @if(\Request::route()->getName() == 'web.profile.product')
                
                    @include('website.profile.myproduct')
                
                @endif

                <!-- myproduct -->

            </div>
        </div>

    </div>
</section>
<!--end profile-pg-->

@push('js')

    <script type="text/javascript" src="{{ url('web') }}/back-end/logout.js"></script>
                                        
    @if(session('categoryId') == 1)

        <script type="text/javascript" src="{{ url('web') }}/js/profile.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/back-end/delete-favorite.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/back-end/done-sale.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/back-end/delete-product.js"></script>
        
        @elseif(session('categoryId') == 2)
        
        <script type="text/javascript" src="{{ url('web') }}/back-end/delete-product.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/back-end/done-sale.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/profile.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/back-end/delete-favorite.js"></script>
  
    @endif

    @if(session('edit-user') == 'true')


    <script>


        var db = firebase.firestore();

        // Add a new document in collection "users"
        db.collection("users").doc('userId_' + "{{ auth()->user()->id }}").update({
            'name': "{{ auth()->user()->first_name }}" +' '+ "{{ auth()->user()->second_name }}" +' '+ "{{ auth()->user()->last_name }}",
            'email': "{{ auth()->user()->email }}",
            'id': "userId_" + "{{ auth()->user()->id }}",
            'createdAt': firebase.firestore.FieldValue.serverTimestamp(),
            'chattingWith': null,
            'typingTo': null,
            'unReadingCount': 0,
            'status': 'online',
            'slug': "{{ auth()->user()->slug }}",
            'lastSeen': firebase.firestore.FieldValue.serverTimestamp(),
        })
        .then(() => {

            console.log("Document successfully written!");

        })
        .catch((error) => {

            console.error("Error writing document: ", error);
            
        });


    </script>
                        
    @endif

    @php

    session()->forget('edit-user')    

    @endphp    

@endpush

@endsection