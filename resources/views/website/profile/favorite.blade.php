    
    <div class="profile-main-grid col-lg-9 col-md-8">
        <!--start profile-main-grid-->
        <div class="main-profile-div sm-raduis gray-bg">

            <div class="row" id="append-empty">
                
                @forelse(auth()->user()->favorites as $favorite)
                    @if($favorite->product->category_id == 1)
                        <h2 class="circle-item second_color"> السيارات المفضلة :</h2>
                    @else
                        <h2 class="circle-item second_color"> العقارات المفضلة :</h2>
                    @endif
                    <!--start div-->
                    <div class="col-12 profile-data">
                        <!--start product-div-->
                        <div class="product-div has_seudo remove-product" data-aos="fade-in">
                            <div class="row">
                                <div class="col-lg-10  right-product-grid">
                                    <div class="right-product">
                                        <div class="product-img sm-raduis">
                                            <img src="{{ $favorite->product->imageone() }}" alt="product">
                                            <!--note: if sell dont`t add class if rent yellow-badge else add class orange-badge-->
                                            <span class="badges yellow-badge">{{ $favorite->product->status }} </span>

                                        </div>

                                        <div class="product-details">
                                            <h3 class="product-title first_color">
                                                {{ $favorite->product->type->name }} |
                                                @if($favorite->product->category_id == 1)
                                                {{ $favorite->product->make_year }}
                                                @else

                                                    {{ $favorite->product->space }} م

                                                @endif
                                            </h3>
                                            <span
                                                class="product-price second_color text-center-dir">{{ $favorite->product->price }}
                                                رس</span>
                                            <div class="product-description">
                                                <ul>

                                                    @if($favorite->product->category_id == 1)

                                                        <li>سنه الصنع : {{ $favorite->product->make_year }}</li>
                                                        <li>الممشي : {{ $favorite->product->the_walker }} كم/ساعة</li>
                                                        <li>السعة : {{ $favorite->product->capacity }}</li>

                                                    @else

                                                        @if($favorite->product->number_of_shops)<li>عدد المحلات: {{ $favorite->product->number_of_shops }} </li>@endif
                                                        @if($favorite->product->neighborhood)<li>الحي: {{ $favorite->product->neighborhood }} </li>@endif
                                                        @if($favorite->product->street_view)<li>عرض الواجهه: {{ $favorite->product->street_view }} م</li>@endif
                                                        @if($favorite->product->border)<li>الحدود: {{ $favorite->product->border }} </li>@endif

                                                        @if(
                                                            $favorite->product->subcategory_id == 18 ||
                                                            $favorite->product->subcategory_id == 19 ||
                                                            $favorite->product->subcategory_id == 20 ||
                                                            $favorite->product->subcategory_id == 21 ||
                                                            $favorite->product->subcategory_id == 24 ||
                                                            $favorite->product->subcategory_id == 25 ||
                                                            $favorite->product->subcategory_id == 26 ||
                                                            $favorite->product->subcategory_id == 28 ||
                                                            $favorite->product->subcategory_id == 29 ||
                                                            $favorite->product->subcategory_id == 30 ||
                                                            $favorite->product->subcategory_id == 31 ||
                                                            $favorite->product->subcategory_id == 32)

                                                        @else

                                                            @if($favorite->product->number_halls_for_two)<li>عدد الصالات للشقه رقم 2: {{ $favorite->product->number_halls_for_two }} </li>@endif
                                                            @if($favorite->product->women_boards_for_two)<li>عدد مجالس النساء للشقة رقم 2: {{ $favorite->product->women_boards_for_two }} </li>@endif
                                                            @if($favorite->product->men_boards_for_two)<li>عدد مجالس الرجال للشقة رقم 2: {{ $favorite->product->men_boards_for_two }} </li>@endif
                                                            @if($favorite->product->number_of_bedrooms_for_two)<li>عدد عرف النوم للشقه رقم 2: {{ $favorite->product->number_of_bedrooms_for_two }} </li>@endif
                                                            @if($favorite->product->number_halls_for_one)<li>عدد الصالات للشقه رقم 1: {{ $favorite->product->number_halls_for_one }} </li>@endif
                                                            @if($favorite->product->women_boards_for_one)<li>عدد مجالس النساء للشقة 1: {{ $favorite->product->women_boards_for_one }} </li>@endif
                                                            @if($favorite->product->men_boards_for_one)<li>عدد مجالس الرجال للشقة 1: {{ $favorite->product->men_boards_for_one }} </li>@endif
                                                            @if($favorite->product->number_of_bedrooms_for_one)<li>عدد عرف النوم للشقه 1: {{ $favorite->product->number_of_bedrooms_for_one }} </li>@endif

                                                        @endif

                                                        @if($favorite->product->shops)<li> محلات تجارية: {{ $favorite->product->shops }} </li>@endif
                                                        @if($favorite->product->number_of_sections)<li>عدد الاقسام: {{ $favorite->product->number_of_sections }} </li>@endif
                                                        @if($favorite->product->inside_height)<li>الارتفاع من الداخل: {{ $favorite->product->inside_height }} م</li>@endif
                                                        @if($favorite->product->street_or_lane_width)<li>عرض شارع او ممر: {{ $favorite->product->street_or_lane_width }} م</li>@endif
                                                        @if($favorite->product->fuel_tank_capacity)<li>سعة خزانات الموقود: {{ $favorite->product->fuel_tank_capacity }} لتر</li>@endif
                                                        @if($favorite->product->number_pumps)<li>عدد المضخات: {{ $favorite->product->number_pumps }} </li>@endif
                                                        @if($favorite->product->on_highway)<li>علي طريق سريع : {{ $favorite->product->on_highway }} </li>@endif
                                                        @if($favorite->product->storehouse)<li>عدد المستودع: {{ $favorite->product->storehouse }} </li>@endif
                                                        @if($favorite->product->number_bathrooms)<li>عدد دورات المياة: {{ $favorite->product->number_bathrooms }} </li>@endif
                                                        @if($favorite->product->number_of_swimming_pools)<li>عدد المسابح: {{ $favorite->product->number_of_swimming_pools }} </li>@endif
                                                        @if($favorite->product->number_kitchens)<li>عدد المطابخ: {{ $favorite->product->number_kitchens }} </li>@endif
                                                        @if($favorite->product->men_boards)<li>عدد مجالس الرجال: {{ $favorite->product->men_boards }} </li>@endif
                                                        @if($favorite->product->women_councils)<li>عدد مجالس النساء: {{ $favorite->product->women_councils }} </li>@endif
                                                        @if($favorite->product->number_of_master_bedrooms)<li>عدد غرف النوم الماستر: {{ $favorite->product->number_of_master_bedrooms }} </li>@endif
                                                        @if($favorite->product->number_of_roles)<li>عدد الادوار: {{ $favorite->product->number_of_roles }} </li>@endif
                                                        @if($favorite->product->space)<li>المساحة: {{ $favorite->product->space }} م</li>@endif
                                                        @if($favorite->product->building_erea)<li>مسطح البناء: {{ $favorite->product->building_erea }} م</li>@endif
                                                        @if($favorite->product->number_halls)<li>عدد الصالات: {{ $favorite->product->number_halls }} </li>@endif
                                                        @if($favorite->product->number_bedroom)<li>عدد غرف النوم: {{ $favorite->product->number_bedroom }} </li>@endif

                                                    @endif

                                                <!-- BEGIN: attribute and opttion -->
                                                    @if($favorite->product->subcategory_id == 16 ||
                                                        $favorite->product->subcategory_id == 17 ||
                                                        $favorite->product->subcategory_id == 18 ||
                                                        $favorite->product->subcategory_id == 19 ||
                                                        $favorite->product->subcategory_id == 20 ||
                                                        $favorite->product->subcategory_id == 21 ||
                                                        $favorite->product->subcategory_id == 22 ||
                                                        $favorite->product->subcategory_id == 23 ||
                                                        $favorite->product->subcategory_id == 24 ||
                                                        $favorite->product->subcategory_id == 25 ||
                                                        $favorite->product->subcategory_id == 26 ||
                                                        $favorite->product->subcategory_id == 27 ||
                                                        $favorite->product->subcategory_id == 28 ||
                                                        $favorite->product->subcategory_id == 29 ||
                                                        $favorite->product->subcategory_id == 30 ||
                                                        $favorite->product->subcategory_id == 31 ||
                                                        $favorite->product->subcategory_id == 32 ||
                                                        $favorite->product->subcategory_id == 33)
                                                    @else
                                                        @foreach($favorite->product->attributeandoption()->limit(7)->get() as $attributewithoption)
                                                            <li>{{ $attributewithoption->attribute->name }} : {{ $attributewithoption->option->name }}</li>
                                                    @endforeach

                                                @endif
                                                <!-- END: attribute and opttion -->

                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-2 left-product-grid">
                                    <div class="left-product text-left-dir">
                                        <div class="profile-edit-icons">

                                            <a href="{{ route('web.product' , $favorite->product->slug) }}" class="profile-edit-icon"
                                                title="عرض"><i class="fa fa-eye"></i></a>
                                            <button type="button"
                                                class="profile-edit-icon remove-button" data-id="{{ $favorite->product->id }}" data-action="{{ route('web.add.to.favorite') }}" title="حذف"><i
                                                    class="far fa-trash-alt"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!--end product-div-->
                    </div>
                    <!--end div-->

                @empty  

                    @include('website.layouts.empty')

                @endforelse

            </div>
        </div>
    </div>