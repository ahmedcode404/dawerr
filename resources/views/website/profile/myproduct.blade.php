
    <div class="profile-main-grid col-lg-9 col-md-8">
        <!--start profile-main-grid-->
        <div class="main-profile-div sm-raduis gray-bg">


            <div class="row appent-notfound">


                <!--start div-->

                    @forelse($products as $product)

                        @if($product->category_id == 1)
                        <h2 class="circle-item second_color">إعلانات السيارات :</h2>
                        @else
                        <h2 class="circle-item second_color">إعلانات العقارات :</h2>
                        @endif
                        <div class="col-12 profile-data product-delete">
                            <!--start product-div-->
                            <div class="product-div has_seudo remove-product" data-aos="fade-in">
                                <div class="row">
                                    <div class="col-lg-10  right-product-grid">
                                        <div class="right-product">
                                            <div class="product-img sm-raduis">
                                                <img src="{{ $product->imageone() }}" alt="product">
                                                <!--note: if sell dont`t add class if rent yellow-badge else add class orange-badge-->
                                                <div class="appent-done-sale-{{$product->id}}">
                                                @if($product->status != 'تم البيع')

                                                    <span class="badges yellow-badge hide-this">{{ $product->status }} <i class="fa fa-ellipsis-v"></i></span>

                                                    <div class="product-change-status hide-this">
                                                        <div class="custom-checkbox circle-checkbox">
                                                            <input type="checkbox" class="hidden-input done-sale" data-id="{{ $product->id }}" data-action="{{ route('web.done.sale') }}"
                                                                id="main_check_{{$product->id}}">
                                                            <label for="main_check_{{$product->id}}">تم البيع</label>
                                                        </div>
                                                    </div>                                                    
                                                
                                                @else
                                                    
                                                    <span class="badges red-badge">{{ $product->status }}</span>
                                                
                                                @endif
                                                </div>
                                            </div>

                                            <div class="product-details">
                                                <h3 class="product-title first_color">

                                                    {{ $product->type->name }} |
                                                    @if($product->category_id == 1)

                                                        {{ $product->make_year }}

                                                    @else

                                                        {{ $product->space }}

                                                    @endif

                                                </h3>
                                                <span
                                                    class="product-price second_color text-center-dir">{{ $product->price }}
                                                    رس</span>
                                                <div class="product-description">
                                                    <ul>

                                                    @if($product->category_id == 1)

                                                        <li>سنه الصنع : {{ $product->make_year }}</li>
                                                        <li>الممشي : {{ $product->the_walker }} كم/ساعة</li>
                                                        <li>السعة : {{ $product->capacity }}</li>

                                                    @else

                                                        @if($product->number_of_shops)<li>عدد المحلات: {{ $product->number_of_shops }} </li>@endif
                                                        @if($product->neighborhood)<li>الحي: {{ $product->neighborhood }} </li>@endif
                                                        @if($product->street_view)<li>عرض الواجهه: {{ $product->street_view }} م</li>@endif
                                                        @if($product->border)<li>الحدود: {{ $product->border }} </li>@endif

                                                        @if(
                                                            $product->subcategory_id == 18 ||
                                                            $product->subcategory_id == 19 ||
                                                            $product->subcategory_id == 20 ||
                                                            $product->subcategory_id == 21 ||
                                                            $product->subcategory_id == 24 ||
                                                            $product->subcategory_id == 25 ||
                                                            $product->subcategory_id == 26 ||
                                                            $product->subcategory_id == 28 ||
                                                            $product->subcategory_id == 29 ||
                                                            $product->subcategory_id == 30 ||
                                                            $product->subcategory_id == 31 ||
                                                            $product->subcategory_id == 32)
                                                            
                                                        @else                                                        

                                                            @if($product->number_halls_for_two)<li>عدد الصالات للشقه رقم 2: {{ $product->number_halls_for_two }} </li>@endif
                                                            @if($product->women_boards_for_two)<li>عدد مجالس النساء للشقة رقم 2: {{ $product->women_boards_for_two }} </li>@endif
                                                            @if($product->men_boards_for_two)<li>عدد مجالس الرجال للشقة رقم 2: {{ $product->men_boards_for_two }} </li>@endif
                                                            @if($product->number_of_bedrooms_for_two)<li>عدد عرف النوم للشقه رقم 2: {{ $product->number_of_bedrooms_for_two }} </li>@endif
                                                            @if($product->number_halls_for_one)<li>عدد الصالات للشقه رقم 1: {{ $product->number_halls_for_one }} </li>@endif
                                                            @if($product->women_boards_for_one)<li>عدد مجالس النساء للشقة 1: {{ $product->women_boards_for_one }} </li>@endif
                                                            @if($product->men_boards_for_one)<li>عدد مجالس الرجال للشقة 1: {{ $product->men_boards_for_one }} </li>@endif
                                                            @if($product->number_of_bedrooms_for_one)<li>عدد عرف النوم للشقه 1: {{ $product->number_of_bedrooms_for_one }} </li>@endif
                                                        
                                                        @endif
                                                        
                                                        @if($product->shops)<li> محلات تجارية: {{ $product->shops }} </li>@endif
                                                        @if($product->number_of_sections)<li>عدد الاقسام: {{ $product->number_of_sections }} </li>@endif
                                                        @if($product->inside_height)<li>الارتفاع من الداخل: {{ $product->inside_height }} م</li>@endif
                                                        @if($product->street_or_lane_width)<li>عرض شارع او ممر: {{ $product->street_or_lane_width }} م</li>@endif
                                                        @if($product->fuel_tank_capacity)<li>سعة خزانات الموقود: {{ $product->fuel_tank_capacity }} لتر</li>@endif
                                                        @if($product->number_pumps)<li>عدد المضخات: {{ $product->number_pumps }} </li>@endif
                                                        @if($product->on_highway)<li>علي طريق سريع : {{ $product->on_highway }} </li>@endif
                                                        @if($product->storehouse)<li>عدد المستودع: {{ $product->storehouse }} </li>@endif
                                                        @if($product->number_bathrooms)<li>عدد دورات المياة: {{ $product->number_bathrooms }} </li>@endif
                                                        @if($product->number_of_swimming_pools)<li>عدد المسابح: {{ $product->number_of_swimming_pools }} </li>@endif
                                                        @if($product->number_kitchens)<li>عدد المطابخ: {{ $product->number_kitchens }} </li>@endif
                                                        @if($product->men_boards)<li>عدد مجالس الرجال: {{ $product->men_boards }} </li>@endif
                                                        @if($product->women_councils)<li>عدد مجالس النساء: {{ $product->women_councils }} </li>@endif
                                                        @if($product->number_of_master_bedrooms)<li>عدد غرف النوم الماستر: {{ $product->number_of_master_bedrooms }} </li>@endif
                                                        @if($product->number_of_roles)<li>عدد الادوار: {{ $product->number_of_roles }} </li>@endif
                                                        @if($product->space)<li>المساحة: {{ $product->space }} م</li>@endif
                                                        @if($product->building_erea)<li>مسطح البناء: {{ $product->building_erea }} م</li>@endif
                                                        @if($product->number_halls)<li>عدد الصالات: {{ $product->number_halls }} </li>@endif
                                                        @if($product->number_bedroom)<li>عدد غرف النوم: {{ $product->number_bedroom }} </li>@endif                                                    

                                                    @endif

                                                    <!-- BEGIN: attribute and opttion -->
                                                        @if($product->subcategory_id == 16 || 
                                                            $product->subcategory_id == 17 ||
                                                            $product->subcategory_id == 18 ||
                                                            $product->subcategory_id == 19 ||
                                                            $product->subcategory_id == 20 ||
                                                            $product->subcategory_id == 21 ||
                                                            $product->subcategory_id == 22 ||
                                                            $product->subcategory_id == 23 ||
                                                            $product->subcategory_id == 24 ||
                                                            $product->subcategory_id == 25 ||
                                                            $product->subcategory_id == 26 ||
                                                            $product->subcategory_id == 27 ||
                                                            $product->subcategory_id == 28 ||
                                                            $product->subcategory_id == 29 ||
                                                            $product->subcategory_id == 30 ||
                                                            $product->subcategory_id == 31 ||
                                                            $product->subcategory_id == 32 ||
                                                            $product->subcategory_id == 33)
                                                        @else
                                                            @foreach($product->attributeandoption()->limit(7)->get() as $attributewithoption)
                                                                <li>{{ $attributewithoption->attribute->name }} : {{ $attributewithoption->option->name }}</li>
                                                            @endforeach

                                                        @endif
                                                    <!-- END: attribute and opttion -->

                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-2 left-product-grid">
                                        <div class="left-product text-left-dir">
                                            <div class="profile-edit-icons">
                                                <a href="{{ route('web.edit.product' , $product->slug) }}" class="profile-edit-icon"
                                                    title="تعديل"><i class="fa fa-pencil-alt"></i></a>
                                                <a href="{{ route('web.product' , $product->slug) }}" class="profile-edit-icon"
                                                    title="عرض"><i class="fa fa-eye"></i></a>
                                                <button type="button"
                                                    class="profile-edit-icon remove-myproduct" data-id="{{ $product->id }}" data-action="{{ route('web.delete.product') }}" title="حذف"><i
                                                        class="far fa-trash-alt"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!--end product-div-->

                        </div>
                    @empty

                        @include('website.layouts.empty')

                    @endforelse

                <!--end div-->
                <div class="paginate-center">

                    {!! $products->links() !!}

                </div>

            </div>
        </div>
    </div>