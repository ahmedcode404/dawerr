@extends('website.layouts.app')
@section('title' , 'الرئيسية')
@push('css')


    @if(session('categoryId') == 1)


    @elseif(session('categoryId') == 2)

    <link rel="stylesheet" href="{{ url('web') }}/css/building-style.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('web') }}/css/building-responsive.css" type="text/css" />    

    @endif    

@endpush
@section('content')


        
        <input type="hidden" id="name-reciver" value="{{ $user->first_name .' '. $user->second_name .' '. $user->last_name }}">
        <input type="hidden" id="reciver-id" value="{{ $user->id }}">
        <input type="hidden" id="image-reciver" value="{{ $user->imageuser() }}">
        <input type="hidden" id="name-sender" value="{{ Auth::user()->first_name .' '. Auth::user()->second_name .' '. Auth::user()->last_name }}">
        <input type="hidden" id="sender-id" value="{{ Auth::user()->id }}">
        <input type="hidden" id="image-sender" value="{{ Auth::user()->imageuser() }}">
        <input type="hidden" id="sound" value="{{ url('website/sound.mp3') }}">        
             

        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">المحادثات</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                المحادثات
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->




        <!--start chat-pg
          ================-->

        <section class="chat-pg text-right margin-div" data-aos="fade-in">
            <div class="container">
                <div class="main-chat">
                    <div class="row">
                        <!--start chat-contant-grid-->
                        <div class="col-xl-3  col-lg-4 col-md-6 chat-contant-grid">
                            <!--start chat-content-list-->
                            <div class="chat-content-list">
                                <!--start search-form-->
                                <form class="chat-form gray-bg-form">
                                    <div class="chat-search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="search-user"
                                                placeholder="البحث " />

                                            <!-- <button type="submit"><i class="fa fa-search"></i></button> -->

                                        </div>
                                    </div>
                                </form>
                                <!--end search-form-->

                                <ul class="list-unstyled chat-users hirozintal-scroll" id="all-users">


                                </ul>

                                <button type="button"
                                    class="simple-btn users-close auto-icon d-lg-none d-md-inline-block"><i
                                        class="fa fa-arrow-up"></i></button>

                            </div>
                            <!--end chat-content-list-->
                        </div>
                        <!--end chat-contant-grid-->

                        <!--start main-chat-grid-->
                        <div class="col-xl-9 col-lg-8 col-md-12 main-chat-grid">
                            <form action="" id="form-chat" data-upload="{{ route('web.uploade.file.chat') }}">
                                <div class="main-chat-div">

                                    <div class="main-chat-owner">
                                        <div class="chat-owner gray-color">
                                            <div class="chat-img">
                                                <img src="{{ $user->ImageUser() }}" alt="person" />
                                            </div>
                                            <h3 class="second_color">{{ $user->first_name }} {{ $user->second_name }} {{ $user->last_name }}</h3>
                                            <p id="online"></p>
                                        </div>
                                    </div>

                                    <div class="inner-chat-div hirozintal-scroll" id="content-meassages">


                                    </div>

                                    <div class="chat-attch">
                                        <div class="ch-att">
                                            <textarea id="input-chat" class="form-control" required></textarea>
                                            <div class="attach-file">
                                                <input type="file" id="upload-chat" class="hidden-input"
                                                    accept=" .jpg, .png,.jpeg" multiple>
                                                <label for="upload-chat" class="chat-label auto-icon"><i
                                                        class="fa fa-camera-retro"></i></label>
                                            </div>
                                            <div class="custom-btn">
                                                <button type="submit">
                                                    <div class="front">إرسال</div>
                                                    <div class="back auto-icon"><i class="fa fa-paper-plane"></i></div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                        <!--end login-form-grid-->
                    </div>
                </div>
            </div>
        </section>

        <!--end chat-pg-->



        <!--start product-icons-->
        <div class="product-icons text-center auto-icon d-lg-none d-md-inline-block">
            <button class="product-icon users-show"><i class="fa fa-users"></i></button>
        </div>
        <!--end product-icons-->





@push('js')

    <script type="text/javascript" src="{{ url('web') }}/back-end/chat.js"></script>

    @if(session('categoryId') == 1)



      
    

    @elseif(session('categoryId') == 2)
        

    

    @endif

@endpush

@endsection