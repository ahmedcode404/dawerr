<!DOCTYPE html>

<html dir="rtl">

<head>
    <title>دور</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content=" website" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#2B2B2B">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#2B2B2B">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#2B2B2B">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('web') }}/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('web') }}/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('web') }}/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('web') }}/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('web') }}/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('web') }}/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('web') }}/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('web') }}/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('web') }}/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('web') }}/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('web') }}/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('web') }}/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('web') }}/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="{{ url('web') }}/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#2B2B2B">
    <meta name="msapplication-TileImage" content="{{ url('web') }}/images/favicon/ms-icon-144x144.png">


    <!-- Style sheet
         ================ -->

    <link rel="stylesheet" href="{{ url('web') }}/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        type="text/css" />
    <link rel="stylesheet" href="{{ url('web') }}/css/general.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('web') }}/css/hero.css" type="text/css" />
    <script type="text/javascript" src="{{ url('web') }}/js/jquery-3.4.1.min.js"></script>

</head>

<body>



    <!-- start-hero4=====
========== -->
    <div class="hero4 error404">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <div class="logo_error404">
                        <img src="{{ url('web') }}/images/main/hero/Daweer - دور Logo white.png" alt="">
                    </div>  
                </div>

                <div class="col-lg-12">
                    <div class="text_error404">
                        <h2 class="mb-1 not-found">الصفحة غير موجودة </h2>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="img_error404"> 
                        <lottie-player class="lottie1" src="{{ url('web') }}/js/not found svg-not-found/data.json"  speed="1"  loop autoplay></lottie-player>
                    </div> 
                </div>
            </div>
<<<<<<< HEAD
=======
            <div class="back_arrow">  
                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fas fa-arrow-left"></i> </a> 
            </div>
>>>>>>> d7d1904f47785f02320bccfa496a93519b1548e7
        </div>
    </div>

    <!-- end-hero4===
====== -->


    <!-- scripts
         ================ -->
    <script type="text/javascript" src="{{ url('web') }}/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/respond.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/popper.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/aos.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/anime.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/hero.js"></script>
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>

</body>

</html>

                    {{--<div class="col-lg-12 row pn">
                    <div class="col-lg-6 col-md-6">
                        <!-- start sub_hero4 -->
                        <div class="sub-hero4">
                            <!-- logo_hero4  -->
                            <div class="logo_hero4">
                                <img src="{{ url('web') }}/images/main/hero/Daweer - دور Logo white.png" alt="">
                            </div>  

                        </div>
                        <!-- start text_hero4 -->
                    </div>

                    <!-- Error page-->
                    <div class="misc-wrapper">

                        <div class="misc-inner p-2 p-sm-3">
                            <div class="w-100 text-center">
                                <h2 class="mb-1 not-found">صفحة غير موجوده 🕵🏻‍♀️</h2>
                            </div>
                        </div>

                    </div>
                    <!-- / Error page-->

                </div>--}}