   
@extends('website.layout-auth.app')
@section('title' , 'نسيت كلمة المرور')
@section('content')



    <!-- start-hero4=============== -->

    <section class="login-pg white-bg" data-aos="zoom-in-down">
        <div class="login-div">

            <form class="light-bg-form login-form" action="{{ route('web.send.code.reset.password') }}" method="post">
                @csrf
                <div class="text-center">
                    <h3 class="second_color">نسيت كلمة المرور</h3>
                    <p class="first_color">يرجي إدخال رقم الجوال لإرسال كود التحقق</p>
                </div>
                <div class="form-group">
                    <input type="tel" name="phone" class="form-control" placeholder="رقم الجوال" minlength="11" maxlength="14"
                        required>
                </div>

                <div class="text-center">
                    <div class="form-group">
                        <button type="submit" class="border-btn"><span>إرسال </span></button>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <!-- end-hero4========= -->


<!-- js -->
@push('js')

    <!--for this page only-->
    <script type="text/javascript" src="{{ url('web') }}/js/login.js"></script>

@endpush
<!-- js -->

@endsection