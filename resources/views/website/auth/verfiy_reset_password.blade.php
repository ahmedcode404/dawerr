   
@extends('website.layout-auth.app')
@section('title' , 'تفعيل الحساب')
@section('content')



    <!-- start-hero4=============== -->

    <section class="login-pg white-bg" data-aos="zoom-in-down">
        <div class="login-div">

            <form class="light-bg-form login-form" action="{{ route('web.active.code.reset.password') }}" method="post">
                @csrf
                <div class="text-center">
                    <h3 class="second_color">كود التحقق</h3>
                    <p class="first_color">يرجي إدخال كود التحقق لتأكيد التسجيل بالموقع</p>
                </div>

                <input type="hidden" name="slug" value="{{ $slug }}">

                <div class="form-group">
                    <input type="number" name="code" class="form-control no_apperance_number" placeholder="كود التحقق" minlength="4"
                        maxlength="4" required>
                </div>
                <br>
                <div class="text-center form-group first_color">
                    لم يتم إرسال كود التحقق ؟ 
                    <a href="{{ route('web.resend.code' , $slug) }}" class="second_color first_color_hover decorate-link">
                        إعاده إرسال 
                    
                    </a>
                </div>
                <div class="text-center">
                    <div class="form-group">
                        <button type="submit" class="border-btn"><span>إرسال </span></button>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <!-- end-hero4========= -->


<!-- js -->
@push('js')

    <!--for this page only-->
    <script type="text/javascript" src="{{ url('web') }}/js/login.js"></script>

@endpush
<!-- js -->

@endsection