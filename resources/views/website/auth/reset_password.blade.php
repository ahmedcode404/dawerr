   
@extends('website.layout-auth.app')
@section('title' , 'تغيير كلمة المرور')
@section('content')



    <!-- start-hero4=============== -->

    <section class="login-pg white-bg" data-aos="zoom-in-down">
        <div class="login-div">

            <form class="light-bg-form login-form" action="{{ route('web.new.password') }}" method="post">
                @csrf
                <div class="text-center">
                    <h3 class="second_color">تغيير كلمة المرور</h3>
                </div>

                <div class="form-group">
                    <input type="password" id="password" class="form-control" minlength="6" name="password" placeholder="كلمة المرور"
                        required>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="confirm_password" minlength="6" placeholder="تأكيد كلمة المرور"
                        required>
                </div>

                <div class="text-center">
                    <div class="form-group">
                        <button type="submit" class="border-btn"><span>حفظ </span></button>
                    </div>
                </div>
            </form>
        </div>
    </section>    

    <!-- end-hero4========= -->


<!-- js -->
@push('js')

    <!--for this page only-->
    <script type="text/javascript" src="{{ url('web') }}/js/login.js"></script>

@endpush
<!-- js -->

@endsection