    
@extends('website.layout-auth.app')
@section('title' , 'انشاء حساب')
@section('content')



    <!-- start-hero4=============== -->

    <section class="login-pg" data-aos="zoom-in-down">
        <div class="login-div">

            <form class="dark-form login-form" action="{{ route('web.register.store') }}" method="post">
                @csrf
                <div class="text-center">
                    <img src="{{ url('web') }}/images/main/white-logo.png" alt="logo">
                    <h3 class="second_color">إنشاء حساب </h3>
                </div>

                <div class="text-center">
                    <p class="second_color"><small><i class="fa fa-exclamation-triangle"></i> ملحوظه :</small> {{ session('categoryId') == 2 ? 'انته الان تقوم بأنشاء حساب في قسم العقارات' : 'انته الان تقوم بأنشاء حساب في قسم السيارات' }}</p>
                </div>

                <div class="form-group">
                    <select class="form-control select-input" id="account_type" name="type_id" required>
                        <option selected disabled>نوع الحساب</option>
                        <option value="1">حساب شخصي</option>
                        <option value="2">حساب تجاري</option>
                    </select>
                    @if($errors->has('type_id'))
                        <div class="error">{{ $errors->first('type_id') }}</div>
                    @endif                    
                </div>                

                <div id="commercial-login">
                    <div class="form-group">
                        <input type="text" class="form-control req" placeholder="إسم المعرض" value="{{ old('company_name') }}" name="company_name" required>
                        @if($errors->has('company_name'))
                            <div class="error">{{ $errors->first('company_name') }}</div>
                        @endif    
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control req" placeholder="إسم البنك" value="{{ old('name_bank') }}" name="name_bank" required>
                        @if($errors->has('name_bank'))
                            <div class="error">{{ $errors->first('name_bank') }}</div>
                        @endif                      
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control no_apperance_number req" value="{{ old('commercial_register') }}" min="1" placeholder="السجل التجاري"
                            name="commercial_register" required>
                            @if($errors->has('commercial_register'))
                                <div class="error">{{ $errors->first('commercial_register') }}</div>
                            @endif                        
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control no_apperance_number req" min="1" value="{{ old('tax_number') }}" placeholder="الرقم الضريبي"
                            name="tax_number" required>
                            @if($errors->has('tax_number'))
                                <div class="error">{{ $errors->first('tax_number') }}</div>
                            @endif                         
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control req" value="{{ old('name_account_bank') }}" placeholder="اسم الحساب"
                            name="name_account_bank" required>
                            @if($errors->has('name_account_bank'))
                                <div class="error">{{ $errors->first('name_account_bank') }}</div>
                            @endif                         
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control no_apperance_number req" value="{{ old('number_account_bank') }}" min="1" placeholder="رقم الحساب"
                            name="number_account_bank" required>
                            @if($errors->has('number_account_bank'))
                                <div class="error">{{ $errors->first('number_account_bank') }}</div>
                            @endif                       
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control no_apperance_number req" value="{{ old('number_eban_bank') }}" min="1" placeholder="رقم الإيبان "
                            name="number_eban_bank" required>
                            @if($errors->has('number_eban_bank'))
                                <div class="error">{{ $errors->first('number_eban_bank') }}</div>
                            @endif                       
                    </div>
                    <div id="map-text" class="map-div">
                        <div class="form-group">
                            <i class="fa fa-map-marker-alt" id="myloc"></i>
                            <input id="pac-input" class="controls form-control" type="text" placeholder="ابحث عن موقعك">
                        </div>
                        <div class="form-group">
                            <div id="map3"></div>
                            <input id="lat" class="" type="hidden">
                            <input id="lng" class="" type="hidden">
                        </div>
                    </div>
                </div>                

                <div id="user-login">
                    <div class="form-group">
                        <input type="text" class="form-control req" placeholder="الاسم الأول" value="{{ old('first_name') }}" name="first_name" required>
                        @if($errors->has('first_name'))
                            <div class="error">{{ $errors->first('first_name') }}</div>
                        @endif   
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control req" placeholder="الاسم الثاني" value="{{ old('second_name') }}" name="second_name" required>
                        @if($errors->has('second_name'))
                            <div class="error">{{ $errors->first('second_name') }}</div>
                        @endif   
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control req" placeholder="الاسم العائلة" value="{{ old('last_name') }}" name="last_name" required>
                        @if($errors->has('last_name'))
                            <div class="error">{{ $errors->first('last_name') }}</div>
                        @endif                    
                    </div>
                </div>

                <div class="form-group">
                    <select class="form-control city_id select-input" id="city_id" name="city_id" required>
                        <option selected disabled>اختر المدينة</option>
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}" {{ old('city_id') == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                        @endforeach

                    </select>
                    @if($errors->has('city_id'))
                        <div class="error">{{ $errors->first('city_id') }}</div>
                    @endif                    
                </div>                 

                <div class="form-group">
                    <input type="tel" class="form-control req" name="phone"  value="{{ old('phone') }}" placeholder="رقم الجوال" minlength="11" maxlength="14" required>
                    @if($errors->has('phone'))
                        <div class="error">{{ $errors->first('phone') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <input type="email" class="form-control" placeholder="البريد الإلكتروني" value="{{ old('email') }}" name="email" required>
                    @if($errors->has('email'))
                        <div class="error">{{ $errors->first('email') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <input type="password" id="password" class="form-control" name="password" placeholder="كلمة المرور" required>
                    @if($errors->has('password'))
                        <div class="error">{{ $errors->first('password') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="confirm_password" placeholder="تأكيد كلمة المرور" required>
                </div>

                <div class="form-group">
                    <div class="custom-checkbox">
                        <input type="checkbox" class="hidden-input" name="agreemnt" id="check_cond_1" required>
                        <label for="check_cond_1"> الموافقة علي <a href="{{ route('web.static' , 'terms_and_conditions') }}" class="second_color"
                                target="_blank">الشروط والأحكام</a></label>
                    </div>
                </div>

                <div class="text-center">
                    <div class="form-group">
                        <button type="submit" class="border-btn white-btn"><span>تسجيل </span></button>
                    </div>
                </div>

                <div class="text-center form-group white-text">
                    لديك حساب بالفعل ؟ <a href="{{ route('web.login') }}" class="second_color decorate-link">تسجيل دخول </a>
                </div>
            </form>
        </div>
    </section>

    <!-- end-hero4========= -->


<!-- js -->
@push('js')

    <!--for this page only-->
    <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/login.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/login-map.js"></script>
    <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWZCkmkzES9K2-Ci3AhwEmoOdrth04zKs&callback=initMap&libraries=places&v=weeklylanguage=ar"
    async></script>

@endpush
<!-- js -->

@endsection