    
@extends('website.layout-auth.app')
@section('title' , 'تسجيل الدخول')
@section('content')



    <!-- start-hero4=============== -->

        <section class="login-pg" data-aos="zoom-in-down">
            <div class="login-div">

                <form class="dark-form login-form" action="{{ route('web.login.auth') }}" method="post">
                    @csrf
                    <div class="text-center">
                        <img src="{{ url('web') }}/images/main/white-logo.png" alt="logo">
                        <h3 class="second_color">تسجيل دخول </h3>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="رقم الجوال/البريد الإلكتروني" required>
                    </div>

                    <div class="form-group">
                        <input type="password" id="password" class="form-control" name="password" placeholder="كلمة المرور"
                            required>
                    </div>

                    <div class="form-group text-left-dir">
                        <a href="{{ route('web.forget.password') }}" class="gray-color"> نسيت كلمة المرور؟</a>
                    </div>
                    <div class="text-center">
                        <div class="form-group">
                            <button type="submit" class="border-btn white-btn"><span>دخول </span></button>
                        </div>
                    </div>

                    <div class="text-center form-group white-text">
                        ليس لديك حساب؟ <a href="{{ route('web.register') }}" class="second_color decorate-link">تسجيل حساب </a>
                    </div>
                </form>
            </div>
        </section>    

    <!-- end-hero4========= -->


<!-- js -->
@push('js')

    <!--for this page only-->
    <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/login.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/login-map.js"></script>
    <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWZCkmkzES9K2-Ci3AhwEmoOdrth04zKs&callback=initMap&libraries=places&v=weeklylanguage=ar"
    async></script>

@endpush
<!-- js -->

@endsection