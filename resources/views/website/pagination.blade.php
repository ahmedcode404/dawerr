@extends('website.layouts.app')
@section('title' , 'الرئيسية')
@push('css')


    @if(session('categoryId') == 1)


    @elseif(session('categoryId') == 2)

        <!--for building only-->
        <link rel="stylesheet" href="{{ url('web') }}/css/building-style.css" type="text/css" />
        <link rel="stylesheet" href="{{ url('web') }}/css/building-responsive.css" type="text/css" />    

    @endif    

@endpush
@section('content')

    @include('website.layouts.loading')

    @if(session('categoryId') == 1)

        <!-- get pagination cars -->
        @include('website.cars.pagination')

    @elseif(session('categoryId') == 2)

        <!-- get pagination buildings  -->
        @include('website.buildings.pagination')

    @endif

@push('js')

    @if(session('categoryId') == 1)
    <script src="{{ url('web') }}/js/date.js"></script>
    <script src="{{ url('web') }}/js/ar-date.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/back-end/index.js"></script>
    
    

    @elseif(session('categoryId') == 2)
        
        <script type="text/javascript" src="{{ url('web') }}/back-end/index.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/index.js"></script>    

    @endif
@endpush

@endsection