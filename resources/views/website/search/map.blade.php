@extends('website.layouts.app')
@section('title' , 'البحث المتقدم')
@push('css')


    @if(session('categoryId') == 1)


    @elseif(session('categoryId') == 2)


    @endif    

@endpush
@section('content')

    @if(session('categoryId') == 2)

        <!-- get home buildings  -->
        @include('website.buildings.map')

    @endif

@push('js')

    @if(session('categoryId') == 2)

        <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
        <!-- <script src="{{ url('web') }}/js/custom-map.js" type="text/javascript"></script> -->
        <script src="https://unpkg.com/@googlemaps/markerwithlabel/dist/index.min.js"></script>


        <script>

            //map height
            $(document).ready(function () {
                //progressbar
                var header = $("header").outerHeight(),
                    search = $(".full-search").outerHeight(),
                    windowHeight = $(window).height(),
                    div_height = windowHeight - (header + search);
                    $("#map").height(div_height)
            });


            //full-search-form  validation 
            var $form_full = $(".full-search-form");
            $.validator.addMethod("letters", (function (e, i) {
                return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
            })), $form_full.validate({
                errorPlacement: function (e, i) {
                    i.after(e)
                },
                rules: {},
                submitHandler: function () {
                    $form_full[0].submit()
                }
            });            


            //map
            function initMap() {


                
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 7,
                    center: new google.maps.LatLng(24.774265, 46.738586),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,

                });

                var markers = new Array();

                var products = @json($products);

                // Add the markers and infowindows to the map
                // for (var i = 0; i < locations.length; i++) {
                if (products.length != 0) {

                    $.each(products , function(i , e){

                        var marker = new MarkerWithLabel({
                            icon: '',
                            position: new google.maps.LatLng(e.lat, e.lng),
                            labelContent: '<a href="'+ document.location.origin+'/web/product/'+ e.slug +'"  target="_blank" class="map-link"><img width="40px" height="40px" src="'+ document.location.origin+'/storage/'+e.image_one +'"><div class="main-inner-label" >'+ e.price +'  رس</div></a>',
                            labelClass: "maplabel", // the CSS class for the label
                            labelAnchor: new google.maps.Point(-32, -65),
                            map: map,
                            url: document.location.origin+'/product/'+ e.slug
                        });
    
                        google.maps.event.addListener(marker, 'click', function () {
                            window.location.href = this.url;
                        })
                        
                        markers.push(marker);
    
                    });

                } else 
                {

                    var marker = new MarkerWithLabel({
                        map: map,
                    });

                    
                    markers.push(marker); 

                }
                       
                // }

                function autoCenter() {
                    //  Create a new viewpoint bound
                    var bounds = new google.maps.LatLngBounds();
                    //  Go through each...
                    for (var i = 0; i < markers.length; i++) {
                        bounds.extend(markers[i].position);
                    }
                    //  Fit these bounds to the map
                    map.fitBounds(bounds);
                }

                autoCenter();

            }

            
        </script>

        <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWZCkmkzES9K2-Ci3AhwEmoOdrth04zKs&callback=initMap&libraries=&v=weekly&language=ar"
        async></script>

    @endif

@endpush

@endsection