@extends('website.layouts.app')
@section('title' , 'نتائج البحث')
@push('css')


    @if(session('categoryId') == 1)


    @elseif(session('categoryId') == 2)

    <link rel="stylesheet" href="{{ url('web') }}/css/building-style.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('web') }}/css/building-responsive.css" type="text/css" />

    @endif    

@endpush

@section('content')

    @if(session('categoryId') == 1)

        <!-- get home cars -->
        @include('website.cars.search-result')

    @elseif(session('categoryId') == 2)
        <!-- get home buildings  -->
        @include('website.buildings.search-result')

    @endif

@push('js')

    <script type="text/javascript" src="{{ url('web') }}/back-end/index.js"></script>

    @if(session('categoryId') == 1)

    
    @elseif(session('categoryId') == 2)
    

    @endif
@endpush

@endsection