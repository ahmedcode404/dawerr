@extends('website.layouts.app')
@section('title' , 'البحث المتقدم')
@push('css')


    @if(session('categoryId') == 1)


    @elseif(session('categoryId') == 2)


    @endif    

@endpush
@section('content')

    @if(session('categoryId') == 1)

        <!-- get home cars -->
        @include('website.cars.search-advanced')

    @elseif(session('categoryId') == 2)

        <!-- get home buildings  -->
        @include('website.buildings.search-advanced')

    @endif

@push('js')

    @if(session('categoryId') == 1)

        <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
        <script src="{{ url('web') }}/js/date.js"></script>
        <script src="{{ url('web') }}/js/ar-date.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/cars/advanced-search.js"></script>    
        <script type="text/javascript" src="{{ url('web') }}/back-end/search-advanced.js"></script>    
        
    @elseif(session('categoryId') == 2)
    
        <!--for this page only-->
        <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/building/advanced-search.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/back-end/get-attribute-search.js"></script>    


    @endif
@endpush

@endsection