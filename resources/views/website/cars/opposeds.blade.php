        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">معارض السيارات</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                معارض السيارات
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->



        <!-- start categories-div
         ================ -->
        <section class="categories-div margin-div" data-aos="fade-in">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10 col-md-9  col-sm-8 xs-center">
                        <h3 class="shadow-title first_color">معارض السيارات </h3>
                    </div>
                    <div class="col-xl-2 col-md-3 col-sm-4 col-9">
                        <form action="#" class="orange-form">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" id="fillter-opposed" data-action="{{ route('web.opposeds') }}" name="city_id">
                                    <option disabled selected>المدينة</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row justify-content-center" id="itemContainer">

                    @forelse($opposeds as $opposed)
                    <!--start category-->
                    <div class="col-lg-4 col-sm-6">
                        <div class="category-content big-raduis">
                            <a href="{{ route('web.opposed' , $opposed->slug) }}">
                                <div class="img has_seudo"><img src="{{ $opposed->imageuser() }}" alt="logo"></div>
                                <h3 class="second_color">{{ $opposed->first_name }}</h3>
                                {{--<p>
                                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد
                                    النص العربى حيث يمكنك أن
                                    . تولد مثل هذا النص
                                </p>--}}
                                <div class="arrow-link first_color auto-icon text-left-dir">
                                    <i class="fa fa-long-arrow-alt-left"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!--end category-->
                    @empty

                        @include('website.layouts.empty')

                    @endforelse
                </div>

                
                <div class="paginate-center">
    
                    {!! $opposeds->links() !!}
    
                </div>                
                
            </div>
        </section>
        <!--end categories-div-->