


<!-- start main-slider
 ================ -->
<section class="main-slider">
    <div class="container">
        <div class="row">
            <!--start col-->
            <div class="col-lg-12">
                <div class="slider">

                    <div id="main-owl" class="owl-carousel owl-theme main-slider">

                        @foreach($sliders as $slider)

                            <!--start item-->
                            <div class="item">
                                <div class="full-width-img main-slide-item"
                                    style="background-image:url({{ url('storage/' . $slider->image) }})">
                                    <div class="slider-caption">
                                        <div>
                                            <p>
                                                {{ $slider->content }}
                                            </p>


                                            @auth
                                                @if(in_array(session('categoryId') , auth()->user()->category()->pluck('category_id')->toArray()))

                                                    <a href="{{ route('web.add.product') }}">{{ $slider->title }}</a>
                                                    
                                                @else

                                                    <a href="#" class="category-diffrent">{{ $slider->title }}</a>

                                                @endif

                                            @else

                                                <a href="" class="login-first" data-redirectlogin="{{ route('web.login') }}">{{ $slider->title }}</a>

                                            @endauth






                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end item-->

                        @endforeach

                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
<!--end main slider-->


<!-- start full-search
 ================ -->
<section class="full-search">
    <div class="container">
        <form class="full-search-form orange-form" action="{{ route('web.search') }}" method="post">
            @csrf
            <div class="row align-items-start">
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="form-group" data-aos="fade-in">
                        <input type="text" name="company_name" class="form-control" placeholder="اسم الشركة">
                    </div>
                </div>

                <div class="col-xl-7 col-lg-6 select-width">
                    <div class="select-div" data-aos="fade-in" data-aos-delay="300">
                        <div class="form-group">
                            <select class="form-control select-input" name="type">
                                <option value="''">النوع</option>

                                @foreach($types as $type)

                                    <option value="{{ $type->id }}">{{ $type->name }}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="select-div" data-aos="fade-in" data-aos-delay="400">
                        <div class="form-group" data-aos="fade-in">
                            <input type="text" name="category" class="form-control" placeholder="الفئة">
                        </div>
                    </div>

                    <div class="select-div" data-aos="fade-in" data-aos-delay="500">
                        <div class="form-group">
                            <input type="text" name="make_year" class="form-control datepick" autocomplete="off"
                                    placeholder="سنة الصنع" >
                        </div>
                    </div>

                    <div class="search-btn-grid" data-aos="fade-in" data-aos-delay="600">
                        <button type="submit" class="search-btn auto-icon"><i class="fa fa-search"></i>
                            <span>بحث</span></button>
                    </div>

                </div>

                <div class="col-xl-2 col-lg-12 text-left-dir xs-center" data-aos="fade-in" data-aos-delay="700">
                    <div class="form-group">
                        <div class="custom-btn">
                            <a href="{{ route('web.search.advanced') }}">
                                <div class="front">بحث متقدم</div>
                                <div class="back auto-icon"><i class="fa fa-search"></i></div>
                            </a>
                        </div>

                    </div>
                </div>


            </div>
        </form>
    </div>
</section>
<!--end full-search-->


<!-- start main-products
 ================ -->
<section class="main-products">
    <div class="container" id="append-data">

        <!--start product-div-->
        @forelse($products as $product)
            <div class="product-div has_seudo" data-aos="fade-in">
                <div class="row">
                    <div class="col-lg-10  right-product-grid">
                        <div class="right-product">
                            <a href="{{ route('web.product' , $product->slug) }}">
                                <div class="product-img sm-raduis">
                                    <img src="{{ $product->imageone() }}" alt="product">
                                    <!--note: if new dont`t add class else add class yellow-badge-->
                                    
                                    @if($product->status != 'تم البيع')

                                        <span class="badges">{{ $product->status }}</span>

                                    @else

                                        <span class="badges red-badge">{{ $product->status }}</span>

                                    @endif
                                </div>

                                <div class="product-details">
                                    <h3 class="product-title first_color">{{ $product->type->name }} {{ $product->make_year }}</h3>
                                    <span class="product-price second_color text-center-dir">{{ $product->price }} رس</span>
                                    <div class="product-description">
                                        <ul>

                                            <li>سنه الصنع : {{ $product->make_year }}</li>
                                            <li>الممشي : {{ $product->the_walker }} كم/ساعة</li>

                                            <!-- BEGIN: attribute and opttion -->
                                                @foreach($product->attributeandoption()->limit(7)->get() as $attributewithoption)
                                                    <li>{{ $attributewithoption->attribute->name }} : {{ $attributewithoption->option->name }}</li>
                                                @endforeach
                                            <!-- END: attribute and opttion -->

                                        </ul>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div>
                    <div class="col-lg-2 left-product-grid">
                        <div class="left-product text-left-dir">
                            <!--note: if individual add class individual else dont`t add class-->
                            <div class="owner-type individual">{{ $product->user->typeauth->name }}</div>

                            @auth
                                @if(auth()->user()->id == $product->user_id)
                                    <!-- hidden icon favorite -->
                                @else

                                    @if(in_array($product->category_id , auth()->user()->category()->pluck('category_id')->toArray()))
                                    
                                        <div class="favourite-checkbox auto-icon text-left-dir">
                                            <input type="checkbox" {{ $product->favorites->contains('user_id' , auth()->user()->id) == true ? 'checked' : '' }} class="hidden-input" id="fav_{{ $product->id }}">
                                            <label for="fav_{{ $product->id }}" class="add-to-favorite" data-id="{{ $product->id }}" data-action="{{ route('web.add.to.favorite') }}"><i class="fa fa-heart"></i></label>
                                        </div>

                                    @else

                                        <div class="favourite-checkbox auto-icon text-left-dir">
                                            <label for="fav_{{ $product->id }}" class="category-diffrent"><i class="fa fa-heart"></i></label>
                                        </div> 

                                    @endif
                                    
                                @endif
                            @else
                            
                                <div class="favourite-checkbox auto-icon text-left-dir">
                                    <label for="fav_{{ $product->id }}" class="login-first" data-redirectlogin="{{ route('web.login') }}"><i class="fa fa-heart" ></i></label>
                                </div>

                            @endauth

                        </div>
                    </div>
                </div>
                <a href="{{ route('web.product' , $product->slug) }}" class="hover_btn auto-icon"><span> المزيد</span></a>

            </div>

        @empty
        
            @include('website.layouts.empty')
        
        @endforelse
        <!--end product-div-->

        <!--end product-div-->
        <div class="text-center paginate-center">

            {!! $products->links() !!}

        </div>    
    </div>
</section>
<!--end main products-->



