    


            <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                
            </div>

            <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                <div class="row align-items-end">

                    @foreach($options as $key=>$option)

                        <div class="col-lg-3 col-4">
                            <div class="form-group">
                                <div class="custom-checkbox circle-checkbox">
                                    <input type="radio" name="option_{{ $attribute }}" value="{{ $option->id }}" multiple class="hidden-input"
                                        id="main_check_{{ $option->id }}">
                                    <label for="main_check_{{ $option->id }}">{{ $option->name }}</label>
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>
            </div>

