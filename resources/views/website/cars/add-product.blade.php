        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">إضافة إعلان</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                إضافة إعلان

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->


        @if($errors->any())
            <div class="alert alert-danger">
                <p><strong>Opps Something went wrong</strong></p>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif

        <!-- start advanced-search
         ================ -->
        <section class="advanced-search margin-div">
            <div class="container">
                <h2 class="shadow-title first_color text-center">إضافة إعلان</h2>
                <form class="add-product-form gray-form big-inputs" action="{{ route('web.store.product') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> موقع الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div id="map-text">
                                <div class="form-group">
                                    <i class="fa fa-map-marker-alt" id="myloc"></i>
                                    <input id="pac-input" class="controls form-control" value="{{ old('address') }}" name="address" type="text" placeholder=""/>
                                    @if($errors->has('address'))
                                        <div class="error">{{ $errors->first('address') }}</div>
                                    @endif                                   
                                </div>
                                <div class="form-group">
                                    <div id="map2"></div>
                                    <input id="lat" name="lat" class="" value="{{ old('lat') }}" type="hidden">
                                    <input id="lng" name="lng" class="" value="{{ old('lng') }}" type="hidden">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إسم الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{ old('name') }}" name="name" required />
                                @if($errors->has('name'))
                                    <div class="error">{{ $errors->first('name') }}</div>
                                @endif                                    
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> الشركة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name_company" value="{{ old('name_company') }}" required />
                                @if($errors->has('name_company'))
                                    <div class="error">{{ $errors->first('name_company') }}</div>
                                @endif                                  
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> النوع:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" name="subcategory_id" required>
                                    <option selected disabled>إختر</option>
                                    @foreach($subcategories as $subcategory)
                                        <option value="{{ $subcategory->id }}" {{ $subcategory->id == old('subcategory_id') ? 'selected' : '' }}>{{ $subcategory->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('subcategory_id'))
                                    <div class="error">{{ $errors->first('subcategory_id') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> الفئة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="category" value="{{ old('category') }}" required />
                                @if($errors->has('category'))
                                    <div class="error">{{ $errors->first('category') }}</div>
                                @endif                                   
                            </div>
                        </div>
                    </div>
                    
                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> الممشي:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" min="1" class="form-control" name="the_walker" value="{{ old('the_walker') }}" required />
                                @if($errors->has('the_walker'))
                                    <div class="error">{{ $errors->first('the_walker') }}</div>
                                @endif                                   
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> السعة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" min="1" class="form-control" name="capacity" value="{{ old('capacity') }}" required />
                                @if($errors->has('capacity'))
                                    <div class="error">{{ $errors->first('capacity') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> سنة الصنع:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" min="1" class="form-control" name="make_year" value="{{ old('make_year') }}" required />
                                @if($errors->has('make_year'))
                                    <div class="error">{{ $errors->first('make_year') }}</div>
                                @endif                                  
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> السعر:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="price" value="{{ old('price') }}" min="1" required />
                                @if($errors->has('price'))
                                    <div class="error">{{ $errors->first('price') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> المدينة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" name="city_id" required>
                                    <option selected disabled>إختر</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}" {{ $city->id == old('city_id')  ? 'selected' : '' }}>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('city_id'))
                                    <div class="error">{{ $errors->first('city_id') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>                    

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> رقم الجوال:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="tel" class="form-control" name="phone" value="{{ auth()->user()->phone }}" minlength="11" maxlength="14"
                                    required />
                                    @if($errors->has('phone'))
                                        <div class="error">{{ $errors->first('phone') }}</div>
                                    @endif                                      
                            </div>

                            <div class="form-group">
                                <div class="custom-checkbox">
                                    <input type="checkbox" name="show_phone" value="1" {{ old('show_phone') == 1 ? 'checked' : '' }} class="hidden-input" id="check_phone">
                                    <label for="check_phone"><em class="second_color">إخفاء رقم الجوال .</em> بهذا
                                        الخيار لنا يستطيع العملاء الاتصال بك على رقم هاتفك والتواصل فقط يتم من خلال
                                        رسائل دور .</label>
                                        @if($errors->has('show_phone'))
                                            <div class="error">{{ $errors->first('show_phone') }}</div>
                                        @endif                                        
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> البريد الإلكتروني :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="email" class="form-control" value="{{ auth()->user()->email }}" name="email" required />
                                @if($errors->has('email'))
                                    <div class="error">{{ $errors->first('email') }}</div>
                                @endif                                  
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> صور الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_one" id="file_1"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_1"><img src="{{ url('web') }}/images/products/file.png"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                        @if($errors->has('image_one'))
                                            <div class="error">{{ $errors->first('image_one') }}</div>
                                        @endif                                          
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_two" id="file_2"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_2"><img src="{{ url('web') }}/images/products/file.png"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                        @if($errors->has('image_two'))
                                            <div class="error">{{ $errors->first('image_two') }}</div>
                                        @endif                                           
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_three" id="file_3"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_3"><img src="{{ url('web') }}/images/products/file.png"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                        @if($errors->has('image_three'))
                                            <div class="error">{{ $errors->first('image_three') }}</div>
                                        @endif                                         
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div multi-file">
                                            <input type="file" class="hidden-input" name="images[]" multiple id="file_4"
                                                accept=" .jpg, .png,.jpeg" />
                                            <label for="file_4"
                                                class="row no-marg-row align-items-center hirozintal-scroll"><img
                                                    src="{{ url('web') }}/images/products/add.png" alt="إصافة صورة">
                                                <div class="col-12 images-multi"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> حالة الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" value="جديد" {{ old('status') == 'جديد' ? 'checked' : '' }} class="hidden-input"
                                                id="main_check_1">
                                            <label for="main_check_1">جديد</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" value="مستعمل" {{ old('status') == 'مستعمل' ? 'checked' : '' }} class="hidden-input"
                                                id="main_check_2">
                                            <label for="main_check_2">مستعمل</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($errors->has('status'))
                                <div class="error">{{ $errors->first('status') }}</div>
                            @endif                                 
                        </div>
                    </div>

                        @foreach($attributes as $ke=>$attribute)
                            <div class="row align-items-start" data-aos="fade-in">

                                <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                                    <label class="second_color circle-item"> {{$attribute->name}} :</label>
                                    <input type="hidden" name="attribute[]" multiple value="{{ $attribute->id }}"/>
                                </div>

                                <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                                    <div class="row align-items-end">

                                        @foreach($attribute->options as $key=>$option)

                                            <div class="col-lg-3 col-4">
                                                <div class="form-group">
                                                    <div class="custom-checkbox circle-checkbox">
                                                        
                                                        <input type="checkbox" class="check-option remove-{{$attribute->id}}" name="option_{{ $attribute->id }}" data-id="{{ $attribute->id }}" value="{{ $option->id }}" class="hidden-input"
                                                            id="main_check_{{ $option->id }}">
                                                        <label for="main_check_{{ $option->id }}" {{ $option->id ==  old('option_'.$attribute->id) ? 'selected' : '' }}>{{ $option->name }}</label>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        @endforeach
                        
                        <hr>

                        <div class="row align-items-start" data-aos="fade-in">
                            <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                                <label class="second_color circle-item"> اضافات:</label>
                            </div>
                            <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                                <div class="form-group">
                                    <select class="form-control select-input" id="get-addition" name="addition_id" data-action="{{ route('web.get.addition') }}" required>
                                        <option selected disabled>إختر</option>
                                        @foreach($additions as $addition)
                                            <option value="{{ $addition->id }}" {{ old('addition') == $addition->id ? 'selected' : '' }}>{{ $addition->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row align-items-start" id="append-this-addition" data-aos="fade-in">
                            
                        </div>

                        <div class="row align-items-start" data-aos="fade-in">
                            <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                                <label class="second_color circle-item"> {{ $otheradditions->name }}:</label>

                            </div>
                            <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                                <div class="form-group">
                                    <select class="form-control select-input" id="get-option-other-addition" name="attribute[]" data-action="{{ route('web.get.option.other.addition') }}" required>
                                        <option selected disabled>إختر</option>
                                        @foreach($otheradditions->attributes as $attribute)
                                            <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>                        
                        
                        <div class="row align-items-start" id="append-this-other-addition" data-aos="fade-in">

                        </div>



                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إضافات أخرى :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <textarea class="form-control" name="details"></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item">اتفاقية العمولة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <div>
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" name="agreemnt_1" id="check_cond_1"
                                            required>
                                        <label for="check_cond_1"> أتعهد واقسم بالله أنا المعلن أن أدفع عمولة الموقع وهي
                                            {{ $commission->value }}% من قيمة البيع سواء تم البيع عن طريق الموقع أو بسببه
                                            <button type="button" class="simple-btn second_color" data-toggle="modal"
                                                data-target="#agreement-modal">( عرض الإتفاقية )</button>
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" name="agreemnt_2" id="check_cond_2"
                                            required>
                                        <label for="check_cond_2"> كما أتعهد بدفع العمولة خلال {{ $day->value }} أيام من استلام كامل
                                            مبلغ المبايعة
                                            <button type="button" class="simple-btn second_color" data-toggle="modal"
                                                data-target="#agreement-modal">( عرض الإتفاقية )</button></label>
                                    </div>
                                </div>

                            </div>
                            @if($errors->has('agreemnt'))
                                <div class="error">{{ $errors->first('agreemnt') }}</div>
                            @endif                                 
                        </div>
                    </div>
                    <div class="col-lg-12 text-center sm-order-5" data-aos="fade-in">
                        <div class="form-group">
                            <button type="submit" class="border-btn"><span>إضافة </span></button>
                        </div>
                    </div>



                </form>
            </div>
        </section>
        <!--end advanced-search-->



    <!--start pop up-->
    <div class="modal fade" id="agreement-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="modal-header col-12">
                        <h5 class="modal-title second_color" id="exampleModalLongTitle">اتفاقية العمولة</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="agreement-div text-center margin-bottom-div">
                        <p class="first_color">
                            بسم الله الرحمن الرحيم
                            <br>
                            قال الله تعالى:" وَأَوْفُواْ بِعَهْدِ اللهِ إِذَا عَاهَدتُّمْ وَلاَ تَنقُضُواْ الأَيْمَانَ
                            بَعْدَ تَوْكِيدِهَا وَقَدْ جَعَلْتُمُ اللهَ
                            عَلَيْكُمْ كَفِيلاً "
                            <br>
                            صدق الله العظيم

                        </p>

                        <!--start product-note-->
                        <div class="product-note light-bg text-center second_color">
                            <span class="circle-item">ملاحظة :</span>
                            عمولة الموقع هي على المعلن ولا تبرأ ذمة المعلن من العمولة إلا في حال دفعها
                        </div>
                        <!--start product-note-->
                    </div>



                </div>

            </div>
        </div>
    </div>
    <!--end pop up-->