    
    @foreach($attributes as $key=>$attribute)

        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
            <label class="second_color circle-item"> {{ $attribute->name }}:</label>
            <input type="hidden" name="attribute[]" multiple value="{{ $attribute->id }}"/>

        </div>
        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
            <div class="form-group">

                <select class="form-control select-input" name="option_{{$attribute->id}}" required>
                    <option selected disabled>إختر</option>
                   
                    @foreach($attribute->options as $option)
                        
                        <option value="{{ $option->id }}" {{ $option->id ==  old('option_'.$attribute->id) ? 'selected' : '' }}>{{ $option->name }}</option>
                        
                    @endforeach

                </select>

            </div>
        </div> 

    @endforeach