        <!--start product-div-->
        @forelse($products as $product)
            <div class="product-div has_seudo" data-aos="fade-in">
                <div class="row">
                    <div class="col-lg-10  right-product-grid">
                        <div class="right-product">
                            <a href="{{ route('web.product' , $product->slug) }}">
                                <div class="product-img sm-raduis">
                                    <img src="{{ $product->imageone() }}" alt="product">
                                    <!--note: if new dont`t add class else add class yellow-badge-->
                                    
                                    @if($product->status != 'تم البيع')

                                        <span class="badges">{{ $product->status }}</span>

                                    @else

                                        <span class="badges red-badge">{{ $product->status }}</span>

                                    @endif
                                </div>

                                <div class="product-details">
                                    <h3 class="product-title first_color">{{ $product->type->name }} {{ $product->make_year }}</h3>
                                    <span class="product-price second_color text-center-dir">{{ $product->price }} رس</span>
                                    <div class="product-description">
                                        <ul>

                                            <li>سنه الصنع : {{ $product->make_year }}</li>
                                            <li>الممشي : {{ $product->the_walker }} كم/ساعة</li>

                                            <!-- BEGIN: attribute and opttion -->
                                                @foreach($product->attributeandoption()->limit(7)->get() as $attributewithoption)
                                                    <li>{{ $attributewithoption->attribute->name }} : {{ $attributewithoption->option->name }}</li>
                                                @endforeach
                                            <!-- END: attribute and opttion -->

                                        </ul>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div>
                    <div class="col-lg-2 left-product-grid">
                        <div class="left-product text-left-dir">
                            <!--note: if individual add class individual else dont`t add class-->
                            <div class="owner-type individual">{{ $product->user->typeauth->name }}</div>

                            @auth
                                @if(auth()->user()->id == $product->user_id)
                                    <!-- hidden icon favorite -->
                                @else

                                    @if(auth()->user()->category_id == $product->category_id)
                                    
                                        <div class="favourite-checkbox auto-icon text-left-dir">
                                            <input type="checkbox" {{ $product->favorites->contains('user_id' , auth()->user()->id) == true ? 'checked' : '' }} class="hidden-input" id="fav_{{ $product->id }}">
                                            <label for="fav_{{ $product->id }}" class="add-to-favorite" data-id="{{ $product->id }}" data-action="{{ route('web.add.to.favorite') }}"><i class="fa fa-heart"></i></label>
                                        </div>

                                    @else

                                        <div class="favourite-checkbox auto-icon text-left-dir">
                                            <label for="fav_{{ $product->id }}" class="category-diffrent"><i class="fa fa-heart"></i></label>
                                        </div> 

                                    @endif
                                    
                                @endif
                            @else
                            
                                <div class="favourite-checkbox auto-icon text-left-dir">
                                    <label for="fav_{{ $product->id }}" class="login-first" data-redirectlogin="{{ route('web.login') }}"><i class="fa fa-heart" ></i></label>
                                </div>

                            @endauth

                        </div>
                    </div>
                </div>
                <a href="{{ route('web.product' , $product->slug) }}" class="hover_btn auto-icon"><span> المزيد</span></a>

            </div>

        @empty
        
            @include('website.layouts.empty')
        
        @endforelse
        <!--end product-div-->

        <!--end product-div-->
        <div class="paginate-center">

            {!! $products->links() !!}

        </div>  