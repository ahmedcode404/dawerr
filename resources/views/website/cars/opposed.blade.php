        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">{{ $opposed->first_name }}</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                {{ $opposed->first_name }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->



        <!-- start categories-list
         ================ -->
        <section class="categories-list margin-div" data-aos="fade-in">
            <div class="container">
                <div class="circle-list">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="circle-item">
                                <span class="second_color">الاسم : </span>{{ $opposed->first_name }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="circle-item">
                                <span class="second_color">اسم البنك : </span> {{ $opposed->name_bank }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="circle-item">
                                <span class="second_color">السجل التجاري : </span> {{ $opposed->commercial_register }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="circle-item">
                                <span class="second_color">اسم الحساب : </span> {{ $opposed->name_account_bank }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="circle-item">
                                <span class="second_color">رقم الجوال : </span> <em dir="ltr">{{ $opposed->phone }}</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="circle-item">
                                <span class="second_color">رقم الحساب : </span> {{ $opposed->number_account_bank }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="circle-item">
                                <span class="second_color">الموقع : </span> {{ $opposed->address }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="circle-item">
                                <span class="second_color">رقم الإيبان : </span> {{ $opposed->number_eban_bank }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end categories-list-->



        <!-- start main-products
         ================ -->
        <section class="main-products">
            <div class="container">

                <!--start product-div-->
                @foreach($opposed->products as $product)
                    <div class="product-div has_seudo" data-aos="fade-in">
                        <div class="row">
                            <div class="col-lg-10  right-product-grid">
                                <div class="right-product">
                                    <a href="{{ route('web.product' , $product->slug) }}">
                                        <div class="product-img sm-raduis">
                                            <img src="{{ $product->imageone() }}" alt="product">
                                            <!--note: if new dont`t add class else add class yellow-badge-->
                                            <span class="badges">{{ $product->status }}</span>
                                        </div>

                                        <div class="product-details">
                                            <h3 class="product-title first_color">{{ $product->type->name }} {{ $product->make_year }}</h3>
                                            <span class="product-price second_color text-center-dir">{{ $product->price }} رس</span>
                                            <div class="product-description">
                                                <ul>

                                                    <li>سنه الصنع : {{ $product->make_year }}</li>
                                                    <li>الممشي : {{ $product->the_walker }} كم/ساعة</li>

                                                    <!-- BEGIN: attribute and opttion -->
                                                        @foreach($product->attributeandoption()->limit(7)->get() as $attributewithoption)
                                                            <li>{{ $attributewithoption->attribute->name }} : {{ $attributewithoption->option->name }}</li>
                                                        @endforeach
                                                    <!-- END: attribute and opttion -->

                                                </ul>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                            </div>
                            <div class="col-lg-2 left-product-grid">
                                <div class="left-product text-left-dir">
                                    <!--note: if individual add class individual else dont`t add class-->
                                    <div class="owner-type individual">{{ $product->user->typeauth->name }}</div>

                                    @auth
                                        @if(auth()->user()->id == $product->user_id)
                                            <!-- hidden icon favorite -->
                                        @else

                                            @if(auth()->user()->category_id == $product->category_id)
                                            
                                                <div class="favourite-checkbox auto-icon text-left-dir">
                                                    <input type="checkbox" {{ $product->favorites->contains('user_id' , auth()->user()->id) == true ? 'checked' : '' }} class="hidden-input" id="fav_{{ $product->id }}">
                                                    <label for="fav_{{ $product->id }}" class="add-to-favorite" data-id="{{ $product->id }}" data-action="{{ route('web.add.to.favorite') }}"><i class="fa fa-heart"></i></label>
                                                </div>

                                            @else

                                                <div class="favourite-checkbox auto-icon text-left-dir">
                                                    <label for="fav_{{ $product->id }}" class="category-diffrent"><i class="fa fa-heart"></i></label>
                                                </div> 

                                            @endif
                                            
                                        @endif
                                    @else
                                    
                                        <div class="favourite-checkbox auto-icon text-left-dir">
                                            <label for="fav_{{ $product->id }}" class="login-first" data-redirectlogin="{{ route('web.login') }}"><i class="fa fa-heart" ></i></label>
                                        </div>

                                    @endauth

                                </div>
                            </div>
                        </div>
                        <a href="{{ route('web.product' , $product->slug) }}" class="hover_btn auto-icon"><span> المزيد</span></a>

                    </div>
                @endforeach
                <!--end product-div-->
                @if($opposed->products->isNotEmpty())
                <div class="text-center load-more-div" data-aos="zoom-out">
                    <div class="holder text-center">
                        <a class="jp-previous">←</a>
                        <a class="jp-current">1</a>
                        <a>2</a>
                        <a class="jp-next"> →</a>
                    </div>
                </div>                
                @endif
            </div>
        </section>
        <!--end main products-->

        <!-- start multi-banner
         ================ -->
        {{--<section class="multi-banner text-center">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-6" data-aos="fade-in">
                        <figure>
                            <a href="product-details.html"> <img src="../images/banner/3.png" alt="banner"></a>
                        </figure>
                    </div>
                    <div class="col-md-6" data-aos="fade-in">
                        <figure>
                            <a href="product-details.html"> <img src="../images/banner/3.png" alt="banner"></a>
                        </figure>
                    </div>

                </div>
            </div>
        </section>
        <!--end multi-banner-->



        <!-- start main-products
         ================ -->
        <section class="main-products more-product-section">
            <div class="container">
                <!--start product-div-->
                <div class="product-div has_seudo" data-aos="fade-in">
                    <div class="row">
                        <div class="col-lg-10  right-product-grid">
                            <div class="right-product">
                                <a href="product-details.html">
                                    <div class="product-img sm-raduis">
                                        <img src="../images/products/1.png" alt="product">
                                        <!--note: if sell dont`t add class if rent yellow-badge else add class orange-badge-->
                                        <span class="badges yellow-badge">مستعمل</span>
                                    </div>

                                    <div class="product-details">
                                        <h3 class="product-title first_color">تويوتا كرولا 2022</h3>
                                        <span class="product-price second_color text-center-dir">599.000 رس</span>
                                        <div class="product-description">
                                            <ul>
                                                <li>نظام الدفع : 2*4</li>
                                                <li>نظام الدفع : 2*4</li>
                                                <li>نظام الدفع : 2*4</li>
                                                <li>ناقل الحركة : يدوي</li>
                                                <li>ناقل الحركة : يدوي</li>
                                                <li>ناقل الحركة : يدوي</li>
                                                <li>نوع الوقود: بنزين</li>
                                                <li>نوع الوقود: بنزين</li>
                                                <li>نوع الوقود: بنزين</li>
                                            </ul>
                                        </div>
                                    </div>
                                </a>

                            </div>
                        </div>
                        <div class="col-lg-2 left-product-grid">
                            <div class="left-product text-left-dir">
                                <!--note: if individual add class individual else dont`t add class-->
                                <div class="owner-type individual">حساب أفراد</div>
                                <div class="favourite-checkbox auto-icon text-left-dir">
                                    <input type="checkbox" class="hidden-input" id="fav_6">
                                    <label for="fav_6"><i class="fa fa-heart"></i></label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <a href="product-details.html" class="hover_btn auto-icon"><span> المزيد</span></a>

                </div>
                <!--end product-div-->


                <!--start product-div-->
                <div class="product-div has_seudo" data-aos="fade-in">
                    <div class="row">
                        <div class="col-lg-10  right-product-grid">
                            <div class="right-product">
                                <a href="product-details.html">
                                    <div class="product-img sm-raduis">
                                        <img src="../images/products/1.png" alt="product">
                                        <!--note: if sell dont`t add class if rent yellow-badge else add class orange-badge-->
                                        <span class="badges">بيع</span>
                                    </div>

                                    <div class="product-details">
                                        <h3 class="product-title first_color">تويوتا كرولا 2022</h3>
                                        <span class="product-price second_color text-center-dir">599.000 رس</span>
                                        <div class="product-description">
                                            <ul>
                                                <li>نظام الدفع : 2*4</li>
                                                <li>نظام الدفع : 2*4</li>
                                                <li>نظام الدفع : 2*4</li>
                                                <li>ناقل الحركة : يدوي</li>
                                                <li>ناقل الحركة : يدوي</li>
                                                <li>ناقل الحركة : يدوي</li>
                                                <li>نوع الوقود: بنزين</li>
                                                <li>نوع الوقود: بنزين</li>
                                                <li>نوع الوقود: بنزين</li>
                                            </ul>
                                        </div>
                                    </div>
                                </a>

                            </div>
                        </div>
                        <div class="col-lg-2 left-product-grid">
                            <div class="left-product text-left-dir">
                                <!--note: if individual add class individual else dont`t add class-->
                                <div class="owner-type">معرض المملكة للسيارات</div>
                                <div class="favourite-checkbox auto-icon text-left-dir">
                                    <input type="checkbox" class="hidden-input" id="fav_7">
                                    <label for="fav_7"><i class="fa fa-heart"></i></label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <a href="product-details.html" class="hover_btn auto-icon"><span> المزيد</span></a>

                </div>
                <!--end product-div-->

                <div class="text-center load-more-div" data-aos="zoom-out">
                    <div class="holder text-center">
                        <a class="jp-previous">←</a>
                        <a class="jp-current">1</a>
                        <a>2</a>
                        <a class="jp-next"> →</a>
                    </div>
                </div>

            </div>
        </section>--}}
        <!--end main products-->