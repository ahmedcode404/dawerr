        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">تعديل الاعلان</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                تعديل الاعلان

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->


        @if($errors->any())
            <div class="alert alert-danger">
                <p><strong>Opps Something went wrong</strong></p>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif

        <!-- start advanced-search
         ================ -->
        <section class="advanced-search margin-div">
            <div class="container">
                <h2 class="shadow-title first_color text-center">تعديل الاعلان</h2>
                <form class="add-product-form gray-form big-inputs" action="{{ route('web.update.product') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <input type="hidden" name="item" value="{{ $product->id }}">

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> موقع الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div id="map-text">
                                <div class="form-group">
                                    <i class="fa fa-map-marker-alt" id="myloc"></i>
                                    <input id="pac-input" class="controls form-control" name="address" value="{{ $product->address }}" type="text" placeholder=""/>
                                    @if($errors->has('address'))
                                        <div class="error">{{ $errors->first('address') }}</div>
                                    @endif                                   
                                </div>
                                <div class="form-group">
                                    <div id="map2"></div>
                                    <input id="lat" name="lat" class="" value="{{ $product->lat }}" type="hidden">
                                    <input id="lng" name="lng" class="" value="{{ $product->lng }}" type="hidden">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إسم الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" value="{{ $product->name }}" required />
                                @if($errors->has('name'))
                                    <div class="error">{{ $errors->first('name') }}</div>
                                @endif                                    
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> الشركة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name_company" value="{{ $product->name_company }}" required />
                                @if($errors->has('name_company'))
                                    <div class="error">{{ $errors->first('name_company') }}</div>
                                @endif                                  
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> النوع:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" name="subcategory_id" required>
                                    <option selected disabled>إختر</option>
                                    @foreach($subcategories as $subcategory)
                                        <option value="{{ $subcategory->id }}" {{ $product->subcategory_id == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('subcategory_id'))
                                    <div class="error">{{ $errors->first('subcategory_id') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> الفئة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="category" value="{{ $product->category }}" required />
                                @if($errors->has('category'))
                                    <div class="error">{{ $errors->first('category') }}</div>
                                @endif                                   
                            </div>
                        </div>
                    </div>
                    
                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> الممشي:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="the_walker" value="{{ $product->the_walker }}" required />
                                @if($errors->has('the_walker'))
                                    <div class="error">{{ $errors->first('the_walker') }}</div>
                                @endif                                   
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> السعة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="capacity" value="{{ $product->capacity }}" required />
                                @if($errors->has('capacity'))
                                    <div class="error">{{ $errors->first('capacity') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> سنة الصنع:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="make_year" value="{{ $product->make_year }}" required />
                                @if($errors->has('make_year'))
                                    <div class="error">{{ $errors->first('make_year') }}</div>
                                @endif                                  
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> السعر:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="price" value="{{ $product->price }}" min="1" required />
                                @if($errors->has('price'))
                                    <div class="error">{{ $errors->first('price') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> المدينة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" name="city_id" required>
                                    <option selected disabled>إختر</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}" {{ $product->city_id == $city->id ? 'selected' : ''  }}>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('city_id'))
                                    <div class="error">{{ $errors->first('city_id') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>                    

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> رقم الجوال:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="tel" class="form-control" name="phone" value="{{ $product->phone }}" minlength="11" maxlength="14"
                                    required />
                                    @if($errors->has('phone'))
                                        <div class="error">{{ $errors->first('phone') }}</div>
                                    @endif                                      
                            </div>

                            <div class="form-group">
                                <div class="custom-checkbox">
                                    <input type="checkbox" name="show_phone" value="1" {{ $product->show_phone == 1 ? 'ckecked' : '' }} class="hidden-input" id="check_phone">
                                    <label for="check_phone"><em class="second_color">إخفاء رقم الجوال .</em> بهذا
                                        الخيار لنا يستطيع العملاء الاتصال بك على رقم هاتفك والتواصل فقط يتم من خلال
                                        رسائل دور .</label>
                                        @if($errors->has('show_phone'))
                                            <div class="error">{{ $errors->first('show_phone') }}</div>
                                        @endif                                        
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> البريد الإلكتروني :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" value="{{ $product->email }}" required />
                                @if($errors->has('email'))
                                    <div class="error">{{ $errors->first('email') }}</div>
                                @endif                                  
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> صور الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_one" id="file_1"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_1"><img src="{{ $product->imageone() }}"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                        @if($errors->has('image_one'))
                                            <div class="error">{{ $errors->first('image_one') }}</div>
                                        @endif                                          
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_two" id="file_2"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_2"><img src="{{ $product->imagetwo() }}"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                        @if($errors->has('image_two'))
                                            <div class="error">{{ $errors->first('image_two') }}</div>
                                        @endif                                           
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_three" id="file_3"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_3"><img src="{{ $product->imagethree() }}"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                        @if($errors->has('image_three'))
                                            <div class="error">{{ $errors->first('image_three') }}</div>
                                        @endif                                         
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div multi-file">
                                            <input type="file" class="hidden-input" name="images[]" multiple id="file_4"
                                                accept=" .jpg, .png,.jpeg" />
                                            <label for="file_4"
                                                class="row no-marg-row align-items-center hirozintal-scroll append-defualt">
                                                @if($product->images->isNotEmpty())

                                                @else
                                                    <img src="{{ url('web') }}/images/products/add.png" alt="إصافة صورة">
                                                @endif
                                                <div class="col-12 images-multi">

                                                    @forelse($product->images as $image)
                                                        <div class="two-div">
                                                            <img src="{{ url('storage/' . $image->path) }}" class="image-{{$image->id}}" alt="">
                                                            <a href="" class="delete-image image-{{$image->id}}" data-action="{{ route('web.delete.image') }}" id="{{ $image->id }}">x</a>
                                                        </div>
                                                    @empty

                                                    @endforelse

                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> حالة الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" {{ $product->status == 'جديد' ? 'checked' : '' }} value="جديد" class="hidden-input"
                                                id="main_check_1">
                                            <label for="main_check_1">جديد</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" {{ $product->status == 'مستعمل' ? 'checked' : '' }} value="مستعمل" class="hidden-input"
                                                id="main_check_2">
                                            <label for="main_check_2">مستعمل</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($errors->has('status'))
                                <div class="error">{{ $errors->first('status') }}</div>
                            @endif                                 
                        </div>
                    </div>

                        @foreach($attributes as $ke=>$attribute)
                            <div class="row align-items-start" data-aos="fade-in">

                                <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                                    <label class="second_color circle-item"> {{$attribute->name}} :</label>
                                    <input type="hidden" name="attribute[]" multiple value="{{ $attribute->id }}"/>
                                </div>

                                <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                                    <div class="row align-items-end">

                                        @foreach($attribute->options as $key=>$option)

                                            <div class="col-lg-3 col-4">
                                                <div class="form-group">
                                                    <div class="custom-checkbox circle-checkbox">
                                                        <input type="checkbox" class="check-option remove-{{$attribute->id}}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{ $attribute->id }}" data-id="{{ $attribute->id }}" value="{{ $option->id }}" multiple class="hidden-input"
                                                            id="main_check_{{ $option->id }}">
                                                        <label for="main_check_{{ $option->id }}">{{ $option->name }}</label>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        @endforeach
                        
                        <hr>

                        <div class="row align-items-start" data-aos="fade-in">
                            <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                                <label class="second_color circle-item"> اضافات:</label>
                            </div>
                            <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                                <div class="form-group">
                                    <select class="form-control select-input" id="get-addition" name="addition_id" data-action="{{ route('web.get.addition') }}" required>
                                        <option selected disabled>إختر</option>
                                        @foreach($additions as $addition)
                                            <option value="{{ $addition->id }}" {{ $addition->id == $product->addition_id ? 'selected' : '' }}>{{ $addition->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row align-items-start" id="append-this-addition" data-aos="fade-in">

                            @foreach($product->addition->attributes as $key=>$attribute)

                                <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                                    <label class="second_color circle-item"> {{ $attribute->name }}:</label>
                                    <input type="hidden" name="attribute[]" multiple value="{{ $attribute->id }}"/>

                                </div>
                                <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                                    <div class="form-group">

                                        <select class="form-control select-input" name="option_{{$attribute->id}}" required>
                                            <option selected disabled>إختر</option>
                                        
                                            @foreach($attribute->options as $option)
                                                
                                                <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>
                                                
                                            @endforeach

                                        </select>

                                    </div>
                                </div> 

                            @endforeach

                        </div>

                        <div class="row align-items-start" data-aos="fade-in">
                            <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                                <label class="second_color circle-item"> {{ $otheradditions->name }}:</label>

                            </div>
                            @php
                                $attr = [];
                            @endphp
                            <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                                <div class="form-group">
                                    <select class="form-control select-input" id="get-option-other-addition" name="attribute[]" data-action="{{ route('web.get.option.other.addition') }}" required>
                                        <option selected disabled>إختر</option>
                                        @foreach($otheradditions->attributes as $attribute)

                                            @php 

                                                if($product->attributeandoption->contains('attribute_id' , $attribute->id) == true)
                                                {
                                                
                                                    $attr[] = $attribute;
        
                                                }

                                            @endphp
                                        
                                            <option value="{{ $attribute->id }}" {{ $product->attributeandoption->contains('attribute_id' , $attribute->id) ? 'selected' : '' }}>{{ $attribute->name }}</option>
                                        
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>   
                      

                    
                        <div class="row align-items-start" id="append-this-other-addition" data-aos="fade-in">

                            <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                    
                            </div>
                
                            <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                                <div class="row align-items-end">
                                        
                                    @foreach($attr[0]->options as $key=>$option)
                
                                        <div class="col-lg-3 col-4">
                                            <div class="form-group">
                                                <div class="custom-checkbox circle-checkbox">
                                                    <input type="radio" name="option_{{ $option->attribute->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} value="{{ $option->id }}" multiple class="hidden-input"
                                                        id="main_check_{{ $option->id }}">
                                                    <label for="main_check_{{ $option->id }}">{{ $option->name }}</label>
                                                </div>
                                            </div>
                                        </div>
                
                                    @endforeach
                
                                </div>
                            </div>

                        </div>



                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إضافات أخرى :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <textarea class="form-control" name="details">{{ $product->details }}</textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item">اتفاقية العمولة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <div>
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" checked name="agreemnt_1" id="check_cond_1"
                                            required>
                                        <label for="check_cond_1"> أتعهد واقسم بالله أنا المعلن أن أدفع عمولة الموقع وهي
                                            {{ $commission->value }}% من قيمة البيع سواء تم البيع عن طريق الموقع أو بسببه
                                            <button type="button" class="simple-btn second_color" data-toggle="modal"
                                                data-target="#agreement-modal">( عرض الإتفاقية )</button>
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" checked name="agreemnt_2" id="check_cond_2"
                                            required>
                                        <label for="check_cond_2"> كما أتعهد بدفع العمولة خلال {{ $day->value }} أيام من استلام كامل
                                            مبلغ المبايعة
                                            <button type="button" class="simple-btn second_color" data-toggle="modal"
                                                data-target="#agreement-modal">( عرض الإتفاقية )</button></label>
                                    </div>
                                </div>

                            </div>
                            @if($errors->has('agreemnt'))
                                <div class="error">{{ $errors->first('agreemnt') }}</div>
                            @endif                                 
                        </div>
                    </div>
                    <div class="col-lg-12 text-center sm-order-5" data-aos="fade-in">
                        <div class="form-group">
                            <button type="submit" class="border-btn"><span>إضافة </span></button>
                        </div>
                    </div>



                </form>
            </div>
        </section>
        <!--end advanced-search-->


    <!--start pop up-->
    <div class="modal fade" id="agreement-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="modal-header col-12">
                        <h5 class="modal-title second_color" id="exampleModalLongTitle">اتفاقية العمولة</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="agreement-div text-center margin-bottom-div">
                        <p class="first_color">
                            بسم الله الرحمن الرحيم
                            <br>
                            قال الله تعالى:" وَأَوْفُواْ بِعَهْدِ اللهِ إِذَا عَاهَدتُّمْ وَلاَ تَنقُضُواْ الأَيْمَانَ
                            بَعْدَ تَوْكِيدِهَا وَقَدْ جَعَلْتُمُ اللهَ
                            عَلَيْكُمْ كَفِيلاً "
                            <br>
                            صدق الله العظيم

                        </p>

                        <!--start product-note-->
                        <div class="product-note light-bg text-center second_color">
                            <span class="circle-item">ملاحظة :</span>
                            عمولة الموقع هي على المعلن ولا تبرأ ذمة المعلن من العمولة إلا في حال دفعها
                        </div>
                        <!--start product-note-->
                    </div>



                </div>

            </div>
        </div>
    </div>
    <!--end pop up-->        