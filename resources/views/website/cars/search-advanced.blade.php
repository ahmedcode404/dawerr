        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">البحث المتقدم</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                البحث المتقدم

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->




        <!-- start advanced-search
         ================ -->
        <section class="advanced-search margin-div">
            <div class="container">
                <h2 class="shadow-title first_color text-center">البحث المتقدم</h2>
                <form class="advanced-search-form orange-form big-inputs" action="{{ route('web.search.advanced.store') }}" method="post">
                    @csrf
                    <div class="row align-items-start">
                        <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" multiple name="name_company[]">

                                    <option disabled value="">اسم الشركة</option>

                                    @foreach($companies as $company)

                                        <option value="{{ $company->id }}">{{ $company->name_company }}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" name="type">
                                    <option selected value="">النوع</option>

                                    @foreach($subcategories as $subcategory)

                                        <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4  col-sm-12 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <input type="text" class="form-control" name="category" placeholder="الفئة" />
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4  col-sm-12 col-12">
                            <input type="text" name="make_year" class="form-control datepick" autocomplete="off"
                                placeholder="سنة الصنع">
                        </div>

                        <div class="col-xl-4 col-lg-4  col-sm-12 col-12">
                            <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input" name="city_id">
                                        <option selected value="">المدينة</option>

                                        @foreach($cities as $city)

                                            <option value="{{ $city->id }}">{{ $city->name }}</option>

                                        @endforeach

                                    </select>

                            </div>
                        </div>


                        <div class="col-xl-4 col-md-6 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" name="price_id">
                                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                                    @foreach($prices as $price)

                                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>


                        <div class="col-xl-4 col-md-6 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" name="status">
                                    <option selected value="">الحالة </option>
                                    <option value="جديد">جديد</option>
                                    <option value="مستعمل">مستعمل</option>
                                </select>
                            </div>
                        </div>                        


                        <div class="col-xl-4 col-md-6 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" name="addition_id">
                                    <option selected value="">اختر </option>

                                    @foreach($additions as $addition)

                                        <option value="{{ $addition->id }}">{{ $addition->name }}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>                        

<!-- ####################################################################### -->

                        @foreach($attributes as $attribute)

                            <div class="col-xl-4 col-md-6 col-12">
                                <div class="form-group" data-aos="fade-in">
                                    <select class="form-control select-input" name="option[]">
                                        <option selected value="">{{ $attribute->name }}</option>

                                        @foreach($attribute->options as $option)

                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        @endforeach

                        @foreach($additions[0]->attributes as $key=>$add)

                            @if($add->type == 'select')

                                <div class="col-xl-4 col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <select class="form-control select-input" name="option[]">
                                            <option selected value="">{{ $add->name }}</option>
                                            
                                            @foreach($add->options as $opt)

                                                <option value="{{ $opt->id }}">{{ $opt->name }}</option>

                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                            @else 

                                <div class="col-6">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group checkboxes-inline-title">
                                                        <h4 class="second_color"> {{ $add->name }} :</h4>
                                                        <div class="inline-custom-checkboxes">

                                                            @foreach($add->options as $op)

                                                                <div class="custom-checkbox">
                                                                    <input type="checkbox" name="option[]" class="hidden-input check-option remove-{{$add->id}}" value="{{ $op->id }}" data-id="{{ $add->id }}"
                                                                        id="check_{{$op->id}}">
                                                                    <label for="check_{{$op->id}}">{{$op->name}}</label>
                                                                </div>

                                                            @endforeach
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                        
                                        </div>
                                    </div>
                                </div>                            

                            @endif

                        @endforeach


                        <div class="col-12">
                            <div class="form-group" data-aos="fade-in">
                                <div class="row">
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                        <h3 class="arrow-title second_color">
                                            {{ $otheradditions->name }} :
                                        </h3>
                                    </div>

                                    <div class="col-xl-10 col-lg-10 col-md-9 col-12">
                                        <div class="form-group" data-aos="fade-in">
                                            <select class="form-control select-input directions"
                                                name="option[]" id="get-option-other-addition" data-action="{{ route('web.get.other.addition') }}">
                                                <option selected value="">الجهات الأصلية و الفرعية
                                                </option>

                                                @foreach($otheradditions->attributes as $attribute)

                                                    <option value="{{  $attribute->id }}"> {{ $attribute->name }} </option>

                                                @endforeach

                                            </select>
                                        </div>

                                        <div id="append-this-other-addition"></div>

                                    </div>
                                </div>
                            </div>
                        </div>

<!-- ####################################################################### -->

                            </div>


                            <div class="col-lg-12 text-center sm-order-5" data-aos="fade-in">
                                <div class="form-group">
                                    <button type="submit" class="border-btn"><span>بحث</span></button>

                                </div>
                            </div>


                        </div>

                </form>
            </div>
        </section>
        <!--end advanced-search-->
