@extends('website.layouts.app')
@section('title' , 'الرئيسية')

@push('css')

  

@endpush

@section('content')



        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">{{ $static->title }}</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                {{ $static->title }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->



        <!-- start static-pg
         ================ -->
        <section class="static-pg margin-div first_color" data-aos="fade-in">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center">
                        <h3 class="shadow-title first_color"> {{ $static->title }} </h3>

                            {{ $static->content }}

                    </div>
                </div>
            </div>
        </section>
        <!--end static-pg-->





@push('js')



@endpush

@endsection