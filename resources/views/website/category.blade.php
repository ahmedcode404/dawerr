    
<!DOCTYPE html>

<html dir="rtl">

<head>
    <title>دور | الاقسام</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content=" website" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#2B2B2B">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#2B2B2B">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#2B2B2B">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="msapplication-TileColor" content="#2B2B2B">
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('web') }}/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('web') }}/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('web') }}/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('web') }}/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('web') }}/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('web') }}/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('web') }}/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('web') }}/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('web') }}/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('web') }}/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('web') }}/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('web') }}/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('web') }}/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="{{ url('web') }}/images/favicon/manifest.json">


    <!-- Style sheet
         ================ -->

    <link rel="stylesheet" href="{{ url('web') }}/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        type="text/css" />
    <link rel="stylesheet" href="{{ url('web') }}/css/general.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('web') }}/css/hero.css" type="text/css" />
    <script type="text/javascript" src="{{ url('web') }}/js/jquery-3.4.1.min.js"></script>


</head>

<body>

    <!-- start-hero4=============== -->

    <div class="hero4">
        <div class="container">
            <div class="row align-items-center min-hero">
                <div class="col-lg-12 row pn">
                    <div class="col-lg-6 col-md-6">
                        <!-- start sub_hero4 -->
                        <div class="sub-hero4">
                            <!-- logo_hero4  -->
                            <div class="logo_hero4">
                                <img src="{{ url('web') }}/images/main/hero/Daweer - دور Logo white.png" alt="">
                            </div>

                            <!-- start text_hero4_hero -->
                            <div class="text_hero4">
                                <p id="typing-text" class="typewrite" data-period="200"
                                    data-type='[    "   هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة ","    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص . من مولد النص العربى   "]'>
                                    <span class="wrap"></span> </p>
                            </div>

                            <!-- start media_hero -->
                            <div class="media_hero4" id="set-5">
                                <ul>
                                    <li><a href="{{ $facebook->value }}" target="_blank"> <i class="fab fa-facebook-f"></i> </a></li>
                                    <li><a href="{{ $twitter->value }}" target="_blank"> <i class="fab fa-twitter"></i> </a></li>
                                    <li><a href="{{ $instagram->value }}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="{{ $snapchat->value }}" target="_blank"><i class="fab fa-snapchat-ghost"></i></a></li>
                                </ul>

                            </div>
                            <!-- end media_hero -->

                        </div>
                        <!-- start text_hero4 -->
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <a class="link-hero4" href="{{ route('web.home' , $categories[0]->name) }}">
                            <div class="img-hero4">
                                <img src="{{ url('web') }}/images/main/hero/brad-starkey-qgkluH64uI8-unsplash.png" alt="">
                                <span></span>
                            </div>

                            <div class="text-img_hero4">
                                <h2> السيارات </h2>
                            </div>
                        </a>

                        <a class="link-hero4" href="{{ route('web.home' , $categories[1]->name) }}">
                            <div class="img-hero4">
                                <img src="{{ url('web') }}/images/main/hero/REALESTATE.png" alt="">
                                <span></span>
                            </div>

                            <div class="text-img_hero4">
                                <h2> العقارات </h2>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- end-hero4========= -->




    <script type="text/javascript" src="{{ url('web') }}/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/respond.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/popper.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/aos.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/anime.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/hero.js"></script>

</body>

</html>