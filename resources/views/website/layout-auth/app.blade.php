<!DOCTYPE html>

<html dir="rtl">

<head>
    <title>دور | @yield('title')</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content=" website" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#2B2B2B">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#2B2B2B">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#2B2B2B">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="msapplication-TileColor" content="#2B2B2B">
    <meta name="msapplication-TileImage" content="../images/favicon/ms-icon-144x144.png">

    @include('website.layout-auth.style')

</head>

<body class="no-trans cars-body login-body">




    @yield('content')


    @include('website.layout-auth.script')


</body>

</html>