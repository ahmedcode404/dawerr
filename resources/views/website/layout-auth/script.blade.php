    <script type="text/javascript" src="{{ url('web') }}/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/respond.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/popper.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/aos.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/anime.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/messages_ar.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/custom-sweetalert.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/custom.js"></script>

    @stack('js')


    @if(session('error'))

        <script>

            swal.fire({

                title: "{{ session('error') }}",
                type: 'error',
                showConfirmButton: true,
                
            });  

        </script>

    @endif

    @if(session('success'))

        <script>

            swal.fire({

                title: "{{ session('success') }}",
                type: 'success',
                showConfirmButton: true,

            });  

        </script>

    @endif

    @if(session('note'))

        <script>

            swal.fire({

                title: "{{ session('note') }}",
                type: 'warning',
                showConfirmButton: true,

            });  

        </script>

    @endif

    @if(session('home'))

        <script>

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
                },
                buttonsStyling: true
            })

            swalWithBootstrapButtons.fire({
                title: "{{ session('home') }}",
                type: 'warning',
                showCancelButton: true,
                heightAuto:true,
                confirmButtonText: 'نعم',
                cancelButtonText: 'لا',
            }).then((result) => {
                if (result.value) {

                    window.location.href = "{{ route('web.category') }}";

                } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
                ) {
                
                }
            })

        </script>

    @endif