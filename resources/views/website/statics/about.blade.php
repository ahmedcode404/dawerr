@extends('website.layouts.app')
@section('title' , 'من نحن')

@push('css')

  

@endpush

@section('content')

        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">{{ $about->title }}</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                {{ $about->title }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->

        <!-- start static-pg
         ================ -->
         <section class="static-pg margin-div about-pg first_color" data-aos="fade-in">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center">
                        <h3 class="shadow-title first_color"> {{ $about->title }} </h3>
                    </div>


                    <div class="col-md-4">
                        <img src="{{ url('storage/' . $about->image) }}" alt="about">
                    </div>
                    <div class="col-md-8">

                        {{ $about->content }}

                    </div>
                    <div class="col-12">
                        <br> <br>

                    </div>
                    <div class="col-md-8">

                        {{ $about->content2 }}
                        
                    </div>
                    <div class="col-md-4">
                        <img src="{{ url('storage/' . $about->image2) }}" alt="about">
                    </div>

                </div>
            </div>
        </section>
        <!--end static-pg-->        


@push('js')



@endpush


@endsection