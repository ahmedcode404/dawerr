@extends('website.layouts.app')
@section('title' , 'تواصل معنا')

@push('css')

  

@endpush

@section('content')



        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">تواصل معنا</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                تواصل معنا
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->



        <!-- start contact-pg
         ================ -->
         <section class="contact-pg margin-div">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center">
                        <h3 class="shadow-title first_color">تواصل معنا</h3>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-5">
                        <div class="contact-main-list">
                            <ul class="list-unstyled">
                                <li dir="ltr"><a href="tel:+112345678990">
                                        <i class="fa fa-phone"></i>
                                        <span class="second_color"> : رقم الجوال </span>

                                        {{ $phone->value }}
                                        
                                    </a>
                                </li>

                                <li><a href="mailto:+112345678990">
                                        <i class="fa fa-envelope"></i>
                                        <span class="second_color"> البريد الإلكتروني :</span>

                                        {{ $email->value }}

                                    </a>
                                </li>

                                <li><a href="https://www.google.com/maps/search/?api=1&query={{ $lat->value }},{{ $lng->value }}" target="_blank">
                                        <i class="fa fa-map-marker-alt"></i>
                                        <span class="second_color">موقعنا :</span>

                                        {{ $address->value }}

                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="contact-socialmedia auto-icon text-center" data-aos="zoom-in">
                            <ul class="list-inline">
                                <li><a href="{{ $twitter->value }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="{{ $facebook->value }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="{{ $snapchat->value }}" target="_blank"><i class="fab fa-snapchat-ghost"></i></a></li>
                                <li><a href="{{ $instagram->value }}" target="_blank"><i class="fa fa-camera-retro"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-1 d-xl-block d-none">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-7">
                        <form class="contact-form gray-bg-form" method="post" action="{{ route('web.store.contact') }}">
                            @csrf
                            <div class="form-group" data-aos="fade-in">
                                <input type="text" class="form-control" placeholder="الإسم الأول" name="first_name"
                                    required>
                            </div>

                            <div class="form-group" data-aos="fade-in">
                                <input type="text" class="form-control" placeholder="الإسم الأخير" name="last_name"
                                    required>
                            </div>

                            <div class="form-group" data-aos="fade-in">
                                <input type="email" class="form-control" placeholder="البريد الإلكتروني" name="email"
                                    required>
                            </div>


                            <div class="form-group" data-aos="fade-in">
                                <textarea class="form-control" name="message" placeholder="الرسالة" required></textarea>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="border-btn"><span>إرسال </span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!--end contact-pg-->






@push('js')

<script type="text/javascript" src="{{ url('web') }}/js/contact.js"></script>


@endpush

@endsection