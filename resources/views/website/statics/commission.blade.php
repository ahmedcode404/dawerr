@extends('website.layouts.app')
@section('title' , 'حساب العمولة')

@push('css')

  

@endpush

@section('content')



        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">{{ $commission->title }}</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                {{ $commission->title }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->



        <!-- start commission-text
         ================ -->
         <section class="commission-text margin-div" data-aos="fade-in">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="shadow-title first_color">حساب العمولة </h3>
                        <div style="color:#C36F09; line-height: 40px;">
                            {{ $commission->content }}
                            <br><br><br>
                        </div>

                        <ul style="list-style-type: circle; color: #2b2b2b;list-style-position: inside;">

                            @foreach($agreements as $agreement)
                                <li style="margin-bottom:10px">
                                    {{ $agreement->content }}
                                </li>
                            @endforeach
                            
                        </ul>

                    </div>
                </div>
            </div>
        </section>
        <!--end commission-text-->

        <form action="#"  class="commission-form">

            <!-- start commission-div
         ================ -->
            <section class="commission-div margin-div gray-bg" data-aos="fade-in">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="commission-calcualte">
                                <input type="hidden" value="{{ $tax->value }}" id="tax">
                                <div class="light-bg"><span class="second_color">أدخل سعر البيع</span><input
                                        type="number" id="price-product"  placeholder="مثال :  15000" min="1" class="commission-price">
                                </div>
                                <div class="white-bg"><span class="gray-color">العمولة المستحقة</span><input
                                        type="number" placeholder="مثال :  5 ريال" class="commission-ratio" readonly>
                                </div>
                            </div>

                            <div class="commission-value text-center">ضريبة القيمة المضافة هي {{ $tax->value }}% من حساب العمولة</div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end commission-div-->


            <!--start commission-final-result-->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="light-bg commission-final-result text-center first_color" data-aos="fade-in">
                            <span class="second_color"> المبلغ الإجمالي ( حساب العمولة مضاف إليها الضريبة ) :</span>
                            <span class="first_color commission-result">0</span> رس
                            <input type="hidden" value="" name="price" id="price">

                        </div>
                    </div>
                </div>
            </div>
            <!--start commission-final-result-->


            <!-- start payment-methods
         ================ -->
            <section class="payment-methods margin-div" data-aos="fade-in">
                <div class="container">
                    <div class="row align-items-end">
                        <div class="col-12 text-center">
                            <h3 class="shadow-title first_color"> طرق دفع العمولة :</h3>
                        </div>


                        @foreach($banks as $bank)

                            <div class="col-lg-6 col-6">
                                <div class="form-group">
                                    <div class="custom-checkbox circle-checkbox checkbox-has-img">
                                        <input type="radio" name="bank_id" value="{{ $bank->id }}" class="hidden-input append-url-to-button" data-id="{{ $bank->slug }}" id="main_check_{{$bank->id}}">
                                        <label for="main_check_{{$bank->id}}" class="gray-bg sm-raduis"><img
                                                src="{{ url('storage/' . $bank->logo) }}" alt="payment"></label>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                        



                        <div class="col-lg-12 text-center">
                            <div class="form-group">
                                <button type="button" data-action="" data-id="" id="payment-now" class="border-btn"><span>متابعة </span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end payment-methods-->

        </form>
  







@push('js')

<script type="text/javascript" src="{{ url('web') }}/js/commission.js"></script>
<script type="text/javascript" src="{{ url('web') }}/back-end/commission.js"></script>

@endpush

@endsection