        @forelse($opposeds as $opposed)
            <!--start category-->
            <div class="col-lg-4 col-sm-6">
                <div class="category-content big-raduis">
                    <a href="{{ route('web.opposed' , $opposed->slug) }}">
                        <div class="img has_seudo"><img src="{{ $opposed->imageuser() }}" alt="logo"></div>
                        <h3 class="second_color">{{ $opposed->first_name }}</h3>
                        {{--<p>
                            هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد
                            النص العربى حيث يمكنك أن
                            . تولد مثل هذا النص
                        </p>--}}
                        <div class="arrow-link first_color auto-icon text-left-dir">
                            <i class="fa fa-long-arrow-alt-left"></i>
                        </div>
                    </a>
                </div>
            </div>
            <!--end category-->
        @empty

            <p style="margin: auto">لا توجد نتائج </p>

        @endforelse