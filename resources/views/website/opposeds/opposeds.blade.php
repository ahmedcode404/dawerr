@extends('website.layouts.app')
@if(session('categoryId') == 1)
@section('title' , 'معارض السيارات')
@elseif(session('categoryId') == 2)
@section('title' , 'معارض العقارات')
@endif
@push('css')


    @if(session('categoryId') == 1)


    @elseif(session('categoryId') == 2)

  

    @endif    

@endpush
@section('content')

    @if(session('categoryId') == 1)

        <!-- get home cars -->
        @include('website.cars.opposeds')

    @elseif(session('categoryId') == 2)

        <!-- get home buildings  -->
        @include('website.buildings.opposeds')

    @endif

@push('js')

    <script type="text/javascript" src="{{ url('web') }}/back-end/fillter-opposed.js"></script>
    <!--for this page only-->
    <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/jPages.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/pagination.js"></script>


    @if(session('categoryId') == 1)
        

    @elseif(session('categoryId') == 2)
    
        <link rel="stylesheet" href="{{ url('web') }}/css/building-style.css" type="text/css" />
        <link rel="stylesheet" href="{{ url('web') }}/css/building-responsive.css" type="text/css" />

    @endif

    
@endpush

@endsection