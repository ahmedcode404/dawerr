@extends('website.layouts.app')
@section('title' , 'تعديل الاعلان')

@push('css')



@endpush

@section('content')

    @if($product->category_id == 1)

        <!-- get home cars -->
        @include('website.cars.edit-product')

    @elseif($product->category_id == 2)

        <!-- get home buildings  -->
        @include('website.buildings.edit-product')

    @endif

@push('js')


    <script type="text/javascript" src="{{ url('web') }}/back-end/delete-image.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/back-end/get-addition.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/cars/edit-product.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWZCkmkzES9K2-Ci3AhwEmoOdrth04zKs&callback=initMap&libraries=places&v=weeklylanguage=ar"
        async></script>
    @if(session('categoryId') == 1)


    @elseif(session('categoryId') == 2)

        <script type="text/javascript" src="{{ url('web') }}/back-end/edit-product.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/back-end/get-attribute.js"></script>

     
    @endif

@endpush

@endsection