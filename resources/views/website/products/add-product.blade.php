@extends('website.layouts.app')
@section('title' , 'أضافة إعلان')

@push('css')



@endpush

@section('content')

    @if(session('categoryId') == 1)

        <!-- get home cars -->
        @include('website.cars.add-product')

    @elseif(session('categoryId') == 2)

        <!-- get home buildings  -->
        @include('website.buildings.add-product')

    @endif

@push('js')


    @if(session('categoryId') == 1)

        <script type="text/javascript" src="{{ url('web') }}/back-end/get-addition-load.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/back-end/get-addition.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/cars/add-products.js"></script>
        <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWZCkmkzES9K2-Ci3AhwEmoOdrth04zKs&callback=initMap&libraries=places&v=weeklylanguage=ar"
        async></script>
        
        
    @elseif(session('categoryId') == 2)
        
        <script type="text/javascript" src="{{ url('web') }}/back-end/get-attribute.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
        <script type="text/javascript" src="{{ url('web') }}/js/building/add-product.js"></script>
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWZCkmkzES9K2-Ci3AhwEmoOdrth04zKs&callback=initMap&libraries=places&v=weeklylanguage=ar"
            async></script>
        
    @endif

@endpush

@endsection