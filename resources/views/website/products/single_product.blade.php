@extends('website.layouts.app')
@section('title' , 'تفاصيل المنتج')

@push('css')

    <link href="{{ url('web') }}/css/jssor.css" rel="stylesheet" />
    <script type='text/javascript'
        src='//platform-api.sharethis.com/js/sharethis.js#property=58b550fc949fce00118ce697&product=inline-share-buttons'
        async='async'></script>

@endpush

@section('content')

        @if($product->category_id == 1)

            <!-- get home cars -->
            @include('website.cars.single_product')

        @elseif($product->category_id == 2)

            <!-- get home buildings  -->
            @include('website.buildings.single_product')

        @endif

@push('js')

        <script src="{{ url('web') }}/js/jssor.slider-28.0.0.min.js" type="text/javascript"></script>
        <script src="{{ url('web') }}/js/custom-jssor.js" type="text/javascript"></script>
        <script src="{{ url('web') }}/js/product-details.js" type="text/javascript"></script>    
        <script type="text/javascript" src="{{ url('web') }}/back-end/product-details.js"></script>

        @if($product->category_id == 1)


        @elseif($product->category_id == 2)

            <link rel="stylesheet" href="{{ url('web') }}/css/building-style.css" type="text/css" />
            <link rel="stylesheet" href="{{ url('web') }}/css/building-responsive.css" type="text/css" />    

        @endif

@endpush

@endsection