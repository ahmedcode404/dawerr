@extends('website.layouts.app')
@section('title' , 'حساب العمولة')

@push('css')

  

@endpush

@section('content')



        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">حساب العمولة</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                <a href="{{ route('web.commission') }}">حساب العمولة</a>
                                التحويل البنكي

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->


        <!-- start advanced-search
         ================ -->
        <section class="advanced-search margin-div">
            <div class="container">
                <h2 class="shadow-title first_color text-center">التحويل البنكي</h2>
                <form class="payment-form gray-form big-inputs" action="{{ route('web.payment.now') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> مبلغ العمولة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control price_result" name="commission_amount" value="{{ old('commission_amount') }}" readonly />
                                @if($errors->has('commission_amount'))
                                    <div class="error">{{ $errors->first('commission_amount') }}</div>
                                @endif                              
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> رقم الإعلان:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{ old('numebr_product') }}" name="numebr_product" placeholder="12345" required>

                                @if($errors->has('numebr_product'))
                                    <div class="error">{{ $errors->first('numebr_product') }}</div>
                                @endif                            
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إسم المستخدم :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{ old('name_user') }}" name="name_user" required />
                                @if($errors->has('name_user'))
                                    <div class="error">{{ $errors->first('name_user') }}</div>
                                @endif                               
                            </div>
                        </div>
                    </div>



                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> اسم البنك :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" value="{{ old('bank_id') }}" name="bank_id" required>
                                    <option selected disabled>إختر</option>

                                        @foreach($banks as $bank)

                                            <option value="{{ $bank->id }}" {{ old('bank_id') == $bank->id ? 'selected' : '' }}>{{ $bank->name }}</option>
 
                                        @endforeach
                                        
                                </select>
                                @if($errors->has('bank_id'))
                                    <div class="error">{{ $errors->first('bank_id') }}</div>
                                @endif                             
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> تاريخ التحويل:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" name="transfer_date" value="{{ old('transfer_date') }}" class="form-control datepick" autocomplete="off"
                                    required>
                                    @if($errors->has('transfer_date'))
                                        <div class="error">{{ $errors->first('transfer_date') }}</div>
                                    @endif                               
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إسم المحول:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{ old('transfer_name') }}" name="transfer_name" required />
                                @if($errors->has('transfer_name'))
                                    <div class="error">{{ $errors->first('transfer_name') }}</div>
                                @endif                                 
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> رقم الجوال:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" minlength="11" maxlength="14"
                                    required />
                                    @if($errors->has('phone'))
                                        <div class="error">{{ $errors->first('phone') }}</div>
                                    @endif 
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> ملاحظات :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <textarea class="form-control" name="note">{{ old('note') }}</textarea>
                                @if($errors->has('note'))
                                    <div class="error">{{ $errors->first('note') }}</div>
                                @endif 
                            </div>
                        </div>
                    </div>




                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> صورة إيصال التحويل :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <div class="custom-file-div single-file payment-file">
                                    <input type="file" class="hidden-input" name="image" id="file_1"
                                        accept=" .jpg, .png,.jpeg" required />
                                    <label for="file_1" class="form-control"><span>إختر الصورة</span></label>
                                    <div class="img-preview"></div>
                                    @if($errors->has('image'))
                                        <div class="error">{{ $errors->first('image') }}</div>
                                    @endif                                 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 text-center sm-order-5" data-aos="fade-in">
                        <div class="form-group">
                            <button type="submit" class="border-btn"><span>إرسال </span></button>
                        </div>
                    </div>



                </form>
            </div>
        </section>
        <!--end advanced-search-->



@push('js')

    <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
    <script src="{{ url('web') }}/js/date.js"></script>
    <script src="{{ url('web') }}/js/ar-date.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/payment.js"></script>

@endpush

@endsection