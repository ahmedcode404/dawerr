        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            @if($product->user->type_id == 2)
                            <h2 class="white-text">{{ $product->user->first_name }} {{ $product->user->second_name }} {{ $product->user->last_name }}</h2>
                            @endif
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                @if($product->user->type_id == 2)
                                <a href="{{ route('web.opposed' , $product->user->slug) }}">{{ $product->user->first_name }} {{ $product->user->second_name }} {{ $product->user->last_name }}</a>
                                @endif
                                {{ $product->name }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->



        <!--start product-details-pg
          ================-->
        <section class="producut-details-pg  margin-div">
            <div class="container">
                <!--start slider-->
                <div class="row">
                    <div class="col-12" data-aos="fade-in">
                        <div id="jssor_1">
                            <!-- Loading Screen -->
                            <div data-u="loading" class="jssorl-009-spin">
                                <img src="{{ url('web') }}/images/main/spin.svg" />
                            </div>

                            <span class="adv-num second_color" id="code-copy" dir="ltr">{{ $product->code }}</span>

                            <div data-u="slides" class="slides-slider">
                                
                                @forelse($product->images as $image)
                                    
                                    <div>
                                        <img data-u="image" src="{{ url('storage/' . $image->path) }}" />
                                        <img data-u="thumb" src="{{ url('storage/' . $image->path) }}" />
                                    </div> 
                                     
                                @empty
                                
                                     <div>
                                        <img data-u="image" src="{{ $product->imageOne() }}" />
                                        <img data-u="thumb" src="{{ $product->imageOne() }}" />
                                    </div>
                                    
                                     <div>
                                        <img data-u="image" src="{{ $product->imageTwo() }}" />
                                        <img data-u="thumb" src="{{ $product->imageThree() }}" />
                                    </div> 
                                    
                                @endforelse


                            </div>
                            <!-- Thumbnail Navigator -->
                            <div data-u="thumbnavigator" class="jssort101 side-jssor-menu" data-autocenter="2"
                                data-scale-left="0.75">
                                <div data-u="slides">
                                    <div data-u="prototype" class="side-jssor-menu-item">
                                        <div data-u="thumbnailtemplate" class="t"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Arrow Navigator -->
                            <div data-u="arrowleft" class="jssora093 arrowleft slid-moves" data-autocenter="2">
                                <svg viewbox="0 0 16000 16000">
                                    <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                                    <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
                                    <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
                                </svg>
                            </div>
                            <div data-u="arrowright" class="jssora093 arrowright slid-moves" data-autocenter="2">
                                <svg viewbox="0 0 16000 16000">
                                    <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                                    <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
                                    <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end slider-->

                <!--start product-details-inner-->
                <div class="product-details-inner  margin-div">
                    <div class="main-product-details">
                        <div class="row">
                            <div class="col-lg-8 col-md-6" data-aos="fade-in">
                                <h3 class="first_color">{{ $product->name }} | {{ $product->type->name }} | {{ $product->space }} م</h3>
                                <h4 class="price second_color">{{ $product->price }} رس </h4>
                                <!--note: if sell dont`t add class if rent yellow-badge else add class orange-badge-->

                                @if($product->status != 'تم البيع')

                                    <span class="badges">{{ $product->status }}</span>

                                @else

                                    <span class="badges red-badge">{{ $product->status }}</span>
                                    
                                @endif
                                <!-- if sold add class red-badge -->
                                <!-- <span class="badges red-badge">تم البيع</span> -->

                            </div>
                            <div class="col-lg-4 col-md-6" data-aos="fade-in">
                                <!--note: if individual add class individual else dont`t add class-->
                                <div class="light-bg">
                                    <div class="owner-type">{{ $product->user->first_name }} {{ $product->user->second_name }} {{ $product->user->last_name }}</div>
                                </div>
                                <div class="gray-color product-date"> {{ $product->created_at->isoFormat('ll') }} </div>
                                <!-- if sold add this -->
                                <!-- <div class="gray-color"><span class="second_color">تم البيع : </span>21 مايو 2021 </div> -->

                            </div>

                            <div class="col-12" data-aos="fade-in">
                                <div class="circle-list gray-color">
                                    <div class="row">

                                        @if($product->number_of_shops)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد المحلات: {{ $product->number_of_shops }} </div>
                                            </div>
                                        @endif
                                        @if($product->neighborhood)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">الحي: {{ $product->neighborhood }} </div>
                                            </div>
                                        @endif
                                        @if($product->street_view)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عرض الواجهه: {{ $product->street_view }} م</div>
                                            </div>
                                        @endif
                                        @if($product->border)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">الحدود: {{ $product->border }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_halls_for_two)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد الصالات للشقه رقم 2: {{ $product->number_halls_for_two }} </div>
                                            </div>
                                        @endif
                                        @if($product->women_boards_for_two)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد مجالس النساء للشقة رقم 2: {{ $product->women_boards_for_two }} </div>
                                            </div>
                                        @endif
                                        @if($product->men_boards_for_two)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد مجالس الرجال للشقة رقم 2: {{ $product->men_boards_for_two }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_of_bedrooms_for_two)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد عرف النوم للشقه رقم 2: {{ $product->number_of_bedrooms_for_two }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_halls_for_one)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد الصالات للشقه رقم 1: {{ $product->number_halls_for_one }} </div>
                                            </div>
                                        @endif
                                        @if($product->women_boards_for_one)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد مجالس النساء للشقة 1: {{ $product->women_boards_for_one }} </div>
                                            </div>
                                        @endif
                                        @if($product->men_boards_for_one)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد مجالس الرجال للشقة 1: {{ $product->men_boards_for_one }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_of_bedrooms_for_one)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد عرف النوم للشقه 1: {{ $product->number_of_bedrooms_for_one }} </div>
                                            </div>
                                        @endif
                                        @if($product->shops)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item"> محلات تجارية: {{ $product->shops }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_of_sections)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد الاقسام: {{ $product->number_of_sections }} </div>
                                            </div>
                                        @endif
                                        @if($product->inside_height)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">الارتفاع من الداخل: {{ $product->inside_height }} م</div>
                                            </div>
                                        @endif
                                        @if($product->street_or_lane_width)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عرض شارع او ممر: {{ $product->street_or_lane_width }} م</div>
                                            </div>
                                        @endif
                                        @if($product->fuel_tank_capacity)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">سعة خزانات الموقود: {{ $product->fuel_tank_capacity }} لتر</div>
                                            </div>
                                        @endif
                                        @if($product->number_pumps)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد المضخات: {{ $product->number_pumps }} </div>
                                            </div>
                                        @endif
                                        @if($product->on_highway)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">علي طريق سريع : {{ $product->on_highway }} </div>
                                            </div>
                                        @endif
                                        @if($product->storehouse)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد المستودع: {{ $product->storehouse }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_bathrooms)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد دورات المياة: {{ $product->number_bathrooms }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_of_swimming_pools)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد المسابح: {{ $product->number_of_swimming_pools }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_kitchens)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد المطابخ: {{ $product->number_kitchens }} </div>
                                            </div>
                                        @endif
                                        @if($product->men_boards)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد مجالس الرجال: {{ $product->men_boards }} </div>
                                            </div>
                                        @endif
                                        @if($product->women_councils)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد مجالس النساء: {{ $product->women_councils }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_of_master_bedrooms)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد غرف النوم الماستر: {{ $product->number_of_master_bedrooms }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_of_roles)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد الادوار: {{ $product->number_of_roles }} </div>
                                            </div>
                                        @endif
                                        @if($product->space)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">المساحة: {{ $product->space }} م</div>
                                            </div>
                                        @endif
                                        @if($product->building_erea)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">مسطح البناء: {{ $product->building_erea }} م</div>
                                            </div>
                                        @endif
                                        @if($product->number_halls)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد الصالات: {{ $product->number_halls }} </div>
                                            </div>
                                        @endif
                                        @if($product->number_bedroom)
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">عدد غرف النوم: {{ $product->number_bedroom }} </div>
                                            </div>
                                        @endif
                                        <!-- attribute and option product -->
                                        @foreach($product->attributeandoption as $attributewithoption)
                                        
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">{{ $attributewithoption->attribute->name }} : {{ $attributewithoption->option->name }} </div>
                                            </div>
                                        
                                        @endforeach
                                        <!-- attribute and option product -->                                        

                                        @if($product->subcategory_id == 6)

                                            <!-- show direction -->
                                            @foreach($product->directione as $direct)
                                            
                                            <div class="col-xl-3 col-lg-4 col-6">
                                                <div class="circle-item">{{ $direct->dire->name }} : {{ $direct->value }} م </div>
                                            </div>
                                            
                                            @endforeach
                                            <!-- show direction -->

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end product-details-inner-->

                <!--start additional-product-details-->
                <div class="additional-product-details  margin-div">
                    <div class="additional-product-info" data-aos="fade-in">
                        <div class="row">
                            <div class="col-12">
                                <div class="product-tabs big-raduis">
                                    <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                role="tab" aria-controls="home" aria-selected="true">المزيد من
                                                التفاصيل</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile"
                                                role="tab" aria-controls="profile" aria-selected="false">التواصل</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane show active" id="home" role="tabpanel"
                                            aria-labelledby="home-tab">
                                            <div class="inner-tab">
                                                <p>
                                                    {{ $product->details ? $product->details  : 'لا يوجد تفاصيل اضافية ' }}
                                                </p>
                                            </div>


                                        </div>
                                        <div class="tab-pane" id="profile" role="tabpanel"
                                            aria-labelledby="profile-tab">
                                            <div class="inner-tab">
                                                <div class="contact-product-info">
                                                    <ul class="list-unstyled" dir="ltr">
                                                        @if($product->show_phone == false)
                                                            <li><a href="tel:{{ $product->user->phone }}" class="light-bg">{{ $product->user->phone }}<i class="fa fa-phone"></i></a></li>
                                                            <li><a href="https://wa.me/{{ $product->user->phone }}" target="_blank" class="light-bg">{{ $product->user->phone }}<i class="fab fa-whatsapp"></i></a></li>
                                                        @endif
                                                        <li><a href="mailto:{{ $product->email }}" class="light-bg">{{ $product->email }}<iclass="fa fa-envelope"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end additional-product-details-->


                <!--start product-note-->
                <div class="product-note light-bg text-center second_color">
                    <div> المبايعة وجها لوجه بمكان عام و بتحويل بنكي يقلل الخطر و الاحتيال </div>
                </div>
                <!--start product-note-->

            </div>
        </section>
        <!--end product-details-pg-->

        <!--start product-icons-->
        <div class="product-icons text-center auto-icon">


            @auth
                @if(auth()->user()->id == $product->user_id)
                    <!-- hidden icon favorite -->
                @else

                    @if(in_array($product->category_id , auth()->user()->category()->pluck('category_id')->toArray()))
                    
                        <div class="favourite-checkbox">
                            <input type="checkbox" {{ $product->favorites->contains('user_id' , auth()->user()->id) == true ? 'checked' : '' }} class="hidden-input" id="fav_{{ $product->id }}">
                            <label for="fav_{{ $product->id }}" class="product-icon add-to-favorite" data-id="{{ $product->id }}" data-action="{{ route('web.add.to.favorite') }}"><i class="fa fa-heart"></i></label>
                        </div>

                    @else

                        <div class="favourite-checkbox">
                            <label for="fav_{{ $product->id }}" class="product-icon category-diffrent"><i class="fa fa-heart"></i></label>
                        </div> 

                    @endif
                    
                @endif
            @else
            
                <div class="favourite-checkbox">
                    <label for="fav_{{ $product->id }}" class="product-icon login-first" data-redirectlogin="{{ route('web.login') }}"><i class="fa fa-heart" ></i></label>
                </div>

            @endauth        


            @if($product->show_phone == false)
                <a href="https://wa.me/{{ $product->user->phone }}" class="product-icon" target="_blank" title="تواصل عبر الواتس اب"><i class="fab fa-whatsapp"></i></a>
            @endif
            <button data-network="sharethis" class="st-custom-button product-icon fa fa-share-alt"
                title="مشاركة"></button>

            @auth

                @if(auth()->user()->id == $product->user_id)
                    <!-- hidden icon favorite -->
                @else

                    @if(in_array($product->category_id , auth()->user()->category()->pluck('category_id')->toArray()))
                        @if(!$product->reports->contains('user_id' , auth()->user()->id))
                        <button class="product-icon spam-report" data-toggle="modal" data-target="#spam-modal" title="إبلاغ"><i class="far fa-flag"></i></button>
                        @endif
                        <a href="" data-action="{{ route('web.chat' , $product->user->slug) }}" data-name="{{ auth()->user()->first_name .' '. auth()->user()->second_name .' '. auth()->user()->last_name }}" data-slug="{{ auth()->user()->slug }}" data-image="{{ auth()->user()->imageuser() }}" data-id="{{ auth()->user()->id }}" data-email="{{ auth()->user()->email }}" id="redirect-chat" class="product-icon" title="تواصل عبر رسايل دور"><i
                            class="far fa-comment"></i></a>                        
                    @else

                        <button class="product-icon category-diffrent" title="إبلاغ"><i class="far fa-flag"></i></button>

                    @endif
                @endif

            @else
                        
                <button class="product-icon login-first" data-redirectlogin="{{ route('web.login') }}" title="إبلاغ"><i class="far fa-flag"></i></button>                
                <button class="product-icon login-first" data-redirectlogin="{{ route('web.login') }}" title="تواصل عبر رسايل دور"><i class="far fa-comment"></i></button>

            @endauth  

        </div>
        <!--end product-icons-->



    <!--start pop up-->
    <div class="modal fade" id="spam-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="modal-header col-12">
                        <h5 class="modal-title second_color" id="exampleModalLongTitle">إبلاغ عن المنتج</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="agreement-div">
                        <form action="{{ route('web.report.product') }}" method="post" class="flag-form">
                            @csrf
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <textarea name="report" class="form-control" required></textarea>
                            <div class="form-group text-center">
                                <button type="submit" class="border-btn"><span>إرسال </span></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--end pop up-->        