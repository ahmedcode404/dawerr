<div class="pro-append-{{$dir_id}}">
        <p class="second_color">
        @if($dir_id == 1)
            شمال
        @elseif($dir_id == 2)
            شرق
        @elseif($dir_id == 3)
            غرب
        @elseif($dir_id == 4)
            جنوب
        @endif
    </p>
    <div class="row">
@foreach($directions as $direction)

    <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

        <div class="custom-checkbox">
                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$dir_id}} check-directione" value="{{ $direction->id }}" id="{{ $direction->id }}" data-id="{{$dir_id}}" name="direction_id[]">
                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                    </div>

            <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$dir_id}} small-input-add" data-id="{{$dir_id}}" id="{{ $direction->id }}-{{$dir_id}}" name="direction_{{ $direction->id }}_{{$dir_id}}" required placeholder="بطول">

    </div>

@endforeach
</div>
</div>
