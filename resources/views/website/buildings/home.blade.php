
        <!-- start main-slider
         ================ -->
         <section class="main-slider">
            <div class="container">
                <div class="row">
                    <!--start col-->
                    <div class="col-lg-12">
                        <div class="slider">

                            <div id="main-owl" class="owl-carousel owl-theme main-slider">

                                <!--start item-->
                                @foreach($sliders as $slider)
                                    <div class="item">
                                        <div class="full-width-img main-slide-item"
                                            style="background-image:url({{  $slider->imageone() }})">
                                            <div class="slider-caption">
                                                <div>
                                                    <p>
                                                        {{ $slider->content }}
                                                    </p>

                                                    @auth
                                                        @if(in_array(session('categoryId') , auth()->user()->category()->pluck('category_id')->toArray()))

                                                            <a href="{{ route('web.add.product') }}">{{ $slider->title }}</a>
                                                            
                                                        @else

                                                            <a href="#" class="category-diffrent">{{ $slider->title }}</a>

                                                        @endif

                                                    @else

                                                        <a href="" class="login-first" data-redirectlogin="{{ route('web.login') }}">{{ $slider->title }}</a>

                                                    @endauth

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <!--end item-->

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end main slider-->


        <!-- start full-search
         ================ -->
        <section class="full-search">
            <div class="container">
                <form class="full-search-form orange-form" action="{{ route('web.search') }}" method="post">
                    @csrf
                    <div class="row align-items-start">
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" name="type">
                                    <option selected value="">نوع العقار</option>

                                        @foreach($types as $subcategory)

                                            <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>

                                        @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="col-xl-7 col-lg-6 select-width">
                            <div class="select-div" data-aos="fade-in" data-aos-delay="300">
                                <div class="form-group">
                                    <input type="number" name="space" class="form-control" min="1" max="5000"
                                        step="1" placeholder="المساحة">

                                </div>
                            </div>
                            <div class="select-div" data-aos="fade-in" data-aos-delay="400">
                                <div class="form-group">
                                    <select class="form-control select-input"  name="number_street">
                                        <option selected value="">عدد الشوارع</option>
                                        <option value="علي شارع">علي شارع</option>
                                        <option value="علي  شارعين">علي  شارعين</option>
                                        <option value="علي 3 شوارع">علي 3 شوارع</option>
                                        <option value="علي 4 شوارع">علي 4 شوارع</option>
                                    </select>
                                </div>
                            </div>

                            <div class="select-div" data-aos="fade-in" data-aos-delay="500">
                                <div class="form-group">
                                    <input type="number" name="street_view" class="form-control" min="1" max="5000"
                                        step="1" placeholder="عرض الشارع">
                                </div>
                            </div>
                            <div class="search-btn-grid" data-aos="fade-in" data-aos-delay="600">
                                <button type="submit" class="search-btn auto-icon"><i class="fa fa-search"></i>
                                    <span>بحث</span></button>
                            </div>
                        </div>

                        <div class="col-xl-2 col-lg-12 text-left-dir xs-center" data-aos="fade-in" data-aos-delay="700">
                            <div class="form-group">
                                <a href="{{ route('web.search.map') }}" class="search-btn auto-icon reverse-searvh-btn"
                                    title="بحث بالخريطة"><i class="fa fa-map-marked-alt"></i>
                                </a>
                                <div class="custom-btn">
                                    <a href="{{ route('web.search.advanced') }}">
                                        <div class="front">بحث متقدم</div>
                                        <div class="back auto-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>

                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </section>
        <!--end full-search-->


        <!-- start main-products
         ================ -->
        <section class="main-products">
            <div class="container" id="append-data">

                <!--start product-div-->
                @forelse($products as $product)

                    <div class="product-div has_seudo" data-aos="fade-in">
                        <div class="row">
                            <div class="col-lg-10  right-product-grid">
                                <div class="right-product">
                                    <a href="{{ route('web.product' , $product->slug) }}">
                                        <div class="product-img sm-raduis">
                                            <img src="{{ $product->imageone() }}" alt="product">
                                            <!--note: if sell dont`t add class if rent yellow-badge else add class orange-badge-->

                                            @if($product->status != 'تم البيع')

                                                <span class="badges">{{ $product->status }}</span>

                                            @else

                                                <span class="badges red-badge">{{ $product->status }}</span>

                                            @endif
                                            
                                        </div>

                                        <div class="product-details">
                                            <h3 class="product-title first_color">{{ $product->name }} | {{ $product->type->name }} | {{ $product->space }} متر</h3>
                                            <span class="product-price second_color text-center-dir">{{ $product->price }} رس</span>
                                            <div class="product-description">
                                                <ul>

                                                    @if($product->number_of_shops)<li>عدد المحلات: {{ $product->number_of_shops }} </li>@endif
                                                    @if($product->neighborhood)<li>الحي: {{ $product->neighborhood }} </li>@endif
                                                    @if($product->street_view)<li>عرض الواجهه: {{ $product->street_view }} م</li>@endif
                                                    @if($product->border)<li>الحدود: {{ $product->border }} </li>@endif

                                                    @if(
                                                        $product->subcategory_id == 18 ||
                                                        $product->subcategory_id == 19 ||
                                                        $product->subcategory_id == 20 ||
                                                        $product->subcategory_id == 21 ||
                                                        $product->subcategory_id == 24 ||
                                                        $product->subcategory_id == 25 ||
                                                        $product->subcategory_id == 26 ||
                                                        $product->subcategory_id == 28 ||
                                                        $product->subcategory_id == 29 ||
                                                        $product->subcategory_id == 30 ||
                                                        $product->subcategory_id == 31 ||
                                                        $product->subcategory_id == 32)
                                                        
                                                    @else  

                                                        @if($product->number_halls_for_two)<li>عدد الصالات للشقه رقم 2: {{ $product->number_halls_for_two }} </li>@endif
                                                        @if($product->women_boards_for_two)<li>عدد مجالس النساء للشقة رقم 2: {{ $product->women_boards_for_two }} </li>@endif
                                                        @if($product->men_boards_for_two)<li>عدد مجالس الرجال للشقة رقم 2: {{ $product->men_boards_for_two }} </li>@endif
                                                        @if($product->number_of_bedrooms_for_two)<li>عدد عرف النوم للشقه رقم 2: {{ $product->number_of_bedrooms_for_two }} </li>@endif
                                                        @if($product->number_halls_for_one)<li>عدد الصالات للشقه رقم 1: {{ $product->number_halls_for_one }} </li>@endif
                                                        @if($product->women_boards_for_one)<li>عدد مجالس النساء للشقة 1: {{ $product->women_boards_for_one }} </li>@endif
                                                        @if($product->men_boards_for_one)<li>عدد مجالس الرجال للشقة 1: {{ $product->men_boards_for_one }} </li>@endif
                                                        @if($product->number_of_bedrooms_for_one)<li>عدد عرف النوم للشقه 1: {{ $product->number_of_bedrooms_for_one }} </li>@endif
                                                    
                                                    @endif

                                                    @if($product->shops)<li> محلات تجارية: {{ $product->shops }} </li>@endif
                                                    @if($product->number_of_sections)<li>عدد الاقسام: {{ $product->number_of_sections }} </li>@endif
                                                    @if($product->inside_height)<li>الارتفاع من الداخل: {{ $product->inside_height }} م</li>@endif
                                                    @if($product->street_or_lane_width)<li>عرض شارع او ممر: {{ $product->street_or_lane_width }} م</li>@endif
                                                    @if($product->fuel_tank_capacity)<li>سعة خزانات الموقود: {{ $product->fuel_tank_capacity }} لتر</li>@endif
                                                    @if($product->number_pumps)<li>عدد المضخات: {{ $product->number_pumps }} </li>@endif
                                                    @if($product->on_highway)<li>علي طريق سريع : {{ $product->on_highway }} </li>@endif
                                                    @if($product->storehouse)<li>عدد المستودع: {{ $product->storehouse }} </li>@endif
                                                    @if($product->number_bathrooms)<li>عدد دورات المياة: {{ $product->number_bathrooms }} </li>@endif
                                                    @if($product->number_of_swimming_pools)<li>عدد المسابح: {{ $product->number_of_swimming_pools }} </li>@endif
                                                    @if($product->number_kitchens)<li>عدد المطابخ: {{ $product->number_kitchens }} </li>@endif
                                                    @if($product->men_boards)<li>عدد مجالس الرجال: {{ $product->men_boards }} </li>@endif
                                                    @if($product->women_councils)<li>عدد مجالس النساء: {{ $product->women_councils }} </li>@endif
                                                    @if($product->number_of_master_bedrooms)<li>عدد غرف النوم الماستر: {{ $product->number_of_master_bedrooms }} </li>@endif
                                                    @if($product->number_of_roles)<li>عدد الادوار: {{ $product->number_of_roles }} </li>@endif
                                                    @if($product->space)<li>المساحة: {{ $product->space }} م</li>@endif
                                                    @if($product->building_erea)<li>مسطح البناء: {{ $product->building_erea }} م</li>@endif
                                                    @if($product->number_halls)<li>عدد الصالات: {{ $product->number_halls }} </li>@endif
                                                    @if($product->number_bedroom)<li>عدد غرف النوم: {{ $product->number_bedroom }} </li>@endif
                                                    <!-- attribute and option product -->
                                                    @if($product->subcategory_id == 16 || 
                                                        $product->subcategory_id == 17 ||
                                                        $product->subcategory_id == 18 ||
                                                        $product->subcategory_id == 19 ||
                                                        $product->subcategory_id == 20 ||
                                                        $product->subcategory_id == 21 ||
                                                        $product->subcategory_id == 22 ||
                                                        $product->subcategory_id == 23 ||
                                                        $product->subcategory_id == 24 ||
                                                        $product->subcategory_id == 25 ||
                                                        $product->subcategory_id == 26 ||
                                                        $product->subcategory_id == 27 ||
                                                        $product->subcategory_id == 28 ||
                                                        $product->subcategory_id == 29 ||
                                                        $product->subcategory_id == 30 ||
                                                        $product->subcategory_id == 31 ||
                                                        $product->subcategory_id == 32 ||
                                                        $product->subcategory_id == 33)
                                                    @else  

                                                        @foreach($product->attributeandoption()->limit(3)->get() as $attributewithoption)
                                                        <li>{{ $attributewithoption->attribute->name }}: {{ $attributewithoption->option->name }} </li>
                                                        @endforeach
                                                        
                                                    @endif
                                                    <!-- attribute and option product -->

                                                </ul>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                            </div>
                            <div class="col-lg-2 left-product-grid">
                                <div class="left-product text-left-dir">
                                    <!--note: if individual add class individual else dont`t add class-->
                                    <div class="owner-type individual">{{ $product->user->typeauth->name }}</div>

                                    @auth

                                        @if(auth()->user()->id == $product->user_id)
                                            <!-- hidden icon favorite -->
                                        @else
                                            @if(in_array($product->category_id , auth()->user()->category()->pluck('category_id')->toArray()))
                                            
                                                <div class="favourite-checkbox auto-icon text-left-dir">
                                                    <input type="checkbox" {{ $product->favorites->contains('user_id' , auth()->user()->id) == true ? 'checked' : '' }} class="hidden-input" id="fav_{{ $product->id }}">
                                                    <label for="fav_{{ $product->id }}" class="add-to-favorite" data-id="{{ $product->id }}" data-action="{{ route('web.add.to.favorite') }}"><i class="fa fa-heart"></i></label>
                                                </div>
                                                
                                            @else 
                                                <div class="favourite-checkbox auto-icon text-left-dir">
                                                    <label for="fav_{{ $product->id }}" class="category-diffrent" ><i class="fa fa-heart"></i></label>
                                                </div>
                                            @endif
                                        @endif
                                    @else
                                    
                                        <div class="favourite-checkbox auto-icon text-left-dir">
                                            <label for="fav_{{ $product->id }}" class="login-first" data-redirectlogin="{{ route('web.login') }}"><i class="fa fa-heart" ></i></label>
                                        </div>

                                    @endauth                                    


                                </div>
                            </div>
                        </div>
                        <a href="{{ route('web.product' , $product->slug) }}" class="hover_btn auto-icon"><span> المزيد</span></a>

                    </div>

                @empty
        
                    @include('website.layouts.empty')

                @endforelse
                
                <!--end product-div-->
                <div class="paginate-center">
    
                    {!! $products->links() !!}
    
                </div>
            </div>
            
            
        </section>
        <!--end main products-->