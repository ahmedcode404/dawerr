        <!-- start full-search
         ================ -->
         <section class="full-search map-pg first_bg">
            <div class="container">
                <form class="full-search-form orange-form" action="{{ route('web.search.map') }}" method="get">

                    <div class="row align-items-start">
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" name="city_id">
                                    <option selected value="">العنوان</option>

                                    @foreach($cities as $city)

                                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="col-xl-7 col-lg-6 select-width">
                            <div class="select-div" data-aos="fade-in" data-aos-delay="300">
                                <div class="form-group">
                                    <select class="form-control select-input" name="type">
                                        <option selected value="">نوع العقار</option>
                                        @foreach($subcategories as $subcategory)

                                            <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="select-div" data-aos="fade-in" data-aos-delay="400">
                                <div class="form-group">
                                    <select class="form-control select-input" name="price_id">
                                        <option selected value="">السعر </option>

                                        @foreach($prices as $price)

                                            <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="select-div" data-aos="fade-in" data-aos-delay="500">
                                <div class="form-group">
                                    <input type="number" name="space" class="form-control" min="1" max="5000"
                                        step="1" placeholder="المساحة">

                                </div>
                            </div>


                            <div class="search-btn-grid" data-aos="fade-in" data-aos-delay="600">
                                <button type="submit" class="search-btn auto-icon"><i class="fa fa-search"></i>
                                    <span>بحث</span></button>
                            </div>
                        </div>

                        <div class="col-xl-2 col-lg-12 text-left-dir xs-center" data-aos="fade-in" data-aos-delay="700">
                            <div class="form-group">
                                <div class="custom-btn">

                                    <a href="{{ route('web.search.advanced') }}">

                                        <div class="front">بحث متقدم</div>
                                        <div class="back auto-icon"><i class="fa fa-search"></i></div>

                                    </a>

                                </div>

                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </section>
        <!--end full-search-->

        <div id="map"></div>