
    <!-- BEGIN: attribute of subcategory -->
    @if($subcategory == 6)


        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
  

        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">            
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>    

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>    

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_roles" placeholder="عدد الادوار">
            </div>
        </div>   

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_shops" placeholder="عدد المحلات">
            </div>
        </div>    


        @foreach($attributes as $attribute)
        <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
        
        <div class="{{ $attribute->cat ? 'col-md-12' : 'col-md-6' }} col-12">
            @if($attribute->cat)
                <p>وصف الحدود</p>
            @endif
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input all-option"  name="option[]">
                    <option disabled selected>{{  $attribute->name }}</option>

                    @foreach($attribute->options as $option)
                    
                    <option value="{{ $option->id }}">{{ $option->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        @endforeach    


        @foreach($directions as $direction)
        

            <div class="col-md-3 col-12">

                <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                <input type="checkbox" class="direction-{{ $direction->id }} check-directione" value="{{ $direction->id }}" id="{{ $direction->id }}" name="direction_id[]">

                <div class="form-group" data-aos="fade-in">
                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}" id="{{ $direction->id }}" name="direction[]"  placeholder="بطول">
                </div>

            </div> 

        @endforeach

    @elseif($subcategory == 7)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>  
    <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>   

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_roles" placeholder="الدور">
            </div>
        </div>   

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياة">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="storehouse" placeholder="عدد المستودع">
            </div>
        </div>      


        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                @if($attribute->cat)
                    <p>وصف الحدود</p>
                @endif

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option"  name="option[]">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach       

    @elseif($subcategory == 8)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>  
    <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>   
        
    @elseif($subcategory == 9)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>  
    <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="on_highway" placeholder="علي طريق سريع">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>  

        <div class="col-md-6 col-12">

            <div class="form-group" data-aos="fade-in">

                <select class="form-control select-input"  name="fuel_id" id="add-input-fuel">

                    <option disabled selected>عناصر الوقود</option>
                    <option value="1">كافة انواع البنزين</option>
                    <option value="2">كافة انواع البنزين وديزل</option>
                    <option value="3">كافة انواع البنزين وديزل وكيروسين</option>
                
                </select>

            </div>

        </div>        

        <div class="append-waqod col-12">

       

        </div>

        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach         
    
    @elseif($subcategory == 10)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>           

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>          

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>

    @elseif($subcategory == 11)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

    
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>           

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_or_lane_width" placeholder="عرض شارع او ممر">
            </div>
        </div>       

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="inside_height" placeholder="الارتفاع من الداخل">
            </div>
        </div>       


        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-12 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach         

    @elseif($subcategory == 12)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>       
        
        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>          

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_sections" placeholder="عدد الاقسام">
            </div>
        </div>       

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_swimming_pools" placeholder="عدد المسابح">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>       

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>    

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="shops" placeholder="محلات تجارية">
            </div>
        </div>        

        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach   
        
        
    @elseif($subcategory == 13)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>       
        
        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>          

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_sections" placeholder="عدد الاقسام">
            </div>
        </div>       

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_swimming_pools" placeholder="عدد المسابح">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>       

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>    

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="shops" placeholder="محلات تجارية">
            </div>
        </div>        



        @foreach($attributes as $attribute)

            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach       

    @elseif($subcategory == 14)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_or_lane_width" placeholder="عرض شارع او ممر">
            </div>
        </div>       

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="inside_height" placeholder="الارتفاع من الداخل">
            </div>
        </div>    
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_roles" placeholder="عدد الادوار">
            </div>
        </div>          

        @foreach($attributes as $attribute)

            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                        <option disabled selected>{{ $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach   


    @elseif($subcategory == 15)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_or_lane_width" placeholder="عرض شارع او ممر">
            </div>
        </div>       

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="inside_height" placeholder="الارتفاع من الداخل">
            </div>
        </div>    
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_roles" placeholder="عدد الادوار">
            </div>
        </div>      

        @foreach($attributes as $attribute)

            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                        <option disabled selected>{{ $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach      


    @elseif($subcategory == 16)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>



        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>    

    @elseif($subcategory == 17)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>



        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  
    @elseif($subcategory == 18)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  
        
        
    @elseif($subcategory == 19)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>   

    @elseif($subcategory == 20)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 21)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 22)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 23)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 24)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 25)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 26)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 27)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 28)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>        


        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 29)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>        

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>        


        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 30)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>        

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 31)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div> 
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>        

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>           

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

    @elseif($subcategory == 32)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>      

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>       

    @elseif($subcategory == 33)

        <div class="col-xl-6 col-lg-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="city_id">
                    <option selected value="">العنوان</option>

                    @foreach($cities as $city)

                        <option value="{{ $city->id }}">{{ $city->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>      
        <div class="col-xl-6 col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input" name="price_id">
                    <option selected value="">السعر ( مثال : من 100 - 250 رس )</option>

                    @foreach($prices as $price)

                        <option value="{{ $price->id }}">من {{ $price->price_from }} إلي {{ $price->price_to }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="text" class="form-control" name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="border" min="3" placeholder="الحدود">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input  type="number" class="form-control" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>      

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input  type="checkbox" value="{{ $option->id }}" name="option[]" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option"  name="option[]" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         


    @endif
    <!-- END: attribute of subcategory -->
