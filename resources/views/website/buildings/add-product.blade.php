        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">إضافة إعلان</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                إضافة إعلان

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->

        @if($errors->any())
            <div class="alert alert-danger">
                <p><strong>Opps Something went wrong</strong></p>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif        

        <!-- start advanced-search
         ================ -->
        <section class="advanced-search margin-div">
            <div class="container">
                <h2 class="shadow-title first_color text-center">إضافة إعلان</h2>
                <form class="add-product-form gray-form big-inputs" action="{{ route('web.store.product') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> موقع الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div id="map-text">
                                <div class="form-group">
                                    <i class="fa fa-map-marker-alt" id="myloc"></i>
                                    <input id="pac-input" name="address" value="{{ old('address') }}" class="controls form-control" type="text" placeholder=""/>
                                </div>
                                <div class="form-group">
                                    <div id="map2"></div>
                                    <input type="hidden" id="lat" name="lat" value="{{ old('lat') }}">
                                    <input type="hidden" id="lng" name="lng" value="{{ old('lng') }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إسم الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{ old('name') }}" name="name" required />
                            </div>
                        </div>
                    </div>




                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> النوع:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" name="subcategory_id" id="subcategory-select" data-action="{{ route('web.get.attribute') }}" required>
                                    <option selected disabled>إختر</option>

                                        @foreach($subcategories as $subcategory)

                                            <option value="{{ $subcategory->id }}" {{ old('subcategory_id') == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->name }}</option>

                                        @endforeach
                                        
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xl-2  d-xl-block d-none"></div>

                        <div class="row col-xl-10  align-items-end no-marg-row appended-div"></div>

                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> المساحة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" value="{{ old('space') }}" name="space" min="1" required />
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> السعر:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" value="{{ old('price') }}" name="price" min="1" required />
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> المدينة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" name="city_id" required>

                                    <option selected disabled>إختر</option>

                                        @foreach($cities as $city)

                                            <option value="{{ $city->id }}" {{ old('city_id') == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>

                                        @endforeach
                                        
                                </select>
                            </div>
                        </div>
                    </div>                      

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> رقم الجوال:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="tel" class="form-control" name="phone" value="{{ auth()->user()->phone }}" minlength="11" maxlength="14"
                                    required />
                            </div>

                            <div class="form-group">
                                <div class="custom-checkbox">
                                    <input type="checkbox" name="show_phone" {{ old('show_phone') == 1 ? 'checked' : '' }} value="1" class="hidden-input" id="check_phone">
                                    <label for="check_phone"><em class="second_color">إخفاء رقم الجوال .</em> بهذا
                                        الخيار لنا يستطيع العملاء الاتصال بك على رقم هاتفك والتواصل فقط يتم من خلال
                                        رسائل دور .</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> البريد الإلكتروني :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="email" class="form-control" value="{{ auth()->user()->email }}" name="email" required />
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> صور الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_one" id="file_1"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_1"><img src="{{ url('web') }}/images/products/file.png"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_two" id="file_2"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_2"><img src="{{ url('web') }}/images/products/file.png"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_three" id="file_3"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_3"><img src="{{ url('web') }}/images/products/file.png"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div multi-file">
                                            <input type="file" class="hidden-input" name="images[]" multiple id="file_4"
                                                accept=" .jpg, .png,.jpeg" />
                                            <label for="file_4"
                                                class="row no-marg-row align-items-center hirozintal-scroll"><img
                                                    src="{{ url('web') }}/images/products/add.png" alt="إصافة صورة">
                                                <div class="col-12 images-multi"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> حالة الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" value="للبيع" {{ old('status') == 'للبيع' ? 'checked' : '' }} class="hidden-input"
                                                id="main_check_1" required>
                                            <label for="main_check_1">بيع</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" value="للايجار" {{ old('status') == 'للايجار' ? 'checked' : '' }} class="hidden-input"
                                                id="main_check_2" required>
                                            <label for="main_check_2">إيجار</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" value="استثمار" {{ old('status') == 'استثمار' ? 'checked' : '' }} class="hidden-input"
                                                id="main_check_3" required>
                                            <label for="main_check_3">استثمار</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{--<div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> المدخل :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="main_radio_2" class="hidden-input"
                                                id="main_check_21">
                                            <label for="main_check_21">خاص</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="main_radio_2" class="hidden-input"
                                                id="main_check_22">
                                            <label for="main_check_22">مشترك</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> عدد الصالات:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="main_sala" min="1" required />
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> عدد غرف النوم :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="main_bedroom" min="1" required />
                            </div>
                        </div>
                    </div>--}}

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إضافات أخرى :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <textarea class="form-control" name="details">{{ old('details') }}</textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item">اتفاقية العمولة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <div>
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" name="agreement_1" id="agreement_1"
                                            required>
                                        <label for="agreement_1"> أتعهد واقسم بالله أنا المعلن أن أدفع عمولة الموقع وهي
                                            1% من قيمة البيع سواء تم البيع عن طريق الموقع أو بسببه
                                            <button type="button" class="simple-btn second_color" data-toggle="modal"
                                                data-target="#agreement-modal">( عرض الإتفاقية )</button>
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" name="agreement_2" id="agreement_2"
                                            required>
                                        <label for="agreement_2"> كما أتعهد بدفع العمولة خلال 10 أيام من استلام كامل
                                            مبلغ المبايعة
                                            <button type="button" class="simple-btn second_color" data-toggle="modal"
                                                data-target="#agreement-modal">( عرض الإتفاقية )</button></label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center sm-order-5" data-aos="fade-in">
                        <div class="form-group">
                            <button type="submit" class="border-btn"><span>إضافة </span></button>
                        </div>
                    </div>



                </form>
            </div>
        </section>
        <!--end advanced-search-->