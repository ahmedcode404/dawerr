
    <!-- BEGIN: attribute of subcategory -->
    @if($subcategory == 6)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>    

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_roles" placeholder="عدد الادوار">
            </div>
        </div>   

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_shops" placeholder="عدد المحلات">
            </div>
        </div>    


        @foreach($attributes as $attribute)
        <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
        
        <div class="{{ $attribute->cat ? 'col-md-12' : 'col-md-6' }} col-12">
            <div class="form-group" data-aos="fade-in">
                <select class="form-control select-input all-option" required name="option_{{$attribute->id}}">
                    <option disabled selected>{{  $attribute->name }}</option>

                    @foreach($attribute->options as $option)
                    
                    <option value="{{ $option->id }}">{{ $option->name }}</option>

                    @endforeach

                </select>
            </div>
        </div>

        @endforeach


        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 7)


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>   

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_roles" placeholder="الدور">
            </div>
        </div>   

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياة">
            </div>
        </div>      

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="storehouse" placeholder="عدد المستودع">
            </div>
        </div>      


        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                @if($attribute->cat)
                    <p>وصف الحدود</p>
                @endif

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach       

    @elseif($subcategory == 8)

        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach 

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div> 
        
    @elseif($subcategory == 9)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="on_highway" placeholder="علي طريق سريع">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>  

        <div class="col-md-6 col-12">

            <div class="form-group" data-aos="fade-in">

                <select class="form-control select-input" required name="fuel_id" id="add-input-fuel">

                    <option disabled selected>عناصر الوقود</option>
                    <option value="1">كافة انواع البنزين</option>
                    <option value="2">كافة انواع البنزين وديزل</option>
                    <option value="3">كافة انواع البنزين وديزل وكيروسين</option>
                
                </select>

            </div>

        </div>        

        <div class="append-waqod col-12">

       

        </div>

        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach         
    
    @elseif($subcategory == 10)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>            

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>          

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 11)
    
    
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>          

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_or_lane_width" placeholder="عرض شارع او ممر">
            </div>
        </div>       

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="inside_height" placeholder="الارتفاع من الداخل">
            </div>
        </div>       


        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-12 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach  
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 12)


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>      
        
        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>          

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_sections" placeholder="عدد الاقسام">
            </div>
        </div>       

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_swimming_pools" placeholder="عدد المسابح">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>       

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>    

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="shops" placeholder="محلات تجارية">
            </div>
        </div>        

        @foreach($attributes as $attribute)
            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach   
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>
    @elseif($subcategory == 13)


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>       
        
        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>          

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_sections" placeholder="عدد الاقسام">
            </div>
        </div>       

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_swimming_pools" placeholder="عدد المسابح">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>       

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>    

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="shops" placeholder="محلات تجارية">
            </div>
        </div>        



        @foreach($attributes as $attribute)

            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                        <option disabled selected>{{  $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach   
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

        <div class="col-md-6 col-12">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>

        @endforeach

        <hr>
        <div class="col-md-12 col-12" id="append-direction">

        </div>
    @elseif($subcategory == 14)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  


        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_or_lane_width" placeholder="عرض شارع او ممر">
            </div>
        </div>       

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="inside_height" placeholder="الارتفاع من الداخل">
            </div>
        </div>    
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_roles" placeholder="عدد الادوار">
            </div>
        </div>          

        @foreach($attributes as $attribute)

            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                        <option disabled selected>{{ $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach   

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 15)
    
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>   

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_or_lane_width" placeholder="عرض شارع او ممر">
            </div>
        </div>       

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="inside_height" placeholder="الارتفاع من الداخل">
            </div>
        </div>    
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_roles" placeholder="عدد الادوار">
            </div>
        </div>      

        @foreach($attributes as $attribute)

            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

            <div class="col-md-6 col-12">

                <div class="form-group" data-aos="fade-in">

                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                        <option disabled selected>{{ $attribute->name }}</option>

                        @foreach($attribute->options as $option)
                        
                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                        @endforeach

                    </select>

                </div>

            </div>

        @endforeach      

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 16)



        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
           
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>    

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>
    @elseif($subcategory == 17)



        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>
    @elseif($subcategory == 18)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>
        
    @elseif($subcategory == 19)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
             
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>   

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 20)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>    
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 21)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
             
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>


        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 22)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
         
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 23)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
            
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div> 
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 24)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
             
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>    
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 25)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
            
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>        

    @elseif($subcategory == 26)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
             
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 27)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
     
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 28)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
           
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>        


        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>        
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 29)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
        
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>        

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>        


        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>   
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>        

    @elseif($subcategory == 30)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>       
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>        

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div> 
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 31)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
      
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div> 
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_one" placeholder="عدد غرف نوم الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_one" placeholder="عدد مجالس الرجال الشقة 1">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_one" placeholder="عدد مجالس النساء الشقة 1">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_one" placeholder="عدد الصالات الشقة 1">
            </div>
        </div>        

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_bedrooms_for_two" placeholder="عدد غرف نوم الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards_for_two" placeholder="عدد مجالس الرجال الشقة 2">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_boards_for_two" placeholder="عدد مجالس النساء الشقة 2">
            </div>
        </div>
  
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls_for_two" placeholder="عدد الصالات الشقة 2">
            </div>
        </div>           

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>  
        
        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @elseif($subcategory == 32)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  
     
        
        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>      

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>       

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>        

    @elseif($subcategory == 33)

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="text" class="form-control"  name="neighborhood" placeholder="الحي">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="building_erea" placeholder="مساحة مسطح البناء">
            </div>
        </div> 

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="street_view" placeholder="عرض شارع الواجهة">
            </div>
        </div>           

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_of_master_bedrooms" placeholder="عدد غرف النوم الماستر">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bedroom" placeholder="عدد غرف النوم">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_bathrooms" placeholder="عدد دورات المياه">
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_halls" placeholder="عدد الصالات">
            </div>
        </div>  

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="men_boards" placeholder="مجالس الرجال">
            </div>
        </div>     

        <div class="col-md-6 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="women_councils" placeholder="مجالس النساء">
            </div>
        </div>  

        <div class="col-md-12 col-12">
            <div class="form-group" data-aos="fade-in">
                <input required type="number" class="form-control" min="1" name="number_kitchens" placeholder="عدد المطابخ">
            </div>
        </div>      

        <div class="col-12">
            <div class="form-group" data-aos="fade-in">
                <div class="row">

                    @foreach($attributes as $attribute)

                        @if($attribute->type == 'radio')

                            @if($attribute->cat)
                            <div class="col-12">
                                <h3 class="arrow-title second_color">
                                    {{ $attribute->cat }}
                                </h3>
                            </div>
                            @endif
                            <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                <div class="form-group checkboxes-inline-title">
                                    <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="inline-custom-checkboxes">

                                        @foreach($attribute->options as $option)

                                            <div class="custom-checkbox">
                                                <input required type="checkbox" value="{{ $option->id }}" name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        @else

                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
            
                            <div class="col-md-6 col-12">

                                <div class="form-group" data-aos="fade-in">

                                    <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                        <option disabled selected>{{  $attribute->name }}</option>

                                        @foreach($attribute->options as $option)
                                        
                                            <option value="{{ $option->id }}">{{ $option->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>                            

                        @endif

                    @endforeach     

                </div>
            </div>
        </div>         

        <div class="col-md-12 col-12">
            <p> وصف الحدود *تبدأ من ثلاث حدود</p>
        </div>

        @foreach($dirs as $dir)

            <div class="col-md-3 col-12">
                <div class="form-group" data-aos="fade-in">
                    <div class="custom-checkbox">
                        <input required type="checkbox" value="{{ $dir->id }}" name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                        <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <div class="col-md-12 col-12" id="append-direction">

        </div>

    @endif
    <!-- END: attribute of subcategory -->
