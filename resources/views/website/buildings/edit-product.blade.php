        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">تعديل الاعلان</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                تعديل الاعلان

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->

        @if($errors->any())
            <div class="alert alert-danger">
                <p><strong>Opps Something went wrong</strong></p>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif        

        <!-- start advanced-search
         ================ -->
        <section class="advanced-search margin-div">
            <div class="container">
                <h2 class="shadow-title first_color text-center">تعديل الاعلان</h2>
                <form class="add-product-form gray-form big-inputs" action="{{ route('web.update.product') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <input type="hidden" name="item" value="{{ $product->id }}">


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> موقع الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div id="map-text">
                                <div class="form-group">
                                    <i class="fa fa-map-marker-alt" id="myloc"></i>
                                    <input id="pac-input" name="address" value="{{ $product->address }}" class="controls form-control" type="text" placeholder=""/>
                                </div>
                                <div class="form-group">
                                    <div id="map2"></div>
                                    <input type="hidden" id="lat" name="lat" value="{{ $product->lat }}">
                                    <input type="hidden" id="lng" name="lng" value="{{ $product->lng }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إسم الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{ $product->name }}" name="name" required />
                            </div>
                        </div>
                    </div>




                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> النوع:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" name="subcategory_id" id="subcategory-select" data-action="{{ route('web.get.attribute') }}" required>
                                    <option selected disabled>إختر</option>

                                        @foreach($subcategories as $subcategory)

                                            <option value="{{ $subcategory->id }}" {{ $product->subcategory_id == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->name }}</option>

                                        @endforeach
                                        
                                </select>
                            </div>
                        </div>
                    </div>





                    <div class="row">

                        <div class="col-xl-2  d-xl-block d-none"></div>

                        <div class="row col-xl-10  align-items-end no-marg-row appended-div">
                            <!-- BRGIN: get attribbute of subcategory -->
                            @if($product->subcategory_id == 6)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  
 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>    

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_roles" value="{{ $product->number_of_roles }}" placeholder="عدد الادوار">
                                    </div>
                                </div>   

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_shops" value="{{ $product->number_of_shops }}" placeholder="عدد المحلات">
                                    </div>
                                </div>    


                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)
                                <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                <div class="{{ $attribute->cat ? 'col-md-12' : 'col-md-6' }} col-12">
                                    @if($attribute->cat)
                                        <p>وصف الحدود</p>
                                    @endif
                                    <div class="form-group" data-aos="fade-in">
                                        <select class="form-control select-input all-option" required name="option_{{$attribute->id}}">
                                            <option disabled selected>{{  $attribute->name }}</option>

                                            @foreach($attribute->options as $option)
                                            
                                            <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                                @endforeach 
                                
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>

                            @elseif($product->subcategory_id == 7)


                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>   

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_roles" value="{{ $product->number_of_roles }}" placeholder="الدور">
                                    </div>
                                </div>   

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياة">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="storehouse" value="{{ $product->storehouse }}" placeholder="عدد المستودع">
                                    </div>
                                </div>      


                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                    <div class="col-md-6 col-12">

                                        @if($attribute->cat)
                                            <p>وصف الحدود</p>
                                        @endif

                                        <div class="form-group" data-aos="fade-in">

                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}">
                                                <option disabled selected>{{  $attribute->name }}</option>

                                                @foreach($attribute->options as $option)
                                                
                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                @endforeach

                                            </select>

                                        </div>

                                    </div>

                                @endforeach       

                            @elseif($product->subcategory_id == 8)

                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                    <div class="col-md-6 col-12">

                                        <div class="form-group" data-aos="fade-in">

                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                <option disabled selected>{{  $attribute->name }}</option>

                                                @foreach($attribute->options as $option)
                                                
                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                @endforeach

                                            </select>

                                        </div>

                                    </div>

                                @endforeach 

                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div> 

                            @elseif($product->subcategory_id == 9)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="on_highway" value="{{ $product->on_highway }}" placeholder="علي طريق سريع">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">

                                    <div class="form-group" data-aos="fade-in">

                                        <select class="form-control select-input" required name="fuel_id" id="add-input-fuel">

                                            <option disabled selected>عناصر الوقود</option>
                                            <option value="1" {{ $product->fuel_id == 1 ? 'selected' : '' }}>كافة انواع البنزين</option>
                                            <option value="2" {{ $product->fuel_id == 2 ? 'selected' : '' }}>كافة انواع البنزين وديزل</option>
                                            <option value="3" {{ $product->fuel_id == 3 ? 'selected' : '' }}>كافة انواع البنزين وديزل وكيروسين</option>
                                        
                                        </select>

                                    </div>

                                </div>        

                                <div class="append-waqod col-12">

                                    <div class="col-md-12 col-12">
                                        <div class="form-group" data-aos="fade-in">
                                            <input required type="number" class="form-control" name="number_pumps" value="{{ $product->number_pumps }}" placeholder="عدد مضخات الوقود">
                                        </div>
                                    </div>  

                                    <div class="col-md-12 col-12">
                                        <div class="form-group" data-aos="fade-in">
                                            <input required type="number" class="form-control" name="fuel_tank_capacity" value="{{ $product->fuel_tank_capacity }}" placeholder="سعة خزانات الوقود">
                                        </div>
                                    </div>                                     

                                </div>

                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                    <div class="col-md-6 col-12">

                                        <div class="form-group" data-aos="fade-in">

                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                <option disabled selected>{{  $attribute->name }}</option>

                                                @foreach($attribute->options as $option)
                                                
                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                @endforeach

                                            </select>

                                        </div>

                                    </div>

                                @endforeach         

                            @elseif($product->subcategory_id == 10)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>        

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>          

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 11)


                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>         

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_or_lane_width" value="{{ $product->street_or_lane_width }}" placeholder="عرض شارع او ممر">
                                    </div>
                                </div>       

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="inside_height" value="{{ $product->inside_height }}" placeholder="الارتفاع من الداخل">
                                    </div>
                                </div>       


                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                    <div class="col-md-12 col-12">

                                        <div class="form-group" data-aos="fade-in">

                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                <option disabled selected>{{  $attribute->name }}</option>

                                                @foreach($attribute->options as $option)
                                                
                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                @endforeach

                                            </select>

                                        </div>

                                    </div>

                                @endforeach   
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 12)


                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>      

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>          

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_sections" value="{{ $product->number_of_sections }}" placeholder="عدد الاقسام">
                                    </div>
                                </div>       

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_swimming_pools" value="{{ $product->number_of_swimming_pools }}" placeholder="عدد المسابح">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>       

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>    

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="shops" value="{{ $product->shops }}" placeholder="محلات تجارية">
                                    </div>
                                </div>        

                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)
                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                    <div class="col-md-6 col-12">

                                        <div class="form-group" data-aos="fade-in">

                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                <option disabled selected>{{  $attribute->name }}</option>

                                                @foreach($attribute->options as $option)
                                                
                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                @endforeach

                                            </select>

                                        </div>

                                    </div>

                                @endforeach   

                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                


                            @elseif($product->subcategory_id == 13)


                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>      

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>          

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_sections" value="{{ $product->number_of_sections }}" placeholder="عدد الاقسام">
                                    </div>
                                </div>       

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_swimming_pools" value="{{ $product->number_of_swimming_pools }}" placeholder="عدد المسابح">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>       

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>    

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="shops" value="{{ $product->shops }}" placeholder="محلات تجارية">
                                    </div>
                                </div>        



                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                    <div class="col-md-6 col-12">

                                        <div class="form-group" data-aos="fade-in">

                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                <option disabled selected>{{  $attribute->name }}</option>

                                                @foreach($attribute->options as $option)
                                                
                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                @endforeach

                                            </select>

                                        </div>

                                    </div>

                                @endforeach   
                                
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 14)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_or_lane_width" value="{{ $product->street_or_lane_width }}" placeholder="عرض شارع او ممر">
                                    </div>
                                </div>       

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="inside_height" value="{{ $product->inside_height }}" placeholder="الارتفاع من الداخل">
                                    </div>
                                </div>    

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_roles" value="{{ $product->number_of_roles }}" placeholder="عدد الادوار">
                                    </div>
                                </div>          

                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                    <div class="col-md-6 col-12">

                                        <div class="form-group" data-aos="fade-in">

                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                <option disabled selected>{{ $attribute->name }}</option>

                                                @foreach($attribute->options as $option)
                                                
                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                @endforeach

                                            </select>

                                        </div>

                                    </div>

                                @endforeach   


                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                


                            @elseif($product->subcategory_id == 15)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  


                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_or_lane_width" value="{{ $product->street_or_lane_width }}" placeholder="عرض شارع او ممر">
                                    </div>
                                </div>       

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="inside_height" value="{{ $product->inside_height }}" placeholder="الارتفاع من الداخل">
                                    </div>
                                </div>    

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_roles" value="{{ $product->number_of_roles }}" placeholder="عدد الادوار">
                                    </div>
                                </div>      

                                @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                    <div class="col-md-6 col-12">

                                        <div class="form-group" data-aos="fade-in">

                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                <option disabled selected>{{ $attribute->name }}</option>

                                                @foreach($attribute->options as $option)
                                                
                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                @endforeach

                                            </select>

                                        </div>

                                    </div>

                                @endforeach      


                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 16)



                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <p>*تبدأ من ثلاث حدود</p>
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="border" min="3" value="{{ $product->border }}" placeholder="الحدود">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards"  value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>

                            @elseif($product->subcategory_id == 17)



                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 18)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                


                            @elseif($product->subcategory_id == 19)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 20)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>    

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>


                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_two" value="{{ $product->number_of_bedrooms_for_two }}" placeholder="عدد غرف نوم الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_two" value="{{ $product->men_boards_for_two }}" placeholder="عدد مجالس الرجال الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_two" value="{{ $product->women_boards_for_two }}" placeholder="عدد مجالس النساء الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_two" value="{{ $product->number_halls_for_two }}" placeholder="عدد الصالات الشقة 2">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>  
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 21)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  
  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>


                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_two" value="{{ $product->number_of_bedrooms_for_two }}" placeholder="عدد غرف نوم الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_two" value="{{ $product->men_boards_for_two }}" placeholder="عدد مجالس الرجال الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_two" value="{{ $product->women_boards_for_two }}" placeholder="عدد مجالس النساء الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_two" value="{{ $product->number_halls_for_two }}" placeholder="عدد الصالات الشقة 2">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>        

                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 22)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>    
                                
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 23)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>    

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>       

                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 24)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>      

                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 25)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_two" value="{{ $product->number_of_bedrooms_for_two }}" placeholder="عدد غرف نوم الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_two" value="{{ $product->men_boards_for_two }}" placeholder="عدد مجالس الرجال الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_two" value="{{ $product->women_boards_for_two }}" placeholder="عدد مجالس النساء الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_two" value="{{ $product->number_halls_for_two }}" placeholder="عدد الصالات الشقة 2">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>   
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 26)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>      

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div> 
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 27)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div> 
                                
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 28)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>    

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>        


                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>   
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 29)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>
                               

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>        

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_two" value="{{ $product->number_of_bedrooms_for_two }}" placeholder="عدد غرف نوم الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_two" value="{{ $product->men_boards_for_two }}" placeholder="عدد مجالس الرجال الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_two" value="{{ $product->women_boards_for_two }}" placeholder="عدد مجالس النساء الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_two" value="{{ $product->number_halls_for_two }}" placeholder="عدد الصالات الشقة 2">
                                    </div>
                                </div>        


                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>    
                                
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 30)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>        

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div> 
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 31)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_one" value="{{ $product->number_of_bedrooms_for_one }}" placeholder="عدد غرف نوم الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_one" value="{{ $product->men_boards_for_one }}" placeholder="عدد مجالس الرجال الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_one" value="{{ $product->women_boards_for_one }}" placeholder="عدد مجالس النساء الشقة 1">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_one" value="{{ $product->number_halls_for_one }}" placeholder="عدد الصالات الشقة 1">
                                    </div>
                                </div>        

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_bedrooms_for_two" value="{{ $product->number_of_bedrooms_for_two }}" placeholder="عدد غرف نوم الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards_for_two" value="{{ $product->men_boards_for_two }}" placeholder="عدد مجالس الرجال الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_boards_for_two" value="{{ $product->women_boards_for_two }}" placeholder="عدد مجالس النساء الشقة 2">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls_for_two" value="{{ $product->number_halls_for_two }}" placeholder="عدد الصالات الشقة 2">
                                    </div>
                                </div>           

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 32)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>      

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div> 

                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                

                            @elseif($product->subcategory_id == 33)

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="text" class="form-control" name="neighborhood" value="{{ $product->neighborhood }}" placeholder="الحي">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="building_erea" value="{{ $product->building_erea }}" placeholder="مساحة مسطح البناء">
                                    </div>
                                </div> 

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="street_view" value="{{ $product->street_view }}" placeholder="عرض شارع الواجهة">
                                    </div>
                                </div>           

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_of_master_bedrooms" value="{{ $product->number_of_master_bedrooms }}" placeholder="عدد غرف النوم الماستر">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bedroom" value="{{ $product->number_bedroom }}" placeholder="عدد غرف النوم">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_bathrooms" value="{{ $product->number_bathrooms }}" placeholder="عدد دورات المياه">
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_halls" value="{{ $product->number_halls }}" placeholder="عدد الصالات">
                                    </div>
                                </div>  

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="men_boards" value="{{ $product->men_boards }}" placeholder="مجالس الرجال">
                                    </div>
                                </div>     

                                <div class="col-md-6 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="women_councils" value="{{ $product->women_councils }}" placeholder="مجالس النساء">
                                    </div>
                                </div>  

                                <div class="col-md-12 col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <input required type="number" class="form-control" name="number_kitchens" value="{{ $product->number_kitchens }}" placeholder="عدد المطابخ">
                                    </div>
                                </div>     

                                <div class="col-12">
                                    <div class="form-group" data-aos="fade-in">
                                        <div class="row">

                                            @foreach($product->type->attributes->where('cat' , '!=' , 'not') as $attribute)

                                                @if($attribute->type == 'radio')

                                                    @if($attribute->cat)
                                                    <div class="col-12">
                                                        <h3 class="arrow-title second_color">
                                                            {{ $attribute->cat }}
                                                        </h3>
                                                    </div>
                                                    @endif
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                                                        <div class="form-group checkboxes-inline-title">
                                                            <h4 class="second_color"> {{ $attribute->name }} :</h4>
                                                            <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">

                                                            <div class="inline-custom-checkboxes">

                                                                @foreach($attribute->options as $option)

                                                                    <div class="custom-checkbox">
                                                                        <input required type="checkbox" value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'checked' : '' }} name="option_{{$attribute->id}}" class="hidden-input check-option remove-{{$attribute->id}} all-option" data-id="{{$attribute->id}}" id="check_{{ $option->id }}">
                                                                        <label for="check_{{ $option->id }}">{{ $option->name }}</label>
                                                                    </div>

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <input type="hidden" name="attribute[]" value="{{ $attribute->id }}">
                                    
                                                    <div class="col-md-6 col-12">

                                                        <div class="form-group" data-aos="fade-in">

                                                            <select class="form-control select-input all-option" required name="option_{{$attribute->id}}" id="{{ $attribute->id }}">
                                                                <option disabled selected>{{  $attribute->name }}</option>

                                                                @foreach($attribute->options as $option)
                                                                
                                                                    <option value="{{ $option->id }}" {{ $product->attributeandoption->contains('option_id' , $option->id) ? 'selected' : '' }}>{{ $option->name }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>                            

                                                @endif

                                            @endforeach     

                                        </div>
                                    </div>
                                </div>  
                                
                                <div class="col-md-12 col-12">
                                    <p> وصف الحدود *تبدأ من ثلاث حدود</p>
                                </div>

                                @foreach($dirs as $k=>$dir)

                                        <div class="col-md-3 col-12">
                                            <div class="form-group" data-aos="fade-in">
                                                <div class="custom-checkbox">
                                                    <input required type="checkbox" value="{{ $dir->id }}" {{  $product->directione->contains('dire_id' , $dir->id) ? 'checked' : '' }} name="dir_id[]" class="hidden-input click-append-direction" data-id="{{$dir->id}}" data-action="{{ route('web.get.direction') }}" id="check_{{ $dir->id }}">
                                                    <label for="check_{{ $dir->id }}">{{ $dir->name }}</label>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                <hr>
                                        
                                <div class="col-md-12 col-12" id="append-direction">

                                    @foreach($product->directione()->groupBy('dire_id')->get() as $k => $arrayDir)
                                        <div class="pro-append-{{ $arrayDir->dire_id }}">
                                        
                                            @php 


                                                $new_directions = App\Models\DirectioneProduct::where([['dire_id' , $arrayDir->dire_id] , ['product_id' , $product->id]])->get();

                                            @endphp

                                            <p class="second_color">

                                                @if($arrayDir->dire_id == 1)
                                                    شمال
                                                @elseif($arrayDir->dire_id == 2)
                                                    شرق
                                                @elseif($arrayDir->dire_id == 3)
                                                    غرب
                                                @elseif($arrayDir->dire_id == 4)
                                                    جنوب
                                                @endif

                                            </p>

                                            
                                            <div class="row">

                                                @foreach($directions as $key => $direction)
                                            
                                                <div class="col-xl-3 col-lg-4 col-sm-6  direction-add-col">

                                                    <div class="custom-checkbox">
                                                        <input  type="checkbox" class="direction-{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} check-directione" value="{{ $direction->id }}" {{ $new_directions->where('directione_id' , $direction->id) ? 'checked' : '' }} id="{{ $direction->id }}" data-id="{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_id[]">
                                                        <label for="{{ $direction->id }}">{{ $direction->name }}</label>
                                                    </div>

                                                    <input type="number" class="form-control keypress-checkbox get-value-{{$direction->id}}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}} small-input-add" data-id="{{$direction->dire_id}}" value="{{ $new_directions->where('directione_id' , $direction->id)->first()['value'] }}" id="{{ $direction->id }}-{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" name="direction_{{ $direction->id }}_{{$new_directions->where('directione_id' , $direction->id)->first()['dire_id']}}" required placeholder="بطول">

                                                </div>
                                                
                                                @endforeach  

                                            </div>

                                        </div>
                                    @endforeach        
                                        
                                </div>                                


                            @endif
                            <!-- END: get attribbute of subcategory -->


                        </div>

                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> المساحة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" value="{{ $product->space }}" name="space" min="1" required />
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> السعر:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" value="{{ $product->price }}" name="price" min="1" required />
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> المدينة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <select class="form-control select-input" name="city_id" required>

                                    <option selected disabled>إختر</option>

                                        @foreach($cities as $city)

                                            <option value="{{ $city->id }}" {{ $product->city_id == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>

                                        @endforeach
                                        
                                </select>
                            </div>
                        </div>
                    </div>                      

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> رقم الجوال:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="tel" class="form-control" name="phone" value="{{ $product->phone }}" minlength="11" maxlength="14"
                                    required />
                            </div>

                            <div class="form-group">
                                <div class="custom-checkbox">
                                    <input type="checkbox" name="show_phone" {{ $product->show_phone == 1 ? 'checked' : '' }} value="1" class="hidden-input" id="check_phone">
                                    <label for="check_phone"><em class="second_color">إخفاء رقم الجوال .</em> بهذا
                                        الخيار لنا يستطيع العملاء الاتصال بك على رقم هاتفك والتواصل فقط يتم من خلال
                                        رسائل دور .</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> البريد الإلكتروني :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="email" class="form-control" value="{{ $product->email }}" name="email" required />
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> صور الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_one" id="file_1"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_1"><img src="{{ $product->imageone() }}"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_two" id="file_2"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_2"><img src="{{ $product->imagetwo() }}"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div single-file">
                                            <input type="file" class="hidden-input" name="image_three" id="file_3"
                                                accept=" .jpg, .png,.jpeg" required />
                                            <label for="file_3"><img src="{{ $product->imagethree() }}"
                                                    alt="إصافة صورة"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="form-group">
                                        <div class="custom-file-div multi-file">
                                            <input type="file" class="hidden-input" name="images[]" multiple id="file_4"
                                                accept=" .jpg, .png,.jpeg" />
                                            <label for="file_4"
                                                class="row no-marg-row align-items-center hirozintal-scroll">
                                                @if($product->images->isNotEmpty())

                                                @else
                                                    <img src="{{ url('web') }}/images/products/add.png" alt="إصافة صورة">
                                                @endif
                                                <div class="col-12 images-multi">

                                                    @forelse($product->images as $image)
                                                        <div class="two-div">
                                                            <img src="{{ url('storage/' . $image->path) }}" class="image-{{$image->id}}" alt="">
                                                            <a href="" class="delete-image image-{{$image->id}}" data-action="{{ route('web.delete.image') }}" id="{{ $image->id }}">x</a>
                                                        </div>
                                                    @empty

                                                    @endforelse
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> حالة الإعلان :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" value="للبيع" {{ $product->status == 'للبيع' ? 'checked' : '' }} class="hidden-input"
                                                id="main_check_1" required>
                                            <label for="main_check_1">بيع</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" value="للايجار" {{ $product->status == 'للايجار' ? 'checked' : '' }} class="hidden-input"
                                                id="main_check_2" required>
                                            <label for="main_check_2">إيجار</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="status" value="استثمار" {{ $product->status == 'استثمار' ? 'checked' : '' }} class="hidden-input"
                                                id="main_check_3" required>
                                            <label for="main_check_3">استثمار</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{--<div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> المدخل :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="row align-items-end">
                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="main_radio_2" class="hidden-input"
                                                id="main_check_21">
                                            <label for="main_check_21">خاص</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-4">
                                    <div class="form-group">
                                        <div class="custom-checkbox circle-checkbox">
                                            <input type="radio" name="main_radio_2" class="hidden-input"
                                                id="main_check_22">
                                            <label for="main_check_22">مشترك</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> عدد الصالات:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="main_sala" min="1" required />
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> عدد غرف النوم :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="main_bedroom" min="1" required />
                            </div>
                        </div>
                    </div>--}}

                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item"> إضافات أخرى :</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <textarea class="form-control" name="details">{{ $product->details }}</textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-start" data-aos="fade-in">
                        <div class="col-xl-2 col-md-3 col-sm-4 col-12">
                            <label class="second_color circle-item">اتفاقية العمولة:</label>
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-8 col-12">
                            <div class="form-group">
                                <div>
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" name="agreement_1" checked id="agreement_1"
                                            required>
                                        <label for="agreement_1"> أتعهد واقسم بالله أنا المعلن أن أدفع عمولة الموقع وهي
                                            1% من قيمة البيع سواء تم البيع عن طريق الموقع أو بسببه
                                            <button type="button" class="simple-btn second_color" data-toggle="modal"
                                                data-target="#agreement-modal">( عرض الإتفاقية )</button>
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" name="agreement_2" checked id="agreement_2"
                                            required>
                                        <label for="agreement_2"> كما أتعهد بدفع العمولة خلال 10 أيام من استلام كامل
                                            مبلغ المبايعة
                                            <button type="button" class="simple-btn second_color" data-toggle="modal"
                                                data-target="#agreement-modal">( عرض الإتفاقية )</button></label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center sm-order-5" data-aos="fade-in">
                        <div class="form-group">
                            <button type="submit" class="border-btn"><span>إضافة </span></button>
                        </div>
                    </div>



                </form>
            </div>
        </section>
        <!--end advanced-search-->