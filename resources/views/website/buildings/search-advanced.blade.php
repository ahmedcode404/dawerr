        <!-- start pages-header
         ================ -->
         <section class="pages-header text-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="page-header-content" data-aos="zoom-out">
                            <h2 class="white-text">البحث المتقدم</h2>
                            <div class="breadcrumbes second_color">
                                <a href="{{ route('web.home' , session('categorySlug')) }}"><i class="fa fa-home"></i>الرئيسية</a>
                                البحث المتقدم

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-header-->




        <!-- start advanced-search
         ================ --->

        <!-- start advanced-search
         ================ -->
         <section class="advanced-search margin-div">
            <div class="container">
                <h2 class="shadow-title first_color text-center">البحث المتقدم</h2>
                <form class="advanced-search-form gray-form big-inputs" action="{{ route('web.search.advanced.store') }}" method="post">
                    @csrf
                    <div class="row align-items-start">
                        <div class="col-md-12 col-12">
                            <div class="form-group" data-aos="fade-in">
                                <select class="form-control select-input" name="subcategory_id" id="subcategory-select" data-action="{{ route('web.get.attribute.search') }}" required>
                                    <option selected disabled>إختر</option>

                                        @foreach($subcategories as $subcategory)

                                            <option value="{{ $subcategory->id }}" {{ old('subcategory_id') == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->name }}</option>

                                        @endforeach
                                        
                                </select>
                            </div>
                        </div>


                        <div class="row align-items-start no-marg-row col-12  sm-order-4 appended-div">
                        </div>
                        <div class="col-lg-12 text-center sm-order-5" data-aos="fade-in">

                            <div class="form-group">
                                <button type="submit" class="border-btn"><span>بحث</span></button>
                            </div>
                            
                        </div>


                    </div>

                </form>
            </div>
        </section>
        <!--end advanced-search-->


        <!-- start advanced-search
         ================ --->
