<header>
        <div class="scroll_progress">
            <div></div>
        </div>
        <div class="container">
            <div class="row align-items-center">
                <!--start col-->
                <div class="col-lg-6 md-center logo-grid">
                    <!--start main-logo-->
                    <div class="main-logo">
                        <div class="progress-wrap">
                            <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-4 -4 106 106">
                                <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
                            </svg>
                        </div>
                        <a href="{{ route('web.home' , session('categorySlug')) }}">
                            <img src="{{ url('storage/' . $logo->value) }}" alt="logo">
                        </a>
                    </div>
                    <!--end main-logo-->
                    @if(session('categoryId') == $categories[0]->id)
                        <!-- 0 -->
                        <a href="{{ route('web.opposeds') }}" class="top-link first_color sm-raduis"><i class="fa fa-th-large"></i>معارض السيارات</a>
                        <a href="{{ route('web.home' , $categories[1]->name ) }}" class="top-link first_color sm-raduis"><i class="fa fa-building"></i>العقارات</a>
                        
                    @else
                        <!-- 1 -->
                        <a href="{{ route('web.opposeds') }}" class="top-link first_color sm-raduis"><i class="fa fa-th-large"></i>معارض العقارات</a>
                        <a href="{{ route('web.home' , $categories[0]->name ) }}" class="top-link first_color sm-raduis"><i class="fa fa-building"></i>السيارات</a>
                        
                    @endif
                </div>

                <!--end col-->

                <!--start col-->
                <div class="col-lg-6 text-left-dir md-center search-grid">
                    <div class="main-icons-grid">

                        <!--start main-header-icons-->

                        <div class="main-header-icons auto-icon">
                            
                            @auth
                                <!--start messages-->
                                <div class="header-icon messages-login">
                                    <div class="icon-img  slide-link" data-toggle="tooltip" data-placement="top"
                                        data-title="الرسائل">
                                        <i class="fa fa-envelope"></i>
                                        <div id="count-message"></div>
                                        <!-- <span class="num" id="count-message"></span> -->
                                    </div>
                                    <div class="slide-menu">
                                        <div class="notification-slide">
                                            <ul class="list-unstyled hirozintal-scroll" id="message-unread">

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--end notifications-->
                            @endauth

                            <!--start user-login-->
                            <div class="header-icon user-login">
                                <div class="icon-img  slide-link" data-toggle="tooltip" data-placement="top"
                                    data-title="تسجيل">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="slide-menu">
                                    <ul class="list-unstyled">
                                        @auth
                                            <li> <a href="{{ route('web.logout') }}" class="see-more has_seudo">تسجيل خروج</a></li>
                                            @if(auth()->user()->role == 'admin')

                                                <li> <a href="{{ route('dashboard.dashboard') }}" class="see-more has_seudo">لوحة التحكم</a></li>

                                            @endif                                            
                                            <li> <a href="{{ route('web.profile') }}" class="see-more has_seudo">حسابي</a></li>
                                        @else
                                            <li> <a href="{{ route('web.login') }}" class="see-more has_seudo">تسجيل دخول</a></li>
                                            <li> <a href="{{ route('web.register') }}" class="first_color second_color_hover">تسجيل جديد</a></li>
                                        @endauth

                                    </ul>
                                </div>
                            </div>
                            <!--end user-login-->

                            <!--start responsive-search-->
                            <div class="header-icon d-lg-none d-inline-block">
                                <button class="icon-img responsive-search  slide-link">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                            <!--end nresponsive-search-->
                        </div>
                        <!--end main-header-icons-->

                        <!--start search-->
                        <div class="header-icon search">
                            <div class="search-bg">
                                <div class="main-search">
<<<<<<< HEAD
                                    <form action="{{ route('web.public.search') }}" method="post" class="search-form" id="searchform">
                                        @csrf
=======
                                    <form action="{{ route('web.public.search') }}" method="get" class="search-form" id="searchform">

>>>>>>> d7d1904f47785f02320bccfa496a93519b1548e7
                                        <input type="text" placeholder="{{ session('categoryId') == 2 ? 'البحث عن عقار' : 'البحث عن سيارة' }}" name="search" class="form-control" required>
                                        <button class="auto-icon"><i class="fa fa-search"></i></button>

                                    </form>
                                </div>
                                <button class="close-search d-lg-none d-inline-block auto-icon"><i
                                        class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <!--end search-->
                    </div>
                </div>
                <!--end col-->
            </div>
        </div>
    </header>