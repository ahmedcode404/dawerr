<!-- start  footer-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 footer-logo-grid">
                <div class="footer-logo">
                    <a href="{{ route('web.home' , session('categorySlug')) }}">
                        <img src="{{ url('storage/' . $logo->value) }}" alt="logo">
                    </a>

                    {!!  $about2->value !!}

                </div>
            </div>

            <div class="col-lg-6 footer-lists-grid">
                <div class="row no-marg-row">
                    <div class="col-6">
                        <div class="footer-list">
                            <ul class="list-unstyled">
                                <li><a href="{{ route('web.about') }}">من نحن</a></li>
                                <li><a href="{{ route('web.static' , 'terms_and_conditions') }}">الشروط والأحكام</a></li>
                                <li><a href="{{ route('web.static' , 'user_agreement') }}">اتفاقية المستخدم</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="footer-list">
                            <ul class="list-unstyled">
                                <li><a href="{{ route('web.commission') }}">حساب العمولة</a></li>
                                <li><a href="{{ route('web.static' , 'help_center') }}">مركز المساعدة</a></li>
                                <li><a href="{{ route('web.contact') }}">تواصل معنا</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="socialmedia auto-icon xs-center">
                    <ul class="list-unstyled">
                        <li><a href="{{ $twitter->value }}" target="_blank"><i class="fab fa-twitter"></i><span>تابعنا علي
                                    تويتر</span></a></li>
                        <li><a href="{{ $facebook->value }}" target="_blank"><i class="fab fa-facebook-f"></i><span>تابعنا علي
                                    فيسبوك</span></a></li>
                        <li><a href="{{ $snapchat->value }}" target="_blank"><i class="fab fa-snapchat-ghost"></i><span>تابعنا علي
                                    سناب شات</span></a></li>
                        <li><a href="{{ $instagram->value }}" target="_blank"><i class="fa fa-camera-retro"></i><span>تابعنا علي
                                    انستجرام</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--end footer-->

<!--start copyright-->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 xs-center">
                <a href="https://jaadara.com/" target="_blank"> تصميم وتطوير شركة جدارة <img
                        src="{{ url('web') }}/images/main/jadara.png" alt="جدارة" />
                </a>
            </div>

            <div class="col-sm-6 text-left-dir xs-center">
                جميع حقوق النشر محفوظة © 2021
            </div>
        </div>
    </div>
</div>
<!--end copyright-->
</div>