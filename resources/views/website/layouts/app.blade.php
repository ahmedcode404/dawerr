<!DOCTYPE html>

<html dir="rtl">

<head>
    <title>دور | @yield('title')</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content=" website" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}" />

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#2B2B2B">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#2B2B2B">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#2B2B2B">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="msapplication-TileColor" content="#2B2B2B">
    <meta name="msapplication-TileImage" content="../images/favicon/ms-icon-144x144.png">

    @include('website.layouts.style')

</head>

<body class="no-trans cars-body">

@if(Auth::user())

    <input type="hidden" id="sender-id"      value="{{ Auth::user()->id }}">
    <input type="hidden" id="email-firebase" value="{{ auth()->user()->email }}">
    <input type="hidden" id="id-firebase"    value="{{ auth()->user()->id }}">
    <input type="hidden" id="image-firebase" value="{{ auth()->user()->imageuser() }}">
    <input type="hidden" id="slug-firebase"  value="{{ auth()->user()->slug }}">
    <input type="hidden" id="name-firebase"  value="{{ Auth::user()->first_name .' '. Auth::user()->second_name .' '. Auth::user()->last_name }}">

@endif

<!-- <audio id="audio" src="{{ url('web/sound.mp3') }}" autoplay="false"></audio> -->

    <div class="custom-colors auto-icon">
        <i class="fa fa-moon"></i>
    </div>

    <!-- BEGIN: header -->

        @include('website.layouts.header')

    <!--END: header-->

    <!-- BEGIN: content -->
        <div class="main-content">
            
            <!-- BEGIN: content -->
            
            @yield('content')
            
            <!-- END: content -->
            
            <!-- condition remove footer of page search map -->
            @if(\Request::route()->getName() != 'web.search.map')
                
                <!-- BEGIN: content -->
                
                @include('website.layouts.footer')
                
                <!-- END: content -->
            
            @endif
            
        </div>
    <!-- END: content -->

    <div class="fixed-menu text-center auto-icon">
        <div class="row align-content-center no-marg-row">
            
            <div class="col active">
                <a href="index.html"><i class="fa fa-home"></i></a>
            </div>

            <div class="col">
                <a href="chat.html"><i class="far fa-envelope"></i></a>
            </div>

            <div class="col">
                <a href="login.html"><i class="far fa-user"></i></a>
            </div>

            <div class="col responsive-search">
                <button><i class="fa fa-search"></i></button>
            </div>

        </div>
    </div>


    <!--BEGIN: footer-->

        @include('website.layouts.script')

    <!--END: footer-->

</body>

</html>