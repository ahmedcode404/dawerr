    <script type="text/javascript" src="{{ url('web') }}/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/respond.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/popper.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/aos.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/anime.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/messages_ar.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/custom-sweetalert.js"></script>

    <script type="text/javascript" src="{{ url('web') }}/js/custom.js"></script>

    <!--for this page only-->
    <script type="text/javascript" src="{{ url('web') }}/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/select-ar.js"></script>
    <script type="text/javascript" src="{{ url('web') }}/js/index.js"></script>


    <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script>

    <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-analytics.js"></script>

    <script src="https://www.gstatic.com/firebasejs/8.4.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.4.1/firebase-firestore.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.4.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js"></script>    
    <script>


    var firebaseConfig = {
        apiKey: "AIzaSyCFh_UJlPAzwzWSKo89Q0ZCs5s_uzGs_Z4",
        authDomain: "daweer-3f701.firebaseapp.com",
        projectId: "daweer-3f701",
        storageBucket: "daweer-3f701.appspot.com",
        messagingSenderId: "848462477010",
        appId: "1:848462477010:web:6b02b6e125a07bbb575f0b",
        measurementId: "G-6TJ7EDEC3Z"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);


    </script>

    @stack('js')

    @if(session('error'))

        <script>

            swal.fire({

                title: "{{ session('error') }}",
                type: 'error',
                showConfirmButton: true,
                confirmButtonText: 'نعم'
            });  

        </script>

    @endif

    @if(session('success'))

    <script>

        swal.fire({

            title: "{{ session('success') }}",
            type: 'success',
            showConfirmButton: true,
            confirmButtonText: 'نعم'
        });  

    </script>

    @endif

    @if(session('note'))

    <script>

        swal.fire({

            title: "{{ session('note') }}",
            type: 'warning',
            showConfirmButton: true,
            confirmButtonText: 'نعم'
        });  

    </script>

    @endif

    @if(session('home'))

    <script>

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
        })

        swalWithBootstrapButtons.fire({
            title: "{{ session('home') }}",
            type: 'warning',
            showCancelButton: true,
            heightAuto:true,
            confirmButtonText: 'نعم',
            cancelButtonText: 'لا',
        }).then((result) => {
            if (result.value) {

                window.location.href = "{{ route('web.category') }}";

            } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
            ) {
            
            }
        })

    </script>

    @endif