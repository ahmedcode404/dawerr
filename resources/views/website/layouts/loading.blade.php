    @if(session('categoryId') == 1)


        <!--start loading-->
        <div class="loading">
            <div class="loader">
                <div class="car-load">
                    <svg class="car" width="102" height="40" xmlns="http://www.w3.org/2000/svg">
                        <g transform="translate(2 1)" stroke="#C36F09" fill="none" fill-rule="evenodd"
                            stroke-linecap="round" stroke-linejoin="round">
                            <path class="car__body"
                                d="M47.293 2.375C52.927.792 54.017.805 54.017.805c2.613-.445 6.838-.337 9.42.237l8.381 1.863c2.59.576 6.164 2.606 7.98 4.531l6.348 6.732 6.245 1.877c3.098.508 5.609 3.431 5.609 6.507v4.206c0 .29-2.536 4.189-5.687 4.189H36.808c-2.655 0-4.34-2.1-3.688-4.67 0 0 3.71-19.944 14.173-23.902zM36.5 15.5h54.01"
                                stroke-width="3" />
                            <ellipse class="car__wheel--left" stroke-width="3.2" fill="#2b2b2b" cx="83.493" cy="30.25"
                                rx="6.922" ry="6.808" />
                            <ellipse class="car__wheel--right" stroke-width="3.2" fill="#2b2b2b" cx="46.511" cy="30.25"
                                rx="6.922" ry="6.808" />
                            <path class="car__line car__line--top" d="M22.5 16.5H2.475" stroke-width="3" />
                            <path class="car__line car__line--middle" d="M20.5 23.5H.4755" stroke-width="3" />
                            <path class="car__line car__line--bottom" d="M25.5 9.5h-19" stroke-width="3" />
                        </g>
                    </svg>
                </div>
                <div class="load-dots">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--end loading-->


    @else 


    <!--start loading-->
    <div class="loading">
        <div class="loader">
            <div class="car-load">
                <svg class="ha-logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10">
                    <path class="house" d="M1.9 8.5V5.3h-1l4-4.3 2.2  2.1v-.6h1v1.7l1 1.1H7.9v3.2z" />
                    <path class="circut"
                        d="M5 8.5V4m0 3.5l1.6-1.6V4.3M5  6.3L3.5 4.9v-.6m2.7.7l.4.4L7  5M5.9 6.1v.5h.5M4.2 5v.5h-.8m1  1.5v.6h-.6m1.2.8L3.6 6.7M5  8.4l1-.9h.7M4.6 3.6L5 4l.4-.4" />
                    <g>
                        <circle cx="5.5" cy="3.4" r="0.21" />
                        <circle cx="4.5" cy="3.4" r="0.21" />
                        <circle cx="6.6" cy="4.1" r="0.21" />
                        <circle cx="3.5" cy="4.1" r="0.21" />
                        <circle cx="4.2" cy="4.8" r="0.21" />
                        <circle cx="6.1" cy="4.8" r="0.21" />
                        <circle cx="7.1" cy="4.8" r="0.21" />
                        <circle cx="6.6" cy="6.6" r="0.21" />
                        <circle cx="5.9" cy="5.9" r="0.21" />
                        <circle cx="3.2" cy="5.5" r="0.21" />
                        <circle cx="3.5" cy="6.5" r="0.21" />
                        <circle cx="4.4" cy="6.8" r="0.21" />
                        <circle cx="3.6" cy="7.6" r="0.21" />
                        <circle cx="6.9" cy="7.5" r="0.21" />
                    </g>
                </svg>
            </div>
            <div class="load-dots">
                <ul>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>
    </div>
    <!--end loading-->

    <div class="custom-colors auto-icon">
        <i class="fa fa-moon"></i>
    </div>  


    @endif
