<!DOCTYPE html>

<html>

<head>
    <title> دور  </title>
    <meta charset="utf-8">
    <meta name="description" content="welcoma">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- *******************************
    **********************
    **************
 -->

 </head>
 <body style="padding: 0px; margin: 0px; direction: rtl; text-align: right;">
    <div style="width: 750px; margin: auto; background-color: #f5f5f5; padding:20px ;">
        <div style=" background: #fff ; padding: 8px 0px;"  >

                <div style="text-align: left; padding: 20px 0px 10px 100px ;">
                    <img src="https://i.ibb.co/M9FDsKt/2021-01-31-1.png" alt="">
                </div> 

                <div style="margin: 10px 10px;">
                    <h2 style="color:#354052 ; text-align:right">الرد من خلال دور</h2>

                    {{ $data['message'] }}

                </div>

        </div>

        @php

            $phone = App\Models\Setting::where('key' , 'phone')->first();
            $email = App\Models\Setting::where('key' , 'email')->first();
            $about = App\Models\Setting::where('key' , 'about')->first();

        @endphp

        <div style="color: #354052; margin: 30px 0px ; text-align: center;">
            <h2 style="font-size: 30px; margin-bottom: 10px; text-align:center "> دور </h2>
            <p style="text-align:center ">{{$about->value}}</p>

            <p style="text-align:center "> {{$about->value}} {{Carbon\Carbon::now()->year}} </p>
            <a href="mailto:{{$email->value}}"> {{$email->value}}</a>
            <a href="tel:{{$phone->value}}"> {{$phone->value}}</a>
            <a href="{{URL::to('/')}}" style="display: block; margin: 10px 0px ;"> {{URL::to('/')}}</a>

        </div>
      
  
    </div>
</body>
<!-- end-body
=================== -->

</html>

